# customer-ios-app
## Environment configuration
When you clone the app to your computer you need to configure the environment to make your life simpler.
1. Install ruby (you can use `brew` or `rvm`). Google will help you with it ;)
2. Install `bundler`. Call `gem install bundler`
3. Install all the necessary gems. Form the project directory call `bundle install`
4. Install all the necessary cocoapods. From the project directory call `bundle exec pod install`
5. Create `.env` file in the `fastlane` folder and add the following line to this file `MATCH_PASSWORD='<password>'`. Ask ___Alexander Gaidukov___ for the password
6. From the project directory call `bundle exec fastlane match development --readonly` to install all the necessary certificates and provision profiles to your computer. Follow the instructions on the screen
7. That's it!!!

## Send a new build of the application to the Firebase.
1. Ask ___Alexander Gaidukov___ to provide you with `distribution.json` file and put it into `fastlane` folder
2. Add a new line to the `fastlane/.env` file with the following text, `NEARFOOD_TELEGRAM_TOKEN='<token>'`. Ask ___Alexander Gaidukov___ for the token. This token is required to send a message to the Telegram channel
3. From the project directory call `bundle exec fastlane fb_release`. Provider the release notes and wait. 
4. The corresponding message will be sent to the Telegram channel when the app is published.

## Add a new device for testing
__Important!!!__ You need to be a member of the Apple Developer License to perform this action. ___Alexander Gaidukov___ can help you with it.
1. From the project directory call `bundle exec fastlane add_device`
2. Provider device name (like Ivan Ivanov iPhone) and device udid.
3. Follow the instructions on the screen.
4. When all the necessary devices are added, call `bundle exec fastlane recreate_profiles` to add new devices to the provision profiles.

## Create screenshots for the App Store release

The application creates 4 screenshots.
* The main screen with dishes
* Chat screen
* The latest order details
* Cart screen with payment methods sheet.

1. Preparation
   1. Check and fill in all data in the `UITest` structure in the `Constants.swift` file
   2. Check the api environment in the `Constants.swift`
   3. Check and update(if needed) `cart.json` file in the application bundle
   4. You can check the result by running the `GrabGrabTest` scheme
   
2. Create screenshots
   1. Install `imagemagick` by calling `brew install imagemagick`
   2. If you need to change the background you can do this in `fastlane/backgrounds` folder.
   3. If you neeed to change the screenshot labels you can do this in `fastlane/screenshots/en-US/title.string` and `fastlane/screenshots/ru/title.string` files
   4. From the project directory call `bundle exec fastlane generate_screenshots`
   5. You can have a lunch :) It usually takes 40-50 minutes.
  
## Publish application to App Store
__Important!!!__ You need to be a member of the Apple Developer License to perform this action. ___Alexander Gaidukov___ can help you with it.
1. Visit appleid.apple.com/account/manage
2. Generate a new application specific password
3. Add a new line to the `fastlane/.env` file with the following text, `FASTLANE_APPLE_APPLICATION_SPECIFIC_PASSWORD='<password>'`
4. From the project directory call `bundle exec fastlane store_release`
5. Follow the instructions on the screen
