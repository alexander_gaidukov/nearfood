#!/bin/sh
gpg --quiet --batch --yes --decrypt --passphrase="$APPSTORE_AUTHKEY" --output ./fastlane/authkey.json ./fastlane/authkey.json.gpg