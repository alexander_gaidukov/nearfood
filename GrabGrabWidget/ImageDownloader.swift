//
//  ImageDownloader.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import Kingfisher

final class ImageDownloader {
    static func loadImage(url: String?, size: CGSize, completion: @escaping (UIImage?) -> ()) {
        guard let url = url else {
            completion(nil)
            return
        }

        // swiftlint:disable:next force_unwrapping
        var components = URLComponents(url: API.cdnURL, resolvingAgainstBaseURL: false)!
        var path = Path("auto")
        let width = Int((size.width * UIScreen.main.scale).rounded())
        let height = Int((size.height * UIScreen.main.scale).rounded())
        path.append(path: Path("w:\(width)/h:\(height)/rt:fit/g:ce"))
        path.append(path: Path("plain"))
        components.path = Path(components.path).appending(path: path).absolutePath + "/\(url)@png"

        guard let imageURL = components.url else {
            completion(nil)
            return
        }

        KingfisherManager.shared.retrieveImage(with: imageURL) { result in
            completion((try? result.get())?.image)
        }
    }
}
