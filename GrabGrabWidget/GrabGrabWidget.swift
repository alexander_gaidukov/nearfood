//
//  GrabGrabWidget.swift
//  GrabGrabWidget
//
//  Created by Alexandr Gaidukov on 21.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import WidgetKit
import SwiftUI

extension Dish {
    static let placeholder = Dish(uuid: UUID().uuidString,
                                  name: "",
                                  description: "",
                                  quantity: 1,
                                  unit: .grams,
                                  calories: 0,
                                  proteins: 0,
                                  fats: 0,
                                  carbs: 0,
                                  price: 0,
                                  etr: 0,
                                  features: nil,
                                  photos: FailableDecodableArray<Photo>(elements: []))
}

extension CartCalculation {
    static let placeholder = CartCalculation(total: 5000, amount: 5000, charges: nil, problems: nil)
}

extension Estimation {
    static let placeholder = Estimation(uuid: UUID().uuidString, eta: 25 * 60, calculation: CartCalculation.placeholder)
}

extension Order {
    static let placeholder = Order(
        uuid: UUID().uuidString,
        status: .ready,
        counter: 1,
        location: Location(latitude: 0, longitude: 0),
        alternativeLocation: nil,
        estimation: Estimation.placeholder,
        scheduledAt: Date().addingTimeInterval(25 * 60),
        createdAt: Date(),
        arrivedAt: nil,
        cutleryCount: 1,
        contactless: true,
        dishes: [Dish.placeholder, Dish.placeholder],
        combos: nil,
        paymentMethod: nil,
        bundle: nil,
        feedback: nil,
        receipt: nil,
        recipient: nil,
        tip: nil
    )
}

extension Order {
    var url: URL {
        // swiftlint:disable:next force_unwrapping
        URL(string: "grabgrab://grabgrab.com/orders/\(uuid)")!
    }
}

struct OrderEntity {
    let order: Order
    let images: [[UIImage]]
    private let maxImages = 4
    var displayImages: [[UIImage]] {
        let endIndex = min(maxImages, images.count)
        return Array(images[..<endIndex])
    }
    var additionalItemsCount: Int {
        (order.dishes?.count ?? 0) + (order.combos?.count ?? 0) - maxImages
    }
}

struct Provider: TimelineProvider {

    private let webClient = WebClient(accessTokenProvider: AccessTokenProvider.shared, logger: Logger())
    private let policy = TimelineReloadPolicy.after(Date().addingTimeInterval(300))

    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), order: OrderEntity(order: Order.placeholder, images: []))
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let simpleEntry = SimpleEntry(date: Date(), order: OrderEntity(order: Order.placeholder, images: []))
        getTimeline(in: context) {
            completion($0.entries.first ?? simpleEntry)
        }
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {

        let currentDate = Date()

        loadOrders {
            guard let order = $0?.first else {
                DispatchQueue.main.async {
                    let entry = SimpleEntry(date: currentDate, order: nil)
                    let timeline = Timeline(entries: [entry], policy: .never)
                    completion(timeline)
                }
                return
            }

            let photos: [[Photo]] = order.cartItems.map(\.photos)

            self.loadImages(for: photos) {
                let entry = SimpleEntry(date: currentDate, order: OrderEntity(order: order, images: $0))
                let timeline = Timeline(entries: [entry], policy: self.policy)
                completion(timeline)
            }
        }
    }

    private func loadOrders(completion: @escaping ([Order]?) -> ()) {
        guard
            AccessTokenProvider.shared.isLoggedIn,
            let customerUUID = AccessTokenProvider.shared.customerUUID else
        {
            completion(nil)
            return
        }

        let resource = Resource<OrdersResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/orders/active"
        ).map { $0.orders.elements }

        webClient.load(resource: resource) {
            completion(try? $0.get())
        }
    }

    private func loadImages(for photos: [[Photo]], completion: @escaping ([[UIImage]]) -> ()) {
        guard !photos.isEmpty else {
            DispatchQueue.main.async {
                completion([])
            }
            return
        }

        let group = DispatchGroup()

        var imagesArray: [[UIImage?]] = []
        for group in photos {
            imagesArray.append(Array(repeating: nil, count: group.count))
        }

        let images: ThreadSafe<[[UIImage?]]> = ThreadSafe(imagesArray)
        photos.enumerated().forEach { groupEl in
            groupEl.element.enumerated().forEach { photoEl in
                group.enter()
                ImageDownloader.loadImage(url: photoEl.element.url, size: CGSize(width: 44, height: 44)) { image in
                    images.atomically {
                        $0[groupEl.offset][photoEl.offset] = image
                    }
                    group.leave()
                }
            }
        }

        group.notify(queue: .main) {
            var filteredImages: [[UIImage]] = []
            for images in images.value {
                filteredImages.append(images.compactMap { $0 })
            }
            completion(filteredImages)
        }
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let order: OrderEntity?
}

struct PhotosContainerItemView: View {
    var photo: UIImage
    var body: some View {
        ZStack(alignment: .center) {
            Image(uiImage: photo)
                .resizable()
                .aspectRatio(1.0, contentMode: .fit)
        }.frame(maxHeight: .infinity)
    }
}

struct PhotosContainerView: View {
    let photos: [UIImage]

    @ViewBuilder
    var body: some View {
        if photos.isEmpty {
            EmptyView()
        } else if photos.count == 1 {
            Image(uiImage: photos[0])
                .resizable()
                .frame(width: 44, height: 44)
        } else {
            HStack(spacing: -10) {
                VStack(spacing: -10) {
                    PhotosContainerItemView(photo: photos[0])
                        .zIndex(2)
                    if photos.count > 2 {
                        PhotosContainerItemView(photo: photos[2])
                            .zIndex(1)
                    }
                }
                .frame(maxWidth: .infinity)
                .zIndex(2)

                VStack(spacing: -10) {
                    PhotosContainerItemView(photo: photos[1])
                    if photos.count > 3 {
                        PhotosContainerItemView(photo: photos[3])
                    }
                }
                .frame(maxWidth: .infinity)
                .zIndex(1)
            }
        }
    }
}

struct OrderView: View {
    let entity: OrderEntity?
    let statusModel: OrderStatusModel
    let timerInfo: TimerInfoModel

    init(entity: OrderEntity?) {
        self.entity = entity
        self.statusModel = OrderStatusModel(order: entity?.order ?? Order.placeholder, isList: true)
        self.timerInfo = TimerInfoModel(order: entity?.order ?? Order.placeholder, isCard: true)
    }

    @ViewBuilder
    var body: some View {
        if let entity = entity {
            ZStack(alignment: .topLeading) {
                VStack(alignment: .leading, spacing: 8) {
                    Text(entity.order.title)
                        .font(.footnote)
                        .foregroundColor(Color("label_text"))
                    HStack(alignment: .center, spacing: 0) {
                        Text(statusModel.label)
                            .font(.footnote)
                            .foregroundColor(Color(statusModel.color))
                        Spacer()
                        if !timerInfo.isHidden {
                            Group {
                                if timerInfo.shouldTick {
                                    Text(entity.order.scheduledAt, style: .timer)
                                } else {
                                    Text(timerInfo.label)
                                }
                            }
                            .font(.system(size: 16, design: .monospaced))
                            .foregroundColor(Color(timerInfo.color))
                            .padding(8)
                            .frame(width: 66)
                            .background(Color(timerInfo.backgroundColor))
                            .clipShape(Capsule())
                        }
                    }
                }
                .padding(EdgeInsets(top: 8, leading: 12, bottom: 0, trailing: 12))
                VStack(alignment: .center, spacing: 4) {
                    Spacer()
                    HStack(alignment: .center, spacing: -16) {
                        ForEach(Array(entity.displayImages.enumerated()), id: \.offset) {
                            PhotosContainerView(photos: $0.element)
                                .frame(width: 44, height: 44)
                        }
                    }
                    if entity.additionalItemsCount > 0 {
                        Text("+\(entity.additionalItemsCount)")
                            .font(.footnote)
                            .foregroundColor(Color("label_text"))
                    }
                }
                .padding(EdgeInsets(top: 0, leading: 12, bottom: 12, trailing: 12))
                Color.clear
            }.background(Color("background"))
        } else {
            ZStack(alignment: .center) {
                Text(LocalizedStrings.Widget.noOrders)
                    .font(.headline)
                    .foregroundColor(Color("label_text"))
                    .multilineTextAlignment(.center)
                Color.clear
            }.background(Color("background"))
        }

    }
}

struct GrabGrabWidgetEntryView: View {
    var entry: Provider.Entry

    var body: some View {
        OrderView(entity: entry.order)
            .widgetURL(entry.order?.order.url)
    }
}

struct GrabGrabWidgetPlaceholderView: View {
    var entry: Provider.Entry

    var body: some View {
        OrderView(entity: entry.order)
            .redacted(reason: .placeholder)
    }
}

@main
struct GrabGrabWidget: Widget {
    let kind: String = Constants.Identifiers.widgetKind

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            GrabGrabWidgetEntryView(entry: entry)
        }
        .configurationDisplayName(LocalizedStrings.Widget.name)
        .description(LocalizedStrings.Widget.description)
        .supportedFamilies([.systemSmall])
    }
}

struct GrabGrabWidget_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            GrabGrabWidgetEntryView(entry: SimpleEntry(date: Date(), order: OrderEntity(order: Order.placeholder, images: [])))
                .previewContext(WidgetPreviewContext(family: .systemSmall))
                .environment(\.colorScheme, .light)
            GrabGrabWidgetEntryView(entry: SimpleEntry(date: Date(), order: OrderEntity(order: Order.placeholder, images: [])))
                .previewContext(WidgetPreviewContext(family: .systemSmall))
                .environment(\.colorScheme, .dark)
            GrabGrabWidgetPlaceholderView(entry: SimpleEntry(date: Date(), order: OrderEntity(order: Order.placeholder, images: [])))
                .previewContext(WidgetPreviewContext(family: .systemSmall))
                .environment(\.colorScheme, .light)
            GrabGrabWidgetPlaceholderView(entry: SimpleEntry(date: Date(), order: OrderEntity(order: Order.placeholder, images: [])))
                .previewContext(WidgetPreviewContext(family: .systemSmall))
                .environment(\.colorScheme, .dark)
        }
    }
}
