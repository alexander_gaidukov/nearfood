//
//  OnboardingRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol OnboardingRouterType {
    func showLocation(completion: @escaping (OnboardingLocationAction) -> ())
    func showNotifications(completion: @escaping (OnboardingNotificationsAction) -> ())
    func showAddress(completion: @escaping (OnboardingAddressAction) -> ())
    func showAddressSuggestions(address: String?, window: SearchWindow, completion: @escaping (AddressSuggestionsAction) -> ())
    func showCities(completion: @escaping (CitiesAction) -> ())
    func dismiss()
}

final class OnboardingRouter: OnboardingRouterType {

    private let window: UIWindow
    private let factory: OnboardingViewControllersFactoryType

    private var navigationController: GGNavigationController? {
        window.rootViewController as? GGNavigationController
    }

    init(window: UIWindow, factory: OnboardingViewControllersFactoryType) {
        self.window = window
        self.factory = factory
    }

    func showLocation(completion: @escaping (OnboardingLocationAction) -> ()) {
        let controller = factory.locationViewController(completion: completion)
        pushOrShow(viewController: controller)
    }

    func showNotifications(completion: @escaping (OnboardingNotificationsAction) -> ()) {
        let controller = factory.notificationsViewController(completion: completion)
        pushOrShow(viewController: controller)
    }

    func showAddress(completion: @escaping (OnboardingAddressAction) -> ()) {
        let controller = factory.addressViewController(completion: completion)
        pushOrShow(viewController: controller)
    }

    func showCities(completion: @escaping (CitiesAction) -> ()) {
        let controller = factory.citiesViewController(completion: completion)
        let navController = GGNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        navigationController?.present(navController, animated: true, completion: nil)
    }

    private func pushOrShow(viewController: UIViewController) {
        if let navController = navigationController {
            navController.pushViewController(viewController, animated: true)
            return
        }

        let navController = GGNavigationController(rootViewController: viewController)
        window.rootViewController = navController
    }

    func showAddressSuggestions(address: String?, window: SearchWindow, completion: @escaping (AddressSuggestionsAction) -> ()) {
        let controller = factory.addressSuggestionsViewController(address: address, window: window, completion: completion)
        let navController = GGPresentedNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        navigationController?.present(navController, animated: true, completion: nil)
    }

    func dismiss() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
