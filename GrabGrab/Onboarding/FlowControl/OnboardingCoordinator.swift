//
//  OnboardingCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class OnboardingCoordinator: Coordinator {
    var completion: () -> ()
    private let router: OnboardingRouterType
    private let dataProvidersStorage: DataProvidersStorageType

    init(router: OnboardingRouterType,
         dataProvidersStorage: DataProvidersStorageType,
         completion: @escaping () -> ()
    ) {
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
        self.completion = completion
    }

    func start() {
        showNextScreen()
    }

    private func showNextScreen() {
        dataProvidersStorage.onboardingDataProvider.nextNotCompletedService { service in
            DispatchQueue.main.async {
                switch service {
                case .location:
                    self.showLocation()
                case .notifications:
                    self.showNotifications()
                case .none:
                    if !self.dataProvidersStorage.customerDataProvider.isCustomerHasAddress {
                        self.showAddress()
                    } else {
                        self.completion()
                    }
                }
            }
        }
    }

    private func showLocation() {
        router.showLocation { action in
            switch action {
            case .completed:
                self.showNextScreen()
            }
        }
    }

    private func showNotifications() {
        router.showNotifications { action in
            switch action {
            case .completed:
                self.showNextScreen()
            }
        }
    }

    private func showAddress() {
        router.showAddress { action in
            switch action {
            case .completed:
                self.showNextScreen()
            case let .changeAddress(address, window, completion):
                self.showAddressSuggestions(address: address, window: window, completion: completion)
            case .selectCity(let completion):
                self.showCitySelection(completion: completion)
            }
        }
    }

    private func showCitySelection(completion: @escaping (City) -> ()) {
        router.showCities { action in
            self.router.dismiss()
            switch action {
            case .select(let location):
                completion(location)
            case .close:
                break
            }
        }
    }

    private func showAddressSuggestions(address: String?, window: SearchWindow, completion: @escaping (AddressItem) -> ()) {
        router.showAddressSuggestions(address: address, window: window) { action in
            self.router.dismiss()
            switch action {
            case .closed:
                break
            case let .selected(item):
                completion(item)
            }
        }
    }
}
