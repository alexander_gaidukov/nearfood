//
//  OnboardingViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol OnboardingViewControllersFactoryType {
    func locationViewController(completion: @escaping (OnboardingLocationAction) -> ()) -> OnboardingLocationViewController
    func notificationsViewController(completion: @escaping (OnboardingNotificationsAction) -> ()) -> OnboardingNotificationsViewController
    func addressViewController(completion: @escaping (OnboardingAddressAction) -> ()) -> OnboardingAddressViewController
    func addressSuggestionsViewController(
        address: String?,
        window: SearchWindow,
        completion: @escaping (AddressSuggestionsAction) -> ()
    ) -> AddressesSuggestionsViewController
    func citiesViewController(completion: @escaping (CitiesAction) -> ()) -> CitiesViewController
}

struct OnboardingViewControllersFactory: OnboardingViewControllersFactoryType {

    let storyboard: Storyboard = .onboarding

    let managersStorage: ManagersStorageType
    let addressesViewControllersFactory: AddressesViewControllersFactoryType

    func locationViewController(completion: @escaping (OnboardingLocationAction) -> ()) -> OnboardingLocationViewController {
        let presenter = OnboardingLocationPresenter(
            locationManager: managersStorage.locationManager,
            completion: completion
        )
        let controller = OnboardingLocationViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func notificationsViewController(completion: @escaping (OnboardingNotificationsAction) -> ()) -> OnboardingNotificationsViewController {
        let presenter = OnboardingNotificationsPresenter(
            pushNotificationsManager: managersStorage.pushNotificationsManager,
            completion: completion
        )
        let controller = OnboardingNotificationsViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func addressViewController(completion: @escaping (OnboardingAddressAction) -> ()) -> OnboardingAddressViewController {
        let presenter = OnboardingAddressPresenter(addressesFactory: addressesViewControllersFactory,
                                                   completion: completion)
        let controller = OnboardingAddressViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func addressSuggestionsViewController(
        address: String?,
        window: SearchWindow,
        completion: @escaping (AddressSuggestionsAction) -> ()
    ) -> AddressesSuggestionsViewController {
        addressesViewControllersFactory.addressesSuggestionsViewController(
            address: address,
            type: .selection,
            window: window,
            completion: completion
        )
    }

    func citiesViewController(completion: @escaping (CitiesAction) -> ()) -> CitiesViewController {
        addressesViewControllersFactory.citiesViewController(completion: completion)
    }
}
