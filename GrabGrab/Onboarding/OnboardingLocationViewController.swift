//
//  OnboardingLocationViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class OnboardingLocationViewController: UIViewController {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var nextButton: UIButton!

    var presenter: OnboardingLocationPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    private func configureView() {
        titleLabel.text = LocalizedStrings.Onboarding.Location.title
        descriptionLabel.text = LocalizedStrings.Onboarding.Location.description
        nextButton.setTitle(LocalizedStrings.Onboarding.Location.nextTitle, for: .normal)
    }

    @IBAction private func nextTapped() {
        presenter.next()
    }
}
