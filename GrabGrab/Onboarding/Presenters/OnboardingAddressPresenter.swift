//
//  OnboardingAddressPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import CoreLocation

protocol OnboardingAddressPresenterType {
    var addressesViewControllersFactory: AddressesViewControllersFactoryType { get }
    func complete()
    func changeAddress(_ address: String?, searchWindow: SearchWindow, completion: @escaping (AddressItem) -> ())
    func selectCity(completion: @escaping (City) -> ())
}

enum OnboardingAddressAction {
    case completed
    case changeAddress(String?, SearchWindow, (AddressItem) -> ())
    case selectCity((City) -> ())
}

final class OnboardingAddressPresenter: OnboardingAddressPresenterType {

    private let completion: (OnboardingAddressAction) -> ()
    let addressesViewControllersFactory: AddressesViewControllersFactoryType

    init(addressesFactory: AddressesViewControllersFactoryType, completion: @escaping (OnboardingAddressAction) -> ()) {
        self.completion = completion
        self.addressesViewControllersFactory = addressesFactory
    }

    func complete() {
        completion(.completed)
    }

    func changeAddress(_ address: String?, searchWindow: SearchWindow, completion: @escaping (AddressItem) -> ()) {
        self.completion(.changeAddress(address, searchWindow, completion))
    }

    func selectCity(completion: @escaping (City) -> ()) {
        self.completion(.selectCity(completion))
    }
}
