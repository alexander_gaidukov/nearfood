//
//  OnboardingNotificationsPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol OnboardingNotificationsPresenterType {
    func next()
}

enum OnboardingNotificationsAction {
    case completed
}

final class OnboardingNotificationsPresenter: OnboardingNotificationsPresenterType {

    private let completion: (OnboardingNotificationsAction) -> ()
    private let pushNotificationsManager: PushNotificationsManagerType

    init(pushNotificationsManager: PushNotificationsManagerType, completion: @escaping (OnboardingNotificationsAction) -> ()) {
        self.pushNotificationsManager = pushNotificationsManager
        self.completion = completion
    }

    func next() {
        pushNotificationsManager.registerForRemoteNotifications { [weak self] in
            DispatchQueue.main.async {
                self?.completion(.completed)
            }
        }
    }
}
