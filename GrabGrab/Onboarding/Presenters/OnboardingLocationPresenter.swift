//
//  OnboardingLocationPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol OnboardingLocationPresenterType {
    func next()
}

enum OnboardingLocationAction {
    case completed
}

final class OnboardingLocationPresenter: OnboardingLocationPresenterType {

    private let completion: (OnboardingLocationAction) -> ()
    private let locationManager: LocationManagerType

    init(locationManager: LocationManagerType, completion: @escaping (OnboardingLocationAction) -> ()) {
        self.completion = completion
        self.locationManager = locationManager
    }

    func next() {
        locationManager.requestAuthorization {[weak self] in
            DispatchQueue.main.async {
                self?.completion(.completed)
            }
        }
    }
}
