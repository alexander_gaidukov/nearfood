//
//  OnboardingNotificationsViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class OnboardingNotificationsViewController: UIViewController {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var nextButton: UIButton!

    var presenter: OnboardingNotificationsPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    private func configureView() {
        titleLabel.text = LocalizedStrings.Onboarding.Notifications.title
        descriptionLabel.text = LocalizedStrings.Onboarding.Notifications.description
        nextButton.setTitle(LocalizedStrings.Onboarding.Notifications.nextTitle, for: .normal)
    }

    @IBAction private func nextTapped() {
        presenter.next()
    }
}
