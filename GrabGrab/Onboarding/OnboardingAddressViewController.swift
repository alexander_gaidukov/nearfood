//
//  OnboardingAddressViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import YandexMapKit

class OnboardingAddressViewController: UIViewController {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!

    var presenter: OnboardingAddressPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        titleLabel.text = LocalizedStrings.Onboarding.Address.title
        let factory = presenter.addressesViewControllersFactory
        let addressesViewController = factory.addressSelectionViewController(type: .selection) { [weak self] action in
            guard let self = self else { return }
            switch action {
            case .close:
                break
            case .selected:
                self.presenter.complete()
            case let .changeAddress(address, searchWindow, completion):
                self.presenter.changeAddress(address, searchWindow: searchWindow, completion: completion)
            case .selectCity(let completion):
                self.presenter.selectCity(completion: completion)
            }
        }

        containerView.fill(with: addressesViewController.view)
        addChild(addressesViewController)
    }

}
