//
//  AddressesSelectionInfoViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import Pulley

class AddressSelectionInfoViewController: UIViewController {

    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var apartmentsLabel: UILabel!
    @IBOutlet private weak var entranceLabel: UILabel!
    @IBOutlet private weak var floorLabel: UILabel!
    @IBOutlet private weak var doorphoneLabel: UILabel!
    @IBOutlet private weak var noteLabel: UILabel!

    @IBOutlet private weak var additionalInfoView: UIView!
    @IBOutlet private weak var wrongLocationView: UIView!
    @IBOutlet private weak var wrongLocationLabel: UILabel!

    @IBOutlet private weak var addressTextField: GGTextField!
    @IBOutlet private weak var apartmentsTextField: GGTextField!
    @IBOutlet private weak var entranceTextField: GGTextField!
    @IBOutlet private weak var floorTextField: GGTextField!
    @IBOutlet private weak var doorphoneTextField: GGTextField!
    @IBOutlet private weak var noteTextView: GGTextView!

    @IBOutlet private weak var collectionView: UICollectionView!

    var presenter: AddressSelectionPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    private func configureViews() {

        collectionView.register(cellType: SavedAddressCell.self)

        title = LocalizedStrings.Addresses.title

        addressLabel.text = LocalizedStrings.Addresses.addressLabel
        addressTextField.placeholder = LocalizedStrings.Addresses.addressPlaceholder
        apartmentsLabel.text = LocalizedStrings.Addresses.apartmentsLabel
        entranceLabel.text = LocalizedStrings.Addresses.entranceLabel
        floorLabel.text = LocalizedStrings.Addresses.floorLabel
        doorphoneLabel.text = LocalizedStrings.Addresses.doorphoneLabel
        noteLabel.text = LocalizedStrings.Addresses.noteLabel

        noteTextView.placeholder = LocalizedStrings.Addresses.notePlaceholder
        noteTextView.didBeginEditing = { [weak self] in self?.expand() }
        noteTextView.didChange = { [weak self] in self?.presenter.changeNote($0) }

        collectionView.isHidden = presenter.state.savedItems.isEmpty
    }

    private func expand() {
        pulleyViewController?.setDrawerPosition(position: .open, animated: true)
    }

    private func configureBindings() {

        presenter.state.isViewActive.bind(to: view, keyPath: \.isUserInteractionEnabled).addToDisposableBag(disposableBag)

        presenter.state.$location.map { $0.address }.bind(to: addressTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$location.map { $0.apartments }.bind(to: apartmentsTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$location.map { $0.entrance }.bind(to: entranceTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$location.map { $0.floor }.bind(to: floorTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$location.map { $0.doorPhone }.bind(to: doorphoneTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$location.map { $0.note }.bind(to: noteTextView, keyPath: \.text).addToDisposableBag(disposableBag)

        presenter.state.addressError.bind(to: addressTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)
        presenter.state.apartmentsError.bind(to: apartmentsTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)
        presenter.state.entranceError.bind(to: entranceTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)
        presenter.state.floorError.bind(to: floorTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)
        presenter.state.doorPhoneError.bind(to: doorphoneTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.isWrongLocationViewHidden.bind(to: wrongLocationView, keyPath: \.isHidden).addToDisposableBag(disposableBag)

        presenter.state.$checkError
            .map { $0?.localizedDescription }
            .bind(to: wrongLocationLabel, keyPath: \.text)
            .addToDisposableBag(disposableBag)

        presenter.state.isAdditionalInfoViewHidden.bind(to: additionalInfoView, keyPath: \.isHidden).addToDisposableBag(disposableBag)
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        if textField == apartmentsTextField {
            presenter.changeApartments(text)
        } else if textField == entranceTextField {
            presenter.changeEntrance(text)
        } else if textField == floorTextField {
            presenter.changeFloor(text)
        } else if textField == doorphoneTextField {
            presenter.changeDoorPhone(text)
        }
    }

    @IBAction private func wrongLocationTapped() {
        presenter.selectWrongLocation()
    }
}

extension AddressSelectionInfoViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        expand()
        if textField == addressTextField {
            presenter.changeAddress()
            return false
        }
        return true
    }
}

extension AddressSelectionInfoViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter.state.savedItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SavedAddressCell = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(text: presenter.state.savedItems[indexPath.item].title ?? "")
        return cell
    }
}

extension AddressSelectionInfoViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        presenter.selectLocation(presenter.state.savedItems[indexPath.item])
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        SavedAddressCell.size(for: presenter.state.savedItems[indexPath.item].title ?? "")
    }
}

extension AddressSelectionInfoViewController: PulleyDrawerViewControllerDelegate {
    func supportedDrawerPositions() -> [PulleyPosition] {
        [.partiallyRevealed, .open]
    }

    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        presenter.state.contentBottomOffset
    }
}
