//
//  AddressesRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol AddressesRouterType {
    func showAddressSelection(type: AddressSelectionType, completion: @escaping (AddressSelectionAction) -> ())

    func showAddressSuggestions(
        address: String?,
        type: AddressSelectionType,
        searchWindow: SearchWindow,
        completion: @escaping (AddressSuggestionsAction) -> ()
    )

    func showCities(completion: @escaping (CitiesAction) -> ())

    func dismiss()
}

final class AddressesRouter: AddressesRouterType {

    private weak var presentingViewController: UIViewController?
    private let factory: AddressesViewControllersFactoryType

    init(presentingViewController: UIViewController?, factory: AddressesViewControllersFactoryType) {
        self.presentingViewController = presentingViewController
        self.factory = factory
    }

    func showAddressSelection(type: AddressSelectionType, completion: @escaping (AddressSelectionAction) -> ()) {
        let controller = factory.addressSelectionViewController(type: type, completion: completion)
        let navController = GGPresentedNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        topViewController?.present(navController, animated: true, completion: nil)
    }

    func showAddressSuggestions(
        address: String?,
        type: AddressSelectionType,
        searchWindow: SearchWindow,
        completion: @escaping (AddressSuggestionsAction) -> ()
    ) {
        let controller = factory.addressesSuggestionsViewController(
            address: address,
            type: type,
            window: searchWindow,
            completion: completion
        )
        let navController = GGPresentedNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        topViewController?.present(navController, animated: true, completion: nil)
    }

    func showCities(completion: @escaping (CitiesAction) -> ()) {
        let controller = factory.citiesViewController(completion: completion)
        let navController = GGNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        topViewController?.present(navController, animated: true, completion: nil)
    }

    func dismiss() {
        topViewController?.dismiss(animated: true, completion: nil)
    }

    private var topViewController: UIViewController? {
        guard var topCandidate = presentingViewController else { return nil }
        while let controller = topCandidate.presentedViewController {
            topCandidate = controller
        }
        return topCandidate
    }
}
