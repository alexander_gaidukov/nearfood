//
//  AddressesViewControllerFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol AddressesViewControllersFactoryType {
    func addressSelectionViewController(
        type: AddressSelectionType,
        completion: @escaping (AddressSelectionAction) -> ()
    ) -> AddressSelectionViewController

    func addressesSuggestionsViewController(
        address: String?,
        type: AddressSelectionType,
        window: SearchWindow,
        completion: @escaping (AddressSuggestionsAction) -> ()
    ) -> AddressesSuggestionsViewController

    func citiesViewController(completion: @escaping (CitiesAction) -> ()) -> CitiesViewController
}

struct AddressesViewControllersFactory: AddressesViewControllersFactoryType {

    private let storyboard: Storyboard = .addresses

    let dataProvidersStorage: DataProvidersStorageType
    let managersStorage: ManagersStorageType
    let webClient: WebClientType

    func addressSelectionViewController(
        type: AddressSelectionType,
        completion: @escaping (AddressSelectionAction) -> ()
    ) -> AddressSelectionViewController {
        let presenter = AddressSelectionPresenter(
            type: type,
            locationManager: managersStorage.locationManager,
            addressesDataProvider: dataProvidersStorage.addressDataProvider,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            webClient: webClient,
            completion: completion
        )
        let controller = AddressSelectionViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func addressesSuggestionsViewController(
        address: String?,
        type: AddressSelectionType,
        window: SearchWindow,
        completion: @escaping (AddressSuggestionsAction) -> ()
    ) -> AddressesSuggestionsViewController {
        let presenter = AddressSuggestionsPresenter(
            type: type,
            address: address,
            addressesDataProvider: dataProvidersStorage.addressDataProvider,
            searchWindow: window,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            completion: completion
        )
        let controller = AddressesSuggestionsViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func citiesViewController(completion: @escaping (CitiesAction) -> ()) -> CitiesViewController {
        let presenter = CitiesPresenter(citiesManager: managersStorage.citiesManager, completion: completion)
        let controller = CitiesViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
