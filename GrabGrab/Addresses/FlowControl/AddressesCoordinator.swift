//
//  AddressesCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum AddressCoordinatorAction {
    case selected(Location)
    case closed
}

enum AddressSelectionType {
    case selection
    case new(Location)
}

final class AddressesCoordinator: Coordinator {

    private var completion: (AddressCoordinatorAction) -> ()

    private let router: AddressesRouterType
    private let dataProvidersStorage: DataProvidersStorageType
    private let type: AddressSelectionType

    init(
        router: AddressesRouterType,
        dataProvidersStorage: DataProvidersStorageType,
        type: AddressSelectionType,
        completion: @escaping (AddressCoordinatorAction) -> ()
    ) {
        self.completion = completion
        self.type = type
        self.dataProvidersStorage = dataProvidersStorage
        self.router = router
    }

    func start() {
        router.showAddressSelection(type: type) { action in
            switch action {
            case .close:
                self.router.dismiss()
                self.completion(.closed)
            case .selected(let location):
                self.router.dismiss()
                self.completion(.selected(location))
            case let .changeAddress(address, window, completion):
                self.showAddressSuggestions(address: address, window: window, completion: completion)
            case .selectCity(let completion):
                self.showCitySelection(completion: completion)
            }
        }
    }

    private func showCitySelection(completion: @escaping (City) -> ()) {
        router.showCities { action in
            self.router.dismiss()
            switch action {
            case .select(let location):
                completion(location)
            case .close:
                break
            }
        }
    }

    private func showAddressSuggestions(address: String?, window: SearchWindow, completion: @escaping (AddressItem) -> ()) {
        router.showAddressSuggestions(address: address, type: type, searchWindow: window) { action in
            self.router.dismiss()
            switch action {
            case .closed:
                break
            case let .selected(item):
                completion(item)
            }
        }
    }
}
