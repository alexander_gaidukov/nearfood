//
//  AddressSuggestionsPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AddressSuggestionsPresenterType {
    var state: AddressSuggestionsViewState { get }
    func select(address: AddressItem)
    func search(for string: String)
    func close()
}

enum AddressItem {
    case suggestion(AddressSuggestionItem)
    case saved(Location)
}

struct AddressSuggestionsViewState {
    @Observable
    var isLoading: Bool = false

    @Observable
    fileprivate var suggestedItems: [AddressSuggestionItem] = []

    fileprivate var savedItems: [Location]

    @Observable
    var searchText: String?

    var items: Observable<[AddressItem]> {
        let savedItems = self.savedItems
        return $searchText.combine(with: $suggestedItems) {
            $0?.isEmpty != false ? savedItems.map { .saved($0) } : $1.map { .suggestion($0) }
        }
    }

}

enum  AddressSuggestionsAction {
    case closed
    case selected(AddressItem)
}

final class AddressSuggestionsPresenter: AddressSuggestionsPresenterType {

    private let completion: (AddressSuggestionsAction) -> ()
    private let addressesDataProvider: AddressesDataProviderType
    private var searchWindow: SearchWindow

    private(set) var state: AddressSuggestionsViewState

    init(
        type: AddressSelectionType,
        address: String?,
        addressesDataProvider: AddressesDataProviderType,
        searchWindow: SearchWindow,
        customerDataProvider: CustomerDataProviderType,
        completion: @escaping (AddressSuggestionsAction) -> ()
    ) {
        self.completion = completion
        var savedItems: [Location] = []
        if case .selection = type {
           savedItems = customerDataProvider.customer?.locations?.reversed() ?? []
        }
        self.state = AddressSuggestionsViewState(savedItems: savedItems, searchText: address)
        self.addressesDataProvider = addressesDataProvider
        self.searchWindow = searchWindow
        if address?.isEmpty == false {
            makeSearch()
        }
    }

    func close() {
        completion(.closed)
    }

    func search(for string: String) {
        state.searchText = string
        makeSearch()
    }

    func select(address: AddressItem) {
        switch address {
        case .saved:
            completion(.selected(address))
        case .suggestion(let item):
            handleSuggestion(item: item)
        }
    }

    private func makeSearch() {
        addressesDataProvider.suggestAddresses(for: state.searchText ?? "", window: searchWindow) { [weak self] items in
            self?.state.suggestedItems = items
        }
    }

    private func handleSuggestion(item: AddressSuggestionItem) {
        if item.isLeaf {
            state.isLoading = true
            addressesDataProvider.location(for: item.searchText, window: searchWindow) {[weak self] in
                self?.state.isLoading = false
                if let latitude = $0, let longitude = $1 {
                    var result = item
                    result.latitude = latitude
                    result.longitude = longitude
                    DispatchQueue.main.async {
                        self?.completion(.selected(.suggestion(result)))
                    }
                }
            }
        } else {
            search(for: item.displayText)
        }
    }
}
