//
//  CitySelectionPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CitiesPresenterType {
    var state: CitiesViewState { get }
    func select(city: City)
    func close()
}

struct CitiesViewState {
    @Observable
    var cities: [City]
}

enum CitiesAction {
    case select(City)
    case close
}

final class CitiesPresenter: CitiesPresenterType {

    private let completion: (CitiesAction) -> ()

    private(set) var state: CitiesViewState

    private let citiesManager: CitiesManagerType

    init(citiesManager: CitiesManagerType, completion: @escaping (CitiesAction) -> ()) {
        self.citiesManager = citiesManager
        state = CitiesViewState(cities: citiesManager.cities)
        self.completion = completion
        NotificationCenter.default.addObserver(self, selector: #selector(citiesDidChange), name: .citiesDidChangeNotification, object: nil)
        citiesManager.refresh()
    }

    func select(city: City) {
        completion(.select(city))
    }

    func close() {
        completion(.close)
    }

    @objc private func citiesDidChange() {
        state.cities = citiesManager.cities
    }
}
