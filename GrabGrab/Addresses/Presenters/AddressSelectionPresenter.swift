//
//  AddressSelectionPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import CoreLocation

protocol AddressSelectionPresenterType {
    var state: AddressSelectionViewState { get }
    var userLocation: CLLocation? { get }
    var searchWindow: SearchWindow { get set }
    var saveButtonTitle: String { get }

    func changeAddress()
    func changeApartments(_ apartments: String)
    func changeEntrance(_ entrance: String)
    func changeFloor(_ floor: String)
    func changeDoorPhone(_ doorphone: String)
    func changeNote(_ note: String)
    func updateLocation(latitude: Double, longitude: Double)
    func selectLocation(_ location: Location)
    func selectWrongLocation()
    func save()
    func close()
}

struct AddressSelectionViewState {
    @Observable
    var isUserLocationAvailable: Bool

    @Observable
    var location: Location

    var savedItems: [Location]

    @Observable
    var isAddressAvailable: Bool?

    @Observable
    var areas: [Area] = []

    @Observable
    var error: WebError?

    @Observable
    var checkError: WebError?

    @Observable
    var isLoading: Bool = false

    @Observable
    var schedule: String?

    @Observable
    var pinTitle: String? = LocalizedStrings.Addresses.iamHereLabel

    var contentBottomOffset: CGFloat {
        264 + (savedItems.isEmpty ? 0 : 44)
    }

    var isScheduleViewHidden: Observable<Bool> {
        $schedule.map { $0?.isEmpty != false }
    }

    var isButtonEnabled: Observable<Bool> {
        $location.combine(with: $isAddressAvailable) { $0.isValid && $1 == true }
    }

    var isLocationSelected: Observable<Bool> {
        $location.map { !$0.isEmpty }
    }

    var isViewActive: Observable<Bool> {
        $isLoading.map { !$0 }
    }

    var isWrongLocationViewHidden: Observable<Bool> {
        $isAddressAvailable.map { $0 != false }
    }

    var isAdditionalInfoViewHidden: Observable<Bool> {
        $isAddressAvailable.map { $0 == false }
    }

    var addressError: Observable<String?> {
        $error.map { $0?.apiError?.address?.localizedDescription }
    }

    var apartmentsError: Observable<String?> {
        $error.map { $0?.apiError?.apartments?.localizedDescription }
    }

    var entranceError: Observable<String?> {
        $error.map { $0?.apiError?.entrance?.localizedDescription }
    }

    var floorError: Observable<String?> {
        $error.map { $0?.apiError?.floor?.localizedDescription }
    }

    var doorPhoneError: Observable<String?> {
        $error.map { $0?.apiError?.door_phone?.localizedDescription }
    }
}

enum AddressSelectionAction {
    case close
    case selected(Location)
    case changeAddress(String?, SearchWindow, (AddressItem) -> ())
    case selectCity((City) -> ())
}

final class AddressSelectionPresenter: AddressSelectionPresenterType {

    var userLocation: CLLocation? {
        locationManager.currentLocation
    }

    var searchWindow: SearchWindow = SearchWindow(southWest: (0, 0), northEast: (0, 0))

    private let completion: (AddressSelectionAction) -> ()
    private var locationManager: LocationManagerType
    private let webClient: WebClientType
    private let addressesDataProvider: AddressesDataProviderType
    private let customerDataProvider: CustomerDataProviderType
    private let type: AddressSelectionType

    private var shouldSelectAfterCheck: Bool = false

    var saveButtonTitle: String {
        switch type {
        case .selection:
           return LocalizedStrings.Addresses.saveButtonTitle
        case .new:
            return LocalizedStrings.Common.select
        }
    }

    private(set) var state: AddressSelectionViewState

    init(
        type: AddressSelectionType,
        locationManager: LocationManagerType,
        addressesDataProvider: AddressesDataProviderType,
        customerDataProvider: CustomerDataProviderType,
        webClient: WebClientType,
        completion: @escaping (AddressSelectionAction) -> ()
    ) {
        self.locationManager = locationManager
        self.addressesDataProvider = addressesDataProvider
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        self.type = type

        let location: Location
        switch type {
        case .selection:
            location = customerDataProvider.currentLocation ??
                Location(
                    latitude: locationManager.currentLocation?.coordinate.latitude ?? 0,
                    longitude: locationManager.currentLocation?.coordinate.longitude ?? 0
                )
        case .new(let loc):
            location = loc.isEmpty ?
                Location(
                    latitude: locationManager.currentLocation?.coordinate.latitude ?? 0,
                    longitude: locationManager.currentLocation?.coordinate.longitude ?? 0
                )
                : loc
        }

        var savedItems: [Location] = []
        if case .selection = type {
            savedItems = customerDataProvider.customer?.locations?.reversed().filter { $0.title?.isEmpty == false } ?? []
        }

        self.state = AddressSelectionViewState(isUserLocationAvailable: locationManager.currentLocation != nil,
                                               location: location,
                                               savedItems: savedItems)
        self.completion = completion
        if self.locationManager.currentLocation == nil {
            self.locationManager.delegate = self
        }

        loadAvailableAreas()
        locationManager.start()
    }

    deinit {
        locationManager.stop()
    }

    func changeApartments(_ apartments: String) {
        var location = state.location
        location.apartments = apartments
        location.uuid = ""
        state.location = location
    }

    func changeEntrance(_ entrance: String) {
        var location = state.location
        location.entrance = entrance
        location.uuid = ""
        state.location = location
    }

    func changeDoorPhone(_ doorphone: String) {
        var location = state.location
        location.doorPhone = doorphone
        location.uuid = ""
        state.location = location
    }

    func changeFloor(_ floor: String) {
        var location = state.location
        location.floor = floor
        location.uuid = ""
        state.location = location
    }

    func changeNote(_ note: String) {
        var location = state.location
        location.note = note
        location.uuid = ""
        state.location = location
    }

    func changeAddress() {
        completion(.changeAddress(state.location.address, searchWindow, {[weak self] addressItem in
            guard let self = self else { return }
            switch addressItem {
            case let .suggestion(item):
                var location = self.state.location
                location.latitude = item.latitude
                location.longitude = item.longitude
                location.address = item.displayText
                location.uuid = ""
                location.entrance = ""
                location.apartments = ""
                location.floor = ""
                location.doorPhone = ""
                location.note = ""
                self.state.location = location
            case let .saved(location):
                self.state.location = location
            }
        }))
    }

    func selectLocation(_ location: Location) {
        shouldSelectAfterCheck = true
        state.location = location
    }

    func updateLocation(latitude: Double, longitude: Double) {
        if state.location.latitude != latitude || state.location.longitude != longitude {
            var location = state.location
            location.uuid = ""
            location.latitude = latitude
            location.longitude = longitude
            location.entrance = ""
            location.apartments = ""
            location.floor = ""
            location.doorPhone = ""
            location.note = ""
            state.location = location
        }
        loadAndCheckAddress()
    }

    func save() {

        guard case .selection = type else {
            completion(.selected(state.location))
            return
        }

        guard state.location.uuid.isEmpty else {
            completion(.selected(state.location))
            return
        }

        guard let customer = customerDataProvider.customer else {
            completion(.close)
            return
        }

        state.isLoading = true
        state.location.title = nil
        let resource = ResourceBuilder.saveLocationResource(
            location: state.location,
            customerUUID: customer.uuid
        )
        webClient.load(combinedResource: resource) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case let .success(locations):
                self?.customerDataProvider.updateAddresses(locations.1)
                DispatchQueue.main.async {
                    self?.completion(.selected(locations.0))
                }
            case let .failure(error):
                self?.state.error = error
            }
        }
    }

    func close() {
        completion(.close)
    }

    func selectWrongLocation() {
        completion(.selectCity({[weak self] city in
            self?.updateLocation(latitude: city.latitude, longitude: city.longitude)
        }))
    }

    private func loadAvailableAreas() {
        webClient.load(resource: ResourceBuilder.availableAreasResource()) {[weak self] result in
            self?.state.areas = ((try? result.get()) ?? []).sorted {
                $0.isPaid || !$1.isPaid
            }
        }
    }

    private func loadAndCheckAddress() {
        guard state.isLocationSelected.wrappedValue,
              let customer = customerDataProvider.customer else {
            return
        }
        state.isLoading = true

        let group = DispatchGroup()

        group.enter()
        let resource = ResourceBuilder.checkLocationResource(
            latitude: state.location.latitude,
            longitude: state.location.longitude,
            customerUUID: customer.uuid
        )
        webClient.load(resource: resource) {[weak self] result in
            switch result {
            case .success(let response):
                self?.state.schedule = response.schedule
                self?.state.isAddressAvailable = true
                self?.state.pinTitle = response.fee > 0 ?
                    "\(LocalizedStrings.Addresses.paidArea) (\(response.fee.string(with: Formatters.priceFormatter) ?? ""))"
                    : LocalizedStrings.Addresses.iamHereLabel
            case .failure(let error):
                self?.state.checkError = error
                self?.state.pinTitle = LocalizedStrings.Addresses.iamHereLabel
                self?.state.isAddressAvailable = false
            }
            group.leave()
        }

        group.enter()
        addressesDataProvider.address(from: state.location.latitude, longitude: state.location.longitude) {[weak self] in
            self?.state.isLoading = false
            self?.state.location.address = $0 ?? ""
            group.leave()
        }

        group.notify(queue: .main) {[weak self] in
            self?.state.isLoading = false
            if self?.shouldSelectAfterCheck == true {
                self?.save()
            }
            self?.shouldSelectAfterCheck = false
        }
    }
}

extension AddressSelectionPresenter: LocationManagerDelegate {
    func locationManager(_ locationManager: LocationManager, didFailWith error: LocationManagerError) {}

    func locationManager(_ locationManager: LocationManager, didUpdateLocation location: CLLocation) {
        state.isUserLocationAvailable = true
        if !state.isLocationSelected.wrappedValue {
            state.location.latitude = location.coordinate.latitude
            state.location.longitude = location.coordinate.longitude
        }

        locationManager.delegate = nil
    }
}
