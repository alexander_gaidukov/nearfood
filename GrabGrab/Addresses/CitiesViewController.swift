//
//  CitiesViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    var presenter: CitiesPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    private func configureView() {
        title = LocalizedStrings.Cities.title
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(closeTapped))
        tableView.register(cellType: CityCell.self)
        tableView.tableFooterView = UIView(frame: .zero)
    }

    private func configureBindings() {
        presenter.state.$cities.observe { [weak self] _ in self?.tableView.reloadData() }.addToDisposableBag(disposableBag)
    }

    @objc private func closeTapped() {
        presenter.close()
    }
}

extension CitiesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.state.cities.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CityCell = tableView.dequeue(forIndexPath: indexPath)
        let city = presenter.state.cities[indexPath.row]
        cell.configure(city: city)
        return cell
    }
}

extension CitiesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.select(city: presenter.state.cities[indexPath.row])
    }
}
