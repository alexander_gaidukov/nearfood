//
//  AddressSelectionMapViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import YandexMapKit

class AddressSelectionMapViewController: UIViewController {

    @IBOutlet private weak var iamHereView: UIView!
    @IBOutlet private weak var iamHereLabel: UILabel!

    @IBOutlet private weak var mapView: YMKMapView!
    @IBOutlet private weak var userLocationButton: UIButton!

    @IBOutlet private weak var scheduleView: UIView!
    @IBOutlet private weak var scheduleLabel: UILabel!

    @IBOutlet private weak var mapViewBottomOffset: NSLayoutConstraint!

    var presenter: AddressSelectionPresenterType!
    private let disposableBag = DisposableBag()

    private var cameraActionId: String?
    private var areasDisplayed: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                configureMapViewStyle()
            }
        }
    }

    private var searchWindow: SearchWindow {
        let visibleRegion = mapView.mapWindow.map.visibleRegion
        let points = [visibleRegion.topLeft, visibleRegion.topRight, visibleRegion.bottomLeft, visibleRegion.bottomRight]

        let latitudes = points.map(\.latitude)
        let longitudes = points.map(\.longitude)

        // swiftlint:disable:next force_unwrapping
        return SearchWindow(southWest: (latitudes.min()!, longitudes.min()!), northEast: (latitudes.max()!, longitudes.max()!))
    }

    private func configureViews() {
        configureMapViewStyle()
        mapView.mapWindow.map.addCameraListener(with: self)
        mapView.mapWindow.map.logo.setAlignmentWith(YMKLogoAlignment(horizontalAlignment: .left, verticalAlignment: .bottom))

        enableUserLocation()
        mapViewBottomOffset.constant = presenter.state.contentBottomOffset - (pulleyViewController?.drawerCornerRadius ?? 0)
    }

    private func configureBindings() {
        presenter.state.$isUserLocationAvailable
            .map { !$0 }
            .bind(to: userLocationButton, keyPath: \.isHidden)
            .addToDisposableBag(disposableBag)

        presenter.state.isViewActive.bind(to: view, keyPath: \.isUserInteractionEnabled).addToDisposableBag(disposableBag)

        presenter.state.isLocationSelected.map { !$0 }.bind(to: iamHereView, keyPath: \.isHidden).addToDisposableBag(disposableBag)

        presenter.state.$areas.observe {[weak self] _ in self?.showAvailabilityAreas()}.addToDisposableBag(disposableBag)

        presenter.state.$schedule.bind(to: scheduleLabel, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.isScheduleViewHidden.bind(to: scheduleView, keyPath: \.isHidden).addToDisposableBag(disposableBag)

        presenter.state.$pinTitle.bind(to: iamHereLabel, keyPath: \.text).addToDisposableBag(disposableBag)

        presenter.state.$location.observe {[weak self] in
            if !$0.isEmpty { self?.moveToSelectedLocation() }
        }.addToDisposableBag(disposableBag)
    }

    private func enableUserLocation() {
        let userLocationLayer = YMKMapKit.sharedInstance().createUserLocationLayer(with: self.mapView.mapWindow)
        userLocationLayer.setVisibleWithOn(true)
        userLocationLayer.isHeadingEnabled = true
    }

    private func configureMapViewStyle() {
        if #available(iOS 13.0, *) {
            mapView.mapWindow.map.isNightModeEnabled = traitCollection.userInterfaceStyle == .dark
        } else {
            mapView.mapWindow.map.isNightModeEnabled = false
        }
    }

    private func moveToUserLocation() {
        if let userLocation = presenter.userLocation {
            move(to: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        }
    }

    private func moveToSelectedLocation() {
        move(to: presenter.state.location.latitude, longitude: presenter.state.location.longitude)
    }

    private func move(to latitude: Double, longitude: Double) {
        let currentLocation = mapView.mapWindow.map.cameraPosition.target
        let epsilon = 0.000001
        guard fabs(currentLocation.latitude - latitude) >= epsilon || fabs(currentLocation.longitude - longitude) >= epsilon else { return }

        let newPosition = YMKCameraPosition(target: YMKPoint(latitude: latitude, longitude: longitude),
                                            zoom: max(mapView.mapWindow.map.cameraPosition.zoom, 15),
                                            azimuth: 0,
                                            tilt: 0)

        mapView.mapWindow.map.move(with: newPosition,
                                   animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 0.5)) {[weak self] completed in
                                    if completed { self?.presenter.updateLocation(latitude: latitude, longitude: longitude) }

        }
    }

    private func showAvailabilityAreas() {
        guard !areasDisplayed && !presenter.state.areas.isEmpty else { return }
        areasDisplayed = true
        mapView.mapWindow.map.mapObjects.clear()
        presenter.state.areas.forEach {
            let points = $0.borders.points.map { YMKPoint(latitude: $0.latitude, longitude: $0.longitude) }
            let polygon = YMKPolygon(outerRing: YMKLinearRing(points: points), innerRings: [])
            let polygonMapObject = self.mapView.mapWindow.map.mapObjects.addPolygon(with: polygon)
            if $0.isDeactivated {
                polygonMapObject.fillColor  = .deactivatedPolygonBackground
                polygonMapObject.strokeColor = .deactivatedPolygonBorder
            } else if $0.isPaid {
                polygonMapObject.fillColor = .paidPolygonBackground
                polygonMapObject.strokeColor = .paidPolygonBorder
            } else {
                polygonMapObject.fillColor = .polygonBackground
                polygonMapObject.strokeColor = .polygonBorder
            }
            polygonMapObject.strokeWidth = 2.0
        }
    }

    private func cameraPositionChanged() {
        let cameraActionId = UUID().uuidString
        self.cameraActionId = cameraActionId
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard self.cameraActionId == cameraActionId else { return }
            self.presenter.updateLocation(
                latitude: self.mapView.mapWindow.map.cameraPosition.target.latitude,
                longitude: self.mapView.mapWindow.map.cameraPosition.target.longitude
            )
        }
    }

    @IBAction private func userLocationTapped() {
        moveToUserLocation()
    }
}

extension AddressSelectionMapViewController: YMKMapCameraListener {
    func onCameraPositionChanged(
        with map: YMKMap,
        cameraPosition: YMKCameraPosition,
        cameraUpdateSource: YMKCameraUpdateSource,
        finished: Bool
    ) {
        if finished {
            presenter.searchWindow = searchWindow
            if cameraUpdateSource == .gestures {
                cameraPositionChanged()
            }
        } else {
            cameraActionId = nil
        }
    }

}
