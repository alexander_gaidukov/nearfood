//
//  AddressesSuggestionViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class AddressesSuggestionsViewController: UIViewController {

    @IBOutlet private weak var searchField: UITextField!
    @IBOutlet private weak var searchLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!

    var presenter: AddressSuggestionsPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    private func configureView() {
        title = LocalizedStrings.Addresses.Suggestions.title
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: LocalizedStrings.Common.close,
            style: .plain,
            target: self,
            action: #selector(closeTapped)
        )
        searchField.placeholder = LocalizedStrings.Addresses.Suggestions.placeholder
        searchLabel.text = LocalizedStrings.Addresses.Suggestions.label

        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(cellType: AddressSuggestionCell.self)
        searchField.becomeFirstResponder()
    }

    private func configureBindings() {
        presenter.state.items.observe { [weak self] _ in self?.tableView.reloadData() }.addToDisposableBag(disposableBag)
        presenter.state.$searchText.bind(to: searchField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)
    }

    @objc private func closeTapped() {
        presenter.close()
    }

    @IBAction private func textFieldDidChange() {
        let text = searchField.text ?? ""
        presenter.search(for: text)
    }
}

extension AddressesSuggestionsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

extension AddressesSuggestionsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.state.items.wrappedValue.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AddressSuggestionCell = tableView.dequeue(forIndexPath: indexPath)
        if let addressItem = presenter.state.items.wrappedValue.get(at: indexPath.row) {
            cell.configure(item: addressItem)
        }
        return cell
    }
}

extension AddressesSuggestionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        presenter.select(address: presenter.state.items.wrappedValue[indexPath.row])
    }
}

extension AddressesSuggestionsViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}
