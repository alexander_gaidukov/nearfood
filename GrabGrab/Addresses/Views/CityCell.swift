//
//  CityCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!

    func configure(city: City) {
        titleLabel.text = city.name
    }
}
