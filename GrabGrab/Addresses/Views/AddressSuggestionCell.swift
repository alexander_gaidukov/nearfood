//
//  AddressSuggestionCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class AddressSuggestionCell: UITableViewCell {
    @IBOutlet private weak var nameLabel: UILabel!

    func configure(item: AddressItem) {
        switch item {
        case .saved(let location):
            nameLabel.text = location.label
        case .suggestion(let suggestedItem):
            nameLabel.text = suggestedItem.displayText
        }
    }
}
