//
//  SavedAddressCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class SavedAddressCell: UICollectionViewCell {

    @IBOutlet private weak var innerView: UIView!
    @IBOutlet private weak var label: UILabel!

    static func size(for text: String) -> CGSize {
        let width = (text as NSString).size(withAttributes: [.font: UIFont.preferredFont(forTextStyle: .footnote)]).width
        return CGSize(width: width + 24, height: 44)
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                innerView.borderColor = .ratingBorder
            }
        }
    }

    func configure(text: String) {
        label.text = text
    }
}
