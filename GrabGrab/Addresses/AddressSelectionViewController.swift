//
//  AddressSelectionViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import Pulley

class AddressSelectionViewController: UIViewController, KeyboardAvoiding {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var saveButton: UIButton!

    var presenter: AddressSelectionPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
    }

    @objc private func closeTapped() {
        presenter.close()
    }

    private func configureViews() {
        title = LocalizedStrings.Addresses.title
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: LocalizedStrings.Common.close,
            style: .plain,
            target: self,
            action: #selector(closeTapped)
        )
        saveButton.setTitle(presenter.saveButtonTitle, for: .normal)

        let mapVC = AddressSelectionMapViewController.instance(storyboard: .addresses)
        mapVC.presenter = presenter

        let infoVC = AddressSelectionInfoViewController.instance(storyboard: .addresses)
        infoVC.presenter = presenter

        let drawer = infoVC.embededInDrawer()

        let pulleyController = PulleyViewController(contentViewController: mapVC, drawerViewController: drawer)
        pulleyController.drawerTopInset = 10.0
        pulleyController.backgroundDimmingColor = .clear
        pulleyController.drawerBackgroundVisualEffectView = nil
        pulleyController.initialDrawerPosition = .partiallyRevealed
        pulleyController.positionWhenDimmingBackgroundIsTapped = .open
        pulleyController.delegate = self
        insertViewController(pulleyController, toView: containerView)
    }

    private func configureBindings() {
        presenter.state.isButtonEnabled.bind(to: saveButton, keyPath: \.isEnabled).addToDisposableBag(disposableBag)
        presenter.state.isViewActive.bind(to: view, keyPath: \.isUserInteractionEnabled).addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe {[weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    @IBAction private func saveButtonTapped() {
        view.endEditing(true)
        presenter.save()
    }
}

extension AddressSelectionViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension AddressSelectionViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}

extension AddressSelectionViewController: PulleyDelegate {

}
