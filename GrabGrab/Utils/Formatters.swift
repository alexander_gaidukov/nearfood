//
//  Formatters.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

extension Double {
    func string(with formatter: NumberFormatter) -> String? {
        formatter.string(from: NSNumber(value: self))
    }
}

extension Int {
    var etrString: String? {
        guard self > 0 else { return nil }
        return  "+\(self / 60) \(LocalizedStrings.Common.minutes)"
    }
}

extension String {
    static func priceAndDelay(from price: Double, delay: Int?, pricePlusPreposition: Bool) -> String? {
        let delayString = delay?.etrString
        let priceString = price != 0
            ? price.string(with: Formatters.priceFormatter).map { "\(pricePlusPreposition ? "+" : "")\($0)" }
            : nil

        let components = [priceString, delayString].compactMap { $0 }

        if components.isEmpty {
            return nil
        }

        if components.count == 1 {
            return components.first
        }

        return components[0] + " (" + components[1] + ")"
    }
}

struct Formatters {
    static let isoDateFormatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime]
        return formatter
    }()

    static let outputDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()

    static let outputTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        return dateFormatter
    }()

    static let messagesHeaderDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()

    static let timerFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        return formatter
    }()

    static let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .currency
        formatter.minimumFractionDigits = 0
        formatter.locale = Locale(identifier: "ru_RU")
        return formatter
    }()
}
