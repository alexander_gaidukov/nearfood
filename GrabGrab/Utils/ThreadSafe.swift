//
//  ThreadSafe.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class ThreadSafe<A> {
    private var _value: A
    private let queue = DispatchQueue(label: "grab-grab.sync.queue." + UUID().uuidString, qos: .userInitiated)
    init(_ value: A) {
        self._value = value
    }

    var value: A {
        queue.sync { _value }
    }

    @discardableResult
    func atomically(transform: (inout A) -> ()) -> A {
        queue.sync {
            transform(&self._value)
            return self._value
        }
    }
}
