//
//  ImageLoading.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import Kingfisher

extension Kingfisher.DownloadTask: DownloadTask {}

extension UIImage {
    var grayscaled: UIImage? {
        let context = CIContext(options: nil)
        guard let currentFilter = CIFilter(name: "CIPhotoEffectNoir") else { return nil }
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        if let output = currentFilter.outputImage,
            let cgImage = context.createCGImage(output, from: output.extent) {
            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        }
        return nil
    }
}

struct ImageDownloadingProperties {
    enum ImageResizingType: String {
        case fill
        case fit
        case auto
    }

    enum Gravity: String {
        case top = "no"
        case bottom = "so"
        case right = "ea"
        case left = "we"
        case topRight = "noea"
        case topLeft = "nowe"
        case bottomRight = "soea"
        case bottomLeft = "sowe"
        case center = "ce"
        case smart = "sm"
    }

    enum ImageResizing {
        case none
        case scale(ImageResizingType, Gravity)
    }

    var resizing: ImageResizing = .scale(.auto, .center)
    var grayscaled = false
}

extension UIImageView {
    @discardableResult
    func load(
        photo: Photo?,
        placeholder: UIImage? = nil,
        properties: ImageDownloadingProperties = ImageDownloadingProperties(),
        completion: @escaping (UIImage?) -> () = {_ in}
    ) -> DownloadTask? {
        load(url: photo?.url, placeholder: placeholder, properties: properties, completion: completion)
    }

    @discardableResult
    func load(
        attachment: ImageAttachment?,
        placeholder: UIImage? = nil,
        properties: ImageDownloadingProperties = ImageDownloadingProperties(),
        completion: @escaping (UIImage?) -> () = {_ in}
    ) -> DownloadTask? {
        guard let attachment = attachment else {
            image = nil
            completion(nil)
            return nil
        }
        switch attachment {
        case .local(let image):
            self.image = image
            completion(image)
            return nil
        case .remote(let photo):
            return load(
                photo: photo,
                placeholder: placeholder,
                properties: properties,
                completion: completion
            )
        }
    }

    @discardableResult
    func load(
        url: String?,
        placeholder: UIImage? = nil,
        properties: ImageDownloadingProperties = ImageDownloadingProperties(),
        completion: @escaping (UIImage?) -> () = {_ in}
    ) -> DownloadTask? {
        guard let url = url else { return nil }
        var options: KingfisherOptionsInfo?
        if properties.grayscaled {
            options = [.processor(GrayscaleImageProcessor())]
        }

        // swiftlint:disable:next force_unwrapping
        var components = URLComponents(url: API.cdnURL, resolvingAgainstBaseURL: false)!
        var path = Path("auto")
        if case let .scale(type, gravity) = properties.resizing {
            let width = Int((frame.width * UIScreen.main.scale).rounded())
            let height = Int((frame.height * UIScreen.main.scale).rounded())
            path.append(path: Path("w:\(width)/h:\(height)/rt:\(type.rawValue)/g:\(gravity.rawValue)"))
        }
        path.append(path: Path("plain"))
        components.path = Path(components.path).appending(path: path).absolutePath + "/\(url)@png"
        guard let imageURL = components.url else {
            completion(nil)
            return nil
        }
        return kf.setImage(
            with: imageURL,
            placeholder: placeholder,
            options: options,
            completionHandler: { result in completion((try? result.get())?.image) }
        )
    }
}

fileprivate final class GrayscaleImageProcessor: ImageProcessor {

    var identifier: String = "grab-grab.imageprocessing.grayscale.processor"

    func process(item: ImageProcessItem, options: KingfisherParsedOptionsInfo) -> KFCrossPlatformImage? {
        switch item {
        case .image(let image):
            return image.grayscaled
        case .data(let data):
            return UIImage(data: data)?.grayscaled
        }
    }

}
