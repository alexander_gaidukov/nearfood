//
//  Observable.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 26.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class DisposableBag {
    private var disposables: [Disposable] = []
    func add(_ disposable: Disposable) {
        disposables.append(disposable)
    }
}

final  class Disposable {
    private let callback: () -> ()

    deinit {
        callback()
    }

    init(callback: @escaping () -> ()) {
        self.callback = callback
    }

    func addToDisposableBag(_ disposableBag: DisposableBag) {
        disposableBag.add(self)
    }
}

@propertyWrapper
final class Observable<T> {

    typealias Listener = (T) -> ()
    private var iterator = (0...).makeIterator()
    private var listeners: [Int: Listener] = [:]
    private let disposableBag = DisposableBag()

    var wrappedValue: T {
        didSet {
            self.listeners.values.forEach { $0(wrappedValue) }
        }
    }

    var projectedValue: Observable<T> {
        self
    }

    init(wrappedValue value: T) {
        self.wrappedValue = value
    }

    func observe(listener: @escaping Listener) -> Disposable {
        let observer: Listener = { value in
            DispatchQueue.main.async {
                listener(value)
            }
        }
        return add(listener: observer, fire: true)
    }

    func bind<O: AnyObject>(to object: O, keyPath: ReferenceWritableKeyPath<O, T>) -> Disposable {
        let listener: Listener = { [weak object] value in
            DispatchQueue.main.async {
                object?[keyPath: keyPath] = value
            }
        }

        return add(listener: listener, fire: true)
    }

    func map<K>(_ transform: @escaping (T) -> K) -> Observable<K> {
        let result = Observable<K>(wrappedValue: transform(wrappedValue))
        add {[weak result] value in
            result?.wrappedValue = transform(value)
        }.addToDisposableBag(result.disposableBag)
        return result
    }

    func combine<K, M>(with observable: Observable<K>, transform: @escaping (T, K) -> M) -> Observable<M> {
        let result = Observable<M>(wrappedValue: transform(wrappedValue, observable.wrappedValue))

        add {[weak result, weak observable] value in
            guard let observable = observable else { return }
            result?.wrappedValue = transform(value, observable.wrappedValue)
        }.addToDisposableBag(result.disposableBag)

        observable.add {[weak result, weak self] value in
            guard let self = self else { return }
            result?.wrappedValue = transform(self.wrappedValue, value)
        }.addToDisposableBag(result.disposableBag)

        return result
    }

    func combine<K>(with observable: Observable<K>) -> Observable<(T, K)> {
        combine(with: observable) { ($0, $1) }
    }

    private func add(listener: @escaping Listener, fire: Bool = false) -> Disposable {
        // swiftlint:disable:next force_unwrapping
        let index = iterator.next()!
        listeners[index] = listener
        if fire {
           listener(wrappedValue)
        }
        return Disposable { self.listeners[index] = nil }
    }
}
