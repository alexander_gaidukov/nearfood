//
//  Constants.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum Environment: String, CaseIterable {
    case development
    case production
}

struct Constants {
    private init() {}

    struct Environments {
        static var api: Environment = (Bundle.main.infoDictionary?["BuildType"] as? String)
            .flatMap(Environment.init)
            ?? .production

        static let pushNotification: Environment = (Bundle.main.infoDictionary?["PNEnvironment"] as? String)
            .flatMap(Environment.init)
            ?? .production
    }

    struct Intervals {
        static let ordersUpdateInterval: TimeInterval = 60
        static let orderTrackingInterval: TimeInterval = 5
        static let paymentProcessingUpdateInterval: TimeInterval = 5
        static let paymentProcessingLoadingInterval: TimeInterval = 10
        static let storefrontUpdateInterval: TimeInterval = 60
        static let photosLoadingIndicatorInterval: TimeInterval = 1
        static let smsResendInterval = 60
        static let retriesCount = 3
        static let retriesDelay: TimeInterval = 3
    }

    // swiftlint:disable:next type_name
    struct UI {
        static let tabBarHeight: CGFloat = 49
    }

    struct Identifiers {
        static var cloudPaymentsMerchantId: String = {
            Environments.api == .production ? "pk_9d925cbf7ccd6d51f464754765162" : "pk_217b0ae5b18c1e0b6420ecd5bf910"
        }()
        static let applePayMerchantId = "merchant.com.nearfood.customer"
        static let yandexMapKey = "1b2d98b2-510e-45b2-b70f-778e6b2abf39"
        static let widgetKind = "com.nearfood.customer.widget"
        static let appStoreId = "id1551336244"
    }

    struct Cart {
        static let maxAutomaticallyVerifiedDistanceToOrder = 200.0
    }

    struct UITests {
        // swiftlint:disable:next line_length
        static let accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1dWlkIjoiZTE3YjlkMDEtZjQ1MC00NDZjLTgzZWItMjhjOTFjMWY2Y2Q1Iiwia2luZCI6ImN1c3RvbWVyIiwicGF5bG9hZCI6eyJ1dWlkIjoiYmM0NDQ3YjAtYzkzYy00NzExLWE5OWYtYjBmZjI3ODE0MWYwIn19.9xh5Vo7lLJq3cCaWwg_LVwj4JTao6YBYI4JDHfXqgTs"
        static let testModeKey = "TEST_MODE"
        static let address = LocalizedStrings.Addresses.screenshots
        struct Order {
            static let courierCoordinates: GeoPosition = GeoPosition(
                latitude: 56.739602,
                longitude: 37.170767,
                course: 245
            )
            static let timer = "09:41"
        }
    }
}
