//
//  Identifiers.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Identifiers {

    struct TabBar {
        static let storefront = "tabbar.storefront"
        static let account = "tabbar.account"
        static let chat = "tabbar.chat"
        static let cart = "tabbar.cart"
    }

    struct Cart {
        struct PaymentMethod {
            static let changeButton = "cart.paymentMethod.change"
        }
    }

    struct Account {
        static let name = "account.name"
        static let addresses = "account.addresses"
        static let orders = "account.orders"
        static let paymentMethods = "account.paymentMethods"
        static let contacts = "account.contacts"
        static let invitations = "account.invitations"
        static let tips = "account.tips"
        static let logout = "account.logout"
    }

    struct Order {
        static let cell = "order.cell"
    }

    struct Common {
        static let backButton = "buttons.back"
    }
}
