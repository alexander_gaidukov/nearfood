//
//  Refresher.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class Refresher {
    private let action: () -> ()

    private let delay: TimeInterval
    private var isUpdating = false

    init(delay: TimeInterval = 0.2, action: @escaping () -> ()) {
        self.action = action
        self.delay = delay
    }

    func refresh() {
        guard !isUpdating else { return }
        isUpdating = true
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {[weak self] in
            self?.action()
            self?.isUpdating = false
        }
    }
}

final class DelayRefresher {
    private let action: () -> ()
    private let delay: TimeInterval

    private var uuid: String?

    init(delay: TimeInterval, action: @escaping () -> ()) {
        self.action = action
        self.delay = delay
    }

    func refresh() {
        let uuid = UUID().uuidString
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {[weak self] in
            guard uuid == self?.uuid else { return }
            self?.action()
        }
        self.uuid = uuid
    }
}
