//
//  UserDefaultsMigrationV1.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct UserDefaultsMigrationV1: Migration {
    var version: Int { 1 }
    func run() {
        let dictionary = UserDefaults.standard.dictionaryRepresentation()
        let encoder = Coders.encoder
        dictionary.forEach { key, value in

            guard key.contains("grab") else { return }

            let data: Data?

            if let boolValue = value as? Bool {
                data = try? encoder.encode(StorageItemWrapper(item: boolValue))
            } else if let intValue = value as? Int {
                data = try? encoder.encode(StorageItemWrapper(item: intValue))
            } else if let date = (value as? String).flatMap({ Formatters.isoDateFormatter.date(from: $0) }) {
                data = try? encoder.encode(StorageItemWrapper(item: date))
            } else if key == "grab-grab.locations.askedForSave.key", let stringValue = value as? String {
                data = try? encoder.encode(StorageItemWrapper(item: Set(stringValue.components(separatedBy: ","))))
            } else if let stringValue = value as? String {
                data = try? encoder.encode(StorageItemWrapper(item: stringValue))
            } else if let info = (value as? Data)
                        .flatMap({ try? Coders.decoder.decode(CompanyInfo.self, from: $0) }) {
                data = try? encoder.encode(StorageItemWrapper(item: info))
            } else {
                data = nil
            }

            if let data = data {
                UserDefaults.standard.set(data, forKey: key)
            }

        }
    }
}
