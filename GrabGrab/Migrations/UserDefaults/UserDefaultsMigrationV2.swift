//
//  UserDefaultsMigrationV2.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct UserDefaultsMigrationV2: Migration {
    var version: Int { 2 }
    func run() {
        if let locationUUID = UserDefaults.standard.data(forKey: "com.grabgrab.currentlocation") {
            UserDefaults.standard.set(locationUUID, forKey: "com.grabgrab.currentlocation.api.nearfood.ru")
            UserDefaults.standard.removeObject(forKey: "com.grabgrab.currentlocation")
        }

        if let paymentMethod = UserDefaults.standard.data(forKey: "com.grabgrab.paymentMethod") {
            UserDefaults.standard.set(paymentMethod, forKey: "com.grabgrab.paymentMethod.api.nearfood.ru")
            UserDefaults.standard.removeObject(forKey: "com.grabgrab.paymentMethod")
        }
    }
}
