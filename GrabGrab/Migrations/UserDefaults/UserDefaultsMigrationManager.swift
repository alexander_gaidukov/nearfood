//
//  UserDefaultsMigrationManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class UserDefaultsMigrationManager: MigrationsManager {

    private let migrations: [Migration] = [UserDefaultsMigrationV1(), UserDefaultsMigrationV2()]

    @UserDefaultsItem(key: StorageKey("com.grabgrab.userdefaults.version"))
    private var currentVersion: Int?

    func migrate() {
        let currentVersion = self.currentVersion
        if currentVersion == nil {
            clearKeychain()
        }
        let version: Int = currentVersion ?? 0
        migrations.forEach {
            if $0.version > version { $0.run() }
        }
        self.currentVersion = migrations.last?.version ?? 0
    }

    private func clearKeychain() {
        let keys = [
            "com.grabgrab.customer.uuid.api.grab-grab.ru",
            "com.grabgrab.customer.api.grab-grab.ru",
            "com.grabgrab.token.api.grab-grab.ru"
        ]
        keys.forEach { Keychain.delete(key: $0) }
    }
}
