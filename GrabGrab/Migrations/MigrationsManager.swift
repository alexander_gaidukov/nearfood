//
//  MigrationsManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol MigrationsManager {
    func migrate()
}
