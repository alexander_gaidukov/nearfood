//
//  KeychainMigrationV1.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct KeychainMigrationV1: Migration {
    var version: Int { 1 }
    func run() {
        migrateCustomer()
        migrateCustomerUUID()
    }

    private func migrateCustomer() {
        guard let customer = Keychain.load(key: "com.grabgrab.customer") else { return }
        Keychain.save(key: "com.grabgrab.customer.api.nearfood.ru", value: customer)
        Keychain.delete(key: "com.grabgrab.customer")
    }

    private func migrateCustomerUUID() {
        guard let uuid = Keychain.load(key: "com.grabgrab.customer.uuid") else { return }
        Keychain.save(key: "com.grabgrab.customer.uuid.api.nearfood.ru", value: uuid)
        Keychain.delete(key: "com.grabgrab.customer.uuid")
    }
}
