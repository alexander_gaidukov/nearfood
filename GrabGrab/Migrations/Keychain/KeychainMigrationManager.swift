//
//  KeychainMigrationManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class KeychainMigrationManager: MigrationsManager {

    private let migrations: [Migration] = [KeychainMigrationV1(), KeychainMigrationV2()]

    @KeychainItem(key: StorageKey("com.grabgrab.keychain.version"), defaultValue: 0)
    private var currentVersion: Int

    func migrate() {
        let version: Int = currentVersion
        migrations.forEach {
            if $0.version > version { $0.run() }
        }
        currentVersion = migrations.last?.version ?? 0
    }
}
