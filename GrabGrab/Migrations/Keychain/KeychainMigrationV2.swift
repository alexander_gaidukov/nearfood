//
//  KeychainMigrationV2.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 26.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct KeychainMigrationV2: Migration {
    var version: Int { 2 }
    func run() {
        migrateCurrentVersion()
        migrateDeviceUUID()
        ["api.nearfood.ru", "api.grab-grab.ru"].forEach {
            migrateToken(host: $0)
            migrateCustomerUUID(host: $0)
            migrateCustomer(host: $0)
        }
    }

    private func migrateCustomerUUID(host: String) {
        let key = "com.grabgrab.customer.uuid." + host
        guard let uuidData = Keychain.load(key: key),
              let uuid = String(data: uuidData, encoding: .utf8),
              let data = try? Coders.encoder.encode(StorageItemWrapper(item: uuid)) else {
            return
        }
        Keychain.save(key: key, value: data)
    }

    private func migrateCustomer(host: String) {
        let key = "com.grabgrab.customer." + host
        guard let customerData = Keychain.load(key: key),
              let customer = try? Coders.decoder.decode(Customer.self, from: customerData),
              let data = try? Coders.encoder.encode(StorageItemWrapper(item: customer)) else {
            return
        }
        Keychain.save(key: key, value: data)
    }

    private func migrateToken(host: String) {
        let key = "com.grabgrab.token." + host
        guard let tokenData = Keychain.load(key: key),
              let token = String(data: tokenData, encoding: .utf8),
              let data = try? Coders.encoder.encode(StorageItemWrapper(item: token)) else {
            return
        }
        Keychain.save(key: key, value: data)
    }

    private func migrateDeviceUUID() {
        let key = "com.grabgrab.deviceUUID"
        guard let deviceData = Keychain.load(key: key),
              let device = String(data: deviceData, encoding: .utf8),
              let data = try? Coders.encoder.encode(StorageItemWrapper(item: device)) else {
            return
        }
        Keychain.save(key: key, value: data)
    }

    private func migrateCurrentVersion() {
        let key = "com.grabgrab.keychain.version"
        guard let versionData = Keychain.load(key: key),
              let version = String(data: versionData, encoding: .utf8).flatMap({ Int($0) }),
              let data = try? Coders.encoder.encode(StorageItemWrapper(item: version)) else { return }
        Keychain.save(key: key, value: data)
    }
}
