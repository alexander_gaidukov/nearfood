//
//  MenuComboCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuComboCellModel {
    let name: String
    let price: String
    let originalPrice: NSAttributedString
    let etr: String?
    let photos: [Photo]
    let priceTapped: () -> ()
    let cellTapped: ([UIImage?]) -> ()

    init(combo: Combo, priceTapped: @escaping () -> (), cellTapped: @escaping ([UIImage?]) -> ()) {
        self.priceTapped = priceTapped
        self.cellTapped = cellTapped
        name = combo.name
        price = combo.price.string(with: Formatters.priceFormatter) ?? ""
        originalPrice = NSAttributedString(
            string: combo.originalPrice.string(with: Formatters.priceFormatter) ?? "",
            attributes: [
                .strikethroughStyle: NSUnderlineStyle.single.rawValue,
                .strikethroughColor: UIColor.labelText
            ]
        )
        etr = combo.etr?.etrString
        photos = combo.photos
    }
}

final class MenuComboCell: UICollectionViewCell {
    @IBOutlet private weak var photoContainerView: UIView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var priceButton: UIButton!
    @IBOutlet private weak var etrLabel: UILabel!
    @IBOutlet private weak var originalPriceLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!

    private var photosView: ComboPhotosView!

    private var isAnimating: Bool = false {
        didSet {
            priceButton.isUserInteractionEnabled = !isAnimating
        }
    }

    private var model: MenuComboCellModel! {
        didSet {
            configureUI()
        }
    }

    static func height(for model: MenuComboCellModel) -> CGFloat {

        let imagesHeight = min(UIScreen.main.bounds.width - 32, 343)

        let priceWidth = (model.price as NSString).boundingRect(
            with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: 40),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.systemFont(ofSize: 18)],
            context: nil
        ).width + 54

        let dishNameWidth = UIScreen.main.bounds.width - 80 - priceWidth.rounded()
        let dishNameHeight = (model.name as NSString).boundingRect(
            with: CGSize(width: dishNameWidth, height: .greatestFiniteMagnitude),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .callout)],
            context: nil
        ).height
        let dishViewHeight = max(dishNameHeight, 48 + UIFont.preferredFont(forTextStyle: .footnote).pointSize)

        let featuresWidth = UIScreen.main.bounds.width - 64 - priceWidth.rounded()
        var featuresHeight: CGFloat = 0
        if let featuresString = model.etr, !featuresString.isEmpty {
            featuresHeight = (featuresString as NSString).boundingRect(
                with: CGSize(width: featuresWidth, height: .greatestFiniteMagnitude),
                options: [.usesLineFragmentOrigin],
                attributes: [.font: UIFont.preferredFont(forTextStyle: .footnote)],
                context: nil
            ).height
        }

        let height = 34 + imagesHeight + dishViewHeight + featuresHeight
        return height.rounded()
    }

    override func prepareForReuse() {
        photosView.prepareForReuse()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cellTapped)))

        photosView = ComboPhotosView.fromNib()
        photoContainerView.fill(with: photosView)
    }

    func configure(model: MenuComboCellModel) {
        self.model = model
    }

    private func configureUI() {
        nameLabel.text = model.name
        etrLabel.text = model.etr
        originalPriceLabel.attributedText = model.originalPrice
        photosView.configure(with: model.photos)

        if !isAnimating {
            priceButton.setTitle(model.price, for: .normal)
        }
    }

    @objc private func cellTapped() {
        model.cellTapped(photosView.images)
    }

    @IBAction private func priceButtonTapped() {
        isAnimating = true
        let widthConstraint = priceButton.widthAnchor.constraint(equalToConstant: priceButton.frame.width)
        widthConstraint.isActive = true
        let heightConstraint = priceButton.heightAnchor.constraint(equalToConstant: priceButton.frame.height)
        heightConstraint.isActive = true
        UIView.transition(with: priceButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.priceButton.contentHorizontalAlignment = .center
            self.priceButton.setTitle(nil, for: .normal)
            self.priceButton.setImage(#imageLiteral(resourceName: "checkmark"), for: .normal)
        }, completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.transition(with: self.priceButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.priceButton.contentHorizontalAlignment = .left
                    self.priceButton.setTitle(self.model.price, for: .normal)
                    self.priceButton.setImage(#imageLiteral(resourceName: "bucket"), for: .normal)
                }, completion: { _ in
                    self.priceButton.removeConstraint(widthConstraint)
                    self.priceButton.removeConstraint(heightConstraint)
                    self.isAnimating = false
                })
            }
        })
        self.model.priceTapped()
    }
}
