//
//  MenuInvitesCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuInvitesCellModel {
    let description: String
    let close: () -> ()
    let open: () -> ()

    init(count: Int, close: @escaping () -> (), open: @escaping () -> ()) {
        self.close = close
        self.open = open
        self.description = LocalizedStrings.Invitations.MenuCell.description(count: count)
    }
}

class MenuInvitesCell: UICollectionViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var openButton: UIButton!

    static func height(for model: MenuInvitesCellModel) -> CGFloat {
        let availableWidth = UIScreen.main.bounds.width - 200.0
        let descriptionHeight = (model.description as NSString).boundingRect(
            with: CGSize(width: availableWidth, height: .greatestFiniteMagnitude),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .footnote)],
            context: nil
        ).height
        return (descriptionHeight + 182.0).rounded()
    }

    private var model: MenuInvitesCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Invitations.MenuCell.title
        openButton.setTitle(LocalizedStrings.Invitations.MenuCell.button, for: .normal)
    }

    func configure(with model: MenuInvitesCellModel) {
        self.model = model
    }

    private func updateUI() {
        descriptionLabel.text = model.description
    }

    @IBAction private func openButtonTapped() {
        model.open()
    }

    @IBAction private func closeButtonTapped() {
        model.close()
    }

}
