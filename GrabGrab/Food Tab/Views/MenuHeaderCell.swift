//
//  MenuHeaderView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 19.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuHeaderCellModel {
    let title: String
    let hasDescription: Bool
    let didTap: () -> ()
}

extension MenuHeaderCellModel {
    init(menu: Menu, didTap: @escaping () -> ()) {
        title = menu.name
        hasDescription = menu.description?.isEmpty == false
        self.didTap = didTap
    }
}

class MenuHeaderCell: UICollectionViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var infoIconView: UIImageView!
    @IBOutlet private weak var hideInfoConstraint: NSLayoutConstraint!

    private var model: MenuHeaderCellModel! {
        didSet {
            updateUI()
        }
    }

    static func height(for model: MenuHeaderCellModel) -> CGFloat {
        let availableWidth = UIScreen.main.bounds.width - 32 - (model.hasDescription ? 32 : 0)
        let height = (model.title as NSString).boundingRect(
            with: CGSize(width: availableWidth, height: .greatestFiniteMagnitude),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .title1)],
            context: nil
        ).height
        return height + 48
    }

    func configure(model: MenuHeaderCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.title
        infoIconView.isHidden = !model.hasDescription
        hideInfoConstraint.priority = model.hasDescription ? .defaultLow : .defaultHigh
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cellTapped)))
    }

    @objc private func cellTapped() {
        model.didTap()
    }
}
