//
//  MenuSelectorItemCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class MenuSelectorItemCell: UICollectionViewCell {
    @IBOutlet private weak var innerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!

    static func width(for title: String) -> CGFloat {
        let result = (title as NSString).boundingRect(
            with: CGSize(width: .greatestFiniteMagnitude, height: UIFont.preferredFont(forTextStyle: .callout).lineHeight),
            options: [],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .callout)],
            context: nil
        ).width + 32
        return result.rounded()
    }

    func configure(title: String, isSelected: Bool) {
        innerView.backgroundColor = isSelected ? .primary : .priceBackground
        titleLabel.textColor = isSelected ? .white : .mainText
        titleLabel.text = title
    }

}
