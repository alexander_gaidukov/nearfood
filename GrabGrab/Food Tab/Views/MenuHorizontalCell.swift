//
//  MenuHorizontalCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuHorizontalCellModel {
    let cellModels: [FoodCellModel]
    let heights: [CGFloat]
    let selectedIndex: Int
    var itemsCount: Int { cellModels.count }
    let didScroll: (Int) -> ()
}

extension MenuHorizontalCellModel {
    init(cellModels: [FoodCellModel], selectedIndex: Int, didScroll: @escaping (Int) -> ()) {
        self.cellModels = cellModels
        self.selectedIndex = selectedIndex
        self.didScroll = didScroll
        heights = cellModels.map {
            switch $0 {
            case .regular(let itemModel):
                return MenuItemCell.height(for: itemModel)
            case .large(let itemModel):
                return LargeMenuItemCell.height(for: itemModel)
            case .combo(let comboModel):
                return MenuComboCell.height(for: comboModel)
            }
        }
    }
}

class MenuHorizontalCell: UICollectionViewCell {

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var sliderView: SliderView!

    static func height(for model: MenuHorizontalCellModel) -> CGFloat {
        32 + (model.heights.max() ?? 0)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(cellType: MenuItemCell.self)
        collectionView.register(cellType: LargeMenuItemCell.self)
        collectionView.register(cellType: MenuComboCell.self)
        sliderView.didSelect = { [weak self] in
            self?.showItem(at: $0)
        }
    }

    private var model: MenuHorizontalCellModel! {
        didSet {
            updateUI()
        }
    }

    func configure(model: MenuHorizontalCellModel) {
        self.model = model
    }

    private func showItem(at index: Int) {
        sliderView.selectedIndex = index
        self.collectionView.setContentOffset(CGPoint(x: UIScreen.main.bounds.width * CGFloat(index), y: 0), animated: true)
        model.didScroll(index)
    }

    private func didScroll() {
        let index = Int((collectionView.contentOffset.x / collectionView.bounds.width).rounded())
        model.didScroll(index.constrained(maximum: model.itemsCount - 1, minimum: 0))
    }

    private func updateUI() {
        collectionViewHeightConstraint.constant = model.heights.max() ?? 0
        collectionView.reloadData()
        sliderView.itemsCount = model.itemsCount
        sliderView.selectedIndex = model.selectedIndex
        DispatchQueue.main.async {
            self.collectionView.contentOffset = CGPoint(x: UIScreen.main.bounds.width * CGFloat(self.model.selectedIndex), y: 0)
        }
    }
}

extension MenuHorizontalCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        model.cellModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellModel = model.cellModels[indexPath.item]
        switch cellModel {
        case .large(let itemModel):
            let cell: LargeMenuItemCell = collectionView.dequeue(forIndexPath: indexPath)
            cell.configure(model: itemModel)
            return cell
        case .regular(let itemModel):
            let cell: MenuItemCell = collectionView.dequeue(forIndexPath: indexPath)
            cell.configure(model: itemModel)
            return cell
        case .combo(let comboModel):
            let cell: MenuComboCell = collectionView.dequeue(forIndexPath: indexPath)
            cell.configure(model: comboModel)
            return cell
        }
    }
}

extension MenuHorizontalCell: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: model.heights.max() ?? 0)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int((scrollView.contentOffset.x / scrollView.bounds.width).rounded())
        sliderView.selectedIndex = index.constrained(maximum: model.itemsCount - 1, minimum: 0)
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate { didScroll() }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        didScroll()
    }
}
