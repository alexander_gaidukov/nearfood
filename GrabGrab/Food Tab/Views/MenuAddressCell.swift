//
//  MenuAddressViewCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuAddressCellModel {
    var etaText: String?
    var titleText: String
    var subtitleText: String?
    var didSelect: () -> ()

    init(eta: Int?, location: Location, didSelect: @escaping () -> ()) {
        etaText = eta.map { LocalizedStrings.Cart.title(for: $0) }
        titleText = location.label
        self.didSelect = didSelect
        if location.title == nil {
            subtitleText = [
                location.additionalAddressInfo,
                location.note
            ]
            .compactMap { $0 }
            .joined(separator: ", ")
        } else {
            subtitleText = nil
        }
    }
}

final class MenuAddressCell: UICollectionViewCell {
    @IBOutlet private weak var etaLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!

    static func height(for model: MenuAddressCellModel) -> CGFloat {

        let titleWidth = UIScreen.main.bounds.width - 80

        let titleHeight = (model.titleText as NSString).boundingRect(
            with: CGSize(width: titleWidth, height: CGFloat.greatestFiniteMagnitude),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .title2)],
            context: nil
        ).height

        let etaHeight = UIFont.preferredFont(forTextStyle: .callout).lineHeight

        var result = titleHeight + etaHeight + 20

        if let subtitle = model.subtitleText, !subtitle.isEmpty {
            let subtitleWidth = UIScreen.main.bounds.width - 32
            let subtitleHeight = (subtitle as NSString).boundingRect(
                with: CGSize(width: subtitleWidth, height: CGFloat.greatestFiniteMagnitude),
                options: [.usesLineFragmentOrigin],
                attributes: [.font: UIFont.preferredFont(forTextStyle: .footnote)],
                context: nil
            ).height
            result += subtitleHeight + 8
        }

        return result.rounded()
    }

    private var model: MenuAddressCellModel! {
        didSet {
            updateUI()
        }
    }

    func configure(with model: MenuAddressCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.titleText
        subtitleLabel.text = model.subtitleText
        etaLabel.text = model.etaText
    }

    @IBAction private func cellDidTap() {
        model.didSelect()
    }
}
