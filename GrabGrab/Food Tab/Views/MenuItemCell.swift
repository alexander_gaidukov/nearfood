//
//  MenuItemCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuItemCellModel {

    let name: String
    let price: String
    let etr: String?
    let background: Background?
    let innerViewBackground: UIColor?
    let photo: Photo?
    let isFeaturesViewHidden: Bool
    let isSeparatorHidden: Bool
    let layout: Menu.Layout
    let features: String?
    let priceTapped: () -> ()
    let cellTapped: (UIImage?) -> ()

    var topOffset: CGFloat {
        layout == .horizontal ? 6 : 0
    }

    init(
        menuItem: MenuItem,
        layout: Menu.Layout,
        hideSeparator: Bool,
        priceTapped: @escaping () -> (),
        cellTapped: @escaping (UIImage?) -> ()
    ) {
        self.priceTapped = priceTapped
        self.cellTapped = cellTapped
        background = menuItem.properties?.base
        name = menuItem.dish.name
        price = menuItem.dish.price.string(with: Formatters.priceFormatter) ?? ""
        etr = menuItem.dish.etr?.etrString
        photo = menuItem.dish.mainPhoto
        isFeaturesViewHidden = menuItem.dish.features?.isEmpty != false
        features = menuItem.dish.features?.map(\.emoji).joined(separator: " ")
        self.layout = layout
        innerViewBackground = (layout == .vertical && menuItem.properties?.base == nil) ? .clear : .panelBackground
        if layout == .horizontal || menuItem.properties?.base != nil {
            isSeparatorHidden = true
        } else {
            isSeparatorHidden = hideSeparator
        }
    }
}

final class MenuItemCell: UICollectionViewCell {

    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var priceButton: UIButton!
    @IBOutlet private weak var etrLabel: UILabel!
    @IBOutlet private weak var featuresView: UIView!
    @IBOutlet private weak var featuresLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var innerView: UIView!
    @IBOutlet private weak var hideBackgroundConstraint: NSLayoutConstraint!
    @IBOutlet private weak var backgroundColorView: UIView!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var separatorView: UIView!
    @IBOutlet private weak var bgImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var topConstraint: NSLayoutConstraint!

    private var imageLoadingTask: DownloadTask?
    private var bgImageLoadingTask: DownloadTask?

    static func height(for model: MenuItemCellModel) -> CGFloat {
        let availableWidth = UIScreen.main.bounds.width - 176
        let dishNameHeight = (model.name as NSString).boundingRect(
            with: CGSize(width: availableWidth, height: .greatestFiniteMagnitude),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .callout)],
            context: nil
        ).height
        var height = max(100 + dishNameHeight, 168)
        if model.background != nil {
            height += 48
        } else if model.layout == .vertical {
            height -= 12
        }
        return height.rounded()
    }

    private var isAnimating: Bool = false {
        didSet {
            priceButton.isUserInteractionEnabled = !isAnimating
        }
    }

    private var model: MenuItemCellModel! {
        didSet {
            configureUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        configureViews()
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cellTapped)))
    }

    override func prepareForReuse() {
        imageLoadingTask?.cancel()
        imageLoadingTask = nil
        photoImageView.image = nil

        bgImageLoadingTask?.cancel()
        bgImageLoadingTask = nil
        backgroundImageView.image = nil
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                configureViews()
                configureBackground()
            }
        }
    }

    func configure(model: MenuItemCellModel) {
        self.model = model
    }

    private func configureViews() {
        featuresView.layer.shadowColor = UIColor.shadow.cgColor
        featuresView.layer.shadowOpacity = 0.32
        featuresView.layer.shadowOffset = CGSize(width: 6, height: 6)
    }

    private func configureUI() {
        innerView.backgroundColor = model.innerViewBackground
        separatorView.isHidden = model.isSeparatorHidden
        nameLabel.text = model.name
        etrLabel.text = model.etr
        imageLoadingTask = photoImageView.load(photo: model.photo)
        featuresView.isHidden = model.isFeaturesViewHidden
        featuresLabel.text = model.features
        hideBackgroundConstraint.priority = model.background == nil ? .defaultHigh : .defaultLow
        topConstraint.constant = model.topOffset
        configureBackground()

        if !isAnimating {
            priceButton.setTitle(model.price, for: .normal)
        }
    }

    private func configureBackground() {
        guard let background = model.background else {
            backgroundColorView.isHidden = true
            return
        }

        backgroundColorView.isHidden = false

        let isDarkMode: Bool
        if #available(iOS 13.0, *) {
            isDarkMode = traitCollection.userInterfaceStyle == .dark
        } else {
            isDarkMode = false
        }

        let url: String?

        if isDarkMode {
            backgroundColorView.backgroundColor = background.color.dark.uiColor
            url = background.image?.dark
        } else {
            backgroundColorView.backgroundColor = background.color.light.uiColor
            url = background.image?.light
        }

        bgImageLoadingTask = backgroundImageView.load(
            url: url,
            properties: ImageDownloadingProperties(resizing: .none)
        ) { [weak self] image in
            guard let image = image else { return }
            self?.bgImageWidthConstraint.constant = 48.0 * image.size.width / image.size.height
        }
    }

    @objc private func cellTapped() {
        model.cellTapped(photoImageView.image)
    }

    @IBAction private func priceButtonTapped() {
        isAnimating = true
        let widthConstraint = priceButton.widthAnchor.constraint(equalToConstant: priceButton.frame.width)
        widthConstraint.isActive = true
        let heightConstraint = priceButton.heightAnchor.constraint(equalToConstant: priceButton.frame.height)
        heightConstraint.isActive = true
        UIView.transition(with: priceButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.priceButton.contentHorizontalAlignment = .center
            self.priceButton.setTitle(nil, for: .normal)
            self.priceButton.setImage(#imageLiteral(resourceName: "checkmark"), for: .normal)
        }, completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.transition(with: self.priceButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.priceButton.contentHorizontalAlignment = .left
                    self.priceButton.setTitle(self.model.price, for: .normal)
                    self.priceButton.setImage(#imageLiteral(resourceName: "bucket"), for: .normal)
                }, completion: { _ in
                    self.priceButton.removeConstraint(widthConstraint)
                    self.priceButton.removeConstraint(heightConstraint)
                    self.isAnimating = false
                })
            }
        })
        model.priceTapped()
    }
}
