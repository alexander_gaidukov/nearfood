//
//  MenuItemsSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum FoodCellModel {
    case regular(MenuItemCellModel)
    case large(LargeMenuItemCellModel)
    case combo(MenuComboCellModel)
}

struct MenuItemsSectionModel: CollectionViewSectionModel {

    private enum Layout {
        case horizontal(MenuHorizontalCellModel)
        case vertical([FoodCellModel])

        var rowsCount: Int {
            switch self {
            case .horizontal:
                return 1
            case .vertical(let models):
                return models.count
            }
        }

        func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
            switch self {
            case .horizontal(let model):
                let cell: MenuHorizontalCell = collectionView.dequeue(forIndexPath: indexPath)
                cell.configure(model: model)
                return cell
            case .vertical(let models):
                let model = models.get(at: indexPath.item)
                switch model {
                case .regular(let itemModel):
                    let cell: MenuItemCell = collectionView.dequeue(forIndexPath: indexPath)
                    cell.configure(model: itemModel)
                    return cell
                case .large(let itemModel):
                    let cell: LargeMenuItemCell = collectionView.dequeue(forIndexPath: indexPath)
                    cell.configure(model: itemModel)
                    return cell
                case .combo(let model):
                    let cell: MenuComboCell = collectionView.dequeue(forIndexPath: indexPath)
                    cell.configure(model: model)
                    return cell
                case .none:
                    let cell: MenuItemCell = collectionView.dequeue(forIndexPath: indexPath)
                    return cell
                }
            }
        }

        func size(collectionView: UICollectionView, indexPath: IndexPath) -> CGSize {
            switch self {
            case .horizontal(let model):
                return CGSize(
                    width: collectionView.bounds.width,
                    height: MenuHorizontalCell.height(for: model)
                )
            case .vertical(let models):
                guard let model = models.get(at: indexPath.item) else { return .zero }
                switch model {
                case .regular(let itemModel):
                    return CGSize(width: collectionView.bounds.width, height: MenuItemCell.height(for: itemModel))
                case .large(let itemModel):
                    return CGSize(width: collectionView.bounds.width, height: LargeMenuItemCell.height(for: itemModel))
                case .combo(let comboModel):
                    return CGSize(width: collectionView.bounds.width, height: MenuComboCell.height(for: comboModel))
                }
            }
        }
    }

    private let combosLayout: Layout
    private let itemsLayout: Layout

    private let headerModel: MenuHeaderCellModel

    var isEmpty: Bool {
        combosLayout.rowsCount == 0 && itemsLayout.rowsCount == 0
    }

    // swiftlint:disable:next function_body_length
    init(
        menu: Menu?,
        combos: [Combo],
        selectedIndex: Int,
        priceTaped: @escaping (CartItem) -> (),
        itemTapped: @escaping (CartItem, [UIImage?]) -> (),
        headerTapped: @escaping () -> (),
        didScroll: @escaping (Int) -> ()
    ) {
        if let menu = menu {
            headerModel = MenuHeaderCellModel(menu: menu, didTap: headerTapped)

            let models: [FoodCellModel] = menu.items.enumerated().map { item in

                let hideSeparator: Bool
                if item.offset == menu.items.count - 1 {
                    hideSeparator = true
                } else if case .large = menu.items[item.offset + 1].layout {
                    hideSeparator = true
                } else {
                    hideSeparator = false
                }

                switch item.element.layout {
                case .regular:
                    return .regular(
                        MenuItemCellModel(
                            menuItem: item.element,
                            layout: menu.layout,
                            hideSeparator: hideSeparator,
                            priceTapped: { priceTaped(.dish(item.element.dish)) },
                            cellTapped: { itemTapped(.dish(item.element.dish), [$0]) }
                        )
                    )
                case .large:
                    return .large(
                        LargeMenuItemCellModel(
                            menuItem: item.element,
                            layout: menu.layout,
                            priceTapped: { priceTaped(.dish(item.element.dish)) },
                            cellTapped: { itemTapped(.dish(item.element.dish), [$0]) }
                        )
                    )
                }
            }

            if menu.layout == .horizontal, menu.items.count > 1 {
                let horizontalModel = MenuHorizontalCellModel(cellModels: models, selectedIndex: selectedIndex, didScroll: didScroll)
                self.itemsLayout = .horizontal(horizontalModel)
            } else {
                self.itemsLayout = .vertical(models)
            }
        } else {
            headerModel = MenuHeaderCellModel(
                title: LocalizedStrings.Menu.Combos.title,
                hasDescription: false,
                didTap: headerTapped
            )
            self.itemsLayout = .vertical([])
        }

        if !combos.isEmpty {
            let models: [FoodCellModel] = combos.map { combo in
                let model = MenuComboCellModel(
                    combo: combo,
                    priceTapped: { priceTaped(.combo(combo)) },
                    cellTapped: { itemTapped(.combo(combo), $0) }
                )
                return .combo(model)
            }

            if models.count > 1 {
                let horizontalModel = MenuHorizontalCellModel(
                    cellModels: models,
                    selectedIndex: selectedIndex,
                    didScroll: didScroll
                )
                self.combosLayout = .horizontal(horizontalModel)
            } else {
                self.combosLayout = .vertical(models)
            }
        } else {
            self.combosLayout = .vertical([])
        }
    }

    var numberOfItems: Int {
        var count = combosLayout.rowsCount + itemsLayout.rowsCount
        if count > 0 { count += 1 } // Add header row
        return count
    }

    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {

        guard indexPath.item > 0 else {
            let cell: MenuHeaderCell = collectionView.dequeue(forIndexPath: indexPath)
            cell.configure(model: headerModel)
            return cell
        }

        var index = indexPath.item - 1

        if index < combosLayout.rowsCount {
            let newIndexPath = IndexPath(item: index, section: indexPath.section)
            return combosLayout.cell(collectionView: collectionView, indexPath: newIndexPath)
        }

        index -= combosLayout.rowsCount

        let newIndexPath = IndexPath(item: index, section: indexPath.section)
        return itemsLayout.cell(collectionView: collectionView, indexPath: newIndexPath)
    }

    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        guard indexPath.item > 0 else {
            return CGSize(width: collectionView.bounds.width, height: MenuHeaderCell.height(for: headerModel))
        }

        var index = indexPath.item - 1

        if index < combosLayout.rowsCount {
            let newIndexPath = IndexPath(item: index, section: indexPath.section)
            return combosLayout.size(collectionView: collectionView, indexPath: newIndexPath)
        }

        index -= combosLayout.rowsCount

        let newIndexPath = IndexPath(item: index, section: indexPath.section)
        return itemsLayout.size(collectionView: collectionView, indexPath: newIndexPath)
    }

}
