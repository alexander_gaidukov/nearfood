//
//  MenuSurgeSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuSurgeSectionModel: CollectionViewSectionModel {

    private let model: MenuSurgeCellModel?

    init(surge: Surge?, didSelect: @escaping () -> ()) {
        model = surge.map { MenuSurgeCellModel(surge: $0, didSelect: didSelect) }
    }

    var numberOfItems: Int {
        model != nil ? 1 : 0
    }

    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuSurgeCell = collectionView.dequeue(forIndexPath: indexPath)
        if let model = model {
            cell.configure(with: model)
        }
        return cell
    }

    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: 60.0)
    }
}
