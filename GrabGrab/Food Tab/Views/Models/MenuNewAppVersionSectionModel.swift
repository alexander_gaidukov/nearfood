//
//  MenuNewAppVersionSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//
import UIKit

struct MenuNewAppVersionSectionModel: CollectionViewSectionModel {

    private let model: NewAppVersionCellModel
    private let isEmpty: Bool

    init (hasNewVersion: Bool, didSelect: @escaping () -> ()) {
        model = NewAppVersionCellModel(didSelect: didSelect)
        isEmpty = !hasNewVersion
    }

    var numberOfItems: Int {
        isEmpty ? 0 : 1
    }

    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: NewAppVersionCell = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }

    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: NewAppVersionCell.height(for: model))
    }
}
