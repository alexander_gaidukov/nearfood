//
//  MenuInvitationsSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuInvitationsSectionModel: CollectionViewSectionModel {

    private let model: MenuInvitesCellModel
    private let shouldShow: Bool

    init (count: Int, shouldShow: Bool, open: @escaping () -> (), close: @escaping () -> ()) {
        model = MenuInvitesCellModel(count: count, close: close, open: open)
        self.shouldShow = shouldShow && count > 0
    }

    var numberOfItems: Int {
        shouldShow ? 1 : 0
    }

    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuInvitesCell = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model)
        return cell
    }

    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: MenuInvitesCell.height(for: model))
    }
}
