//
//  MenuCurrentOrdersSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuCurrentOrdersSectionModel: CollectionViewSectionModel {

    private let model: CurrentOrdersCellModel
    private let isEmpty: Bool

    init (orders: [Order], didSelect: @escaping (Order) -> ()) {
        model = CurrentOrdersCellModel(orders: orders, didSelect: didSelect)
        isEmpty = orders.isEmpty
    }

    var numberOfItems: Int {
        isEmpty ? 0 : 1
    }

    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CurrentOrdersCell = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }

    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: CurrentOrdersCell.height(for: model))
    }
}
