//
//  MenuAddressSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuAddressSectionModel: CollectionViewSectionModel {

    private let model: MenuAddressCellModel?

    init (location: Location?, eta: Int?, didSelect: @escaping () -> ()) {
        model = location.map { MenuAddressCellModel(eta: eta, location: $0, didSelect: didSelect) }
    }

    var numberOfItems: Int {
        model == nil ? 0 : 1
    }

    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuAddressCell = collectionView.dequeue(forIndexPath: indexPath)
        if let model = model { cell.configure(with: model) }
        return cell
    }

    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        guard let model = model else { return .zero }
        return CGSize(width: collectionView.bounds.width, height: MenuAddressCell.height(for: model))
    }
}
