//
//  MenuSelectorSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuSelectorSectionModel: CollectionViewSectionModel {

    private let model: MenuSelectorCellModel

    init (titles: [String], didSelect: @escaping (Int) -> ()) {
        model = MenuSelectorCellModel(labels: titles, didSelect: didSelect)
    }

    var numberOfItems: Int {
        1
    }

    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuSelectorCell = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model)
        return cell
    }

    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        CGSize(width: collectionView.bounds.width, height: 64.0)
    }
}
