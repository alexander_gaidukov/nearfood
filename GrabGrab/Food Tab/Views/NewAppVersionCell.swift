//
//  NewAppVersionCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct NewAppVersionCellModel {
    var didSelect: () -> ()
}

class NewAppVersionCell: UICollectionViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var updateButton: UIButton!

    static func height(for model: NewAppVersionCellModel) -> CGFloat {
        let textWidth = UIScreen.main.bounds.width - 64

        let titleHeight = (LocalizedStrings.AppVersion.New.title as NSString).boundingRect(
            with: CGSize(width: textWidth, height: .greatestFiniteMagnitude),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .title2)],
            context: nil
        ).height

        let descriptionHeight = (LocalizedStrings.AppVersion.New.description as NSString).boundingRect(
            with: CGSize(width: textWidth, height: .greatestFiniteMagnitude),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .subheadline)],
            context: nil
        ).height

        return titleHeight + descriptionHeight + 132
    }

    private var model: NewAppVersionCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.AppVersion.New.title
        descriptionLabel.text = LocalizedStrings.AppVersion.New.description
        updateButton.setTitle(LocalizedStrings.Common.refresh, for: .normal)
    }

    func configure(model: NewAppVersionCellModel) {
        self.model = model
    }

    func updateUI() {

    }

    @IBAction private func updateTapped() {
        model.didSelect()
    }
}
