//
//  MenuSelectorCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuSelectorCellModel {
    var labels: [String]
    var didSelect: (Int) -> ()
}

final class MenuSelectorCell: UICollectionViewCell {

    @IBOutlet private weak var collectionView: UICollectionView!

    fileprivate var model: MenuSelectorCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        var insets = collectionView.contentInset
        insets.left = 16
        insets.right = 16
        collectionView.contentInset = insets
        collectionView.register(cellType: MenuSelectorItemCell.self)
    }

    func configure(with model: MenuSelectorCellModel) {
        self.model = model
    }

    private func updateUI() {
        collectionView.reloadData()
    }
}

extension MenuSelectorCell: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        model.labels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuSelectorItemCell  = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(title: model.labels[indexPath.item], isSelected: indexPath.item == 0)
        return cell
    }
}

extension MenuSelectorCell: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        CGSize(width: MenuSelectorItemCell.width(for: model.labels[indexPath.item]), height: collectionView.bounds.height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        model.didSelect(indexPath.item)
    }
}
