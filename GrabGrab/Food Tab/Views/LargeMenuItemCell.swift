//
//  LargeMenuItemCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct LargeMenuItemCellModel {
    let name: String
    let price: String
    let etr: String?
    let layout: Menu.Layout
    let background: Background?
    let photo: Photo?
    let features: String?
    let priceTapped: () -> ()
    let cellTapped: (UIImage?) -> ()

    var bottomOffset: CGFloat {
        layout == .horizontal ? 6 : 32
    }

    init(menuItem: MenuItem, layout: Menu.Layout, priceTapped: @escaping () -> (), cellTapped: @escaping (UIImage?) -> ()) {
        self.priceTapped = priceTapped
        self.cellTapped = cellTapped
        background = menuItem.properties?.base
        name = menuItem.dish.name
        price = menuItem.dish.price.string(with: Formatters.priceFormatter) ?? ""
        etr = menuItem.dish.etr?.etrString
        photo = menuItem.dish.mainPhoto
        self.layout = layout
        features = menuItem.dish.features?.map { "\($0.emoji)\($0.name)" }.joined(separator: ", ")
    }
}

final class LargeMenuItemCell: UICollectionViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var priceButton: UIButton!
    @IBOutlet private weak var etrLabel: UILabel!
    @IBOutlet private weak var featuresLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var hideBackgroundConstraint: NSLayoutConstraint!
    @IBOutlet private weak var backgroundColorView: UIView!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var bgImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var bottomOffsetConstraint: NSLayoutConstraint!

    private var imageLoadingTask: DownloadTask?
    private var bgImageLoadingTask: DownloadTask?

    private var isAnimating: Bool = false {
        didSet {
            priceButton.isUserInteractionEnabled = !isAnimating
        }
    }

    private var model: LargeMenuItemCellModel! {
        didSet {
            configureUI()
        }
    }

    static func height(for model: LargeMenuItemCellModel) -> CGFloat {

        let priceWidth = (model.price as NSString).boundingRect(
            with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: 40),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.systemFont(ofSize: 18)],
            context: nil
        ).width + 54

        let dishNameWidth = UIScreen.main.bounds.width - 80 - priceWidth.rounded()
        let dishNameHeight = (model.name as NSString).boundingRect(
            with: CGSize(width: dishNameWidth, height: .greatestFiniteMagnitude),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.preferredFont(forTextStyle: .callout)],
            context: nil
        ).height
        let dishViewHeight = max(dishNameHeight, 40)

        let featuresWidth = UIScreen.main.bounds.width - 64 - priceWidth.rounded()
        var featuresHeight: CGFloat = 0
        if let featuresString = model.features ?? model.etr, !featuresString.isEmpty {
            featuresHeight = (featuresString as NSString).boundingRect(
                with: CGSize(width: featuresWidth, height: .greatestFiniteMagnitude),
                options: [.usesLineFragmentOrigin],
                attributes: [.font: UIFont.preferredFont(forTextStyle: .footnote)],
                context: nil
            ).height
        }

        var height = 345 + dishViewHeight + featuresHeight
        if model.background != nil { height += 16 }
        if model.layout == .horizontal { height -= 26 }
        return height.rounded()
    }

    override func prepareForReuse() {
        imageLoadingTask?.cancel()
        imageLoadingTask = nil
        photoImageView.image = nil

        bgImageLoadingTask?.cancel()
        bgImageLoadingTask = nil
        backgroundImageView.image = nil
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cellTapped)))
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                configureBackground()
            }
        }
    }

    func configure(model: LargeMenuItemCellModel) {
        self.model = model
    }

    private func configureUI() {
        nameLabel.text = model.name
        etrLabel.text = model.etr
        imageLoadingTask = photoImageView.load(photo: model.photo)
        featuresLabel.text = model.features
        bottomOffsetConstraint.constant = model.bottomOffset
        hideBackgroundConstraint.priority = model.background == nil ? .defaultHigh : .defaultLow
        configureBackground()

        if !isAnimating {
            priceButton.setTitle(model.price, for: .normal)
        }
    }

    private func configureBackground() {
        guard let background = model.background else {
            backgroundColorView.isHidden = true
            backgroundImageView.isHidden = true
            return
        }

        backgroundColorView.isHidden = false
        backgroundImageView.isHidden = false

        let isDarkMode: Bool
        if #available(iOS 13.0, *) {
            isDarkMode = traitCollection.userInterfaceStyle == .dark
        } else {
            isDarkMode = false
        }

        let url: String?

        if isDarkMode {
            backgroundColorView.backgroundColor = background.color.dark.uiColor
            url = background.image?.dark
        } else {
            backgroundColorView.backgroundColor = background.color.light.uiColor
            url = background.image?.light
        }

        bgImageLoadingTask = backgroundImageView.load(
            url: url,
            properties: ImageDownloadingProperties(resizing: .none)
        ) {[weak self] image in
            guard let image = image else { return }
            self?.bgImageWidthConstraint.constant = 48.0 * image.size.width / image.size.height
        }
    }

    @objc private func cellTapped() {
        model.cellTapped(photoImageView.image)
    }

    @IBAction private func priceButtonTapped() {
        isAnimating = true
        let widthConstraint = priceButton.widthAnchor.constraint(equalToConstant: priceButton.frame.width)
        widthConstraint.isActive = true
        let heightConstraint = priceButton.heightAnchor.constraint(equalToConstant: priceButton.frame.height)
        heightConstraint.isActive = true
        UIView.transition(with: priceButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.priceButton.contentHorizontalAlignment = .center
            self.priceButton.setTitle(nil, for: .normal)
            self.priceButton.setImage(#imageLiteral(resourceName: "checkmark"), for: .normal)
        }, completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.transition(with: self.priceButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.priceButton.contentHorizontalAlignment = .left
                    self.priceButton.setTitle(self.model.price, for: .normal)
                    self.priceButton.setImage(#imageLiteral(resourceName: "bucket"), for: .normal)
                }, completion: { _ in
                    self.priceButton.removeConstraint(widthConstraint)
                    self.priceButton.removeConstraint(heightConstraint)
                    self.isAnimating = false
                })
            }
        })
        self.model.priceTapped()
    }
}
