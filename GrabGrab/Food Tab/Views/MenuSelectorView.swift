//
//  MenuSelectorView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class MenuSelectorView: UIView {

    @IBOutlet private weak var collectionView: UICollectionView!

    var selectedIndex: Int = 0 {
        didSet {
            updateUI()
        }
    }
    private var labels: [String] = []
    private var didSelect: (Int) -> () = { _ in }

    override func awakeFromNib() {
        super.awakeFromNib()
        var insets = collectionView.contentInset
        insets.left = 16
        insets.right = 16
        collectionView.contentInset = insets
        collectionView.register(cellType: MenuSelectorItemCell.self)
    }

    func configure(with labels: [String], didSelect: @escaping (Int) -> ()) {
        self.labels = labels
        self.didSelect = didSelect
        updateUI()
    }

    private func updateUI() {
        collectionView.reloadData()
        guard selectedIndex < labels.count else { return }
        self.collectionView.scrollToItem(at: IndexPath(item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
}

extension MenuSelectorView: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        labels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuSelectorItemCell  = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(title: labels[indexPath.item], isSelected: indexPath.item == selectedIndex)
        return cell
    }
}

extension MenuSelectorView: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        CGSize(width: MenuSelectorItemCell.width(for: labels[indexPath.item]), height: collectionView.bounds.height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelect(indexPath.item)
    }
}
