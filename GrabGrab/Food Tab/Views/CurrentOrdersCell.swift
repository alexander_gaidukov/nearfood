//
//  CurrentOrdersCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CurrentOrdersCellModel {
    var orders: [Order]
    var isPagerHidden: Bool { orders.count < 2 }
    var didSelect: (Order) -> ()
}

class CurrentOrdersCell: UICollectionViewCell {
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var pageControl: UIPageControl!

    lazy var refresher: Refresher = {
        Refresher {[weak self] in self?.collectionView?.reloadData() }
    }()

    static func height(for model: CurrentOrdersCellModel) -> CGFloat {
        124 + (model.isPagerHidden ? 0 : 28)
    }

    private var model: CurrentOrdersCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(cellType: OrderCell.self)
    }

    func configure(model: CurrentOrdersCellModel) {
        self.model = model
    }

    func updateUI() {
        pageControl.numberOfPages = model.orders.count
        pageControl.isHidden = model.isPagerHidden
        refresher.refresh()
    }

    @IBAction private func pageControlDidChange() {
        let offet = collectionView.frame.width * CGFloat(pageControl.currentPage)
        collectionView.setContentOffset(CGPoint(x: offet, y: 0), animated: true)
    }
}

extension CurrentOrdersCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        model.orders.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: OrderCell = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(with: OrderCellModel(order: model.orders[indexPath.item]))
        return cell
    }
}

extension CurrentOrdersCell: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        collectionView.frame.size
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        model.didSelect(model.orders[indexPath.item])
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int((scrollView.contentOffset.x / scrollView.frame.width).rounded())
            .constrained(maximum: pageControl.numberOfPages - 1, minimum: 0)
    }
}
