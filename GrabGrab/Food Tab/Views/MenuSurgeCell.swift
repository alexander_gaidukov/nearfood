//
//  MenuSurgeCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MenuSurgeCellModel {
    var surge: Surge
    var didSelect: () -> ()

    var title: String {
        surge.price > 0 ? LocalizedStrings.Surge.title : LocalizedStrings.Surge.increasedTimeTitle
    }
}

final class MenuSurgeCell: UICollectionViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var innerView: UIView!

    private var model: MenuSurgeCellModel! {
        didSet {
            updateUI()
        }
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                innerView.borderColor = .ratingBorder
            }
        }
    }

    func configure(with model: MenuSurgeCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.title
    }

    @IBAction private func cellTapped() {
        model.didSelect()
    }

}
