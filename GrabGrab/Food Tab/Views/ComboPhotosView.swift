//
//  ComboPhotosView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class ComboPhotosView: UIView {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private var photoViews: [UIView]!
    @IBOutlet private weak var imageView: UIImageView!

    @IBOutlet private weak var leftContainerView: UIView!
    @IBOutlet private var stackViews: [UIStackView]!

    private var imageLoadingTasks: [DownloadTask] = []

    var grayscaled: Bool = false

    var overlapping: CGFloat = 73 {
        didSet {
            stackViews.forEach { $0.spacing = -overlapping }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.bringSubviewToFront(leftContainerView)
        photoViews.reversed().forEach {
            $0.superview?.bringSubviewToFront($0)
        }
    }

    var images: [UIImage?] {
        if !imageView.isHidden { return [imageView.image] }
        return photoViews.map { ($0.subviews.first as? UIImageView)?.image }
    }

    func prepareForReuse() {
        imageLoadingTasks.forEach { $0.cancel() }
        imageLoadingTasks.removeAll()
        imageView.image = nil
        photoViews.forEach {
            ($0.subviews.first as? UIImageView)?.image = nil
        }
    }

    func configure(with photoURLs: [String], placeholders: [UIImage?]? = nil) {
        configure(with: photoURLs.map { Photo(url: $0, digest: "") }, placeholders: placeholders)
    }

    func configure(with photos: [Photo], placeholders: [UIImage?]? = nil) {
        configure(with: photos.map { .remote($0) }, placeholders: placeholders)
    }

    func configure(with attachments: [ImageAttachment], placeholders: [UIImage?]? = nil) {
        guard attachments.count > 1 else {
            containerView.isHidden = true
            imageView.isHidden = false
            let placeholder = placeholders?.isEmpty == false ? placeholders?[0] : nil

            let task = imageView.load(attachment: attachments.first, placeholder: placeholder) { [weak self] image in
                guard let self = self, self.grayscaled else { return }
                self.imageView.image = image?.grayscaled
            }

            if let task = task {
                imageLoadingTasks.append(task)
            }
            return
        }
        containerView.isHidden = false
        imageView.isHidden = true

        for index in 0..<min(photoViews.count, attachments.count) {
            photoViews[index].isHidden = false
            let placeholder = (placeholders?.count ?? 0) > index ? placeholders?[index] : nil
            let imageView = photoViews[index].subviews.first as? UIImageView
            let tast = imageView?.load(attachment: attachments[index], placeholder: placeholder) { [weak self] image in
                guard let self = self, self.grayscaled else { return }
                imageView?.image = image?.grayscaled
            }

            if let task = tast {
                imageLoadingTasks.append(task)
            }
        }

        if photoViews.count > attachments.count {
            for index in attachments.count..<photoViews.count {
                photoViews[index].isHidden = true
            }
        }
    }
}
