//
//  FoodViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class FoodViewController: UIViewController {

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var errorView: GeneralErrorView!
    @IBOutlet private weak var closedKitchenView: UIView!
    @IBOutlet private weak var closedKitchenTitleLabel: UILabel!
    @IBOutlet private weak var closedKitchenDescriptionLabel: UILabel!
    @IBOutlet private weak var closedKitchenScheduleLabel: UILabel!
    @IBOutlet private weak var menuSelectorContainerView: UIView!

    private let selectorView = MenuSelectorView.fromNib()

    var presenter: FoodPresenterType!
    private let disposableBag = DisposableBag()

    private var isScrollViewDragging = false
    private let selectorOverlapping: CGFloat = 30
    private var menuSelectorOffset: CGFloat?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    private func configureView() {

        menuSelectorContainerView.fill(with: selectorView)

        closedKitchenTitleLabel.font = .boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .title2).pointSize)
        closedKitchenTitleLabel.text = LocalizedStrings.Menu.Closed.title
        closedKitchenDescriptionLabel.text = LocalizedStrings.Menu.Closed.description

        var contentInset = collectionView.contentInset
        contentInset.top += 12
        contentInset.bottom = Constants.UI.tabBarHeight + 48.0
        collectionView.contentInset = contentInset

        collectionView.register(cellType: MenuAddressCell.self)
        collectionView.register(cellType: MenuItemCell.self)
        collectionView.register(cellType: MenuHeaderCell.self)
        collectionView.register(cellType: CurrentOrdersCell.self)
        collectionView.register(cellType: LargeMenuItemCell.self)
        collectionView.register(cellType: MenuComboCell.self)
        collectionView.register(cellType: MenuHorizontalCell.self)
        collectionView.register(cellType: NewAppVersionCell.self)
        collectionView.register(cellType: MenuInvitesCell.self)
        collectionView.register(cellType: MenuSurgeCell.self)
        collectionView.register(cellType: MenuSelectorCell.self)
    }

    // swiftlint:disable:next function_body_length
    private func configureBindings() {

        presenter.state.$error.observe { [weak self] error in
            self?.errorView.configure(error: error)
        }.addToDisposableBag(disposableBag)

        presenter.state.storefrontState.observe { [weak self] state in
            self?.configureStorefrontState(state)
        }.addToDisposableBag(disposableBag)

        presenter.state.schedule.bind(to: closedKitchenScheduleLabel, keyPath: \.text).addToDisposableBag(disposableBag)

        presenter.state.selectorLabels.observe { [weak self] labels in
            guard let self = self else { return }
            self.selectorView.configure(with: labels) {[weak self] index in
                self?.presenter.updateSelectorIndex(index)
            }
        }.addToDisposableBag(disposableBag)

        presenter.models.observe { [weak self] _ in
            guard let self = self else { return }
            self.collectionView.reloadData()
            DispatchQueue.main.async {
                self.menuSelectorOffset = nil
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$selectorIndex.observe { [weak self] index in
            self?.selectorView.selectedIndex = index
        }.addToDisposableBag(disposableBag)

        presenter.state.$selectedIndexPath.observe { [weak self] indexPath in
            guard
                let self = self,
                let indexPath = indexPath,
                let layout = self.collectionView.collectionViewLayout as? StorefrontLayout
            else { return }
            let yOffset = layout.yOffset(for: indexPath) - self.selectorOverlapping
            let maxOffset = max(layout.collectionViewContentSize.height - self.collectionView.bounds.height, 0)
            self.collectionView.setContentOffset(CGPoint(x: 0, y: min(yOffset, maxOffset)), animated: true)

        }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe {[weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    private func configureStorefrontState(_ state: Storefront.State) {
        closedKitchenView.isHidden = state != .closed
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = state == .closed ? .dark : UIScreen.main.traitCollection.userInterfaceStyle
        }
    }

    private func updateSelectorVisibility() {
        if menuSelectorOffset == nil {
            menuSelectorOffset = self.presenter.clippedIndexPath.flatMap {
                (self.collectionView.collectionViewLayout as? StorefrontLayout)?.yOffset(for: $0)
            }
        }

        guard let selectorOffset = menuSelectorOffset else {
            menuSelectorContainerView.isHidden = true
            return
        }
        menuSelectorContainerView.isHidden = collectionView.contentOffset.y < selectorOffset
    }

    @IBAction private func refreshTapped() {
        presenter.refresh()
    }
}

extension FoodViewController: TabRootController {
    var tabBarImage: UIImage {
        #imageLiteral(resourceName: "food_icon")
    }

    var tabBarSelectedImage: UIImage {
        #imageLiteral(resourceName: "food_icon_selected")
    }

    var identifier: String {
        Identifiers.TabBar.storefront
    }
}

extension FoodViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension FoodViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}

extension FoodViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        presenter.models.wrappedValue.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter.models.wrappedValue[section].numberOfItems
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        presenter.models.wrappedValue[indexPath.section].cell(collectionView: collectionView, indexPath: indexPath)
    }
}

extension FoodViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        presenter.models.wrappedValue[indexPath.section].size(for: indexPath, in: collectionView)
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isScrollViewDragging = true
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate { isScrollViewDragging = false }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isScrollViewDragging = false
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSelectorVisibility()
        guard
            isScrollViewDragging,
            let layout = collectionView.collectionViewLayout as? StorefrontLayout
        else { return }
        let offset = scrollView.contentOffset.y
        self.presenter.didScroll(to: layout.section(for: offset + self.selectorOverlapping))
    }
}
