//
//  FoodPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol FoodPresenterType {
    var clippedIndexPath: IndexPath? { get }
    var state: FoodViewState { get }
    var models: Observable<[CollectionViewSectionModel]> { get }
    func refresh()
    func didScroll(to section: Int)
    func updateSelectorIndex(_ index: Int)
}

enum FoodAction {
    case show(CartItem, [UIImage?])
    case info(Menu)
    case changeAddress
    case addToCart(CartItem)
    case showOrder(Order)
    case surge(Surge)
    case showInvitations
}

struct FoodViewState {

    struct FoodCollectionViewState {
        var storefront: Storefront?
        var orders: [Order]
        var location: Location?
        var eta: Int?
        var isAppOutdated: Bool
        var availableInvitations: Int
        var shouldShowInvitations: Bool
    }

    @Observable
    var collectionViewState: FoodCollectionViewState

    var storefrontState: Observable<Storefront.State> {
        $collectionViewState.map { $0.storefront?.state ?? .normal }
    }

    var selectorLabels: Observable<[String]> {
        $collectionViewState.map {
            var titles = $0.storefront?.sections.map(\.menu.name) ?? []
            if $0.storefront?.unlinkedCombos?.isEmpty == false {
                titles.insert(LocalizedStrings.Menu.Combos.title, at: 0)
            }
            return titles
        }
    }

    @Observable
    var selectorIndex: Int = 0

    @Observable
    var selectedIndexPath: IndexPath?

    @Observable
    var isLoading: Bool = false

    @Observable
    var error: WebError?

    var schedule: Observable<String?> {
        $collectionViewState.map { $0.storefront?.kitchen.schedule }
    }
}

// swiftlint:disable:next type_body_length
final class FoodPresenter: FoodPresenterType {

    private(set) var state: FoodViewState
    private(set) var clippedIndexPath: IndexPath?

    private let disposableBag = DisposableBag()

    private let completion: (FoodAction) -> ()
    private let customerDataProvider: CustomerDataProviderType
    private let cartDataProvider: CartDataProviderType
    private let ordersDataProvider: OrdersDataProviderType
    private let storefrontDataProvider: StorefrontDataProviderType
    private let appVersionDataProvider: AppVersionDataProviderType
    private let invitationsDataProvider: InvitationsDataProviderType

    // swiftlint:disable:next function_body_length
    init(
        storefrontDataProvider: StorefrontDataProviderType,
        customerDataProvider: CustomerDataProviderType,
        cartDataProvider: CartDataProviderType,
        ordersDataProvider: OrdersDataProviderType,
        appVersionDataProvider: AppVersionDataProviderType,
        invitationsDataProvider: InvitationsDataProviderType,
        completion: @escaping (FoodAction) -> ()
    ) {
        self.customerDataProvider = customerDataProvider
        self.storefrontDataProvider = storefrontDataProvider
        self.cartDataProvider = cartDataProvider
        self.appVersionDataProvider = appVersionDataProvider
        self.completion = completion
        self.ordersDataProvider = ordersDataProvider
        self.invitationsDataProvider = invitationsDataProvider
        let collectionViewState = FoodViewState.FoodCollectionViewState(
            storefront: storefrontDataProvider.storefront,
            orders: ordersDataProvider.activeOrders,
            location: customerDataProvider.currentLocation,
            eta: cartDataProvider.cart.eta,
            isAppOutdated: appVersionDataProvider.state == .outdated,
            availableInvitations: customerDataProvider.customer?.availableInvitesCount ?? 0,
            shouldShowInvitations: invitationsDataProvider.shouldShowInvitesOnMenu
        )
        self.state = FoodViewState(collectionViewState: collectionViewState)

        updateModels()

        state.$collectionViewState.observe { [weak self] _ in
            self?.updateModels()
        }
        .addToDisposableBag(disposableBag)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(cartChanged),
            name: .cartDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(locationDidChange),
            name: .locationDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(activeOrdersDidChange),
            name: .currentOrderDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(storefrontDidChange),
            name: .storefrontDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appVersionStatusDidChange),
            name: .appVersionStateDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(customerDidChange),
            name: .customerDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(invitationsVisibilityDidChange),
            name: .invitesVisibilityDidChangeNotification,
            object: nil
        )

        refresh()
    }

    private(set) var models = Observable<[CollectionViewSectionModel]>(wrappedValue: [])

    private var offsets: [Int: Int] = [:]
    private var combosOffset = 0

    func selectItem(_ item: CartItem, placeholder: [UIImage?]) {
        completion(.show(item, placeholder))
    }

    func refresh() {
        state.error = nil
        state.isLoading = true
        storefrontDataProvider.refresh { [weak self] error in
            self?.state.isLoading = false
            if self?.storefrontDataProvider.storefront == nil {
                self?.state.error = error
            }
        }
    }

    func didScroll(to section: Int) {
        var count = 0
        for index in 0...section {
            let model = models.wrappedValue[index]
            if (model as? MenuItemsSectionModel)?.isEmpty == false { count += 1 }
        }
        let index = max(count - 1, 0)
        if state.selectorIndex != index {
            state.selectorIndex = index
        }
    }

    func updateSelectorIndex(_ index: Int) {
        guard index != self.state.selectorIndex else { return }
        state.selectorIndex = index
        state.selectedIndexPath = menuIndexPath(at: index)
    }

    private func updateModels() {
        let models = configureModels()
        self.models.wrappedValue = models
        clippedIndexPath = models
            .firstIndex { $0 is MenuSelectorSectionModel }
            .map { IndexPath(item: 0, section: $0) }
    }

    private func showSurge() {
        if case let .surge(surge) = state.storefrontState.wrappedValue {
            completion(.surge(surge))
        }
    }

    private func showInvitations() {
        completion(.showInvitations)
    }

    // swiftlint:disable:next function_body_length
    private func configureModels() -> [CollectionViewSectionModel] {
        let addressModel = MenuAddressSectionModel(
            location: state.collectionViewState.location,
            eta: state.collectionViewState.eta
        ) { [weak self] in
            self?.completion(.changeAddress)
        }

        let ordersModel = MenuCurrentOrdersSectionModel(orders: state.collectionViewState.orders) {[weak self] order in
            self?.showOrder(order)
        }

        guard state.collectionViewState.storefront != nil,
              state.collectionViewState.storefront?.state != .closed else {
            return [addressModel, ordersModel]
        }

        let invitationsModel = MenuInvitationsSectionModel(
            count: state.collectionViewState.availableInvitations,
            shouldShow: state.collectionViewState.shouldShowInvitations,
            open: { [weak self] in self?.showInvitations() },
            close: { [weak self] in self?.invitationsDataProvider.didCloseInvitationsOnMenu() }
        )
        let newAppVersionModel = MenuNewAppVersionSectionModel(hasNewVersion: state.collectionViewState.isAppOutdated) { [weak self] in
            self?.moveToAppStore()
        }

        let priceTapped: (CartItem) -> () = { [weak self] in self?.completion(.addToCart($0))  }
        let itemTapped: (CartItem, [UIImage?]) -> () = { [weak self] in self?.selectItem($0, placeholder: $1) }
        let didScroll: (Int, Int) -> () = { [weak self] in self?.didScroll(menuIndex: $0, position: $1) }

        let itemModels = state.collectionViewState.storefront?.sections.enumerated().map { menu in
            MenuItemsSectionModel(
                menu: menu.element.menu,
                combos: menu.element.combos,
                selectedIndex: offsets[menu.offset, default: 0],
                priceTaped: priceTapped,
                itemTapped: itemTapped,
                headerTapped: { [weak self] in self?.completion(.info(menu.element.menu)) },
                didScroll: { didScroll(menu.offset, $0) }
            )
        } ?? []

        let combosModel = (state.collectionViewState.storefront?.unlinkedCombos).map {
            MenuItemsSectionModel(
                menu: nil,
                combos: $0,
                selectedIndex: combosOffset,
                priceTaped: priceTapped,
                itemTapped: itemTapped,
                headerTapped: {},
                didScroll: { [weak self] in self?.combosOffset = $0 }
            )
        }

        let surgeModel = MenuSurgeSectionModel(surge: state.storefrontState.wrappedValue.surge) { [weak self] in
            self?.showSurge()
        }

        var titles = state.collectionViewState.storefront?.sections.map(\.menu.name) ?? []
        if state.collectionViewState.storefront?.unlinkedCombos?.isEmpty == false {
            titles.insert(LocalizedStrings.Menu.Combos.title, at: 0)
        }

        let menuSelectorModel = MenuSelectorSectionModel(
            titles: state.selectorLabels.wrappedValue,
            didSelect: { [weak self] index in
                self?.updateSelectorIndex(index)
            }
        )

        var models: [CollectionViewSectionModel?] = [
            surgeModel,
            addressModel,
            invitationsModel,
            ordersModel,
            menuSelectorModel,
            combosModel,
            itemModels.first,
            newAppVersionModel
        ]

        if itemModels.count > 1 {
            models += Array(itemModels.dropFirst())
        }

        return models.compactMap { $0 }
    }

    private func menuIndexPath(at index: Int) -> IndexPath? {
        var count = 0
        for (idx, model) in models.wrappedValue.enumerated() {
            guard (model as? MenuItemsSectionModel)?.isEmpty == false else {
                continue
            }
            if count == index { return IndexPath(item: 0, section: idx) }
            count += 1
        }
        return nil
    }

    private func moveToAppStore() {
        appVersionDataProvider.moveToAppStore()
    }

    private func showOrder(_ order: Order) {
        completion(.showOrder(order))
    }

    private func didScroll(menuIndex: Int, position: Int) {
        offsets[menuIndex] = position
    }

    @objc private func cartChanged() {
        let eta = cartDataProvider.cart.eta
        guard state.collectionViewState.eta != eta else {
            return
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.state.collectionViewState.eta = eta
        }
    }

    @objc private func locationDidChange() {
        state.collectionViewState.location = customerDataProvider.currentLocation
        refresh()
    }

    @objc private func storefrontDidChange() {
        state.collectionViewState.storefront = storefrontDataProvider.storefront
        if storefrontDataProvider.storefront != nil && state.error != nil {
            state.error = nil
        }
    }

    @objc private func activeOrdersDidChange() {
        state.collectionViewState.orders = ordersDataProvider.activeOrders
    }

    @objc private func appVersionStatusDidChange() {
        state.collectionViewState.isAppOutdated = appVersionDataProvider.state == .outdated
    }

    @objc private func customerDidChange() {
        state.collectionViewState.availableInvitations = customerDataProvider.customer?.availableInvitesCount ?? 0
    }

    @objc private func invitationsVisibilityDidChange() {
        state.collectionViewState.shouldShowInvitations = invitationsDataProvider.shouldShowInvitesOnMenu
    }
}
