//
//  InfoPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.10.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol InfoPresenterType {
    var title: String { get }
    var description: String { get }
    func close()
}

enum InfoAction {
    case close
}

final class InfoPresenter: InfoPresenterType {
    let title: String
    let description: String

    private let completion: (InfoAction) -> ()

    init(title: String, description: String, completion: @escaping (InfoAction) -> ()) {
        self.title = title
        self.description = description
        self.completion = completion
    }

    func close() {
        completion(.close)
    }
}
