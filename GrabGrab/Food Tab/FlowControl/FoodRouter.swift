//
//  FoodRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol FoodRouterType {
    var topViewController: UIViewController? { get }
    func showMenu(completion: @escaping (FoodAction) -> ()) -> FoodViewController
    func showInfo(title: String, description: String, completion: @escaping (InfoAction) -> ())
    func dismiss(completion: (() -> ())?)
}

final class FoodRouter: NSObject, FoodRouterType {

    private let factory: FoodViewControllersFactoryType

    private weak var mainController: UIViewController?

    init(factory: FoodViewControllersFactoryType) {
        self.factory = factory
        super.init()
    }

    func showMenu(completion: @escaping (FoodAction) -> ()) -> FoodViewController {
        let controller = factory.menuViewController(completion: completion)
        mainController = controller
        return controller
    }

    func showInfo(title: String, description: String, completion: @escaping (InfoAction) -> ()) {
        let controller = factory.infoViewController(title: title, description: description, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func dismiss(completion: (() -> ())?) {
        topViewController?.dismiss(animated: true, completion: completion)
    }

    var topViewController: UIViewController? {
        guard var topCandidate = mainController else { return nil }
        while let controller = topCandidate.presentedViewController {
            topCandidate = controller
        }
        return topCandidate
    }
}

// MARK: - Transitioning Delegates

extension FoodRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
