//
//  FoodCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class FoodCoordinator: TabCoordinator {

    private let router: FoodRouterType
    private let dataProvidersStorage: DataProvidersStorageType
    private let managersStorage: ManagersStorageType
    private let webClient: WebClientType

    private var childCoordinator: Coordinator?

    init(router: FoodRouterType,
         dataProvidersStorage: DataProvidersStorageType,
         managersStorage: ManagersStorageType,
         webClient: WebClientType) {
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
        self.managersStorage = managersStorage
        self.webClient = webClient
    }

    func start() -> UIViewController & TabRootController {
        router.showMenu { action in
            switch action {
            case let .show(item, placeholder):
                self.showDishDetails(cartItem: item, imagePlaceholder: placeholder)
            case .changeAddress:
                self.showAddressSelection()
            case .addToCart(let item):
                self.dataProvidersStorage.cartDataProvider.add(cartItem: item, count: 1)
            case .showOrder(let order):
                self.showOrder(order)
            case .surge(let surge):
                self.showSurge(surge)
            case .info(let menu):
                self.showInfo(for: menu)
            case .showInvitations:
                self.showInvitations()
            }
        }
    }

    private func showInvitations() {
        if let coordinator = childCoordinator, coordinator is InvitationsCoordinator { return }
        guard let topViewController = router.topViewController else { return }

        let factory = InvitationsViewControllersFactory(dataProvidersStorage: dataProvidersStorage)
        let router = InvitationsRouter(factory: factory, presentingViewController: topViewController)
        let invitationsCoordinator = InvitationsCoordinator(router: router) { [weak self] in
            self?.childCoordinator = nil
        }
        invitationsCoordinator.start()
        childCoordinator = invitationsCoordinator
    }

    private func showInfo(for menu: Menu) {
        guard let description = menu.description else { return }
        router.showInfo(title: menu.name, description: description) { action in
            switch action {
            case .close:
                self.router.dismiss {}
            }
        }
    }

    private func showSurge(_ surge: Surge) {
        if let coordinator = childCoordinator, coordinator is SurgeCoordinator { return }
        let factory = SurgeViewControllersFactory()
        let router = SurgeRouter(presentingViewController: self.router.topViewController, factory: factory)
        let coordinator = SurgeCoordinator(surge: surge, showButtons: false, router: router) {[weak self] _ in
            self?.childCoordinator = nil
        }
        coordinator.start()
        self.childCoordinator = coordinator
    }

    private func showAddressSelection() {
        if let coordinator = childCoordinator, coordinator is AddressesCoordinator { return }

        let factory = AddressesViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )

        let router = AddressesRouter(presentingViewController: self.router.topViewController, factory: factory)
        let addressesCoordinator = AddressesCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            type: .selection
        ) { [weak self] action in
            switch action {
            case .selected(let location):
                self?.dataProvidersStorage.customerDataProvider.selectAddress(location)
            case .closed:
                break
            }
            self?.childCoordinator = nil
        }
        addressesCoordinator.start()
        childCoordinator = addressesCoordinator
    }

    private func showDishDetails(cartItem: CartItem, imagePlaceholder: [UIImage?]) {
        if let coordinator = childCoordinator, coordinator is DishDetailCoordinator {
            return
        }
        let factory = DishDetailViewControllersFactory()
        let router = DishDetailRouter(factory: factory, presentingController: self.router.topViewController)
        let coordinator = DishDetailCoordinator(
            cartItem: cartItem,
            imagePlaceholders: imagePlaceholder,
            orderable: true,
            router: router
        ) { [weak self] ordered in
            guard let self = self else { return }
            if ordered {
                self.dataProvidersStorage.cartDataProvider.add(cartItem: cartItem, count: 1)
            }
            self.childCoordinator = nil
        }
        coordinator.start()
        childCoordinator = coordinator
    }

    private func showOrder(_ order: Order) {
        if let coordinator = childCoordinator, coordinator is OrderCoordinator { return }
        let factory = OrderViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = OrderRouter(presentingViewController: self.router.topViewController, factory: factory)
        let orderCoordinator = OrderCoordinator(
            orderUUID: order.uuid,
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        ) { [weak self] in
            self?.childCoordinator = nil
        }
        orderCoordinator.start()
        childCoordinator = orderCoordinator
    }
}
