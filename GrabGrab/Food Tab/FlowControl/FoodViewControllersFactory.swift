//
//  FoodViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 29.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol FoodViewControllersFactoryType {
    func menuViewController(completion: @escaping (FoodAction) -> ()) -> FoodViewController
    func infoViewController(title: String, description: String, completion: @escaping (InfoAction) -> ()) -> InfoViewController
}

struct FoodViewControllersFactory: FoodViewControllersFactoryType {

    let dataProvidersStorage: DataProvidersStorageType

    func menuViewController(completion: @escaping (FoodAction) -> ()) -> FoodViewController {
        let presenter = FoodPresenter(
            storefrontDataProvider: dataProvidersStorage.storefrontDataProvider,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            cartDataProvider: dataProvidersStorage.cartDataProvider,
            ordersDataProvider: dataProvidersStorage.ordersDataProvider,
            appVersionDataProvider: dataProvidersStorage.appVersionDataProvider,
            invitationsDataProvider: dataProvidersStorage.invitationsDataProvider,
            completion: completion
        )
        let controller = FoodViewController.instance()
        controller.presenter = presenter
        return controller
    }

    func infoViewController(title: String, description: String, completion: @escaping (InfoAction) -> ()) -> InfoViewController {
        let presenter = InfoPresenter(title: title, description: description, completion: completion)
        let controller = InfoViewController.instance()
        controller.presenter = presenter
        return controller
    }
}
