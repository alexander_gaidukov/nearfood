//
//  InvitationsViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class InvitationsViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var emptyStateView: UIView!
    @IBOutlet private weak var emptyStateTitleLabel: UILabel!
    @IBOutlet private weak var emptyStateDescriptionLabel: UILabel!

    var presenter: InvitationsPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBundings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    private func configureViews() {
        title = LocalizedStrings.Invitations.title
        emptyStateTitleLabel.text = LocalizedStrings.Invitations.Empty.title
        emptyStateDescriptionLabel.text = LocalizedStrings.Invitations.Empty.description

        tableView.tableFooterView = UIView(frame: .zero)
        tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: Constants.UI.tabBarHeight, right: 0)
        tableView.register(cellType: InvitationCell.self)

        if navigationController?.viewControllers.count == 1 {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                title: LocalizedStrings.Common.close,
                style: .plain,
                target: self,
                action: #selector(closeTapped)
            )
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        if navigationController?.viewControllers.contains(self) != true {
            presenter.close()
        }
    }

    private func configureBundings() {

        presenter.state.isEmptyViewHidden.bind(to: emptyStateView, keyPath: \.isHidden).addToDisposableBag(disposableBag)

        presenter.models.observe {[weak self] _ in
            self?.tableView.reloadData()
        }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe {[weak self] isLoading in
            if isLoading {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] error in
            if let error = error {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    @objc private func closeTapped() {
        presenter.close()
    }
}

extension InvitationsViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }

}

extension InvitationsViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}

extension InvitationsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.models.wrappedValue.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: InvitationCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: presenter.models.wrappedValue[indexPath.item])
        return cell
    }
}
