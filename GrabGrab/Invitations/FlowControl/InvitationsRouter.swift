//
//  InvitationsRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol InvitationsRouterType {
    func showInvitations(completion: @escaping (InvitationsAction) -> ())
    func showShareInvite(invite: Invite)
    func dismiss(completion: @escaping () -> ())
}

final class InvitationsRouter: InvitationsRouterType {

    private let factory: InvitationsViewControllersFactoryType
    private weak var presentingViewController: UIViewController?

    init(factory: InvitationsViewControllersFactoryType, presentingViewController: UIViewController) {
        self.factory = factory
        self.presentingViewController = presentingViewController
    }

    func showInvitations(completion: @escaping (InvitationsAction) -> ()) {
        let controller = factory.invitationsViewController(completion: completion)
        if let navController = presentingViewController as? UINavigationController {
            navController.pushViewController(controller, animated: true)
        } else {
            let navController = GGNavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            presentingViewController?.present(navController, animated: true, completion: nil)
        }
    }

    func showShareInvite(invite: Invite) {
        let controller = factory.shareInviteViewController(invite: invite)
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func dismiss(completion: @escaping () -> ()) {
        topViewController?.dismiss(animated: true, completion: completion)
    }

    var topViewController: UIViewController? {
        var candidate = presentingViewController
        while let controller = candidate?.presentedViewController {
            candidate = controller
        }
        return candidate
    }
}
