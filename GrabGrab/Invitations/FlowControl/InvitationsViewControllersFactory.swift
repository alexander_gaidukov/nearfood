//
//  InvitationsViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol InvitationsViewControllersFactoryType {
    func invitationsViewController(completion: @escaping (InvitationsAction) -> ()) -> InvitationsViewController
    func shareInviteViewController(invite: Invite) -> UIViewController
}

struct InvitationsViewControllersFactory: InvitationsViewControllersFactoryType {

    let dataProvidersStorage: DataProvidersStorageType

    private let storyboard: Storyboard = .invitations

    func invitationsViewController(completion: @escaping (InvitationsAction) -> ()) -> InvitationsViewController {
        let presenter = InvitationsPresenter(
            invitationsDataProvider: dataProvidersStorage.invitationsDataProvider,
            completion: completion
        )
        let controller = InvitationsViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func shareInviteViewController(invite: Invite) -> UIViewController {
        let urls = URL(string: invite.url).map { [$0] } ?? []
        return UIActivityViewController(activityItems: urls, applicationActivities: nil)
    }
}
