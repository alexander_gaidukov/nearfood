//
//  InvitationsCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class InvitationsCoordinator: Coordinator {
    private let completion: () -> ()
    private let router: InvitationsRouterType

    init(router: InvitationsRouterType, completion: @escaping () -> ()) {
        self.router = router
        self.completion = completion
    }

    func start() {
        router.showInvitations { action in
            switch action {
            case .share(let invite):
                self.share(invite)
            case .close:
                self.completion()
                self.router.dismiss {}
            }
        }
    }

    private func share(_ invite: Invite) {
        router.showShareInvite(invite: invite)
    }
}
