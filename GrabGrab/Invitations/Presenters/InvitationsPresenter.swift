//
//  InvitationsPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol InvitationsPresenterType {
    var state: InvitationsViewState { get }
    var models: Observable<[InvitationCellModel]> { get }
    func close()
}

struct InvitationsViewState {
    @Observable
    var isLoading: Bool = false

    @Observable
    var error: WebError?

    @Observable
    var invitations: [Invite]

    var isEmptyViewHidden: Observable<Bool> {
        $isLoading.combine(with: $invitations) { $0 || !$1.isEmpty }
    }
}

enum InvitationsAction {
    case share(Invite)
    case close
}

final class InvitationsPresenter: InvitationsPresenterType {
    private let completion: (InvitationsAction) -> ()
    private let invitationsDataProvider: InvitationsDataProviderType

    private(set) var state: InvitationsViewState

    lazy var models: Observable<[InvitationCellModel]> = {
        state.$invitations.map {
            $0.map { invite in
                InvitationCellModel(invite: invite) { [weak self] in self?.share(invite) }
            }
        }
    }()

    init(invitationsDataProvider: InvitationsDataProviderType, completion: @escaping (InvitationsAction) -> ()) {
        self.completion = completion
        self.invitationsDataProvider = invitationsDataProvider
        state = InvitationsViewState(invitations: invitationsDataProvider.invitations)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(invitationsDidChange),
            name: .invitesDidChangeNotification,
            object: nil
        )
        updateInvitations()
    }

    func share(_ invitation: Invite) {
        completion(.share(invitation))
    }

    func close() {
        completion(.close)
    }

    private func updateInvitations() {
        let isSilentLoad = !state.invitations.isEmpty
        if !isSilentLoad {
            state.isLoading = true
        }
        invitationsDataProvider.updateInvitations { [weak self] error in
            self?.state.isLoading = false
            if !isSilentLoad {
                self?.state.error = error
            }
        }
    }

    @objc private func invitationsDidChange() {
        state.invitations = invitationsDataProvider.invitations
    }
}
