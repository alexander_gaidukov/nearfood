//
//  CheckCartResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct CartChargeDishMetadata: Codable {
    let dishUuid: String
    let count: Int
    let price: Double
    let title: String
}

struct CartComboMetadata: Codable {
    let comboUuid: String
    let count: Int
    let price: Double
    let title: String
}

struct CartChargeMetadata: Codable {
    let title: String
    let note: String?
    let category: String?
    let delay: Int?
}

struct CartCouponMetadata: Codable {
    let title: String
    let note: String?
    let category: String
    let coupon: Coupon
}

struct EstimationCharge: Codable {
    enum Reason {
        case dish(CartChargeDishMetadata)
        case combo(CartComboMetadata)
        case surge(CartChargeMetadata)
        case bonuses(CartChargeMetadata)
        case coupon(CartCouponMetadata)
        case area(CartChargeMetadata)
        case unknown(String, CartChargeMetadata)

        static let dishReason = "dish"
        static let comboReason = "combo"
        static let surgeReason = "surge"
        static let bonusesReason = "bonuses"
        static let areaReason = "area"
        static let couponReason = "coupon"
    }

    enum CodingKeys: String, CodingKey {
        case amount
        case reason
        case metadata
    }

    let reason: Reason
    let amount: Double

    var dishMetadata: CartChargeDishMetadata? {
        guard case let .dish(metadata) = reason else { return nil }
        return metadata
    }

    var comboMetadata: CartComboMetadata? {
        guard case let .combo(metadata) = reason else { return nil }
        return metadata
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        amount = try container.decode(Double.self, forKey: .amount)
        let reason = try container.decode(String.self, forKey: .reason)
        if reason == Reason.surgeReason {
            let metadata = try container.decode(CartChargeMetadata.self, forKey: .metadata)
            self.reason = .surge(metadata)
        } else if reason == Reason.bonusesReason {
            let metadata = try container.decode(CartChargeMetadata.self, forKey: .metadata)
            self.reason = .bonuses(metadata)
        } else if reason == Reason.dishReason {
            let metadata = try container.decode(CartChargeDishMetadata.self, forKey: .metadata)
            self.reason = .dish(metadata)
        } else if reason == Reason.comboReason {
            let metadata = try container.decode(CartComboMetadata.self, forKey: .metadata)
            self.reason = .combo(metadata)
        } else if reason == Reason.couponReason {
            let metadata = try container.decode(CartCouponMetadata.self, forKey: .metadata)
            self.reason = .coupon(metadata)
        } else if reason == Reason.areaReason {
            let metadata = try container.decode(CartChargeMetadata.self, forKey: .metadata)
            self.reason = .area(metadata)
        } else {
            let metadata = try container.decode(CartChargeMetadata.self, forKey: .metadata)
            self.reason = .unknown(reason, metadata)
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(amount, forKey: .amount)

        switch reason {
        case .dish(let metadata):
            try container.encode(Reason.dishReason, forKey: .reason)
            try container.encode(metadata, forKey: .metadata)
        case .combo(let metadata):
            try container.encode(Reason.comboReason, forKey: .reason)
            try container.encode(metadata, forKey: .metadata)
        case .surge(let metadata):
            try container.encode(Reason.surgeReason, forKey: .reason)
            try container.encode(metadata, forKey: .metadata)
        case .bonuses(let metadata):
            try container.encode(Reason.bonusesReason, forKey: .reason)
            try container.encode(metadata, forKey: .metadata)
        case .coupon(let metadata):
            try container.encode(Reason.couponReason, forKey: .reason)
            try container.encode(metadata, forKey: .metadata)
        case .area(let metadata):
            try container.encode(Reason.areaReason, forKey: .reason)
            try container.encode(metadata, forKey: .metadata)
        case .unknown(let reason, let metadata):
            try container.encode(reason, forKey: .reason)
            try container.encode(metadata, forKey: .metadata)
        }
    }
}

struct CartProblem: Decodable {
    private var attributes: [String: [String]]

    private struct CodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }

        var intValue: Int?
        init?(intValue: Int) {
            nil
        }
    }

    init(from decoder: Decoder) throws {
        let containter = try decoder.container(keyedBy: CodingKeys.self)
        var attributes: [String: [String]] = [:]
        for key in containter.allKeys {
            attributes[key.stringValue] = try containter.decode(
                [String].self,
                forKey: CodingKeys(stringValue: key.stringValue)! // swiftlint:disable:this force_unwrapping
            )
        }
        self.attributes = attributes
    }

    subscript(_ key: String) -> [String]? {
        attributes[key]
    }
}

struct CartCalculation: Decodable {
    let total: Double
    let amount: Double
    let charges: FailableDecodableArray<EstimationCharge>?
    let problems: CartProblem?
}

struct Estimation: Decodable {
    let uuid: String
    let eta: Int
    let calculation: CartCalculation
}
