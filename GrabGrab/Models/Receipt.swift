//
//  Receipt.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Receipt: Decodable {
    let url: String
}
