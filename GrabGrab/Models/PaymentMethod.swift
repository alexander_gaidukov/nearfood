//
//  PaymentMethod.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum PaymentMethod: Decodable {
    case applePay(String?)
    case card(BankCard)
    case other(String)

    enum CodingKeys: String, CodingKey {
        case kind
        case name
        case data
    }

    private static let applePayKind = "apple_pay"
    private static let bankCardKind = "bank_card"

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let kind = try container.decode(String.self, forKey: .kind)

        if kind == PaymentMethod.applePayKind {
            self = .applePay(nil)
        } else if kind == PaymentMethod.bankCardKind {
            let card = try container.decode(BankCard.self, forKey: .data)
            self = .card(card)
        } else {
            let name = (try? container.decode(String.self, forKey: .name)) ?? ""
            self = .other(name)
        }
    }
}

extension PaymentMethod {
    var identifier: String {
        switch self {
        case .applePay:
            return "applePay"
        case .card(let card):
            return card.uuid
        case .other:
            return "other"
        }
    }

    var isEnabled: Bool {
        switch self {
        case .applePay:
            return true
        case .card(let card):
            return card.status != .rejected
        case .other:
            return false
        }
    }

    var title: String {
        switch self {
        case .applePay:
            return LocalizedStrings.PaymentMethod.applePay
        case .card(let card):
            return card.title
        case .other(let title):
            return title
        }
    }

    var image: UIImage? {
        switch self {
        case .applePay:
            return #imageLiteral(resourceName: "apple_pay")
        case .card(let card):
            return card.image
        case .other:
            return nil
        }
    }
}

// MARK: - API Calls
extension PaymentMethod {
    var kind: String {
        switch self {
        case .applePay:
            return PaymentMethod.applePayKind
        case .card:
            return PaymentMethod.bankCardKind
        case .other:
            return "other"
        }
    }

    var data: JSON? {
        switch self {
        case .applePay(let token):
            return token.map { ["token": $0] }
        case .card(let bankCard):
            return ["uuid": bankCard.uuid]
        case .other:
            return nil
        }
    }
}
