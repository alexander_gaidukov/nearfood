//
//  City.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct City: Codable {
    let uuid: String
    let name: String
    let latitude: Double
    let longitude: Double
}
