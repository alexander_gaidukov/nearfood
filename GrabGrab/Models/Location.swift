//
//  Location.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

func == (lhs: Location, rhs: Location) -> Bool {
    lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
}

struct Location: Equatable {
    var uuid: String = ""
    var latitude: Double
    var longitude: Double
    var address: String?
    var apartments: String?
    var entrance: String?
    var floor: String?
    var doorPhone: String?
    var title: String?
    var note: String?

    var isValid: Bool {
        latitude != 0 && longitude != 0 && address?.isEmpty == false
    }

    var fullAddress: String {
        guard !ProcessInfo().isInTestMode else {
            return Constants.UITests.address
        }
        return address ?? ""
    }

    var deliveryAddress: String {
        [address, additionalAddressInfo].compactMap { $0 }.joined(separator: ", ")
    }

    var label: String {
        guard let title = title, !title.isEmpty else {
            return fullAddress
        }
        return title
    }

    var isEmpty: Bool {
        latitude == 0 && longitude == 0
    }

    var position: GeoPosition {
        GeoPosition(latitude: latitude, longitude: longitude)
    }

    var updateParams: JSON {
        ["title": title ?? "",
         "apartments": apartments ?? "",
         "entrance": entrance ?? "",
         "floor": floor ?? "",
         "door_phone": doorPhone ?? "",
         "note": note ?? ""
        ]
    }

    var additionalAddressInfo: String? {
        let entranceString = entrance.map { LocalizedStrings.Order.Address.entrance($0) }
        let floorString = floor.map { LocalizedStrings.Order.Address.floor($0) }
        let apartmentsString = apartments.map { LocalizedStrings.Order.Address.apartments($0) }
        let doorphoneString = doorPhone.map { LocalizedStrings.Order.Address.doorphone($0) }
        let items = [entranceString, floorString, apartmentsString, doorphoneString].compactMap { $0 }
        return items.isEmpty ? nil : items.joined(separator: ", ")
    }
}

extension Location: Codable {

}
