//
//  CartDish.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct StoredCartDish: Codable {
    var uuid: String = ""
    var name: String = ""
    var photoUrl: String?
    var quantity: Int = 0
    var unitValue: String = QuantityUnit.grams.rawValue
    var price: Double = 0
    var isAvailable: Bool = true
    var count: Int = 0
    var totalPrice: Double = 0

    var unit: QuantityUnit {
        QuantityUnit(rawValue: unitValue) ?? QuantityUnit.grams
    }
}

extension StoredCartDish {
    init(dish: Dish, count: Int, price: Double? = nil, totalPrice: Double? = nil, isAvailable: Bool = true) {
        self.uuid = dish.uuid
        self.name = dish.name
        self.photoUrl = dish.mainPhoto?.url
        self.quantity = dish.quantity
        self.unitValue = dish.unit.rawValue
        self.price = price ?? dish.price
        self.count = count
        self.totalPrice = totalPrice ?? dish.price * Double(count)
        self.isAvailable = isAvailable
    }
}
