//
//  StoredCartItem.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum StoredCartItem {
    case dish(StoredCartDish)
    case combo(StoredCartCombo)
}
