//
//  CartCombo.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct StoredCartCombo: Codable {
    var uuid: String = ""
    var name: String = ""
    var photoUrl: String?
    var price: Double = 0
    var isAvailable: Bool = true
    var count: Int = 0
    var totalPrice: Double = 0
    var dishes: [StoredCartDish]

    var photoURLs: [String] {
        photoUrl.map { [$0] } ?? dishes.compactMap(\.photoUrl)
    }
}

extension StoredCartCombo {
    init(combo: Combo, count: Int, price: Double? = nil, totalPrice: Double? = nil, isAvailable: Bool = true) {
        self.uuid = combo.uuid
        self.name = combo.name
        self.photoUrl = combo.photo?.url
        self.price = price ?? combo.price
        self.count = count
        self.totalPrice = totalPrice ?? combo.price * Double(count)
        self.isAvailable = isAvailable
        dishes = combo.comboDishes.map {
            StoredCartDish(dish: $0, count: 1)
        }
    }
}
