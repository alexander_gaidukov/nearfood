//
//  Cart.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Cart: Codable {
    var uuid: String = ""
    var eta: Int?
    var totalPrice: Double = 0
    var amount: Double = 0
    var cutlery: Int = 1
    var leaveAtDoor: Bool = false
    var note: String = ""
    var recipient: Recipient?
    var charges: [EstimationCharge] = []
    var dishes: [StoredCartDish] = []
    var combos: [StoredCartCombo] = []

    var draftCoupon: String?

    mutating func clear() {
        uuid = ""
        eta = nil
        totalPrice = 0
        amount = 0
        cutlery = 1
        note = ""
        recipient = nil
        draftCoupon = nil
        charges = []
        dishes = []
        combos = []
    }

    var isEmpty: Bool {
        existingDishes.isEmpty && existingCombos.isEmpty
    }

    var isOrderable: Bool {
        existingDishes.contains { $0.isAvailable } || existingCombos.contains { $0.isAvailable }
    }

    var existingDishes: [StoredCartDish] {
        dishes.filter { $0.count > 0 }
    }

    var existingCombos: [StoredCartCombo] {
        combos.filter { $0.count > 0 }
    }

    var unavailableDishes: [StoredCartDish] {
        dishes.filter { !$0.isAvailable }
    }

    var unavailableCombos: [StoredCartCombo] {
        combos.filter { !$0.isAvailable }
    }

    var hasInvalidItems: Bool {
        !unavailableDishes.isEmpty || !unavailableCombos.isEmpty
    }

    var displayPrice: Double {
        isEmpty ? 0 : amount
    }

    var coupon: Coupon? {
        for charge in charges {
            if case let .coupon(metadata) = charge.reason { return metadata.coupon }
        }
        return nil
    }

    var surge: Surge? {
        for charge in charges {
            if case .surge(let metadata) = charge.reason {
                return Surge(
                    uuid: UUID().uuidString,
                    price: charge.amount,
                    delay: metadata.delay ?? 0
                )
            }
        }
        return nil
    }

    mutating func addOrUpdate(_ cartItem: CartItem, count: Int) {
        switch cartItem {
        case .combo(let combo):
            addOrUpdate(combo, count: count)
        case .dish(let dish):
            addOrUpdate(dish, count: count)
        }
    }

    mutating private func addOrUpdate(_ dish: Dish, count: Int) {
        totalPrice += dish.price * Double(count)
        amount += dish.price * Double(count)
        if let index = dishes.firstIndex(where: { $0.uuid == dish.uuid }) {
            dishes[index].count += count
        } else {
            let cartDish = StoredCartDish(dish: dish, count: count)
            dishes.append(cartDish)
        }
    }

    mutating private func addOrUpdate(_ combo: Combo, count: Int) {
        totalPrice += combo.price * Double(count)
        amount += combo.price * Double(count)
        if let index = combos.firstIndex(where: { $0.uuid == combo.uuid }) {
            combos[index].count += count
        } else {
            let cartCombo = StoredCartCombo(combo: combo, count: count)
            combos.append(cartCombo)
        }
    }
}
