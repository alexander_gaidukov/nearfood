//
//  PhoneNumber.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct PhoneNumber {

    var country: Country {
        didSet {
            if country != oldValue {
                maskValue = ""
            }
        }
    }

    private(set) var maskValue: String

    var rawValue: String {
        country.phoneCode + maskValue
    }

    init(country: Country, maskValue: String = "") {
        self.country = country
        self.maskValue = maskValue
    }

    var formattedMask: String {
        guard !maskValue.isEmpty else {
            return ""
        }

        var valueIndex = 0
        var maskIndex = 0
        var result = ""
        while valueIndex < maskValue.count {
            let maskCH = country.phoneMask[maskIndex]
            if maskCH == "*" {
                let valueCH = maskValue[valueIndex]
                result.append(valueCH)
                valueIndex += 1
            } else {
                result.append(maskCH)
            }
            maskIndex += 1
        }
        return result
    }

    var formattedNumber: String {
        country.phoneCode + " " + formattedMask
    }

    var isValid: Bool {
        maskValue.count == country.maskLength
    }

    mutating func setNumber(number: String) {
        let availableValues = "0123456789"
        let unavailableCharactersSet = CharacterSet(charactersIn: availableValues).inverted
        let value = number.removing(charactersFrom: unavailableCharactersSet)
        guard value.count <= country.maskLength else { return }
        maskValue = value
    }

    mutating func clear() {
        maskValue = ""
    }
}
