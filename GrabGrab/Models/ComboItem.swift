//
//  ComboItem.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct ComboItem: Decodable {
    let uuid: String
    let dish: Dish
}
