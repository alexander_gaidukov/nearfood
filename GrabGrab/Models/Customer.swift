//
//  Customer.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Customer: Codable {
    let uuid: String
    var name: String?
    let phone: String
    var activatedAt: Date?
    var invite: Invite?
    var inviteRequired: Bool?
    var locations: [Location]?
    var paymentMethods: [BankCard]?
    var bonuses: Double?
    var availableInvitesCount: Int?
    var tipsSettings: TipsOption?

    var isActive: Bool { activatedAt != nil }
}
