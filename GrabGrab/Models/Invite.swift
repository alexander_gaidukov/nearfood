//
//  Invite.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Invite: Codable {
    let uuid: String
    let multiplier: Double
    let name: String
    let description: String
    let url: String
    let code: String
    let usages: Int?
    let maxUsages: Int?

    var availableUsages: Int? {
        guard let usages = usages, let maxUsages = maxUsages else { return nil }
        return maxUsages - usages
    }
}
