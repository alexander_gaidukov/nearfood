//
//  Feedback.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct Feedback: Decodable {
    let uuid: String
    let rating: Int?
    let note: String?
}

extension Feedback {
    var isCompleted: Bool {
        (rating ?? 0) > 0
    }
}

extension Feedback {
    var icon: UIImage? {
        switch rating {
        case 5:
           return #imageLiteral(resourceName: "happy")
        case 4:
            return #imageLiteral(resourceName: "good")
        case 3:
            return #imageLiteral(resourceName: "sad")
        case 2:
            return #imageLiteral(resourceName: "bad")
        default:
            return nil
        }
    }
}
