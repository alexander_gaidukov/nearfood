//
//  OrderInfo.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct OrderInfo: Decodable {
    let uuid: String
    let counter: Int
    let createdAt: Date
    let feedbackUuid: String
}

extension OrderInfo {
    var title: String {
        LocalizedStrings.Order.title + " \(counter) - " + Formatters.outputDateFormatter.string(from: createdAt)
    }
}
