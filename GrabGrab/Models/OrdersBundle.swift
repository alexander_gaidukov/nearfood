//
//  Bundle.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct OrdersBundleItem: Decodable {
    let uuid: String
    let status: Order.Status
    let latitude: Double
    let longitude: Double

    var position: GeoPosition {
        GeoPosition(latitude: latitude, longitude: longitude)
    }
}

struct OrdersBundle: Decodable {

    enum Status: String, Decodable {
        case pending
        case inProgress = "in_progress"
        case completed
    }

    let uuid: String
    let status: Status
    let orders: [OrdersBundleItem]?
    let courier: Courier?

    var activeOrders: [OrdersBundleItem]? {
        orders?.filter { $0.status == .ready }
    }
}
