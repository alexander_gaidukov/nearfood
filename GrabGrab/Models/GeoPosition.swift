//
//  GeoPosition.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 19.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct GeoPosition: Decodable {
    let latitude: Double
    let longitude: Double
    let course: Float?
}

extension GeoPosition {
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
        self.course = nil
    }
}
