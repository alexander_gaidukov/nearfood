//
//  Order.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct Order: Decodable {

    enum Status: String, Decodable {
        case pending
        case unpaid
        case paid
        case cooking
        case ready
        case completed
        case cancelled
    }

    let uuid: String
    let status: Status
    let counter: Int
    let location: Location
    let alternativeLocation: Location?
    let estimation: Estimation
    let scheduledAt: Date
    let createdAt: Date
    let arrivedAt: Date?
    let cutleryCount: Int
    var contactless: Bool
    var note: String?
    let dishes: [Dish]?
    let combos: [Combo]?
    let paymentMethod: PaymentMethod?
    let bundle: OrdersBundle?
    var feedback: Feedback?
    let receipt: Receipt?
    let recipient: Recipient?
    var tip: OrderTips?

    var positionInBundle: Int? {
        bundle?.activeOrders?.firstIndex { $0.uuid == self.uuid }
    }

    var isArrived: Bool {
        arrivedAt != nil
    }
    
    var isCancellable: Bool {
        [.pending, .unpaid, .paid].contains(status)
    }

    var deliveryLocation: Location {
        alternativeLocation ?? location
    }

    var cartItems: [CartItem] {
        (combos?.map { .combo($0) } ?? []) + (dishes?.map { .dish($0) } ?? [])
    }

    var deliveryTime: Int {
        guard let arrivedAt = arrivedAt else { return 0 }
        let components = Calendar.current.dateComponents([.hour, .minute], from: createdAt, to: arrivedAt)
        return (components.hour ?? 0) * 60 + (components.minute ?? 0)
    }
}

extension Order {
    var title: String {
        if [.cancelled, .completed].contains(status) {
            return LocalizedStrings.Order.title + " \(counter) - " + Formatters.outputDateFormatter.string(from: createdAt)
        }
        return LocalizedStrings.Order.current + " \(counter)"
    }
}

extension Order.Status {
    var messageColor: UIColor {
        switch self {
        case .unpaid, .cancelled:
            return .errorLabel
        default:
            return .mainText
        }
    }
}

extension Order {
    var isTrackingAvailable: Bool {
        status == .ready && bundle?.status == .inProgress
    }
}
