//
//  OrderTips.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct OrderTips: Decodable {
    enum PaymentStatus: String, Decodable {
        case pending
        case completed
        case rejected
        case cancelled
    }

    let uuid: String
    let paymentStatus: PaymentStatus
    let amount: Double
    let kind: TipsOption.Kind
    let props: TipsOption.Properties
    let receipt: Receipt?
    let processedAt: Date?

    var isProcessed: Bool { processedAt != nil }

    var option: TipsOption {
        TipsOption(amount: amount, kind: kind, props: props)
    }
}
