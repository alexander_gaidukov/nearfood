//
//  ImageAttachment.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum ImageAttachment: Decodable {
    case remote(Photo)
    case local(UIImage)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let photo = try container.decode(Photo.self)
        self = .remote(photo)
    }
}
