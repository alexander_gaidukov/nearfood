//
//  Message.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

func == (lhs: Message, rhs: Message) -> Bool {
    lhs.uuid == rhs.uuid
}

struct Message: Decodable, Equatable {

    struct Author: Decodable {
        let uuid: String
        let name: String
    }

    struct Properties: Decodable {
        var order: Order?
        var amount: Double?
    }

    enum Kind: String, Decodable {
        case message
        case feedback
        case bonuses
    }

    enum SendingState: String, Decodable {
        case inProgress
        case error
    }

    let uuid: String
    let kind: Kind
    let body: String?
    let attachments: [ImageAttachment]?
    let author: Author?
    var properties: Properties?
    let createdAt: Date
    var sendingState: SendingState?
}

extension Message {
    init(customer: Customer, body: String, order: Order?) {
        uuid = UUID().uuidString
        kind = .message
        self.body = body
        self.author = Author(uuid: customer.uuid, name: customer.name ?? "")
        properties = order.map { Properties(order: $0, amount: nil) }
        createdAt = Date()
        sendingState = .inProgress
        attachments = nil
    }

    init(customer: Customer, image: UIImage, order: Order?) {
        uuid = UUID().uuidString
        kind = .message
        body = nil
        attachments = [.local(image)]
        author = Author(uuid: customer.uuid, name: customer.name ?? "")
        properties = order.map { Properties(order: $0, amount: nil) }
        createdAt = Date()
        sendingState = .inProgress
    }
}
