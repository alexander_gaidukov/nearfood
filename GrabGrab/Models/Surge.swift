//
//  Surge.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

func == (lhs: Surge, rhs: Surge) -> Bool {
    lhs.price == rhs.price
}

struct Surge: Decodable, Equatable {
    let uuid: String
    let price: Double
    let delay: Int
}
