//
//  Fees.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.10.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Fees: Decodable {
    let fee: Double
    let refund: Double
}
