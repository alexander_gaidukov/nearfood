//
//  BankCard.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum BankCardType: String, Codable {
    case masterCard = "MasterCard"
    case visa = "Visa"
    case mir = "Mir"
    case unionPay = "UnionPay"
    case jcb = "JCB"
    case americanExpress = "AmericanExpress"
    case dinersClub = "DinersClub"
    case unknown = "Unoknown"
}

enum BankCardStatus: String, Codable {
    case pending
    case confirmed
    case rejected
}

struct BankCard: Codable {
    let uuid: String
    let suffix: String
    let prefix: String
    let confirmationUrl: String?
    let type: FailableDecodable<BankCardType>
    let status: BankCardStatus
}

extension BankCard {
    var image: UIImage {
        switch type.base {
        case .visa?:
            return #imageLiteral(resourceName: "visa")
        case .masterCard?:
            return #imageLiteral(resourceName: "mastercard")
        default:
            return #imageLiteral(resourceName: "credit_card")
        }
    }

    var title: String {
        "****\(suffix)"
    }
}
