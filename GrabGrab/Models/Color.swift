//
//  Color.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct Color: Decodable {

    let red: CGFloat
    let green: CGFloat
    let blue: CGFloat
    let alpha: CGFloat

    init(from decoder: Decoder) throws {
        let containter = try decoder.singleValueContainer()
        let hex = try containter.decode(String.self)
        try self.init(hex: hex)
    }

    init(hex: String) throws {
        guard hex.hasPrefix("#") && hex.count == 7 else {
            throw DecodingError.dataCorrupted(
                DecodingError.Context(codingPath: [], debugDescription: "The color format is incorrect")
            )
        }

        let start = hex.index(hex.startIndex, offsetBy: 1)
        let hexColor = String(hex[start...])

        let scanner = Scanner(string: hexColor)
        var hexNumber: UInt64 = 0

        guard scanner.scanHexInt64(&hexNumber) else {
            throw DecodingError.dataCorrupted(
                DecodingError.Context(codingPath: [], debugDescription: "The color format is incorrect")
            )
        }

        self.red = CGFloat((hexNumber & 0xff0000) >> 16) / 255
        self.green = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
        self.blue = CGFloat(hexNumber & 0x0000ff) / 255
        self.alpha = 1
    }
}

extension Color {
    var uiColor: UIColor {
        UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}
