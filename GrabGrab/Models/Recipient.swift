//
//  Recipient.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Recipient: Codable {
    let uuid: String
    let phone: String
    let name: String
}

extension Recipient {
    var json: JSON {
        ["phone": phone, "name": name]
    }
}
