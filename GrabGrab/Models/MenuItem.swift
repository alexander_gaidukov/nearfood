//
//  MenuItem.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct MenuItem: Decodable {
    enum Layout: String, Decodable {
        case regular
        case large
    }

    let uuid: String
    let layout: Layout
    let dish: Dish
    let properties: FailableDecodable<Background>?
}
