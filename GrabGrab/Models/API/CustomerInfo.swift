//
//  CustomerInfo.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 19.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct CustomerInfo {
    let locations: [Location]
    let bankCards: [BankCard]
    let bonuses: Double
}
