//
//  CountriesResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct CountriesResponse: Codable {
    var codes: [Country]
}
