//
//  PhoneConfirmationResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct SignInResponse: Decodable {
    let token: String
    var owner: Customer
}
