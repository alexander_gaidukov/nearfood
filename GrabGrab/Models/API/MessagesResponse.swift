//
//  MessagesResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct MessagesResponse: Decodable {
    let messages: FailableDecodableArray<Message>
}
