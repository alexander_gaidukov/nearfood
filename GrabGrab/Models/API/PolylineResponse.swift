//
//  PolylineResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct PolylineResponse: Decodable {
    let polyline: String?
    let position: GeoPosition?
}
