//
//  LocationsResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct LocationsResponse: Decodable {
    let locations: FailableDecodableArray<Location>
}
