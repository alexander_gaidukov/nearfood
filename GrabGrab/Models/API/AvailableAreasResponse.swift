//
//  AvailableAreasResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct AvailableAreasResponse: Decodable {
    let areas: [Area]
}
