//
//  BonusesResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct BonusesResponse: Decodable {
    let amount: Double
}
