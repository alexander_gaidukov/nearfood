//
//  AddCardResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct AddCardResponse: Decodable {
    let bankCard: BankCard?
    let secure3D: Secure3d?
}
