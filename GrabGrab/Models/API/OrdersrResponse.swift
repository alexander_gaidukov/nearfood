//
//  OrdersrResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 18.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct OrdersResponse: Decodable {
    let orders: FailableDecodableArray<Order>
}
