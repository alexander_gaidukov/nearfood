//
//  LocationCheckResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct LocationCheckResponse: Decodable {
    let uuid: String
    let name: String?
    let schedule: String
    let fee: Double
    let delay: Double
}
