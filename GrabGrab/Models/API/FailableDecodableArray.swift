//
//  FailableDecodableArray.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct FailableDecodableArray<Element: Decodable>: Decodable {
    private(set) var elements: [Element]
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        var elements: [Element] = []
        if let count = container.count {
            elements.reserveCapacity(count)
        }

        while !container.isAtEnd {
            if let element = try container.decode(FailableDecodable<Element>.self).base {
                elements.append(element)
            }
        }

        self.elements = elements
    }
}

extension FailableDecodableArray {
    init(elements: [Element]) {
        self.elements = elements
    }
}

extension FailableDecodableArray: RandomAccessCollection, MutableCollection {

    var startIndex: Int {
        elements.startIndex
    }

    var endIndex: Int {
        elements.endIndex
    }

    subscript(position: Int) -> Element {
        get {
            elements[position]
        }

        set {
            elements[position] = newValue
        }
    }

    // swiftlint:disable:next identifier_name
    func index(after i: Int) -> Int {
        elements.index(after: i)
    }

    // swiftlint:disable:next identifier_name
    func index(before i: Int) -> Int {
        elements.index(before: i)
    }
}
