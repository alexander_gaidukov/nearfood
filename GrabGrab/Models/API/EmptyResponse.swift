//
//  EmptyResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct EmptyResponse: Decodable {}

extension Resource where A == EmptyResponse {
    init(baseURL: URL,
         path: String,
         method: RequestMethod = .get,
         params: JSON = [:],
         headers: HTTPHeaders = [:],
         attachments: [String: Attachment] = [:],
         shouldDebug: Bool = true) {
        self.init(
            baseURL: baseURL,
            path: path,
            method: method,
            params: params,
            headers: headers,
            attachments: attachments,
            shouldDebug: shouldDebug
        ) { _ in EmptyResponse() }
    }
}
