//
//  PaymentMethodsResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct PaymentMethodsResponse: Decodable {
    var cards: FailableDecodableArray<BankCard>
}
