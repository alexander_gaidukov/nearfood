//
//  InvitationsResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct InvitationsResponse: Decodable {
    let invites: FailableDecodableArray<Invite>
}
