//
//  TipsOptionsResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct TipsOptionsResponse: Decodable {
    let options: [TipsOption]
}
