//
//  FailableDecodable.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

func ==<T: Decodable & Equatable>(lhs: FailableDecodable<T>, rhs: T?) -> Bool {
    lhs.base == rhs
}

func ==<T: Decodable & Equatable>(lhs: T?, rhs: FailableDecodable<T>) -> Bool {
    lhs == rhs.base
}

struct FailableDecodable<Base: Decodable>: Decodable {
    let base: Base?
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        base = try? container.decode(Base.self)
    }
}

extension FailableDecodable: Encodable where Base: Encodable {
    func encode(to encoder: Encoder) throws {
        if let value = base {
            var container = encoder.singleValueContainer()
            try container.encode(value)
        }
    }
}
