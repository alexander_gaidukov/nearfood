//
//  AppStoreVersionResponse.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 26.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct AppStoreVersionResponse: Decodable {
    let results: [AppVersion]?
}
