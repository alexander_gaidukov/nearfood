//
//  TipsOption.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

func == (lhs: TipsOption.Properties, rhs: TipsOption.Properties) -> Bool {
    Int((lhs.value * 100.0).rounded()) == Int((rhs.value * 100.0).rounded())
}

struct TipsOption: Codable, Equatable {
    enum Kind: String, Codable, Equatable {
        case fixed
        case percent
    }

    struct Properties: Codable, Equatable {
        let value: Double
    }

    let amount: Double?
    let kind: Kind
    let props: Properties
}

extension TipsOption {
    init(kind: Kind, value: Double) {
        self.init(amount: nil, kind: kind, props: Properties(value: value))
    }
}

extension TipsOption {
    var formattedValue: String {
        switch kind {
        case .fixed:
            return props.value.string(with: Formatters.priceFormatter) ?? ""
        case .percent:
            return "\(Int((props.value * 100.0).rounded()))%"
        }
    }
}
