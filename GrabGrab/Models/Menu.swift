//
//  Menu.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Menu: Decodable {

    enum Layout: String, Decodable {
        case vertical
        case horizontal
    }

    let layout: Layout
    let uuid: String
    let name: String
    let description: String?
    let items: FailableDecodableArray<MenuItem>
}
