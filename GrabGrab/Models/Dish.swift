//
//  Dish.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum QuantityUnit: String, Decodable {
    case grams
    case liters
    case pieces
    case milliliters
}

extension QuantityUnit {
    var name: String {
        switch self {
        case .grams:
            return LocalizedStrings.Dish.Unit.grams
        case .liters:
            return LocalizedStrings.Dish.Unit.liters
        case .pieces:
            return LocalizedStrings.Dish.Unit.pieces
        case .milliliters:
            return LocalizedStrings.Dish.Unit.milliliters
        }
    }

    var shortName: String {
        switch self {
        case .grams:
            return LocalizedStrings.Dish.Unit.gramsSuffix
        case .liters:
            return LocalizedStrings.Dish.Unit.litersSuffix
        case .pieces:
            return LocalizedStrings.Dish.Unit.piecesSuffix
        case .milliliters:
            return LocalizedStrings.Dish.Unit.millilitersSuffix
        }
    }
}

struct Dish: Decodable {

    enum Feature: String, Decodable {
        case vegetarian
        case vegan
        case spicy
    }

    let uuid: String
    let name: String
    let description: String
    let quantity: Int
    let unit: QuantityUnit
    let calories: Int
    let proteins: Int
    let fats: Int
    let carbs: Int
    let price: Double!
    let etr: Int?
    let features: FailableDecodableArray<Feature>?
    let photos: FailableDecodableArray<Photo>?

    var mainPhoto: Photo? {
        photos?.first
    }
}

extension Dish.Feature {
    var emoji: String {
        switch self {
        case .vegan:
            if #available(iOS 12.1, *) {
                return "🥬"
            } else {
                return "🌿"
            }
        case .vegetarian:
            return "🥦"
        case .spicy:
            return "🌶️"
        }
    }

    var name: String {
        switch self {
        case .vegan:
            return LocalizedStrings.Dish.Features.vegan
        case .vegetarian:
            return LocalizedStrings.Dish.Features.vegetarian
        case .spicy:
            return LocalizedStrings.Dish.Features.spicy
        }
    }
}
