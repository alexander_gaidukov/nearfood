//
//  Secure3d.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Secure3d: Decodable {
    let acsUrl: String
    let paymentRequest: String
    // swiftlint:disable:next identifier_name
    let md: String
}
