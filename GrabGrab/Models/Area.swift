//
//  Area.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Area: Decodable {
    let uuid: String
    let kitchenUuid: String
    let fee: Double
    let deactivatedAt: Date?
    let borders: AreaBorders

    var isPaid: Bool {
        fee > 0
    }

    var isDeactivated: Bool {
        deactivatedAt != nil
    }
}

struct AreaBorders: Decodable {
    let type: String
    let coordinates: [[[Double]]]

    var points: [(latitude: Double, longitude: Double)] {
        var result = [(latitude: Double, longitude: Double)]()
        coordinates.first?.forEach {
            guard $0.count == 2 else { return }
            result.append(($0[1], $0[0]))
        }
        return result
    }
}
