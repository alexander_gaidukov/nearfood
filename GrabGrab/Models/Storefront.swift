//
//  Storefront.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Storefront: Decodable {

    enum State: Equatable {
        case normal
        case closed
        case surge(Surge)

        var surge: Surge? {
            guard case let .surge(surge) = self else {
                return nil
            }
            return surge
        }
    }

    let kitchen: Kitchen
    private let menus: FailableDecodableArray<Menu>
    private let combos: FailableDecodableArray<Combo>?

    private let surge: Surge?

    var sections: [(combos: [Combo], menu: Menu)] {
        menus.compactMap {
            let section = (combos: combos(for: $0), menu: $0)
            if section.combos.isEmpty && section.menu.items.isEmpty { return nil }
            return section
        }
    }

    var unlinkedCombos: [Combo]? {
        combos?.filter { $0.menuUuid == nil }
    }

    var state: State {
        if sections.isEmpty && combos?.isEmpty != false { return .closed }
        if let surge = surge { return .surge(surge) }
        return .normal
    }

    private func combos(for menu: Menu) -> [Combo] {
        guard let combos = combos else { return [] }
        return combos.filter { $0.menuUuid == menu.uuid }
    }
}
