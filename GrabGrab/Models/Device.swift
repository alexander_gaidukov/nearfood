//
//  Device.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Device {
    let uuid: String
    let model: String
    let osVersion: String
    let appVersion: String
}

extension Device {
    var params: JSON {
        ["uuid": uuid, "platform": "ios", "model": model, "os_version": osVersion, "app_version": appVersion]
    }
}
