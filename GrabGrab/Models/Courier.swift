//
//  Courier.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 23.07.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct Courier: Decodable {
    enum VehicleType: String, Decodable {
        case car
        case bike
    }
    let uuid: String
    let vehicleType: Courier.VehicleType
}

extension Courier.VehicleType {
    var icon: UIImage {
        switch self {
        case .car:
            return #imageLiteral(resourceName: "car")
        case .bike:
            return #imageLiteral(resourceName: "bicycle")
        }
    }

    var mapIcon: UIImage {
        switch self {
        case .car:
            return #imageLiteral(resourceName: "car_big")
        case .bike:
            return #imageLiteral(resourceName: "bicycle_big")
        }
    }
}
