//
//  CompanyInfo.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct CompanyInfo: Codable {

    struct Document: Codable {
        let name: String
        let url: String
    }

    struct Link: Codable {
        enum Kind: String, Codable {
            // swiftlint:disable:next identifier_name
            case vk
            case facebook
            case instagram
            case twitter
            case telegram

            var icon: UIImage {
                switch self {
                case .facebook:
                    return #imageLiteral(resourceName: "facebook")
                case .instagram:
                    return #imageLiteral(resourceName: "instagram")
                case .twitter:
                    return #imageLiteral(resourceName: "twitter")
                case .vk:
                    return #imageLiteral(resourceName: "vk")
                case .telegram:
                    return #imageLiteral(resourceName: "telegram")
                }
            }
        }

        let kind: FailableDecodable<Kind>
        let name: String
        let url: String
    }

    let phone: String?
    let email: String?
    let documents: [Document]
    let links: [Link]
}
