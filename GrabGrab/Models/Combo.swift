//
//  Combo.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Combo: Decodable {
    let uuid: String
    let name: String
    let menuUuid: String?
    let price: Double!
    let photo: Photo?
    private let items: [ComboItem]?
    private let dishes: [Dish]?
    let etr: Int?

    var comboDishes: [Dish] {
        items?.map(\.dish) ?? dishes ?? []
    }

    var photos: [Photo] {
        photo.map { [$0] } ?? comboDishes.compactMap(\.mainPhoto)
    }

    var originalPrice: Double {
        comboDishes.reduce(0) { $0 + $1.price }
    }
}
