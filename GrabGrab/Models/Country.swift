//
//  Country.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

func == (lhs: Country, rhs: Country) -> Bool {
    lhs.uuid == rhs.uuid
}

struct Country: Codable, Equatable {
    let uuid: String
    let phoneCode: String
    let countryCode: String
    let countryName: String
    let phoneMask: String

    var maskLength: Int {
        phoneMask.filter { $0 == "*" }.count
    }
}

extension Country {
    func match(to phone: String) -> Bool {
        guard phone.hasPrefix(phoneCode) else { return false }
        return phone.count == maskLength + phoneCode.count
    }
}
