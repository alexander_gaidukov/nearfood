//
//  Background.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Background: Decodable {
    struct AdaptiveColor: Decodable {
        let light: Color
        let dark: Color
    }

    struct AdaptiveImage: Decodable {
        let light: String
        let dark: String
    }

    let color: AdaptiveColor
    let image: AdaptiveImage?
}
