//
//  CartItem.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum CartItem {
    case dish(Dish)
    case combo(Combo)

    var photos: [Photo] {
        switch self {
        case .dish(let dish):
            return dish.mainPhoto.map { [$0] } ?? []
        case .combo(let combo):
            return combo.photos
        }
    }

    var uuid: String {
        switch self {
        case .dish(let dish):
            return dish.uuid
        case .combo(let combo):
            return combo.uuid
        }
    }
}
