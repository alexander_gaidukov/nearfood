//
//  Coupon.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Coupon: Codable {
    enum Kind: String, Codable {
        case discountFixed = "discount_fixed"
        case discountPercent = "discount_percent"
        case cashbackFixed = "cashback_fixed"
        case cashbackPercent = "cashback_percent"
    }

    struct Properties: Codable {
        var value: Double
        var maxAmount: Double?
    }

    let uuid: String
    let name: String
    let description: String
    let code: String
    let kind: Kind
    let props: Properties
}
