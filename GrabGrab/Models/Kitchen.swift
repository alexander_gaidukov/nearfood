//
//  Kitchen.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Kitchen: Decodable {
    let uuid: String
    let name: String
    let schedule: String?
}
