//
//  AppDelegate.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import YandexMapKit
import Firebase

final class GGWindow: UIWindow {
    override func becomeFirstResponder() -> Bool {
        true
    }

    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        guard motion == .motionShake else { return }
        (UIApplication.shared.delegate as? AppDelegate)?.applicationCoordinator.showDebug()
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    fileprivate var applicationCoordinator: ApplicationCoordinator!

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {

        FirebaseApp.configure()

        migrate()

        let logger = Logger()

        let webClient = WebClient(accessTokenProvider: AccessTokenProvider.shared, logger: logger)

        let dataProvidersStorage = DataProvidersStorage(
            webClient: webClient,
            accessTokenProvider: AccessTokenProvider.shared
        )

        let managersStorage = ManagersStorage(
            webClient: webClient,
            dataProviders: dataProvidersStorage
        )

        configureYandexMaps()

        UIApplication.shared.registerForRemoteNotifications()

        self.window = GGWindow(frame: UIScreen.main.bounds)
        applicationCoordinator = ApplicationCoordinator(
            window: self.window!, // swiftlint:disable:this force_unwrapping
            webClient: webClient,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            logger: logger
        )

        applicationCoordinator.start()
        self.window?.makeKeyAndVisible()

        UNUserNotificationCenter.current().delegate = self

        if let url = launchOptions?[.url] as? URL {
            managersStorage.deeplinksManager.handle(url: url)
        }

        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        applicationCoordinator.refreshUserData()
        applicationCoordinator.refreshApplicationData()
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        applicationCoordinator.registerDeviceForPushNotifications(with: deviceToken)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {

    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        applicationCoordinator.handleDeeplink(url)
    }

    func application(
        _ application: UIApplication,
        continue userActivity: NSUserActivity,
        restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void
    ) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let url = userActivity.webpageURL else {
            return false
        }
        return applicationCoordinator.handleDeeplink(url)
    }

    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable: Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
    ) {
        applicationCoordinator.handleSilentPushNotification(
            with: userInfo,
            completion: completionHandler
        )
    }

    private func migrate() {
        let migrationManagers: [MigrationsManager] = [
            KeychainMigrationManager(),
            UserDefaultsMigrationManager()
        ]
        migrationManagers.forEach { $0.migrate() }
    }

    private func configureYandexMaps() {
        YMKMapKit.setApiKey(Constants.Identifiers.yandexMapKey)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        completionHandler([.alert, .sound])
    }

    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        applicationCoordinator.handlePushNotification(
            with: response.notification.request.content.userInfo,
            completion: completionHandler
        )
    }
}
