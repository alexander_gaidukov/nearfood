//
//  ComboPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol ComboPresenterType {
    var state: ComboViewState { get }
    func close()
    func addToCart()
    func showDish(_ dish: Dish, placeholder: UIImage?)
}

struct ComboViewState {
    var photos: [Photo]
    var dishes: [Dish]
    var name: String
    var price: String?
    var originalPrice: NSAttributedString?
    var etr: String?
    var isEtrHidden: Bool {
        etr?.isEmpty != false
    }
    var buttonTitle: String? { price }
    var isButtonHidden: Bool { price == nil }
}

enum ComboAction {
    case close
    case added
    case dish(Dish, UIImage?)
}

extension ComboViewState {
    init(combo: Combo, orderable: Bool) {
        photos = combo.photos
        dishes = combo.comboDishes
        name = combo.name
        price = orderable ?
            combo.price.flatMap { $0.string(with: Formatters.priceFormatter) } ?? ""
            : nil
        let originalPriceString = orderable ?
            combo.originalPrice.string(with: Formatters.priceFormatter) ?? ""
            : nil
        originalPrice = originalPriceString.map {
            NSAttributedString(
                string: $0,
                attributes: [
                    .strikethroughStyle: NSUnderlineStyle.single.rawValue,
                    .strikethroughColor: UIColor.labelText
                ]
            )
        }
        etr = orderable ? combo.etr?.etrString : nil
    }
}

final class ComboPresenter: ComboPresenterType {

    private let completion: (ComboAction) -> ()

    var state: ComboViewState

    init(combo: Combo, orderable: Bool, completion: @escaping (ComboAction) -> ()) {
        self.completion = completion
        self.state = ComboViewState(combo: combo, orderable: orderable)
    }

    func close() {
        completion(.close)
    }

    func addToCart() {
        completion(.added)
    }

    func showDish(_ dish: Dish, placeholder: UIImage?) {
        completion(.dish(dish, placeholder))
    }
}
