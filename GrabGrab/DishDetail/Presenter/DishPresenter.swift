//
//  DishPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol DishPresenterType {
    var state: DishViewState { get }
    func close()
    func addToCart()
    func backTapped()
}

struct DishViewState {
    var photo: Photo?
    var name: String
    var description: String
    var quantity: String
    var unit: String
    var calories: String
    var proteins: String
    var fats: String
    var carbs: String
    var price: String?
    var featuers: String?
    var etr: String?
    var isFeaturesHidden: Bool {
        featuers?.isEmpty != false && etr?.isEmpty != false
    }
    var buttonTitle: String? { price }
    var isButtonHidden: Bool { price == nil }
}

extension DishViewState {
    init(dish: Dish, orderable: Bool) {
        photo = dish.mainPhoto
        name = dish.name
        description = dish.description
        quantity = "\(dish.quantity)"
        unit = dish.unit.name
        calories = "\(dish.calories)"
        proteins = "\(dish.proteins)"
        fats = "\(dish.fats)"
        carbs = "\(dish.carbs)"
        price = orderable ?
            dish.price.flatMap { $0.string(with: Formatters.priceFormatter) } ?? ""
            : nil
        featuers = dish.features?.map { "\($0.emoji)\($0.name)" }.joined(separator: ", ")
        etr = orderable ? dish.etr?.etrString : nil
    }
}

enum DishAction {
    case close
    case added
    case back
}

final class DishPresenter: DishPresenterType {

    private let completion: (DishAction) -> ()

    var state: DishViewState

    init(dish: Dish, orderable: Bool, completion: @escaping (DishAction) -> ()) {
        self.completion = completion
        self.state = DishViewState(dish: dish, orderable: orderable)
    }

    func close() {
        completion(.close)
    }

    func addToCart() {
        completion(.added)
    }

    func backTapped() {
        completion(.back)
    }
}
