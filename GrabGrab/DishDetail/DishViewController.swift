//
//  DishViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class DishViewController: UIViewController, PopupViewController {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!

    @IBOutlet private weak var featuresLabel: UILabel!
    @IBOutlet private weak var etrLabel: UILabel!
    @IBOutlet private weak var hideFeatureConstraint: NSLayoutConstraint!
    @IBOutlet private weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var topOffsetConstraint: NSLayoutConstraint!

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var dishNameLabel: UILabel!
    @IBOutlet private weak var dishDescriptionLabel: UILabel!
    @IBOutlet private weak var quantityLabel: UILabel!
    @IBOutlet private weak var caloriesLabel: UILabel!
    @IBOutlet private weak var proteinsLabel: UILabel!
    @IBOutlet private weak var fatsLabel: UILabel!
    @IBOutlet private weak var carbsLabel: UILabel!

    @IBOutlet private weak var quantityValueLabel: UILabel!
    @IBOutlet private weak var caloriesValueLabel: UILabel!
    @IBOutlet private weak var proteinsValueLabel: UILabel!
    @IBOutlet private weak var fatsValueLabel: UILabel!
    @IBOutlet private weak var carbsValueLabel: UILabel!

    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var backButton: UIButton!

    var presenter: DishPresenterType!
    var imagePlaceholder: UIImage?
    var initiallyOpenned: Bool = false

    private var defaultImageHeight: CGFloat = 375
    private let minImageHeight: CGFloat = 40

    override func viewDidLoad() {
        super.viewDidLoad()
        defaultImageHeight = min(UIScreen.main.bounds.width, 375)
        imageHeightConstraint.constant = defaultImageHeight
        topOffsetConstraint.constant = defaultImageHeight - 40
        configureView()
        if initiallyOpenned {
            hideViewConstraint.priority = .defaultLow
        }
    }

    private func configureView() {
        if navigationController == nil {
            backButton?.removeFromSuperview()
        }

        backButton?.layer.shadowColor = UIColor.shadow.cgColor
        backButton?.layer.shadowOpacity = 0.32
        backButton?.layer.shadowOffset = CGSize(width: 6, height: 6)

        caloriesLabel.text = LocalizedStrings.Dish.caloriesLabel
        proteinsLabel.text = LocalizedStrings.Dish.proteinsLabel
        fatsLabel.text = LocalizedStrings.Dish.fatsLabel
        carbsLabel.text = LocalizedStrings.Dish.carbsLabel
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        imageView.addGestureRecognizer(panGestureRecognizer)

        if let photo = presenter.state.photo {
            self.imageView.load(photo: photo, placeholder: imagePlaceholder)
        }
        dishNameLabel.text = presenter.state.name
        dishDescriptionLabel.text = presenter.state.description
        quantityValueLabel.text = presenter.state.quantity
        quantityLabel.text = presenter.state.unit
        caloriesValueLabel.text = presenter.state.calories
        proteinsValueLabel.text = presenter.state.proteins
        fatsValueLabel.text = presenter.state.fats
        carbsValueLabel.text = presenter.state.carbs

        featuresLabel.text = presenter.state.featuers
        etrLabel.text = presenter.state.etr
        hideFeatureConstraint.priority = presenter.state.isFeaturesHidden ? .defaultHigh : .defaultLow

        addButton.setTitle(presenter.state.buttonTitle, for: .normal)
        addButton.isHidden = presenter.state.isButtonHidden
    }

    @objc func close() {
        presenter.close()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }

    @IBAction private func addButtonTapped() {
        presenter.addToCart()
    }

    @IBAction private func backButtonTapped() {
        presenter.backTapped()
    }
}

extension DishViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        imageHeightConstraint.constant = max(minImageHeight, defaultImageHeight - scrollView.contentOffset.y)
    }
}
