//
//  ComboDishCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct ComboDishCellModel {
    let photo: Photo?
    let name: String
    let isSeparatorHidden: Bool
}

extension ComboDishCellModel {
    init(dish: Dish, isSeparatorHidden: Bool) {
        photo = dish.mainPhoto
        name = dish.name
        self.isSeparatorHidden = isSeparatorHidden
    }
}

final class ComboDishCell: UITableViewCell {
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var separatorView: UIView!

    private var task: DownloadTask?

    override func prepareForReuse() {
        super.prepareForReuse()
        task?.cancel()
        photoImageView.image = nil
    }

    func configure(model: ComboDishCellModel) {
        photoImageView.load(photo: model.photo)
        nameLabel.text = model.name
        separatorView.isHidden = model.isSeparatorHidden
    }
}
