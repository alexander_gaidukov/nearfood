//
//  DishDetailRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol DishDetailRouterType {
    func showDishDetails(
        for dish: Dish,
        orderable: Bool,
        imagePlaceholder: UIImage?,
        completion: @escaping (DishAction) -> ()
    )

    func showComboDetails(
        for combo: Combo,
        orderable: Bool,
        imagePlaceholders: [UIImage?],
        completion: @escaping (ComboAction) -> ()
    )

    func dismiss(completion: (() -> ())?)

    func pop()
}

final class DishDetailRouter: NSObject, DishDetailRouterType {

    private let factory: DishDetailViewControllersFactoryType

    private weak var presentingController: UIViewController?
    private weak var navController: UINavigationController?

    init(
        factory: DishDetailViewControllersFactoryType,
        presentingController: UIViewController?
    ) {
        self.factory = factory
        self.presentingController = presentingController
        super.init()
    }

    func showDishDetails(
        for dish: Dish,
        orderable: Bool,
        imagePlaceholder: UIImage?,
        completion: @escaping (DishAction) -> ()
    ) {
        let controller = factory.dishViewController(
            dish: dish,
            orderable: orderable,
            imagePlaceholder: imagePlaceholder,
            completion: completion
        )

        if let navController = navController {
            controller.initiallyOpenned = true
            navController.pushViewController(controller, animated: true)
        } else {
            controller.modalPresentationStyle = .custom
            controller.transitioningDelegate = self
            presentingController?.present(controller, animated: true, completion: nil)
        }
    }

    func showComboDetails(
        for combo: Combo,
        orderable: Bool,
        imagePlaceholders: [UIImage?],
        completion: @escaping (ComboAction) -> ()
    ) {
        let controller = factory.comboViewController(
            combo: combo,
            orderable: orderable,
            imagePlaceholders: imagePlaceholders,
            completion: completion
        )
        let navController = GGTransitioningNavigationController(rootViewController: controller)
        navController.isNavigationBarHidden = true
        navController.modalPresentationStyle = .custom
        navController.transitioningDelegate = self
        presentingController?.present(navController, animated: true, completion: nil)
        self.navController = navController
    }

    func dismiss(completion: (() -> ())?) {
        presentingController?.dismiss(animated: true, completion: completion)
    }

    func pop() {
        navController?.popViewController(animated: true)
    }
}

// MARK: - Transitioning Delegates

extension DishDetailRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
