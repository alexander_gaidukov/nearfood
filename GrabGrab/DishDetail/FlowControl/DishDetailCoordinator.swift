//
//  DishDetailCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class DishDetailCoordinator: Coordinator {

    private let completion: (Bool) -> ()
    private let router: DishDetailRouterType
    private let cartItem: CartItem
    private let orderable: Bool
    private let imagePlaceholders: [UIImage?]

    init(
        cartItem: CartItem,
        imagePlaceholders: [UIImage?],
        orderable: Bool,
        router: DishDetailRouterType,
        completion: @escaping (Bool) -> ()
    ) {
        self.cartItem = cartItem
        self.router = router
        self.orderable = orderable
        self.imagePlaceholders = imagePlaceholders
        self.completion = completion
    }

    func start() {
        switch cartItem {
        case .dish(let dish):
            let placeholder = imagePlaceholders.isEmpty ? nil : imagePlaceholders[0]
            showDishDetails(dish, orderable: orderable, placeholder: placeholder)
        case .combo(let combo):
            showCombo(combo, orderable: orderable, placeholders: imagePlaceholders)
        }
    }

    private func showDishDetails(_ dish: Dish, orderable: Bool, placeholder: UIImage?) {
        router.showDishDetails(for: dish, orderable: orderable, imagePlaceholder: placeholder) { action in
            switch action {
            case .added:
                self.router.dismiss {
                    self.completion(true)
                }
            case .close:
                self.router.dismiss {
                    self.completion(false)
                }
            case .back:
                self.router.pop()
            }
        }
    }

    private func showCombo(_ combo: Combo, orderable: Bool, placeholders: [UIImage?]) {
        router.showComboDetails(for: combo, orderable: orderable, imagePlaceholders: placeholders) { action in
            switch action {
            case .added:
                self.router.dismiss {
                    self.completion(true)
                }
            case .close:
                self.router.dismiss {
                    self.completion(false)
                }
            case .dish(let dish, let placeholder):
                self.showDishDetails(dish, orderable: false, placeholder: placeholder)
            }
        }
    }
}
