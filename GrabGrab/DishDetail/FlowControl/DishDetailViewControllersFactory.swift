//
//  DishDetailViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol DishDetailViewControllersFactoryType {
    func dishViewController(
        dish: Dish,
        orderable: Bool,
        imagePlaceholder: UIImage?,
        completion: @escaping (DishAction) -> ()
    ) -> DishViewController

    func comboViewController(
        combo: Combo,
        orderable: Bool,
        imagePlaceholders: [UIImage?],
        completion: @escaping (ComboAction) -> ()
    ) -> ComboViewController
}

struct DishDetailViewControllersFactory: DishDetailViewControllersFactoryType {

    let storyboard: Storyboard = .dishDetail

    func dishViewController(
        dish: Dish,
        orderable: Bool,
        imagePlaceholder: UIImage?,
        completion: @escaping (DishAction) -> ()
    ) -> DishViewController {
        let presenter = DishPresenter(dish: dish, orderable: orderable, completion: completion)
        let controller = DishViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        controller.imagePlaceholder = imagePlaceholder
        return controller
    }

    func comboViewController(
        combo: Combo,
        orderable: Bool,
        imagePlaceholders: [UIImage?],
        completion: @escaping (ComboAction) -> ()
    ) -> ComboViewController {
        let presenter = ComboPresenter(
            combo: combo,
            orderable: orderable,
            completion: completion
        )
        let controller = ComboViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        controller.imagePlaceholders = imagePlaceholders
        return controller
    }
}
