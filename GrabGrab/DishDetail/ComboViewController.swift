//
//  ComboViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

final class ComboViewController: UIViewController, PopupViewController {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!

    @IBOutlet private weak var etrLabel: UILabel!
    @IBOutlet private weak var hideEtrConstraint: NSLayoutConstraint!
    @IBOutlet private weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var topOffsetConstraint: NSLayoutConstraint!

    @IBOutlet private weak var photosContainerView: UIView!
    @IBOutlet private weak var comboNameLabel: UILabel!
    @IBOutlet private weak var dishesListLabel: UILabel!
    @IBOutlet private weak var originalPriceLabel: UILabel!

    @IBOutlet private weak var dishesTableView: UITableView!
    @IBOutlet private weak var dishesTableViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet private weak var addButton: UIButton!

    var presenter: ComboPresenterType!
    var imagePlaceholders: [UIImage?] = []
    var photosView = ComboPhotosView.fromNib()

    private var defaultImageHeight: CGFloat = 375
    private let minImageHeight: CGFloat = 40

    private var tableViewHeightObserver: NSKeyValueObservation?

    override func viewDidLoad() {
        super.viewDidLoad()
        defaultImageHeight = min(UIScreen.main.bounds.width, 375)
        imageHeightConstraint.constant = defaultImageHeight
        topOffsetConstraint.constant = defaultImageHeight - 40
        configureView()
    }

    private func configureView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        photosContainerView.addGestureRecognizer(panGestureRecognizer)

        photosContainerView.fill(with: photosView)
        photosView.configure(with: presenter.state.photos, placeholders: imagePlaceholders)

        comboNameLabel.text = presenter.state.name
        originalPriceLabel.attributedText = presenter.state.originalPrice
        dishesListLabel.text = LocalizedStrings.Combo.consistLabel
        etrLabel.text = presenter.state.etr
        hideEtrConstraint.priority = presenter.state.isEtrHidden ? .defaultHigh : .defaultLow

        addButton.setTitle(presenter.state.buttonTitle, for: .normal)
        addButton.isHidden = presenter.state.isButtonHidden

        dishesTableView.register(cellType: ComboDishCell.self)
        tableViewHeightObserver = dishesTableView.observe(\.contentSize) {[weak self] _, _ in
            guard let self = self else { return }
            self.dishesTableViewHeightConstraint.constant = self.dishesTableView.contentSize.height
        }
    }

    @objc func close() {
        presenter.close()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }

    @IBAction private func addButtonTapped() {
        presenter.addToCart()
    }
}

extension ComboViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        imageHeightConstraint.constant = max(minImageHeight, defaultImageHeight - scrollView.contentOffset.y)
    }
}

extension ComboViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.state.dishes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ComboDishCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(
            model: ComboDishCellModel(
                dish: presenter.state.dishes[indexPath.row],
                isSeparatorHidden: indexPath.row == 0
            )
        )
        return cell
    }
}
extension ComboViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let images = photosView.images
        let placeholder = images.count > indexPath.row ? images[indexPath.row] : nil
        presenter.showDish(
            presenter.state.dishes[indexPath.row],
            placeholder: placeholder
        )
    }
}
