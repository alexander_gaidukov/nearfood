//
//  DebugViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class DebugViewController: UIViewController {

    @IBOutlet private weak var segmentedControl: UISegmentedControl!

    var presenter: DebugPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        bindingViews()
    }

    private func configureViews() {
        title = "Debug"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneTapped))
        if #available(iOS 13.0, *) {
            segmentedControl.selectedSegmentTintColor = .primary
        } else {
            segmentedControl.tintColor = .primary
        }

    }

    private func bindingViews() {
        presenter.state.$selectedIndex.bind(to: segmentedControl, keyPath: \.selectedSegmentIndex).addToDisposableBag(disposableBag)
    }

    @IBAction private func changeAPISelected() {
        presenter.select(index: segmentedControl.selectedSegmentIndex)
    }

    @IBAction private func shareAPIErrorsTapped() {
        presenter.shareAPIErrors()
    }

    @objc private func doneTapped() {
        presenter.done()
    }

    @objc private func cancelTapped() {
        presenter.cancel()
    }
}
