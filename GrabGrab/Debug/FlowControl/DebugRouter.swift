//
//  DebugRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit
protocol DebugRouterType {
    func showDebug(completion: @escaping (DebugAction) -> ())
    func dismiss(completion: @escaping () -> ())
    func showShare(url: URL)
}

final class DebugRouter: DebugRouterType {

    private let factory: DebugViewControllersFactoryType
    private let presentingViewController: UIViewController

    init(factory: DebugViewControllersFactoryType, presentingViewController: UIViewController) {
        self.factory = factory
        self.presentingViewController = presentingViewController
    }

    func showDebug(completion: @escaping (DebugAction) -> ()) {
        let controller = factory.debugViewController(completion: completion)
        let navController = GGNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        topViewController.present(navController, animated: true, completion: nil)
    }

    func dismiss(completion: @escaping () -> ()) {
        topViewController.dismiss(animated: true, completion: completion)
    }

    func showShare(url: URL) {
        let controller = factory.shareViewController(url: url)
        topViewController.present(controller, animated: true, completion: nil)
    }

    private var topViewController: UIViewController {
        var topCandidate = presentingViewController
        while let controller = topCandidate.presentedViewController {
            topCandidate = controller
        }
        return topCandidate
    }
}
