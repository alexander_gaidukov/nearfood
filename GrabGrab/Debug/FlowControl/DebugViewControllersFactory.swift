//
//  DebugViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol DebugViewControllersFactoryType {
    func debugViewController(completion: @escaping (DebugAction) -> ()) -> DebugViewController
    func shareViewController(url: URL) -> UIViewController
}

struct DebugViewControllersFactory: DebugViewControllersFactoryType {

    private let storyboard: Storyboard = .debug

    func debugViewController(completion: @escaping (DebugAction) -> ()) -> DebugViewController {
        let presenter = DebugPresenter(completion: completion)
        let controller = DebugViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func shareViewController(url: URL) -> UIViewController {
        UIActivityViewController(activityItems: [url], applicationActivities: nil)
    }
}
