//
//  DebugCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class DebugCoordinator: Coordinator {

    private var completion: (Environment?) -> ()

    private let router: DebugRouterType
    private let logger: LoggerType

    init(router: DebugRouterType, logger: LoggerType, completion: @escaping (Environment?) -> ()) {
        self.completion = completion
        self.router = router
        self.logger = logger
    }

    func start() {
        router.showDebug { action in
            switch action {
            case .cancel:
                self.router.dismiss {
                    self.completion(nil)
                }
            case .done(let env):
                self.router.dismiss {
                    self.completion(env)
                }
            case .shareAPIErrors:
                self.shareAPIErrors()
            }
        }
    }

    private func shareAPIErrors() {
        // swiftlint:disable:next force_unwrapping
        let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let url = documentsURL.appendingPathComponent("APIErrors.json")
        if FileManager.default.fileExists(atPath: url.path) {
            try? FileManager.default.removeItem(atPath: url.path)
        }
        guard let data = logger.apiErrorsData else { return }
        try? data.write(to: url)
        router.showShare(url: url)
    }
}
