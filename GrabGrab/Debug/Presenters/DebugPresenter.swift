//
//  DebugPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol DebugPresenterType {
    var state: DebugViewState { get }
    func select(index: Int)
    func shareAPIErrors()
    func cancel()
    func done()
}

struct DebugViewState {
    @Observable
    var selectedIndex: Int
}

enum DebugAction {
    case cancel
    case done(Environment)
    case shareAPIErrors
}

final class DebugPresenter: DebugPresenterType {
    private(set) var state: DebugViewState
    private let completion: (DebugAction) -> ()
    init(completion: @escaping (DebugAction) -> ()) {
        self.completion = completion
        // swiftlint:disable:next force_unwrapping
        state = DebugViewState(selectedIndex: Environment.allCases.firstIndex(of: Constants.Environments.api)!)
    }

    func select(index: Int) {
        state.selectedIndex = index
    }

    func cancel() {
        completion(.cancel)
    }

    func done() {
        completion(.done(Environment.allCases[state.selectedIndex]))
    }

    func shareAPIErrors() {
        completion(.shareAPIErrors)
    }
}
