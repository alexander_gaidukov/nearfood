//
//  SurgeViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class SurgeViewController: UIViewController, PopupViewController {
    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!

    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var confirmButton: UIButton!
    @IBOutlet private weak var buttonsView: UIView!
    @IBOutlet private weak var hideButtonsConstraint: NSLayoutConstraint!

    var presenter: SurgePresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        confirmButton.setTitle(LocalizedStrings.Surge.Confirnation.confirm, for: .normal)
        cancelButton.setTitle(LocalizedStrings.Surge.Confirnation.cancel, for: .normal)
        buttonsView.isHidden = presenter.state.isButtonsHidden
        hideButtonsConstraint.priority = presenter.state.isButtonsHidden ? .defaultHigh : .defaultLow
        priceLabel.font = UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .title2).pointSize)
        priceLabel.text = presenter.state.priceAndDelay
        titleLabel.text = LocalizedStrings.Surge.title
        descriptionLabel.text = LocalizedStrings.Surge.description
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        contentView.addGestureRecognizer(panGestureRecognizer)
    }

    @IBAction private func cancelTapped() {
        presenter.close()
    }

    @IBAction private func confirmTapped() {
        presenter.confirm()
    }

    @objc func close() {
        presenter.close()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }
}
