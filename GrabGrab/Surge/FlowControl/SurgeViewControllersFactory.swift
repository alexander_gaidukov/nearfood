//
//  SurgeViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol SurgeViewControllersFactoryType {
    func surgeViewController(surge: Surge, showButtons: Bool, completion: @escaping (SurgeAction) -> ()) -> SurgeViewController
}

struct SurgeViewControllersFactory: SurgeViewControllersFactoryType {

    private let storyboard: Storyboard = .surge

    func surgeViewController(surge: Surge, showButtons: Bool, completion: @escaping (SurgeAction) -> ()) -> SurgeViewController {
        let presenter = SurgePresenter(surge: surge, showButtons: showButtons, completion: completion)
        let controller = SurgeViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
