//
//  SurgeCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class SurgeCoordinator: Coordinator {

    private let completion: (Bool) -> ()
    private let router: SurgeRouterType
    private let surge: Surge
    private let showButtons: Bool

    init(surge: Surge, showButtons: Bool, router: SurgeRouterType, completion: @escaping (Bool) -> ()) {
        self.surge = surge
        self.router = router
        self.showButtons = showButtons
        self.completion = completion
    }

    func start() {
        router.showSurge(surge: surge, showButtons: showButtons) { action in
            self.router.dismiss {
                switch action {
                case .complete:
                    self.completion(false)
                case .confirm:
                    self.completion(true)
                }
            }
        }
    }
}
