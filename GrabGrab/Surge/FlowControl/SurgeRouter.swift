//
//  SurgeRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol SurgeRouterType {
    func showSurge(surge: Surge, showButtons: Bool, completion: @escaping (SurgeAction) -> ())
    func dismiss(completion: @escaping () -> ())
}

final class SurgeRouter: NSObject, SurgeRouterType {

    private weak var presentingViewController: UIViewController?
    private var factory: SurgeViewControllersFactoryType

    init(presentingViewController: UIViewController?, factory: SurgeViewControllersFactoryType) {
        self.presentingViewController = presentingViewController
        self.factory = factory
    }

    func showSurge(surge: Surge, showButtons: Bool, completion: @escaping (SurgeAction) -> ()) {
        let controller = factory.surgeViewController(surge: surge, showButtons: showButtons, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        presentingViewController?.present(controller, animated: true, completion: nil)
    }

    func dismiss(completion: @escaping () -> ()) {
        presentingViewController?.dismiss(animated: true, completion: completion)
    }
}

// MARK: - Transitioning Delegates

extension SurgeRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
