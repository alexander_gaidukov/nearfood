//
//  SurgePresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol SurgePresenterType {
    var state: SurgeViewState { get }

    func close()
    func confirm()
}

enum SurgeAction {
    case complete
    case confirm
}

struct SurgeViewState {
    var priceAndDelay: String?
    var isButtonsHidden: Bool
}

final class SurgePresenter: SurgePresenterType {
    private let completion: (SurgeAction) -> ()

    private(set) var state: SurgeViewState

    init(surge: Surge, showButtons: Bool, completion: @escaping (SurgeAction) -> ()) {
        self.completion = completion

        self.state = SurgeViewState(
            priceAndDelay: .priceAndDelay(
                from: surge.price,
                delay: surge.delay,
                pricePlusPreposition: true
            ),
            isButtonsHidden: !showButtons
        )
    }

    func close() {
        completion(.complete)
    }

    func confirm() {
        completion(.confirm)
    }
}
