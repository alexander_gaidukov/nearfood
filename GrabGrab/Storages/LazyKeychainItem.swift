//
//  LazyKeychainItem.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 26.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

@propertyWrapper
struct LazyKeychainItem<Value: Codable> {

    private let decoder: JSONDecoder
    private let encoder: JSONEncoder
    private let key: StorageKey
    private let emptyValue: () -> Value

    var wrappedValue: Value {
        if let value = Keychain.load(key: key.value)
            .flatMap({ try? decoder.decode(StorageItemWrapper<Value>.self, from: $0) })?.item {
            return value
        }

        let newValue = emptyValue()
        if let data = try? encoder.encode(StorageItemWrapper(item: newValue)) {
            Keychain.save(key: key.value, value: data)
        }
        return newValue
    }
    init(
        key: StorageKey,
        emptyValue: @escaping @autoclosure () -> Value,
        encoder: JSONEncoder = Coders.encoder,
        decoder: JSONDecoder = Coders.decoder
    ) {
        self.key = key
        self.emptyValue = emptyValue
        self.encoder = encoder
        self.decoder = decoder
    }
}
