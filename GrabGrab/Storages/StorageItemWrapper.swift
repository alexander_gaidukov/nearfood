//
//  StorageItemWrapper.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct StorageItemWrapper<Value: Codable>: Codable {
    var item: Value
}

protocol AnyOptional {
    var isNil: Bool { get }
}

extension Optional: AnyOptional {
    var isNil: Bool { self == nil }
}
