//
//  Keychain.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class Keychain {

    private static let accessGroup = "group.com.nearfood.customer"
    private static let service = "com.nearfood.customer"

    // MARK: - Private methods ->

    private class func setupSearchDictionary(forIdentifier identifier: String) -> [String: Any] {
        var dictionary: [String: Any] = [kSecClass as String: kSecClassGenericPassword]
        dictionary[kSecAttrService as String] = service

        // swiftlint:disable:next force_unwrapping
        let  encodedIdentifier = identifier.data(using: String.Encoding.utf8)!

        dictionary[kSecAttrGeneric as String] = encodedIdentifier
        dictionary[kSecAttrAccount as String] = encodedIdentifier
        dictionary[kSecAttrAccessGroup as String] = accessGroup

        return dictionary
    }

    private class func searchKeychainCopy(matchingIdentifier identifier: String) -> Data? {

        var searchDictionary = Keychain.setupSearchDictionary(forIdentifier: identifier)

        searchDictionary[kSecMatchLimit as String] = kSecMatchLimitOne

        searchDictionary[kSecReturnData as String] = kCFBooleanTrue

        var foundDict: AnyObject?
        let status: OSStatus = withUnsafeMutablePointer(to: &foundDict) {
            SecItemCopyMatching(searchDictionary as CFDictionary, UnsafeMutablePointer($0))
        }

        let data = status == noErr ? foundDict as? Data : nil
        return data
    }

    private class func updateKeychainValue(value: Data, forIdentifier identifier: String) -> Bool {
        let searchDictionary = Keychain.setupSearchDictionary(forIdentifier: identifier)

        let updateDictionary: [String: Any] = [kSecValueData as String: value]

        let status = SecItemUpdate(searchDictionary as CFDictionary, updateDictionary as CFDictionary)

        return status == errSecSuccess
    }

    // MARK: - Facade methods ->

    class func load(key: String) -> Data? {
        Keychain.searchKeychainCopy(matchingIdentifier: key)
    }

    @discardableResult
    class func save(key: String, value: Data) -> Bool {
        var dictionary = Keychain.setupSearchDictionary(forIdentifier: key)

        dictionary[kSecAttrAccessible as String] = kSecAttrAccessibleAfterFirstUnlock
        dictionary[kSecValueData as String] = value

        let status = SecItemAdd(dictionary as CFDictionary, nil)

        if status == errSecSuccess {
            return true
        }

        if status == errSecDuplicateItem {
            return Keychain.updateKeychainValue(value: value, forIdentifier: key)
        }

        return false
    }

    class func delete(key: String) {
        let dictionary = Keychain.setupSearchDictionary(forIdentifier: key)

        SecItemDelete(dictionary as CFDictionary)
    }

}
