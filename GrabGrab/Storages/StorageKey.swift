//
//  StorageKey.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 26.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct StorageKey {
    private let baseKey: String
    private let domainSpecific: Bool

    var value: String {
        // swiftlint:disable:next force_unwrapping
        domainSpecific ? baseKey + "." + API.baseURL.host! : baseKey
    }

    init(_ baseKey: String, domainSpecific: Bool = false) {
        self.baseKey = baseKey
        self.domainSpecific = domainSpecific
    }
}
