//
//  UserDefaultsItem.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

@propertyWrapper
struct UserDefaultsItem<Value: Codable> {

    private let decoder: JSONDecoder
    private let encoder: JSONEncoder
    private let key: StorageKey
    private let defaultValue: Value
    private let container: UserDefaults

    var wrappedValue: Value {
        get {
            container.data(forKey: key.value)
                .flatMap { try? decoder.decode(StorageItemWrapper<Value>.self, from: $0) }?.item
                ?? defaultValue
        }

        set {
            if let optional = newValue as? AnyOptional, optional.isNil {
                container.removeObject(forKey: key.value)
                return
            }

            if let data = try? encoder.encode(StorageItemWrapper(item: newValue)) {
                container.set(data, forKey: key.value)
            } else {
               container.removeObject(forKey: key.value)
            }
        }
    }
    init(
        key: StorageKey,
        defaultValue: Value,
        encoder: JSONEncoder = Coders.encoder,
        decoder: JSONDecoder = Coders.decoder,
        container: UserDefaults = .standard
    ) {
        self.key = key
        self.defaultValue = defaultValue
        self.encoder = encoder
        self.decoder = decoder
        self.container = container
    }
}

extension UserDefaultsItem where Value: ExpressibleByNilLiteral {
    init(
        key: StorageKey,
        encoder: JSONEncoder = Coders.encoder,
        decoder: JSONDecoder = Coders.decoder,
        container: UserDefaults = .standard
    ) {
        self.init(key: key, defaultValue: nil, encoder: encoder, decoder: decoder, container: container)
    }
}
