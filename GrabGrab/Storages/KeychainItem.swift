//
//  KeychainItem.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 26.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

@propertyWrapper
struct KeychainItem<Value: Codable> {

    private let decoder: JSONDecoder
    private let encoder: JSONEncoder
    private let key: StorageKey
    private let defaultValue: Value

    var wrappedValue: Value {
        get {
            Keychain.load(key: key.value)
                .flatMap { try? decoder.decode(StorageItemWrapper<Value>.self, from: $0) }?.item
                ?? defaultValue
        }

        set {
            if let optional = newValue as? AnyOptional, optional.isNil {
                Keychain.delete(key: key.value)
                return
            }

            if let data = try? encoder.encode(StorageItemWrapper(item: newValue)) {
                Keychain.save(key: key.value, value: data)
            } else {
                Keychain.delete(key: key.value)
            }
        }
    }

    init(
        key: StorageKey,
        defaultValue: Value,
        encoder: JSONEncoder = Coders.encoder,
        decoder: JSONDecoder = Coders.decoder
    ) {
        self.key = key
        self.defaultValue = defaultValue
        self.encoder = encoder
        self.decoder = decoder
    }
}

extension KeychainItem where Value: ExpressibleByNilLiteral {
    init(
        key: StorageKey,
        encoder: JSONEncoder = Coders.encoder,
        decoder: JSONDecoder = Coders.decoder
    ) {
        self.init(key: key, defaultValue: nil, encoder: encoder, decoder: decoder)
    }
}
