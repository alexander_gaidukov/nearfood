//
//  CountriesPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CountriesPresenterType {
    var state: CountriesViewState { get }
    func select(country: Country)
    func close()
}

struct CountriesViewState {
    @Observable
    var countries: [Country]
    var selectedCountry: Country?
}

enum CountriesAction {
    case select(Country)
    case close
}

final class CountriesPresenter: CountriesPresenterType {

    private let completion: (CountriesAction) -> ()

    private(set) var state: CountriesViewState

    private let phoneCodesManager: PhoneCodesManagerType

    init(phoneCodesManager: PhoneCodesManagerType, completion: @escaping (CountriesAction) -> ()) {
        self.phoneCodesManager = phoneCodesManager
        state = CountriesViewState(countries: phoneCodesManager.countries, selectedCountry: phoneCodesManager.selectedCountry)
        self.completion = completion
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(phoneCodesDidChange),
            name: .phoneCodesDidChangeNotification,
            object: nil
        )
        phoneCodesManager.refresh()
    }

    func select(country: Country) {
        completion(.select(country))
    }

    func close() {
        completion(.close)
    }

    @objc private func phoneCodesDidChange() {
        state.countries = phoneCodesManager.countries
    }
}
