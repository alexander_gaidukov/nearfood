//
//  CountryCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var checkmarkImageView: UIImageView!

    func configure(country: Country, isSelected: Bool) {
        nameLabel.text = country.countryName
        codeLabel.text = country.phoneCode
        checkmarkImageView.image = isSelected ? #imageLiteral(resourceName: "checkbox_selected") : #imageLiteral(resourceName: "checkbox")
    }
}
