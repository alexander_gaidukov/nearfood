//
//  CountriesViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CountriesViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    var presenter: CountriesPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    private func configureView() {
        title = LocalizedStrings.Countries.title
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(closeTapped))
        tableView.register(cellType: CountryCell.self)
        tableView.tableFooterView = UIView(frame: .zero)
    }

    private func configureBindings() {
        presenter.state.$countries.observe { [weak self] _ in self?.tableView.reloadData() }.addToDisposableBag(disposableBag)
    }

    @objc private func closeTapped() {
        presenter.close()
    }
}

extension CountriesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.state.countries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CountryCell = tableView.dequeue(forIndexPath: indexPath)
        let country = presenter.state.countries[indexPath.row]
        cell.configure(country: country, isSelected: country == presenter.state.selectedCountry)
        return cell
    }
}

extension CountriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.select(country: presenter.state.countries[indexPath.row])
    }
}
