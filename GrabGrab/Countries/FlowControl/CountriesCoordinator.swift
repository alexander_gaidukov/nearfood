//
//  CountriesCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class CountriesCoordinator: Coordinator {
    var completion: (Country?) -> ()
    private let router: CountriesRouterType

    init(
        router: CountriesRouterType,
        completion: @escaping (Country?) -> ()
    ) {
        self.router = router
        self.completion = completion
    }

    func start() {
        router.showCountries { action in
            self.router.dismiss()
            switch action {
            case .close:
                self.completion(nil)
            case .select(let country):
                self.completion(country)
            }
        }
    }
}
