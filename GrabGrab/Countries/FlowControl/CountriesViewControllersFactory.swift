//
//  CountriesViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol CountriesViewControllersFactoryType {
    func countriesViewController(completion: @escaping (CountriesAction) -> ()) -> CountriesViewController
}

struct CountriesViewControllersFactory: CountriesViewControllersFactoryType {

    let storyboard: Storyboard = .countries

    let managersStorage: ManagersStorageType

    func countriesViewController(completion: @escaping (CountriesAction) -> ()) -> CountriesViewController {
        let presenter = CountriesPresenter(
            phoneCodesManager: managersStorage.phoneCodesManager,
            completion: completion
        )
        let controller = CountriesViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
