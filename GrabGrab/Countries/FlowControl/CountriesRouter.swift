//
//  CountriesRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol CountriesRouterType {
    func showCountries(completion: @escaping (CountriesAction) -> ())
    func dismiss()
}

final class CountriesRouter: CountriesRouterType {
    private weak var presentingController: UIViewController?
    private let factory: CountriesViewControllersFactoryType

    init(presentingController: UIViewController?, factory: CountriesViewControllersFactoryType) {
        self.presentingController = presentingController
        self.factory = factory
    }

    func showCountries(completion: @escaping (CountriesAction) -> ()) {
        let controller = factory.countriesViewController(completion: completion)
        let navController = GGNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        presentingController?.present(navController, animated: true, completion: nil)
    }

    func dismiss() {
        presentingController?.dismiss(animated: true, completion: nil)
    }
}
