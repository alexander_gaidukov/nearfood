//
//  RecipientsViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol RecipientsViewControllersFactoryType {
    func addOrUpdateRecipientViewController(
        for recipient: Recipient?,
        completion: @escaping (AddOrUpdateRecipientAction) -> ()
    ) -> AddOrUpdateRecipientViewController

    func phoneSelectionViewController(completion: @escaping (PhoneSelectionAction) -> ()) -> PhoneSelectionViewController
}

struct RecipientsViewControllersFactory: RecipientsViewControllersFactoryType {
    private let storyboard: Storyboard = .recipients

    let dataProvidersStorage: DataProvidersStorageType
    let managersStorage: ManagersStorageType

    func addOrUpdateRecipientViewController(
        for recipient: Recipient?,
        completion: @escaping (AddOrUpdateRecipientAction) -> ()
    ) -> AddOrUpdateRecipientViewController {
        let presenter = AddOrUpdateRecipientPresenter(
            recipient: recipient,
            phoneCodesManager: managersStorage.phoneCodesManager,
            completion: completion
        )
        let controller = AddOrUpdateRecipientViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func phoneSelectionViewController(completion: @escaping (PhoneSelectionAction) -> ()) -> PhoneSelectionViewController {
        let presenter = PhoneSelectionPresenter(
            phoneCodesManager: managersStorage.phoneCodesManager,
            contactsDataProvider: dataProvidersStorage.contactsDataProvider,
            completion: completion)
        let controller = PhoneSelectionViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
