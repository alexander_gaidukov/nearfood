//
//  RecipientsCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class RecipientsCoordinator: Coordinator {

    private let router: RecipientsRouterType
    private let completion: (Recipient?) -> ()
    private let recipient: Recipient?
    private var managersStorage: ManagersStorageType

    private var childCoordinator: Coordinator?

    init(
        recipient: Recipient?,
        router: RecipientsRouterType,
        managersStorage: ManagersStorageType,
        completion: @escaping (Recipient?) -> ()
    ) {
        self.router = router
        self.recipient = recipient
        self.managersStorage = managersStorage
        self.completion = completion
    }

    func start() {
        router.showAddOrUpdateRecipient(for: recipient) { action in
            switch action {
            case .close(let dismissed):
                if dismissed {
                    self.completion(self.recipient)
                } else {
                    self.router.dismiss {
                        self.completion(self.recipient)
                    }
                }
            case .changePhone(let completion):
                self.changePhone(completion: completion)
            case .select(let recipient):
                self.router.dismiss {
                    self.completion(recipient)
                }
            case .remove:
                self.router.dismiss {
                    self.completion(nil)
                }
            }
        }
    }

    private func changePhone(completion: @escaping (Recipient?) -> ()) {
        router.showPhoneSelectionViewController { action in
            switch action {
            case .close(let dismissed):
                if dismissed {
                    completion(nil)
                } else {
                    self.router.dismiss {
                        completion(nil)
                    }
                }
            case .select(let recipient):
                self.router.dismiss {
                    completion(recipient)
                }
            case .changeCountry(let completion):
                self.showCountriesList(completion: completion)
            case .openSettings:
                UIApplication.shared.openSettings()
            }
        }
    }

    private func showCountriesList(completion: @escaping (Country?) -> ()) {
        if let childCoordinator = childCoordinator, childCoordinator is CountriesCoordinator { return }

        let factory = CountriesViewControllersFactory(managersStorage: managersStorage)
        let router = CountriesRouter(
            presentingController: self.router.topViewController,
            factory: factory
        )

        let coordinator = CountriesCoordinator(router: router) { [weak self] country in
            if let country = country {
                self?.managersStorage.phoneCodesManager.select(country: country)
            }
            completion(country)
            self?.childCoordinator = nil
        }

        coordinator.start()
        childCoordinator = coordinator
    }
}
