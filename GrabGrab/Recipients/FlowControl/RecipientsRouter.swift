//
//  RecipientsRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol RecipientsRouterType {
    var topViewController: UIViewController? { get }

    func showAddOrUpdateRecipient(
        for recipient: Recipient?,
        completion: @escaping (AddOrUpdateRecipientAction) -> ()
    )

    func showPhoneSelectionViewController(completion: @escaping (PhoneSelectionAction) -> ())

    func dismiss(completion: @escaping () -> ())
}

final class RecipientsRouter: RecipientsRouterType {
    private weak var presentingViewController: UIViewController?
    private let factory: RecipientsViewControllersFactoryType

    init(presentingViewController: UIViewController?, factory: RecipientsViewControllersFactoryType) {
        self.presentingViewController = presentingViewController
        self.factory = factory
    }

    func showAddOrUpdateRecipient(for recipient: Recipient?, completion: @escaping (AddOrUpdateRecipientAction) -> ()) {
        let controller = factory.addOrUpdateRecipientViewController(for: recipient, completion: completion)
        let navController = GGPresentedNavigationController(rootViewController: controller)
        topViewController?.present(navController, animated: true, completion: nil)
    }

    func showPhoneSelectionViewController(completion: @escaping (PhoneSelectionAction) -> ()) {
        let controller = factory.phoneSelectionViewController(completion: completion)
        let navController = GGPresentedNavigationController(rootViewController: controller)
        topViewController?.present(navController, animated: true, completion: nil)
    }

    func dismiss(completion: @escaping () -> ()) {
        topViewController?.dismiss(animated: true, completion: completion)
    }

    var topViewController: UIViewController? {
        guard var topCandidate = presentingViewController else { return nil }
        while let controller = topCandidate.presentedViewController {
            topCandidate = controller
        }
        return topCandidate
    }
}
