//
//  NewPhoneCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct NewPhoneCellModel {
    let phone: PhoneNumber

    var isValid: Bool {
        phone.isValid
    }
}

class NewPhoneCell: UITableViewCell {
    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var checkboxImageView: UIImageView!

    func configure(with model: NewPhoneCellModel) {
        phoneLabel.text = model.phone.formattedNumber
        infoLabel.text = model.isValid ?
            LocalizedStrings.Recipient.AddOrUpdate.Phone.info
            : LocalizedStrings.Recipient.AddOrUpdate.Phone.fill
        checkboxImageView.isHidden = !model.isValid
    }
}
