//
//  PhonesSectionViewModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct PhonesSectionViewModel: TableViewSectionModel {

    let contactPhoneModels: [ContactPhoneCellModel]
    let newPhoneModel: NewPhoneCellModel?
    let didSelect: (Recipient) -> ()

    init(
        recipients: [(name: String, phone: PhoneNumber)],
        phone: PhoneNumber?,
        didSelect: @escaping (Recipient) -> ()
    ) {
        contactPhoneModels = recipients.map {
            ContactPhoneCellModel(name: $0.name, phone: $0.phone)
        }

        if recipients.isEmpty, let phone = phone {
            newPhoneModel = NewPhoneCellModel(phone: phone)
        } else {
            newPhoneModel = nil
        }

        self.didSelect = didSelect
    }

    var numberOfRows: Int {
        guard !contactPhoneModels.isEmpty else {
            return newPhoneModel == nil ? 0 : 1
        }

        return contactPhoneModels.count
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: ContactPhoneCell.self)
        tableView.register(cellType: NewPhoneCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < contactPhoneModels.count else {
            let cell: NewPhoneCell = tableView.dequeue(forIndexPath: indexPath)
            if let model = newPhoneModel {
                cell.configure(with: model)
            }
            return cell
        }

        let cell: ContactPhoneCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: contactPhoneModels[indexPath.row])
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        guard indexPath.row < contactPhoneModels.count else {
            if let model = newPhoneModel, model.isValid {
                let recipient = Recipient(
                    uuid: "",
                    phone: model.phone.rawValue,
                    name: ""
                )
                didSelect(recipient)
            }
            return
        }

        let model = contactPhoneModels[indexPath.row]
        let recipient = Recipient(
            uuid: "",
            phone: model.phone.rawValue,
            name: model.name
        )

        didSelect(recipient)
    }
}
