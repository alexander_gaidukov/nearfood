//
//  ContactPhoneCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct ContactPhoneCellModel {
    let name: String
    let phone: PhoneNumber
}

class ContactPhoneCell: UITableViewCell {
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!

    func configure(with model: ContactPhoneCellModel) {
        nameLabel.text = model.name
        phoneLabel.text = model.phone.formattedNumber
    }
}
