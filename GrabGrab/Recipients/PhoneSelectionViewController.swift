//
//  PhoneSelectionViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class PhoneSelectionViewController: UIViewController {

    @IBOutlet private weak var phoneCodeLabel: UILabel!
    @IBOutlet private weak var searchTextField: GGTextField!
    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet private weak var emptyView: UIView!
    @IBOutlet private weak var emptyViewTitle: UILabel!
    @IBOutlet private weak var emptyViewDescription: UILabel!
    @IBOutlet private weak var emptyViewButton: UIButton!
    @IBOutlet private weak var emptyViewButtonHideConstraint: NSLayoutConstraint!

    var presenter: PhoneSelectionPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    private func configureBindings() {
        presenter.state.$phonePrefix
            .map { Optional($0) }
            .bind(to: phoneCodeLabel, keyPath: \.text)
            .addToDisposableBag(disposableBag)

        presenter.state.$searchText
            .map { Optional($0) }
            .bind(to: searchTextField, keyPath: \.text)
            .addToDisposableBag(disposableBag)

        presenter.state.$models
            .observe { [weak self] _ in self?.tableView.reloadData() }
            .addToDisposableBag(disposableBag)

        presenter.state.$isEmptyViewHidden
            .bind(to: emptyView, keyPath: \.isHidden)
            .addToDisposableBag(disposableBag)

        presenter.state.$emptyViewTitle
            .bind(to: emptyViewTitle, keyPath: \.text)
            .addToDisposableBag(disposableBag)

        presenter.state.$emptyViewDescription
            .bind(to: emptyViewDescription, keyPath: \.text)
            .addToDisposableBag(disposableBag)

        presenter.state.$isEmptyViewButtonHidden.observe { [weak self] in
            self?.emptyViewButton.isHidden = $0
            self?.emptyViewButtonHideConstraint.priority = $0 ? .defaultHigh : .defaultLow
        }.addToDisposableBag(disposableBag)
    }

    private func configureViews() {
        if #available(iOS 13.0, *) {
            navigationController?.presentationController?.delegate = self
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                title: LocalizedStrings.Common.close,
                style: .plain,
                target: self,
                action: #selector(closeTapped)
            )
        }

        title = LocalizedStrings.Recipient.AddOrUpdate.Phone.title
        presenter.state.models.forEach { $0.registerCells(in: tableView) }
        tableView.tableFooterView = UIView(frame: .zero)
        emptyViewButton.setTitle(LocalizedStrings.Common.openSettings, for: .normal)
    }

    @objc private func closeTapped() {
        presenter.close(dismissed: false)
    }

    @IBAction private func changeCodeButtonTapped() {
        presenter.changeCountry()
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        presenter.changeSearch(textField.text ?? "")
    }

    @IBAction private func settingsButtonTapped() {
        presenter.openSettings()
    }
}

extension PhoneSelectionViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        presenter.close(dismissed: true)
    }
}

extension PhoneSelectionViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension PhoneSelectionViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.state.models.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.state.models[section].numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        presenter.state.models[indexPath.section].cell(tableView: tableView, indexPath: indexPath)
    }
}

extension PhoneSelectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        presenter.state.models[indexPath.section].didSelect(cell: nil, at: indexPath)
    }
}
