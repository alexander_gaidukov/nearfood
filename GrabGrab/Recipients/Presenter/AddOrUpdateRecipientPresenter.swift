//
//  AddOrUpdateRecipientPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AddOrUpdateRecipientPresenterType {
    var state: AddOrUpdateRecipientViewState { get }

    func close(dismissed: Bool)
    func changeName(_ name: String)
    func changePhone()
    func removeRecipient()
    func addRecipient()
}

enum AddOrUpdateRecipientAction {
    case close(Bool)
    case changePhone((Recipient?) -> ())
    case select(Recipient)
    case remove
}

struct AddOrUpdateRecipientViewState {
    @Observable
    var name: String?

    @Observable
    var phone: PhoneNumber?

    var isAddButtonEnabled: Observable<Bool> {
        $name.combine(with: $phone) {
            $0?.isEmpty == false && $1?.rawValue.isEmpty == false
        }
    }

    var isRemoveButtonHidden: Bool
}

final class AddOrUpdateRecipientPresenter: AddOrUpdateRecipientPresenterType {
    private let completion: (AddOrUpdateRecipientAction) -> ()
    private(set) var state: AddOrUpdateRecipientViewState

    private let phoneCodesManager: PhoneCodesManagerType
    private var nameHasBeenChanged = false

    init(recipient: Recipient?, phoneCodesManager: PhoneCodesManagerType, completion: @escaping (AddOrUpdateRecipientAction) -> ()) {
        self.completion = completion
        self.phoneCodesManager = phoneCodesManager
        self.state = AddOrUpdateRecipientViewState(
            name: recipient?.name,
            phone: (recipient?.phone).map { phoneCodesManager.phoneNumber(from: $0) },
            isRemoveButtonHidden: recipient == nil
        )
    }

    func close(dismissed: Bool) {
        completion(.close(dismissed))
    }

    func changeName(_ name: String) {
        if state.name != name {
            state.name = name
            nameHasBeenChanged = !name.isEmpty
        }
    }

    func changePhone() {
        completion(.changePhone({ [weak self] recipient in
            guard let self = self, let recipient = recipient else { return }
            self.state.phone = self.phoneCodesManager.phoneNumber(from: recipient.phone)
            if !self.nameHasBeenChanged {
                self.state.name = recipient.name
            }
        }))
    }

    func removeRecipient() {
        completion(.remove)
    }

    func addRecipient() {
        guard
            let name = state.name,
            let phone = state.phone?.rawValue,
            !name.isEmpty,
            !phone.isEmpty
        else { return }
        completion(.select(Recipient(uuid: "", phone: phone, name: name)))
    }
}
