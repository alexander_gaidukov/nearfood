//
//  PhoneSelectionPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol PhoneSelectionPresenterType {
    var state: PhoneSelectionViewState { get }
    func changeSearch(_ search: String)
    func changeCountry()
    func close(dismissed: Bool)
    func openSettings()
}

enum PhoneSelectionAction {
    case close(Bool)
    case select(Recipient)
    case changeCountry((Country?) -> ())
    case openSettings
}

struct PhoneSelectionViewState {
    @Observable
    var phonePrefix: String

    @Observable
    var searchText: String

    @Observable
    var models: [TableViewSectionModel]

    @Observable
    var isEmptyViewHidden: Bool

    @Observable
    var emptyViewTitle: String?

    @Observable
    var emptyViewDescription: String?

    @Observable
    var isEmptyViewButtonHidden: Bool
}

final class PhoneSelectionPresenter: PhoneSelectionPresenterType {
    private enum PhoneFieldState {
        case phone
        case search
        case unknown
    }

    private(set) var state: PhoneSelectionViewState
    private let completion: (PhoneSelectionAction) -> ()

    private let phoneCodesManager: PhoneCodesManagerType
    private let contactsDataProvider: ContactsDataProviderType

    private var phoneNumber: PhoneNumber
    private var recipients: [Recipient] = []

    private var phoneFieldState: PhoneFieldState = .unknown

    init(
        phoneCodesManager: PhoneCodesManagerType,
        contactsDataProvider: ContactsDataProviderType,
        completion: @escaping (PhoneSelectionAction) -> ()
    ) {
        self.completion = completion
        self.phoneCodesManager = phoneCodesManager
        self.contactsDataProvider = contactsDataProvider

        self.phoneNumber = PhoneNumber(country: phoneCodesManager.selectedCountry)

        state = PhoneSelectionViewState(
            phonePrefix: phoneCodesManager.selectedCountry.phoneCode,
            searchText: "",
            models: [],
            isEmptyViewHidden: true,
            emptyViewTitle: nil,
            emptyViewDescription: nil,
            isEmptyViewButtonHidden: false
        )

        createModels()
        loadContacts()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appDidBecomeActive),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
    }

    func changeCountry() {
        completion(.changeCountry({ [weak self] country in
            guard let country = country else { return }
            self?.state.phonePrefix = country.phoneCode
            self?.phoneNumber.country = country
            self?.createModels()
        }))
    }

    func changeSearch(_ search: String) {
        defer {
            createModels()
        }

        guard !search.isEmpty else {
            phoneNumber.setNumber(number: search)
            state.searchText = search
            phoneFieldState = .unknown
            return
        }

        switch phoneFieldState {
        case .phone:
            phoneNumber.setNumber(number: search)
            state.searchText = phoneNumber.formattedMask
            if phoneNumber.maskValue.isEmpty {
                phoneFieldState = .unknown
            }
        case .search:
            state.searchText = search
        case .unknown:
            phoneNumber.setNumber(number: search)
            if phoneNumber.formattedMask.isEmpty {
                state.searchText = search
                phoneFieldState = .search
            } else {
                state.searchText = phoneNumber.formattedMask
                phoneFieldState = .phone
            }
        }
    }

    func close(dismissed: Bool) {
        completion(.close(dismissed))
    }

    func openSettings() {
        completion(.openSettings)
    }

    @objc private func appDidBecomeActive() {
        loadContacts()
    }

    private func createModels() {
        let allowsPrefixes = phoneCodesManager.countries.map(\.phoneCode)

        var recipients = self.recipients.filter {
            for prefix in allowsPrefixes {
                if $0.phone.hasPrefix(prefix) { return true }
            }
            return false
        }
        .map {
            (name: $0.name, phone: self.phoneCodesManager.phoneNumber(from: $0.phone))
        }
        .filter { $0.phone.isValid }

        let search: String
        switch phoneFieldState {
        case .phone:
            search = phoneNumber.maskValue
        case .search:
            search = state.searchText
        case .unknown:
            search = ""
        }

        if !search.isEmpty {
            recipients = recipients.filter {
                $0.name.lowercased().contains(search.lowercased()) || $0.phone.maskValue.contains(search)
            }
        }

        let phonesSectionModel = PhonesSectionViewModel(
            recipients: recipients,
            phone: phoneFieldState != .search ? phoneNumber : nil
        ) { [weak self] in self?.select($0) }

        state.models = [phonesSectionModel]
        updateEmptyState(isEmpty: recipients.isEmpty)
    }

    private func updateEmptyState(isEmpty: Bool) {
        if !contactsDataProvider.isEnabled {
            state.emptyViewTitle = LocalizedStrings.Recipient.AddOrUpdate.Phone.Access.title
            state.emptyViewDescription = LocalizedStrings.Recipient.AddOrUpdate.Phone.Access.description
            state.isEmptyViewButtonHidden = false
            state.isEmptyViewHidden = false
        } else if isEmpty && phoneFieldState == .search {
            state.emptyViewTitle = LocalizedStrings.Recipient.AddOrUpdate.Phone.Empty.title
            state.emptyViewDescription = LocalizedStrings.Recipient.AddOrUpdate.Phone.Empty
                .description(for: state.searchText)
            state.isEmptyViewButtonHidden = true
            state.isEmptyViewHidden = false
        } else {
            state.isEmptyViewHidden = true
        }
    }

    private func loadContacts() {
        contactsDataProvider.fetchContacts { [weak self] in
            self?.recipients = $0
            self?.createModels()
        }
    }

    private func select(_ recipient: Recipient) {
        completion(.select(recipient))
    }
}
