//
//  AddOrUpdateRecipientViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class AddOrUpdateRecipientViewController: UIViewController {

    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var nameTextField: GGTextField!
    @IBOutlet private weak var phoneTextField: GGTextField!

    @IBOutlet private weak var deleteRecipientButton: UIButton!
    @IBOutlet private weak var addRecipientButton: UIButton!

    var presenter: AddOrUpdateRecipientPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    private func configureBindings() {
        presenter.state.$name.bind(to: nameTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$phone.map(\.?.formattedNumber)
            .bind(to: phoneTextField, keyPath: \.text)
            .addToDisposableBag(disposableBag)
        presenter.state.isAddButtonEnabled
            .bind(to: addRecipientButton, keyPath: \.isEnabled)
            .addToDisposableBag(disposableBag)
    }

    private func configureViews() {
        if #available(iOS 13.0, *) {
            navigationController?.presentationController?.delegate = self
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                title: LocalizedStrings.Common.close,
                style: .plain,
                target: self,
                action: #selector(closeTapped)
            )
        }

        title = LocalizedStrings.Recipient.AddOrUpdate.title
        descriptionLabel.text = LocalizedStrings.Recipient.AddOrUpdate.description
        nameTextField.placeholder = LocalizedStrings.Recipient.AddOrUpdate.name
        phoneTextField.placeholder = LocalizedStrings.Recipient.AddOrUpdate.phone
        deleteRecipientButton.setTitle(LocalizedStrings.Recipient.AddOrUpdate.remove, for: .normal)
        addRecipientButton.setTitle(LocalizedStrings.Recipient.AddOrUpdate.add, for: .normal)

        deleteRecipientButton.isHidden = presenter.state.isRemoveButtonHidden
    }

    @objc private func closeTapped() {
        presenter.close(dismissed: false)
    }

    @IBAction private func deleteButtonTapped() {
        presenter.removeRecipient()
    }

    @IBAction private func addButtonTapped() {
        presenter.addRecipient()
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        presenter.changeName(textField.text ?? "")
    }

    @IBAction private func changePhoneButtonTapped() {
        presenter.changePhone()
    }
}

extension AddOrUpdateRecipientViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddOrUpdateRecipientViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        presenter.close(dismissed: true)
    }
}
