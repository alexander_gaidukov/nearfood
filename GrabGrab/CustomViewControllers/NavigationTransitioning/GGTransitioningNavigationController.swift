//
//  GGTransitioningNavigationController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class GGTransitioningNavigationController: GGNavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = TransitionCoordinator.shared
    }
}
