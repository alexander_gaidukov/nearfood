//
//  DrawerViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import Pulley

class DrawerViewController: UIViewController {

    @IBOutlet private weak var containerView: UIView!

    var viewController: UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        insertViewController(viewController, toView: containerView)
    }
}

extension DrawerViewController: PulleyDrawerViewControllerDelegate {

    private var defaultPartialRevealHeight: CGFloat { 264.0 }

    func supportedDrawerPositions() -> [PulleyPosition] {
        (viewController as? PulleyDrawerViewControllerDelegate)?
            .supportedDrawerPositions?()
            ?? PulleyPosition.all
    }

    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        (viewController as? PulleyDrawerViewControllerDelegate)?
            .partialRevealDrawerHeight?(bottomSafeArea: bottomSafeArea)
            ?? defaultPartialRevealHeight
    }
}
