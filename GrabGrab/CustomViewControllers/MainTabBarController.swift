//
//  MainTabBarController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol TabRootController {
    var identifier: String { get }
    var tabBarImage: UIImage { get }
    var tabBarSelectedImage: UIImage { get }
}

extension UINavigationController: TabRootController {
    var tabBarImage: UIImage {
        (viewControllers.first as? TabRootController)?.tabBarImage ?? UIImage()
    }

    var tabBarSelectedImage: UIImage {
        (viewControllers.first as? TabRootController)?.tabBarSelectedImage ?? UIImage()
    }

    var identifier: String {
        (viewControllers.first as? TabRootController)?.identifier ?? ""
    }
}

final class MainTabBarController: GGTabBarController {

    private var presenter: MainTabBarPresenterType
    private let disposableBag = DisposableBag()

    init(presenter: MainTabBarPresenterType) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private var cartButton: CartButton = {
        let button = CartButton(amount: 0)
        button.addTarget(self, action: #selector(cartButtonTapped), for: .touchUpInside)
        button.accessibilityIdentifier = Identifiers.TabBar.cart
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureBindings()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleNewMessages),
            name: .newMessagesDidArriveNotification,
            object: nil
        )
    }

    override func configureViews() {

        let cartItem = GGTabBarActionItem(customButton: cartButton)

        let controllers = presenter.tabCoordinators.map { $0.start() }

        var items: [GGTabBarItem] = controllers.enumerated().map {
            let item = GGTabBarSelectionItem(
                icon: $0.element.tabBarImage,
                selectedIcon: $0.element.tabBarSelectedImage,
                tabIndex: $0.offset,
                markerValue: presenter.markerValue(at: $0.offset)
            )
            item.accessibilityIdentifier = $0.element.identifier
            return .selection(item)
        }

        items.append(.action(cartItem))

        setTabBar(withItems: items)
        viewControllers = controllers
    }

    private func configureBindings() {
        presenter.state.$cartAmount.observe {[weak self] amount in
            self?.cartButton.configure(withAmount: amount)
        }.addToDisposableBag(disposableBag)
    }

    @objc private func cartButtonTapped() {
        presenter.showCart()
    }

    @objc private func handleNewMessages() {
        guard let index = viewControllers?.firstIndex(where: { $0 is ChatViewController }) else {
            return
        }
        showMarker(count: presenter.markerValue(at: index), index: index)
    }
}
