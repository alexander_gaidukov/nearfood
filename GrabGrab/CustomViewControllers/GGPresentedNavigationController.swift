//
//  GGPresentedNavigationController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class GGPresentedNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactivePopGestureRecognizer?.isEnabled = false
        if #available(iOS 15.0, *) {
            self.navigationBar.isTranslucent = true
        } else {
            self.navigationBar.isTranslucent = false
        }
        self.navigationBar.barTintColor = .background
        self.navigationBar.tintColor = .presentedNavigationTint
    }

}
