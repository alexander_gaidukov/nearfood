//
//  TableViewSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol TableViewSectionModel {
    var numberOfRows: Int { get }

    func registerCells(in tableView: UITableView)
    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell

    func commit(editingStyle: UITableViewCell.EditingStyle, at indexPath: IndexPath)
    func allowEditing(at indexPath: IndexPath) -> Bool

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath)
}

extension TableViewSectionModel {
    func commit(editingStyle: UITableViewCell.EditingStyle, at indexPath: IndexPath) {

    }

    func allowEditing(at indexPath: IndexPath) -> Bool {
        false
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {

    }
}
