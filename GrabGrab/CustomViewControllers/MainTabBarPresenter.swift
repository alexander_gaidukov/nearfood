//
//  MainTabBarPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol MainTabBarPresenterType {
    var state: MainTabBarState { get }
    var tabCoordinators: [TabCoordinator] { get }
    func showCart()
    func markerValue(at index: Int) -> Int
}

enum MainTabBarAction {
    case showCart
}

struct MainTabBarState {
    @Observable
    var cartAmount: Double
}

final class MainTabBarPresenter: MainTabBarPresenterType {

    let tabCoordinators: [TabCoordinator]
    private let cartDataProvider: CartDataProviderType
    private let messagesDataProvider: MessagesDataProviderType
    let completion: (MainTabBarAction) -> ()

    private(set) var state: MainTabBarState

    init(
        tabCoordinators: [TabCoordinator],
        cartDataProvider: CartDataProviderType,
        messagesDataProvider: MessagesDataProviderType,
        completion: @escaping (MainTabBarAction) -> ()
    ) {
        self.tabCoordinators = tabCoordinators
        self.cartDataProvider = cartDataProvider
        self.messagesDataProvider = messagesDataProvider
        self.completion = completion
        self.state = MainTabBarState(cartAmount: cartDataProvider.cart.displayPrice)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(cartDidChange),
            name: .cartDidChangeNotification,
            object: nil
        )
    }

    @objc private func cartDidChange() {
        state.cartAmount = cartDataProvider.cart.displayPrice
    }

    func showCart() {
        completion(.showCart)
    }

    func markerValue(at index: Int) -> Int {
        let chatIndex = tabCoordinators.firstIndex(where: { $0 is ChatCoordinator })
        return index == chatIndex ? messagesDataProvider.newMessagesCount : 0
    }
}
