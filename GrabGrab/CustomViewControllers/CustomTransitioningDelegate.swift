//
//  PopupTransitioningDelegate.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol CustomTransitioningViewController: AnyObject {
    func animateAppearance(duration: TimeInterval, completion: @escaping () -> ())
    func animateDismiss(duration: TimeInterval, completion: @escaping () -> ())
    func animationDidFinish()
}

extension CustomTransitioningViewController {
    func animationDidFinish() {}
}

protocol PopupViewController: CustomTransitioningViewController {
    var bgView: UIView! { get }
    var hideViewConstraint: NSLayoutConstraint! { get }
    var contentView: UIView! { get }
    func close()
}

extension PopupViewController where Self: UIViewController {
    func animateAppearance(duration: TimeInterval, completion: @escaping () -> ()) {
        bgView.alpha = 0.0
        view.layoutIfNeeded()
        hideViewConstraint.priority = .defaultLow
        UIView.animate(withDuration: duration, animations: {
            self.bgView.alpha = 1.0
            self.view.layoutIfNeeded()
        }, completion: {_ in
            self.animationDidFinish()
            completion()
        })
    }

    func animateDismiss(duration: TimeInterval, completion: @escaping () -> ()) {
        hideViewConstraint.priority = .defaultHigh
        UIView.animate(withDuration: duration, animations: {
            self.bgView.alpha = 0.0
            self.view.layoutIfNeeded()
        }, completion: {_ in
            completion()
        })
    }

    func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        if recognizer.state == .began {

        } else if recognizer.state == .changed {
            let translation = recognizer.translation(in: recognizer.view)
            var frame = contentView.frame
            frame.origin.y += translation.y
            if (frame.origin.y + frame.size.height) >= (view.frame.size.height + 16) {
                contentView.frame = frame
            }
            recognizer.setTranslation(.zero, in: recognizer.view)
        } else {

            let minimumFrameMove = (contentView.frame.height - 16) * 0.3
            let originalPosition = view.frame.height - (contentView.frame.height - 16)

            if (contentView.frame.origin.y - originalPosition) >= minimumFrameMove {
                self.close()
            } else {
                self.view.setNeedsLayout()
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}

final class CustomTransitioningDelegate: NSObject, UIViewControllerAnimatedTransitioning {
    var isPresenting: Bool
    init(isPresenting: Bool) {
        self.isPresenting = isPresenting
        super.init()
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        0.3
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let viewController = isPresenting ?
            transitionContext.viewController(forKey: .to)
            : transitionContext.viewController(forKey: .from)

        guard
            let transitioningController = viewController,
            let controller = (transitioningController as? UINavigationController)?.viewControllers.last ?? viewController,
            let animatingController = controller as? (UIViewController & CustomTransitioningViewController)
        else {
            return
        }

        let duration = transitionDuration(using: transitionContext)

        if isPresenting {
            containerView.addSubview(transitioningController.view)
            animatingController.animateAppearance(duration: duration) {
                transitionContext.completeTransition(true)
            }
        } else {
            animatingController.animateDismiss(duration: duration) {
                transitionContext.completeTransition(true)
            }
        }
    }
}
