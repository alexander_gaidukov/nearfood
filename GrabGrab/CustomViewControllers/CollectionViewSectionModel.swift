//
//  CollectionViewSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol CollectionViewSectionModel {
    var numberOfItems: Int { get }
    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell
    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize
    func didSelectItem(at indexPath: IndexPath)
}

extension CollectionViewSectionModel {
    func didSelectItem(at indexPath: IndexPath) {}
}
