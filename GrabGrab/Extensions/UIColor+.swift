//
// UIColor+.swift
// GrabGrab
//
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen
// !!!Do not change this file manually!!! 
// Add a new color to the Assests.xcassets and build the project. The file will be updated automatically.
//

import UIKit

extension UIColor {
  static let btnLogout = UIColor.color(named: "btn_logout")
  static let btnNavigation = UIColor.color(named: "btn_navigation")
  static let incomeMessage = UIColor.color(named: "income_message")
  static let outcomeMessage = UIColor.color(named: "outcome_message")
  static let outcomeMessageSecondary = UIColor.color(named: "outcome_message_secondary")
  static let errorBackground = UIColor.color(named: "error_background")
  static let errorBanner = UIColor.color(named: "error_banner")
  static let errorBorder = UIColor.color(named: "error_border")
  static let errorLabel = UIColor.color(named: "error_label")
  static let inputBackground = UIColor.color(named: "input_background")
  static let inputBorder = UIColor.color(named: "input_border")
  static let inputSeparator = UIColor.color(named: "input_separator")
  static let deactivatedPolygonBackground = UIColor.color(named: "deactivated_polygon_background")
  static let deactivatedPolygonBorder = UIColor.color(named: "deactivated_polygon_border")
  static let paidPolygonBackground = UIColor.color(named: "paid_polygon_background")
  static let paidPolygonBorder = UIColor.color(named: "paid_polygon_border")
  static let polygonBackground = UIColor.color(named: "polygon_background")
  static let polygonBorder = UIColor.color(named: "polygon_border")
  static let progressBackground = UIColor.color(named: "progress_background")
  static let progressBackgroundSelected = UIColor.color(named: "progress_background_selected")
  static let barTint = UIColor.color(named: "bar_tint")
  static let barTintInversed = UIColor.color(named: "bar_tint_inversed")
  static let presentedNavigationTint = UIColor.color(named: "presented_navigation_tint")
  static let tabbarBackground = UIColor.color(named: "tabbar_background")
  static let tabbarSeparator = UIColor.color(named: "tabbar_separator")
  static let tabbarText = UIColor.color(named: "tabbar_text")
  static let background = UIColor.color(named: "background")
  static let backgroundDisabled = UIColor.color(named: "background_disabled")
  static let badge = UIColor.color(named: "badge")
  static let blueBackground = UIColor.color(named: "blue_background")
  static let checkmark = UIColor.color(named: "checkmark")
  static let greenBackground = UIColor.color(named: "green_background")
  static let labelText = UIColor.color(named: "label_text")
  static let mainText = UIColor.color(named: "main_text")
  static let overlay = UIColor.color(named: "overlay")
  static let panelBackground = UIColor.color(named: "panel_background")
  static let pinBackground = UIColor.color(named: "pin_background")
  static let priceBackground = UIColor.color(named: "price_background")
  static let primary = UIColor.color(named: "primary")
  static let ratingBorder = UIColor.color(named: "rating_border")
  static let secondaryText = UIColor.color(named: "secondary_text")
  static let separator = UIColor.color(named: "separator")
  static let shadow = UIColor.color(named: "shadow")
  static let slider = UIColor.color(named: "slider")
  static let sliderSelected = UIColor.color(named: "slider_selected")
  static let textDisabled = UIColor.color(named: "text_disabled")
  static let timerBackground = UIColor.color(named: "timer_background")
  static let timerGreen = UIColor.color(named: "timer_green")

  private static func color(named: String) -> UIColor {
      UIColor(named: named) ?? .clear
  }
}
