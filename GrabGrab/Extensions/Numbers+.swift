//
//  Numbers+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 06.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

extension Int {
    var formattedTime: String {
        let minutes = self / 60
        let seconds = self % 60
        return String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
    }
}

extension Comparable {
    func constrained(maximum: Self, minimum: Self) -> Self {
        max(min(maximum, self), minimum)
    }
}
