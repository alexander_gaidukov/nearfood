//
//  UITableView+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

extension UITableView {
    func register<Cell: UITableViewCell>(cellType: Cell.Type) {
        register(
            UINib(nibName: String(describing: cellType), bundle: nil),
            forCellReuseIdentifier: String(describing: cellType)
        )
    }

    func dequeue<Cell: UITableViewCell>(forIndexPath indexPath: IndexPath) -> Cell {
        // swiftlint:disable:next force_cast
        dequeueReusableCell(withIdentifier: String(describing: Cell.self), for: indexPath) as! Cell
    }
}
