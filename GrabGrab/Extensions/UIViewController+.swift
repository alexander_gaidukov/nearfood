//
//  UIViewController+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

enum Storyboard: String {
    case main = "Main"
    case login = "Login"
    case onboarding = "Onboarding"
    case account = "Account"
    case cart = "Cart"
    case addresses = "Addresses"
    case payments = "Payments"
    case orders = "Orders"
    case chat = "Chat"
    case surge = "Surge"
    case feedback = "Feedback"
    case appVersions = "AppVersions"
    case welcome = "Welcome"
    case activation = "Activation"
    case intro = "Intro"
    case debug = "Debug"
    case invitations = "Invitations"
    case comment = "Comment"
    case dishDetail = "DishDetail"
    case recipients = "Recipients"
    case countries = "Countries"
    case tips = "Tips"
}

extension UIViewController {
    class func instance(storyboard: Storyboard = .main) -> Self {
        UIStoryboard(
            name: storyboard.rawValue,
            bundle: Bundle(for: Self.self)
        ) // swiftlint:disable:next force_cast
        .instantiateViewController(withIdentifier: String(describing: Self.self)) as! Self
    }
}

protocol KeyboardAvoiding: AnyObject {
    var contentView: UIView! { get }
}

extension KeyboardAvoiding {
    func configureKeyboardAvoiding() {
        IHKeyboardAvoiding.KeyboardAvoiding.avoidingView = contentView
    }
}

extension UIViewController {
    func insertViewController(_ viewController: UIViewController, toView view: UIView? = nil) {
        let containerView: UIView! = view ?? self.view
        viewController.willMove(toParent: self)
        containerView.fill(with: viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }

    func embededInDrawer() -> DrawerViewController {
        let drawer = DrawerViewController.instance()
        drawer.viewController = self
        return drawer
    }
}
