//
//  String+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

extension String {
    subscript (idx: Int) -> Character {
        self[index(startIndex, offsetBy: idx)]
    }

    func removing(charactersFrom characterSet: CharacterSet) -> String {
        let components = self.components(separatedBy: characterSet)
        return components.joined(separator: "")
    }
}

extension String {
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while let range = range(
                of: substring,
                options: options,
                range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex,
                locale: locale
        ) {
            ranges.append(range)
        }
        return ranges
    }

    func separate(each count: Int, with separator: String) -> String {
        enumerated().map {
            $0.offset > 0 && $0.offset.isMultiple(of: count) ? separator + String($0.element) : String($0.element)
        }.joined()
    }
}
