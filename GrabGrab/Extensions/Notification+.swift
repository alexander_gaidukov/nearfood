//
//  Notification+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 18.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

// MARK: - Push Notifications
extension Notification.Name {
    // Silent
    static let shouldUpdateOrdersNotification = Notification.Name("grab-grab.notifications.orders.update")
    static let shouldUpdateMessagesNotification = Notification.Name("grab-grab.notifications.messages.update")
    static let shouldUpdateSurgeNotification = Notification.Name("grab-grab.notifications.surge.update")
    static let shouldUpdateFeedbackNotification = Notification.Name("grab-grab.notifications.feedback.update")
    // Customer
    static let newMessageNotification = Notification.Name("grab-grab.notifications.messages.new")
    static let newFeedbackNotification = Notification.Name("grab-grab.notifications.feedbacks.new")
}

// MARK: - Phone Codes / Cities
extension Notification.Name {
    static let phoneCodesDidChangeNotification = Notification.Name("grab-grab.phonecodes.changed.notification")
    static let citiesDidChangeNotification = Notification.Name("grab-grab.cities.changed.notification")
}

// MARK: - Customer Changes
extension Notification.Name {
    static let locationDidChangeNotification = Notification.Name("grab-grab.location.changed.notification")
    static let paymentMethodDidChangeNotification = Notification.Name("grab-grab.paymentMethod.changed.notification")
    static let customerDidChangeNotification = Notification.Name("grab-grab.customer.changed.notification")
    static let bonusesDidChangeNotification = Notification.Name("grab-grab.bonuses.changed.notification")
    static let tipsDidChangeNotification = Notification.Name("grab-grab.tips.changed.notification")
}

// MARK: - Cart Changes
extension Notification.Name {
    static let cartDidChangeNotification = Notification.Name("grab-grab.cart.changed.notification")
}

// MARK: - Order Changes
extension Notification.Name {
    static let ordersDidChangeNotification = Notification.Name("grab-grab.orders.changed.notification")
    static let currentOrderDidChangeNotification = Notification.Name("grab-grab.orders.current.changed.notification")
    static let showOrderDetailsNotification = Notification.Name("grab-grab.orders.details.show.notification")
}

// MARK: - Chat Changes
extension Notification.Name {
    static let messagesDidChangeNotification = Notification.Name("grab-grab.messages.changed.notification")
    static let newMessagesDidArriveNotification = Notification.Name("grab-grab.messages.arrive.notificaition")
}

// MARK: - Storefront Changes
extension Notification.Name {
    static let storefrontDidChangeNotification = Notification.Name("grab-grab.storefront.changed.notificaition")
    static let storefrontStateDidChangeNotification = Notification.Name("grab-grab.storefront.state.changed.notificaition")
}

// MARK: - App Version State Changes
extension Notification.Name {
    static let appVersionStateDidChangeNotification = Notification.Name("grab-grab.appVersion.state.changed.notificaition")
}

// MARK: - Company Info Changes
extension Notification.Name {
    static let companyInfoDidChangeNotification = Notification.Name("grab-grab.companyInfo.changed.notificaition")
}

// MARK: - Invitations Changes
extension Notification.Name {
    static let receivedInvitationDidChangeNotification = Notification.Name("grab-grab.invitations.received.changed.notificaition")
    static let invitesDidChangeNotification = Notification.Name("grab-grab.invitations.changed.notificaition")
    static let invitesVisibilityDidChangeNotification = Notification.Name("grab-grab.invitations.visibility.changed.notificaition")
}
