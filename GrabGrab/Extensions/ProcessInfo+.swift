//
//  ProcessInfo+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

extension ProcessInfo {
    var isInTestMode: Bool {
        arguments.contains(Constants.UITests.testModeKey)
    }
}
