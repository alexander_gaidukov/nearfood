//
//  UIView+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            layer.cornerRadius
        }

        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable
    var borderWidth: CGFloat {
        get {
            layer.borderWidth
        }

        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable
    var borderColor: UIColor? {
        get {
            layer.borderColor.map(UIColor.init)
        }

        set {
            if #available(iOS 13.0, *) {
                traitCollection.performAsCurrent {
                    self.layer.borderColor = newValue?.cgColor
                }
            } else {
                self.layer.borderColor = newValue?.cgColor
            }
        }
    }
}

extension UIView {
    func fill(with view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        NSLayoutConstraint.activate([
            view.trailingAnchor.constraint(equalTo: trailingAnchor),
            view.leadingAnchor.constraint(equalTo: leadingAnchor),
            view.topAnchor.constraint(equalTo: topAnchor),
            view.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}

extension UIView {
    class func fromNib() -> Self {
        Bundle(for: Self.self).loadNibNamed(
            String(describing: Self.self),
            owner: nil,
            options: nil
        )!.first as! Self // swiftlint:disable:this force_cast force_unwrapping
    }
}

extension UIBarButtonItem {
    static func barButtonItem(withTitle title: String, target: Any?, selector: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .system)
        button.setTitleColor(.btnNavigation, for: .normal)
        button.setTitleColor(.textDisabled, for: .disabled)
        button.setTitle(title, for: .normal)
        button.addTarget(target, action: selector, for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
}

extension UIApplication {
    var safeAreaInsets: UIEdgeInsets {
        windows[0].safeAreaInsets
    }
}

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        return machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
    }
}
