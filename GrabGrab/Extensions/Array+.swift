//
//  Array+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

extension Array {
    func get(at index: Index) -> Element? {
        guard index >= startIndex && index < endIndex else { return nil }
        return self[index]
    }
}

extension Array {
    func asyncFirst(
        where predicate: @escaping (Element, @escaping (Bool) -> ()) -> (),
        completion: @escaping (Element?) -> ()
    ) {
        recursiveAsyncFirst(where: predicate, completion: completion)
    }

    private func recursiveAsyncFirst(
        where predicate: @escaping (Element, @escaping (Bool) -> ()) -> (),
        startIndex: Int = 0,
        completion: @escaping (Element?) -> ()
    ) {
        guard let element = get(at: startIndex) else {
            completion(nil)
            return
        }

        predicate(element) { success in
            if success {
                completion(element)
            } else {
                self.recursiveAsyncFirst(where: predicate, startIndex: startIndex + 1, completion: completion)
            }
        }
    }
}
