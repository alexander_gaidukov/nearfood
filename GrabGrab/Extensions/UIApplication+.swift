//
//  UIApplication+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

extension UIApplication {
    func call(to phone: PhoneNumber) {
        guard let url = URL(string: "tel://\(phone.rawValue)") else { return }
        openIfAvailable(url)
    }

    func email(to mail: String) {
        guard let url = URL(string: "mailto:\(mail)") else { return }
        openIfAvailable(url)
    }

    func openIfAvailable(_ url: URL) {
        if canOpenURL(url) {
            open(url, options: [:], completionHandler: nil)
        }
    }

    func openSettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
        open(url, options: [:], completionHandler: nil)
    }
}
