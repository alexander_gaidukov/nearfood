//
//  NSAttributedString+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

extension NSAttributedString {
    static func instance(html: String, fontSize: Double) -> NSAttributedString {
        let components = html.components(separatedBy: "<b>").flatMap { $0.components(separatedBy: "</b>") }
        let results = components.enumerated().map {
            NSAttributedString(
                string: $0.element,
                attributes: [
                    .font: $0.offset.isMultiple(of: 2)
                        ? UIFont.systemFont(ofSize: CGFloat(fontSize))
                        : UIFont.boldSystemFont(ofSize: CGFloat(fontSize))
                ]
            )
        }
        return results.reduce(into: NSMutableAttributedString()) { $0.append($1) }
    }
}
