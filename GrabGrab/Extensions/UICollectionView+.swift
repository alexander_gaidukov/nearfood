//
//  UICollectionView+.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

extension UICollectionView {
    func register<Cell: UICollectionViewCell>(cellType: Cell.Type) {
        register(
            UINib(nibName: String(describing: cellType), bundle: nil),
            forCellWithReuseIdentifier: String(describing: cellType)
        )
    }

    func dequeue<Cell: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> Cell {
        // swiftlint:disable:next force_cast
        dequeueReusableCell(withReuseIdentifier: String(describing: Cell.self), for: indexPath) as! Cell
    }
}
