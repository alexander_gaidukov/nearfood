//
//  MediaSourceViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class MediaSourceViewController: UIViewController, PopupViewController {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var cameraLabel: UILabel!
    @IBOutlet private weak var libraryLabel: UILabel!
    @IBOutlet private weak var cancelButton: UIButton!

    var presenter: MediaSourcePresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        cameraLabel.text = LocalizedStrings.Chat.Media.camera
        libraryLabel.text = LocalizedStrings.Chat.Media.photoLibrary
        cancelButton.setTitle(LocalizedStrings.Common.cancel, for: .normal)
    }

    func close() {}

    @IBAction private func cameraTapped() {
        presenter.chooseCamera()
    }

    @IBAction private func libraryTapped() {
        presenter.chooseLibrary()
    }

    @IBAction private func cancelTapped() {
        presenter.cancel()
    }
}
