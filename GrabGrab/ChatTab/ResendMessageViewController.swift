//
//  ResendMessageViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class ResendMessageViewController: UIViewController, PopupViewController {
    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var sendLabel: UILabel!
    @IBOutlet private weak var removeLabel: UILabel!
    @IBOutlet private weak var cancelButton: UIButton!

    var presenter: ResendMessagePresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        sendLabel.text = LocalizedStrings.Chat.Actions.sendAgain
        removeLabel.text = LocalizedStrings.Chat.Actions.remove
        cancelButton.setTitle(LocalizedStrings.Common.cancel, for: .normal)
    }

    func close() {}

    @IBAction private func sendTapped() {
        presenter.send()
    }

    @IBAction private func removeTapped() {
        presenter.remove()
    }

    @IBAction private func cancelTapped() {
        presenter.cancel()
    }
}
