//
//  ChatViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController, KeyboardAvoiding, GalleryPresentingViewController {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var textView: GGTextView!
    @IBOutlet private weak var sendButton: UIButton!

    @IBOutlet private weak var bottomOffsetConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet private weak var coverView: UIView!

    private let disposableBag = DisposableBag()

    var presenter: ChatPresenterType!

    private lazy var refresher = {
        Refresher { [weak self] in self?.tableView?.reloadData() }
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.refresh()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
    }

    private func configureViews() {
        coverView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(coverViewTapped)))
        textView.placeholder = LocalizedStrings.Chat.messagePlaceholder
        textView.didChange = {[weak self] text in
            self?.presenter.changeMessage(text)
        }
        textView.didBeginEditing = { [weak self] in self?.coverView.isHidden = false }
        textView.didEndEditing = { [weak self] in self?.coverView.isHidden = true }
        title = LocalizedStrings.Chat.title
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(closeTapped))
        if tabBarController == nil {
            bottomOffsetConstraint.constant = 0
        }
        registerCells()
        tableView.transform = CGAffineTransform(rotationAngle: .pi)
    }

    private func configureBindings() {

        presenter.state.$message.bind(to: textView, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.isSendButtonEnabled.bind(to: sendButton, keyPath: \.isEnabled).addToDisposableBag(disposableBag)

        presenter.models.observe {[weak self] _ in
            self?.refresher.refresh()
        }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe {[weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    private func registerCells() {
        tableView.register(cellType: TextMessageCell.self)
        tableView.register(cellType: ImageMessageCell.self)
        tableView.register(cellType: MessageHeaderCell.self)
        tableView.register(cellType: FeedbackMessageCell.self)
        tableView.register(cellType: BonusesMessageCell.self)
    }

    @IBAction private func sendTapped() {
        presenter.send()
    }

    @IBAction private func photoTapped() {
        presenter.selectPicture()
    }

    @objc private func closeTapped() {
        presenter.close()
    }

    @objc private func coverViewTapped() {
        textView.resignFirstResponder()
    }
}

extension ChatViewController: TabRootController {
    var tabBarImage: UIImage {
        #imageLiteral(resourceName: "chat_icon")
    }

    var tabBarSelectedImage: UIImage {
        #imageLiteral(resourceName: "chat_icon_selected")
    }

    var identifier: String {
        Identifiers.TabBar.chat
    }
}

extension ChatViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}

extension ChatViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension ChatViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.models.wrappedValue.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.models.wrappedValue[section].numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        presenter.models.wrappedValue[indexPath.section].cell(tableView: tableView, indexPath: indexPath)
    }
}

extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        presenter.didShowMessage(at: indexPath)
    }
}
