//
//  FeedbackMessageCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class FeedbackMessageCellModel: MessageCellModel {
    static let availableRatings = [2, 3, 4, 5]

    let title: String
    let photos: [[Photo]]
    let feedback: Feedback?
    let didTap: (Int?) -> ()

    var feedbackIcon: UIImage? {
        feedback?.icon
    }

    var feedbackNote: String? {
        feedback?.note
    }

    var isCompletedViewShown: Bool {
        feedback?.isCompleted == true
    }

    init(order: Order, date: Date, didTap: @escaping (Int?) -> ()) {
        title = order.title
        photos = order.cartItems.map(\.photos)
        feedback = order.feedback
        self.didTap = didTap
        super.init(
            date: date,
            author: Message.Author(uuid: UUID().uuidString, name: LocalizedStrings.Chat.support),
            isIncom: true,
            sendingState: nil,
            errorHandler: {}
        )
    }
}

final class FeedbackMessageCell: MessageCell {
    @IBOutlet private weak var completedFeedbackContainerView: UIView!
    @IBOutlet private weak var completedFeedbackImageView: UIImageView!
    @IBOutlet private weak var completedFeedbackLabel: UILabel!
    @IBOutlet private weak var completedFeedbackNoteLabel: UILabel!
    @IBOutlet private weak var feedbackTitleLabel: UILabel!
    @IBOutlet private weak var orderTitleLabel: UILabel!
    @IBOutlet private var imagesStackView: ImagesStackView!
    @IBOutlet private var rateContainerView: RatingContainerView!

    private var model: FeedbackMessageCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        configureViews()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imagesStackView.prepareForReuse()
    }

    func configure(model: FeedbackMessageCellModel) {
        super.configure(model: model)
        self.model = model
    }

    private func configureViews() {
        feedbackTitleLabel.text = LocalizedStrings.Order.Review.title
        completedFeedbackLabel.text = LocalizedStrings.Order.Review.completeTitle

        rateContainerView.configure(ratings: FeedbackMessageCellModel.availableRatings) { [weak self] in
            self?.model.didTap($0)
        }

        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(containerTapped)))

        imagesStackView.overlapping = 20.0
    }

    private func updateUI() {
        orderTitleLabel.text = model.title
        completedFeedbackImageView.image = model.feedbackIcon
        completedFeedbackNoteLabel.text = model.feedbackNote
        completedFeedbackNoteLabel.isHidden = model.feedbackNote?.isEmpty != false
        completedFeedbackContainerView.isHidden = !model.isCompletedViewShown

        rateContainerView.isHidden = model.isCompletedViewShown
        imagesStackView.configure(with: model.photos)
    }

    @objc private func containerTapped() {
        model.didTap(nil)
    }
}
