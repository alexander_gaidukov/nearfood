//
//  TextMessageCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum MessageCellAlignment {
    case leading
    case trailing
}

final class TextMessageCellModel: MessageCellModel {
    let text: String
    let messageColor: UIColor
    init(
        text: String,
        date: Date,
        author: Message.Author?,
        isIncom: Bool,
        sendingState: Message.SendingState?,
        errorHandler: @escaping () -> ()
    ) {
        self.text = text
        self.messageColor = isIncom ? .mainText : .white
        super.init(date: date, author: author, isIncom: isIncom, sendingState: sendingState, errorHandler: errorHandler)
    }
}

final class TextMessageCell: MessageCell {
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private var leadingLayoutConstraints: [NSLayoutConstraint]!

    var model: TextMessageCellModel! {
        didSet {
            updateUI()
        }
    }

    func configure(model: TextMessageCellModel) {
        super.configure(model: model)
        self.model = model
    }

    private func updateUI() {
        leadingLayoutConstraints.forEach {
            $0.priority = model.alignment == .leading ? .defaultHigh : .defaultLow
        }
        messageLabel.text = model.text
        messageLabel.textColor = model.messageColor
    }
}
