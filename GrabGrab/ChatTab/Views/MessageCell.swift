//
//  MessageCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 06.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class MessageCellModel {
    let author: String?
    let timeLabel: String
    let textColor: UIColor
    let backgroundColor: UIColor
    let alignment: MessageCellAlignment
    let sendingState: Message.SendingState?
    let errorHandler: () -> ()

    init(date: Date, author: Message.Author?, isIncom: Bool, sendingState: Message.SendingState?, errorHandler: @escaping () -> ()) {
        alignment = isIncom ? .leading : .trailing
        backgroundColor = isIncom ? .incomeMessage : .outcomeMessage
        timeLabel = Formatters.outputTimeFormatter.string(from: date)
        textColor = isIncom ? .labelText : .outcomeMessageSecondary
        self.author = isIncom ? author?.name : nil
        self.sendingState = sendingState
        self.errorHandler = errorHandler
    }
}

class MessageCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel?
    @IBOutlet private weak var authorOffsetConstraint: NSLayoutConstraint?
    @IBOutlet private weak var loadingImageView: UIImageView?
    @IBOutlet private weak var errorButton: UIButton?

    private let kRotationAnimationKey = "com.grabgrab.rotationanimationkey"

    private var errorHandler: (() -> ())?

    func configure(model: MessageCellModel) {
        errorHandler = model.errorHandler
        containerView.backgroundColor = model.backgroundColor
        timeLabel.text = model.timeLabel
        timeLabel.textColor = model.textColor
        authorLabel?.text = model.author
        authorOffsetConstraint?.constant = model.author == nil ? 0 : 4

        if model.sendingState == .inProgress {
            loadingImageView?.isHidden = false
            startLoading()
        } else {
            loadingImageView?.isHidden = true
            stopLoading()
        }

        errorButton?.isHidden = model.sendingState != .error
    }

    private func startLoading() {
        if loadingImageView?.layer.animation(forKey: kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")

            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float.pi * 2.0
            rotationAnimation.duration = 0.8
            rotationAnimation.repeatCount = Float.infinity

            loadingImageView?.layer.add(rotationAnimation, forKey: kRotationAnimationKey)
        }
    }

    private func stopLoading() {
        loadingImageView?.layer.removeAnimation(forKey: kRotationAnimationKey)
    }

    @IBAction private func errorButtonTapped() {
        errorHandler?()
    }
}
