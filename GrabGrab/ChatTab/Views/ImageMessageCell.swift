//
//  ImageMessageCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class ImageMessageCellModel: MessageCellModel {
    let attachment: ImageAttachment
    var showImageHandler: (UIImage) -> () = {_ in }

    init(
        attachment: ImageAttachment,
        date: Date,
        author: Message.Author?,
        isIncom: Bool,
        sendingState: Message.SendingState?,
        errorHandler: @escaping () -> ()
    ) {
        self.attachment = attachment
        super.init(date: date, author: author, isIncom: isIncom, sendingState: sendingState, errorHandler: errorHandler)
    }
}

final class ImageMessageCell: MessageCell, GalleryPhotoCell {
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet private weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet private weak var aspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!

    private var model: ImageMessageCellModel! {
        didSet {
            updateUI()
        }
    }

    private var task: DownloadTask?

    override func awakeFromNib() {
        super.awakeFromNib()
        photoView.isUserInteractionEnabled = true
        photoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTapped)))
    }

    override func prepareForReuse() {
        task?.cancel()
        task = nil
        photoView.image = nil
        if loadingIndicator.isAnimating {
            loadingIndicator.stopAnimating()
        }
    }

    func configure(model: ImageMessageCellModel) {
        super.configure(model: model)
        self.model = model
    }

    private func updateUI() {
        leadingConstraint.priority = model.alignment == .leading ? .defaultHigh : .defaultLow

        switch model.attachment {
        case .remote(let photo):
            loadingIndicator.startAnimating()
            task = photoView.load(photo: photo, properties: ImageDownloadingProperties(resizing: .none)) { [weak self] _ in
                self?.loadingIndicator.stopAnimating()
            }
        case .local(let image):
            photoView.image = image
        }
    }

    @objc private func imageTapped() {
        guard let image = photoView.image else { return }
        model.showImageHandler(image)
    }
}
