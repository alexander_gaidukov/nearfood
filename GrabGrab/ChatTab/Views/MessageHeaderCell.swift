//
//  MessageHeaderCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MessageHeaderCellModel {
    let title: String
}

extension MessageHeaderCellModel {
    init(date: Date) {
        if Calendar.current.isDateInToday(date) {
            title = LocalizedStrings.Common.today
        } else if Calendar.current.isDateInYesterday(date) {
            title = LocalizedStrings.Common.yesterday
        } else {
            title = Formatters.messagesHeaderDateFormatter.string(from: date)
        }
    }
}

final class MessageHeaderCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!

    func configure(model: MessageHeaderCellModel) {
        titleLabel.text = model.title
    }
}
