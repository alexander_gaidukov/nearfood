//
//  BonusesMessageCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class BonusesMessageCellModel: MessageCellModel {

    let amount: String

    init(amount: Double, date: Date) {
        self.amount = amount.string(with: Formatters.priceFormatter) ?? ""
        super.init(date: date, author: nil, isIncom: true, sendingState: nil, errorHandler: {})
    }
}

class BonusesMessageCell: MessageCell {
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!

    func configure(model: BonusesMessageCellModel) {
        super.configure(model: model)
        amountLabel.text = model.amount
    }
}
