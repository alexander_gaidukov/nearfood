//
//  MessagesSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct MessagesSectionModel: TableViewSectionModel {

    enum CellModel {
        case text(TextMessageCellModel)
        case image(ImageMessageCellModel)
        case feedback(FeedbackMessageCellModel)
        case bonuses(BonusesMessageCellModel)
    }

    private let models: [CellModel]
    private let headerModel: MessageHeaderCellModel

    init(
        date: Date,
        messages: [Message],
        customerUUID: String,
        errorHandler: @escaping (Message) -> (),
        showImageHandler: @escaping (UIImage, Int) -> (),
        feedbackHandler: @escaping (Message, Int?) -> ()
    ) {
        headerModel = MessageHeaderCellModel(date: date)
        models = messages.flatMap { message -> [CellModel] in

            var result: [CellModel] = []

            if let body = message.body, !body.isEmpty {
                let model = TextMessageCellModel(
                    text: body, date: message.createdAt,
                    author: message.author,
                    isIncom: message.author?.uuid != customerUUID,
                    sendingState: message.sendingState,
                    errorHandler: { errorHandler(message) }
                )
                result.append(.text(model))
            }

            if message.kind == .feedback {
                if let order = message.properties?.order {
                    result.append(
                        .feedback(
                            FeedbackMessageCellModel(
                                order: order,
                                date: message.createdAt,
                                didTap: { feedbackHandler(message, $0) }
                            )
                        )
                    )
                }
            } else if message.kind == .bonuses {
                if let amount = message.properties?.amount {
                    result.append(.bonuses(BonusesMessageCellModel(amount: amount, date: message.createdAt)))
                }
            } else if let attachments = message.attachments, !attachments.isEmpty {
                attachments.forEach {
                    let model = ImageMessageCellModel(attachment: $0,
                                                      date: message.createdAt,
                                                      author: message.author,
                                                      isIncom: message.author?.uuid != customerUUID,
                                                      sendingState: message.sendingState,
                                                      errorHandler: { errorHandler(message) })
                    result.append(.image(model))
                }
            }
            return result
        }

        models.enumerated().forEach { item in
            if case let .image(imageModel) = item.element {
                imageModel.showImageHandler = { image in showImageHandler(image, item.offset) }
            }
        }
    }

    var numberOfRows: Int {
        models.count + 1
    }

    func registerCells(in tableView: UITableView) {}

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row >= models.count {
            let cell: MessageHeaderCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(model: headerModel)
            cell.transform = CGAffineTransform(rotationAngle: -.pi)
            return cell
        }

        let model = models[indexPath.row]
        switch model {
        case .image(let imageModel):
            let cell: ImageMessageCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(model: imageModel)
            cell.transform = CGAffineTransform(rotationAngle: -.pi)
            return cell
        case .text(let textModel):
            let cell: TextMessageCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(model: textModel)
            cell.transform = CGAffineTransform(rotationAngle: -.pi)
            return cell
        case .feedback(let model):
            let cell: FeedbackMessageCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(model: model)
            cell.transform = CGAffineTransform(rotationAngle: -.pi)
            return cell
        case .bonuses(let model):
            let cell: BonusesMessageCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(model: model)
            cell.transform = CGAffineTransform(rotationAngle: -.pi)
            return cell
        }
    }
}
