//
//  ChatPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol ChatPresenterType {
    var models: Observable<[MessagesSectionModel]> { get }
    var state: ChatViewState { get }
    func selectPicture()
    func send()
    func close()
    func changeMessage(_ message: String)
    func didShowMessage(at indexPath: IndexPath)

    func refresh()
}

enum ChatAction {
    case selectPicture((UIImage?) -> ())
    case close
    case resend((ResendMessageAction) -> ())
    case showImage(UIImage, IndexPath)
    case feedback(Message, Int?)
}

struct ChatViewState {
    @Observable
    fileprivate var messages: [Message]

    @Observable
    var message = ""

    @Observable
    var isLoading = false

    @Observable
    var error: WebError?

    var isSendButtonEnabled: Observable<Bool> {
        $message.map { !$0.isEmpty }
    }
}

final class ChatPresenter: ChatPresenterType {

    var state: ChatViewState

    private let completion: (ChatAction) -> ()
    private let order: Order?
    private let messagesDataProvider: MessagesDataProviderType
    private let customerDataProvider: CustomerDataProviderType

    private let historyLoadingOffset = 5
    private let disposableBag = DisposableBag()

    private let syncQueue = DispatchQueue(label: "grab-grab.chat.sync.queue")

    init(
        order: Order?,
        messagesDataProvider: MessagesDataProviderType,
        customerDataProvider: CustomerDataProviderType,
        completion: @escaping (ChatAction) -> ()
    ) {
        self.completion = completion
        self.order = order
        self.messagesDataProvider = messagesDataProvider
        self.customerDataProvider = customerDataProvider
        state = ChatViewState(messages: messagesDataProvider.messages)
        state.$messages.map { [weak self] in
            self?.generateModels($0) ?? []
        }.bind(to: models, keyPath: \.wrappedValue).addToDisposableBag(disposableBag)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(messagesDidChange),
            name: .messagesDidChangeNotification,
            object: nil
        )
    }

    var models = Observable<[MessagesSectionModel]>(wrappedValue: [])

    private func generateModels(_ messages: [Message]) -> [MessagesSectionModel] {
        guard let customer = customerDataProvider.customer else {
            return []
        }
        let groups = Dictionary(grouping: messages) { message -> Date in
            Calendar.current.startOfDay(for: message.createdAt)
        }
        return  groups.keys.sorted().reversed().enumerated().map { value in
            MessagesSectionModel(
                date: value.element,
                messages: groups[value.element]!, // swiftlint:disable:this force_unwrapping
                customerUUID: customer.uuid,
                errorHandler: { [weak self] message in self?.showActions(for: message) },
                showImageHandler: { [weak self] image, index in
                    self?.showImage(image, at: IndexPath(row: index, section: value.offset))
                },
                feedbackHandler: { [weak self] message, rating in
                    self?.showFeedback(for: message, selectedRating: rating)
                })
        }
    }

    func close() {
        completion(.close)
    }

    func changeMessage(_ message: String) {
        state.message = message
    }

    func selectPicture() {
        completion(.selectPicture({[weak self] picture in
            guard let picture = picture else { return }
            self?.sendPicture(picture)
        }))
    }

    func send() {
        guard !state.message.isEmpty else { return }
        messagesDataProvider.send(text: state.message, order: order)
        state.message = ""
    }

    func refresh() {
        if state.messages.isEmpty {
            state.isLoading = true
        }

        messagesDataProvider.refresh {[weak self] error in
            guard let self = self else { return }
            if self.state.isLoading {
                self.state.isLoading = false
                self.state.error = error
            }
        }
    }

    func didShowMessage(at indexPath: IndexPath) {
        var messageIndex = indexPath.item
        if indexPath.section > 0 {
            models.wrappedValue[..<indexPath.section].forEach { messageIndex += $0.numberOfRows }
        }

        if messageIndex < state.messages.count {
            syncQueue.async {[weak self] in
                guard
                    let self = self,
                    messageIndex < self.state.messages.count
                else { return }
                self.messagesDataProvider.messageDidRead(self.state.messages[messageIndex])
            }
        }

        guard messageIndex >= state.messages.count - historyLoadingOffset else { return }
        messagesDataProvider.loadNextPage { _ in }
    }

    @objc private func messagesDidChange() {
        state.messages = messagesDataProvider.messages
    }

    private func showActions(for message: Message) {
        completion(.resend({[weak self] action in
            switch action {
            case .cancel:
                break
            case .send:
                self?.messagesDataProvider.resend(message: message)
            case .remove:
                self?.messagesDataProvider.remove(message: message)
            }
        }))
    }

    private func showFeedback(for message: Message, selectedRating: Int?) {
        completion(.feedback(message, selectedRating))
    }

    private func showImage(_ image: UIImage, at indexPath: IndexPath) {
        completion(.showImage(image, indexPath))
    }

    private func sendPicture(_ picture: UIImage) {
        messagesDataProvider.send(image: picture, order: order)
    }
}
