//
//  ResendMessagePresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol ResendMessagePresenterType {
    func send()
    func remove()
    func cancel()
}

enum ResendMessageAction {
    case send
    case remove
    case cancel
}

final class ResendMessagePresenter: ResendMessagePresenterType {
    private let completion: (ResendMessageAction) -> ()
    init(completion: @escaping (ResendMessageAction) -> ()) {
        self.completion = completion
    }

    func send() {
        completion(.send)
    }

    func remove() {
        completion(.remove)
    }

    func cancel() {
        completion(.cancel)
    }
}
