//
//  MediaSourcePresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol MediaSourcePresenterType {
    func chooseCamera()
    func chooseLibrary()
    func cancel()
}

enum MediaSourceAction {
    case camera
    case library
    case cancel
}

final class MediaSourcePresenter: MediaSourcePresenterType {
    private let completion: (MediaSourceAction) -> ()
    init(completion: @escaping (MediaSourceAction) -> ()) {
        self.completion = completion
    }

    func chooseCamera() {
        completion(.camera)
    }

    func chooseLibrary() {
        completion(.library)
    }

    func cancel() {
        completion(.cancel)
    }
}
