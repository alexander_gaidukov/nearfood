//
//  ChatCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class ChatCoordinator: TabCoordinator, Coordinator {

    var completion: () -> () = {}

    private let router: ChatRouterType
    private let dataProvidersStorage: DataProvidersStorageType
    private let webClient: WebClientType
    private var childCoordinator: Coordinator?

    init(router: ChatRouterType, dataProvidersStorage: DataProvidersStorageType, webClient: WebClientType) {
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
        self.webClient = webClient
    }

    func start() -> UIViewController & TabRootController {
        createChatController()
    }

    func start() {
        createChatController()
    }

    @discardableResult
    private func createChatController() -> UIViewController & TabRootController {
        router.showChat { action in
            switch action {
            case .selectPicture(let completion):
                self.pickImage(completion: completion)
            case .close:
                self.router.dismiss {
                    self.completion()
                }
            case .resend(let completion):
                self.showResendMessage(completion: completion)
            case let .showImage(image, indexPath):
                self.showImage(image, indexPath: indexPath)
            case let .feedback(message, rating):
                self.showFeedback(for: message, selectedRating: rating)
            }
        }
    }

    private func pickImage(completion: @escaping (UIImage?) -> ()) {
        router.showMediaSource { action in
            self.router.dismiss {
                switch action {
                case .cancel:
                    completion(nil)
                case .camera:
                    self.showPicker(withSourceType: .camera, completion: completion)
                case .library:
                    self.showPicker(withSourceType: .photoLibrary, completion: completion)
                }
            }
        }
    }

    private func showPicker(withSourceType sourceType: UIImagePickerController.SourceType, completion: @escaping (UIImage?) -> ()) {
        router.showPicker(withSourceType: sourceType) { [weak self] image in
            self?.router.dismiss {
                completion(image)
            }
        }
    }

    private func showResendMessage(completion: @escaping (ResendMessageAction) -> ()) {
        router.showResendMessage { action in
            self.router.dismiss {
                completion(action)
            }
        }
    }

    private func showImage(_ image: UIImage, indexPath: IndexPath) {
        router.showGallery(for: [image], indexPath: indexPath)
    }

    private func showFeedback(for message: Message, selectedRating: Int?) {
        guard message.kind == .feedback, let order = message.properties?.order else { return }
        if let coordinator = childCoordinator, coordinator is FeedbackCoordinator { return }
        let factory = FeedbackViewControllersFactory(webClient: webClient)
        let router = FeedbackRouter(presentingViewController: self.router.topViewController, factory: factory)
        let coordinator = FeedbackCoordinator(order: .order(order), selectedRating: selectedRating, router: router) {[weak self] feedback in
            if let feedback = feedback {
                self?.dataProvidersStorage.messagesDataProvider.updateFeedback(feedback, for: message)
            }
            (self?.router.topViewController as? KeyboardAvoiding)?.configureKeyboardAvoiding()
            self?.childCoordinator = nil
        }
        coordinator.start()
        childCoordinator = coordinator
    }
}
