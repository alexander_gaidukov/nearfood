//
//  ChatRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import SwiftPhotoGallery

protocol ChatRouterType {
    var topViewController: UIViewController? { get }
    func showChat(completion: @escaping (ChatAction) -> ()) -> UIViewController & TabRootController
    func showMediaSource(completion: @escaping (MediaSourceAction) -> ())
    func showPicker(withSourceType sourceType: UIImagePickerController.SourceType, completion: @escaping (UIImage?) -> ())
    func showResendMessage(completion: @escaping (ResendMessageAction) -> ())
    func showGallery(for images: [UIImage], indexPath: IndexPath)
    func dismiss(completion: @escaping () -> ())
}

final class ChatRouter: NSObject, ChatRouterType {
    private let factory: ChatViewControllersFactoryType

    private weak var presentingViewController: UIViewController?

    private var pickerCompletion: ((UIImage?) -> ())?
    private var galleryDataSource: GalleryDataSource?

    init(presentingViewController: UIViewController?, factory: ChatViewControllersFactoryType) {
        self.factory = factory
        self.presentingViewController = presentingViewController
    }

    func showChat(completion: @escaping (ChatAction) -> ()) -> UIViewController & TabRootController {
        let controller = factory.chatViewController(completion: completion)

        if let presentingController = presentingViewController {
            let navController = GGNavigationController(rootViewController: controller)
            navController.modalPresentationStyle = .fullScreen
            presentingController.present(navController, animated: true, completion: nil)
        } else {
            presentingViewController = controller
        }
        return controller
    }

    func showMediaSource(completion: @escaping (MediaSourceAction) -> ()) {
        let controller = factory.mediaSourceViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showPicker(withSourceType sourceType: UIImagePickerController.SourceType, completion: @escaping (UIImage?) -> ()) {
        guard pickerCompletion == nil else { return }
        pickerCompletion = completion
        let controller = factory.pickerViewController(withSourceType: sourceType, delegate: self)
        controller.modalPresentationStyle = .fullScreen
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showResendMessage(completion: @escaping (ResendMessageAction) -> ()) {
        let controller = factory.resendMessageViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showGallery(for images: [UIImage], indexPath: IndexPath) {
        let dataSource = GalleryDataSource(images: images, indexPath: indexPath)
        self.galleryDataSource = dataSource
        let gallery = SwiftPhotoGallery(delegate: self, dataSource: dataSource)
        gallery.backgroundColor = .black
        gallery.pageIndicatorTintColor = UIColor.gray.withAlphaComponent(0.5)
        gallery.currentPageIndicatorTintColor = .primary
        gallery.hidePageControl = images.count < 2
        gallery.modalPresentationStyle = .custom
        gallery.transitioningDelegate = self
        topViewController?.present(gallery, animated: true, completion: nil)
    }

    func dismiss(completion: @escaping () -> ()) {
        topViewController?.dismiss(animated: true, completion: completion)
    }

    var topViewController: UIViewController? {
        guard var topCandidate = presentingViewController else { return nil }
        while let controller = topCandidate.presentedViewController {
            topCandidate = controller
        }
        return topCandidate
    }
}

extension ChatRouter: UIViewControllerTransitioningDelegate {
    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        if presented is SwiftPhotoGallery, let galleryDataSource = galleryDataSource {
            return GalleryPresentingAnimator(indexPath: galleryDataSource.indexPath)
        }
        return CustomTransitioningDelegate(isPresenting: true)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if dismissed is SwiftPhotoGallery, let galleryDataSource = galleryDataSource {
            return GalleryDismissingAnimator(indexPath: galleryDataSource.indexPath)
        }
        return CustomTransitioningDelegate(isPresenting: false)
    }

    func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController
    ) -> UIPresentationController? {
        if presented is SwiftPhotoGallery {
            return DimmingPresentationController(presentedViewController: presented, presenting: presenting)
        }
        return nil
    }
}

extension ChatRouter: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickerCompletion?(nil)
        pickerCompletion = nil
    }

    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
    ) {

        defer {
            pickerCompletion = nil
        }

        guard let photo = info[.editedImage] as? UIImage ?? info[.originalImage] as? UIImage else {
            pickerCompletion?(nil)
            return
        }

        pickerCompletion?(photo)
    }
}

extension ChatRouter: SwiftPhotoGalleryDelegate {
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        dismiss {
            self.galleryDataSource = nil
        }
    }
}
