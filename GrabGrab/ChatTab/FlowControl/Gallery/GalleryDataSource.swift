//
//  GalleryDataSource.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import SwiftPhotoGallery

protocol GalleryPresentingViewController: AnyObject {
    var tableView: UITableView! { get }
}

protocol GalleryPhotoCell {
    var photoView: UIImageView! { get }
}

final class GalleryDataSource: SwiftPhotoGalleryDataSource {
    private let images: [UIImage]
    var indexPath: IndexPath
    init(images: [UIImage], indexPath: IndexPath) {
        self.images = images
        self.indexPath = indexPath
    }

    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {
        images.count
    }

    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {
        images[forIndex]
    }
}
