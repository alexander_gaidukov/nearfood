//
//  GalleryDismissingAnimator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import SwiftPhotoGallery

final class GalleryDismissingAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    private let indexPath: IndexPath
    private let duration: TimeInterval = 0.5

    init(indexPath: IndexPath) {
        self.indexPath = indexPath
        super.init()
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        guard let toVC = transitionContext.viewController(forKey: .to) as? GGTabBarController,
            let selectedVC = toVC.selectedViewController as? (UIViewController & GalleryPresentingViewController),
            let toView = selectedVC.tableView.cellForRow(at: indexPath) as? (UITableViewCell & GalleryPhotoCell),
            let fromVC = transitionContext.viewController(forKey: .from) as? SwiftPhotoGallery,
            let swiftPhotoGalleryCell = fromVC.imageCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? SwiftPhotoGalleryCell
            else {
                transitionContext.completeTransition(true)
                return
        }

        let containerView = transitionContext.containerView

        // Determine our original and final frames
        let size = swiftPhotoGalleryCell.imageView.frame.size
        let convertedRect = swiftPhotoGalleryCell.imageView.convert(swiftPhotoGalleryCell.imageView.bounds, to: containerView)
        let originFrame = CGRect(origin: convertedRect.origin, size: size)

        let viewToAnimate = UIImageView(frame: originFrame)
        viewToAnimate.center = CGPoint(x: convertedRect.midX, y: convertedRect.midY)
        viewToAnimate.image = swiftPhotoGalleryCell.imageView.image
        viewToAnimate.contentMode = .scaleAspectFill
        viewToAnimate.clipsToBounds = true

        containerView.addSubview(viewToAnimate)

        fromVC.view.isHidden = true

        let finalFrame = toView.convert(toView.imageView?.frame ?? .zero, to: toVC.view)

        // Animate size and position
        UIView.animate(withDuration: duration, animations: {
            viewToAnimate.frame.size.width = finalFrame.width
            viewToAnimate.frame.size.height = finalFrame.height
            viewToAnimate.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
        }, completion: { _ in
            viewToAnimate.removeFromSuperview()
            transitionContext.completeTransition(true)
        })

    }
}
