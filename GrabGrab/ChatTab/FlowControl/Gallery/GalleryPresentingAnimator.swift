//
//  GalleryPresentingAnimator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class GalleryPresentingAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    private let indexPath: IndexPath
    private let duration: TimeInterval = 0.5

    init(indexPath: IndexPath) {
        self.indexPath = indexPath
        super.init()
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        guard let toView = transitionContext.view(forKey: .to),
            let fromVC = transitionContext.viewController(forKey: .from) as? GGTabBarController,
            let selectedVC = fromVC.selectedViewController as? (UIViewController & GalleryPresentingViewController),
            let fromView = selectedVC.tableView?.cellForRow(at: indexPath) as? (UITableViewCell & GalleryPhotoCell)
            else {
                transitionContext.completeTransition(true)
                return
        }

        let finalFrame = toView.frame
        let originFrame = fromView.convert(fromView.imageView?.frame ?? .zero, to: fromVC.view)

        let viewToAnimate = UIImageView(frame: originFrame)
        viewToAnimate.image = fromView.photoView.image
        viewToAnimate.contentMode = .scaleAspectFill
        viewToAnimate.clipsToBounds = true

        let containerView = transitionContext.containerView
        containerView.addSubview(toView)
        containerView.addSubview(viewToAnimate)

        toView.isHidden = true

        // Determine the final image height based on final frame width and image aspect ratio
        let imageAspectRatio = viewToAnimate.image.map { $0.size.width / $0.size.height } ?? 1.0
        let finalImageheight = finalFrame.width / imageAspectRatio

        // Animate size and position
        UIView.animate(withDuration: duration, animations: {
            viewToAnimate.frame.size.width = finalFrame.width
            viewToAnimate.frame.size.height = finalImageheight
            viewToAnimate.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
        }, completion: { _ in
            toView.isHidden = false
            viewToAnimate.removeFromSuperview()
            transitionContext.completeTransition(true)
        })

    }
}
