//
//  ChatViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 29.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import MobileCoreServices

protocol ChatViewControllersFactoryType {
    func chatViewController(completion: @escaping (ChatAction) -> ()) -> ChatViewController
    func mediaSourceViewController(completion: @escaping (MediaSourceAction) -> ()) -> MediaSourceViewController
    func pickerViewController(
        withSourceType sourceType: UIImagePickerController.SourceType,
        delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate
    ) -> UIImagePickerController
    func resendMessageViewController(completion: @escaping (ResendMessageAction) -> ()) -> ResendMessageViewController
}

struct ChatViewControllersFactory: ChatViewControllersFactoryType {

    private let storyboard: Storyboard = .chat

    let order: Order?
    let dataProvidersStorage: DataProvidersStorageType

    func chatViewController(completion: @escaping (ChatAction) -> ()) -> ChatViewController {
        let presenter = ChatPresenter(
            order: order,
            messagesDataProvider: dataProvidersStorage.messagesDataProvider,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            completion: completion
        )
        let controller = ChatViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func mediaSourceViewController(completion: @escaping (MediaSourceAction) -> ()) -> MediaSourceViewController {
        let presenter = MediaSourcePresenter(completion: completion)
        let controller = MediaSourceViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func pickerViewController(
        withSourceType sourceType: UIImagePickerController.SourceType,
        delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate
    ) -> UIImagePickerController {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = delegate
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = sourceType
        imagePickerController.mediaTypes = [kUTTypeImage as String]
        return imagePickerController
    }

    func resendMessageViewController(completion: @escaping (ResendMessageAction) -> ()) -> ResendMessageViewController {
        let presenter = ResendMessagePresenter(completion: completion)
        let controller = ResendMessageViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
