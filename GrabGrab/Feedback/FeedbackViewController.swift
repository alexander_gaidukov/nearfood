//
//  FeedbackViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class FeedbackViewController: UIViewController, PopupViewController, KeyboardAvoiding {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var textView: GGTextView!
    @IBOutlet private weak var keyboardHidingView: UIView!
    @IBOutlet private weak var orderTitleLabel: UILabel!
    @IBOutlet private var imagesStackView: ImagesStackView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private var rateContainerView: RatingContainerView!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var scrollView: UIScrollView!

    var presenter: FeedbackPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
        if presenter.state.rating != nil {
            textView.becomeFirstResponder()
        }
    }

    private func configureView() {
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        imagesStackView.overlapping = 20.0

        orderTitleLabel.text = presenter.title
        titleLabel.text = LocalizedStrings.Order.Review.title
        descriptionLabel.text = LocalizedStrings.Order.Review.description
        sendButton.setTitle(LocalizedStrings.Common.done, for: .normal)

        rateContainerView.configure(ratings: presenter.availableRatings) { [weak self] in
            self?.presenter.changeRating($0)
        }

        configureToolbar()

        textView.placeholder = LocalizedStrings.Order.Review.note
        textView.didBeginEditing = { [weak self] in
            self?.keyboardHidingView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self?.scrollToBottom()
            }
        }
        textView.didEndEditing = { [weak self] in self?.keyboardHidingView.isHidden = true }
        textView.didChange = { [weak self] in self?.presenter.changeNote($0) }

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        contentView.addGestureRecognizer(panGestureRecognizer)

        keyboardHidingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
    }

    private func configureBindings() {
        presenter.state.$photos.observe { [weak self] photos in self?.configureImages(photos) }.addToDisposableBag(disposableBag)
        presenter.state.isButtonEnabled.bind(to: sendButton, keyPath: \.isEnabled).addToDisposableBag(disposableBag)
        presenter.state.$rating
            .bind(to: rateContainerView, keyPath: \.selectedRating)
            .addToDisposableBag(disposableBag)

        presenter.state.$note.bind(to: textView, keyPath: \.text).addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    private func configureImages(_ photos: [[Photo]]) {
        imagesStackView.configure(with: photos)
    }

    private func configureToolbar() {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50.0))
        toolbar.barStyle = .default
        toolbar.barTintColor = .tabbarBackground
        toolbar.tintColor = .presentedNavigationTint
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideKeyboard))
        ]
        toolbar.sizeToFit()
        textView.inputAccessoryView = toolbar
    }

    private func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
        scrollView.setContentOffset(bottomOffset, animated: true)
    }

    @IBAction private func sendTapped() {
        textView.resignFirstResponder()
        presenter.send()
    }

    @objc func close() {
        if textView.isFirstResponder {
            hideKeyboard()
        } else {
            presenter.close()
        }
    }

    @objc private func hideKeyboard() {
        textView.resignFirstResponder()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }

}

extension FeedbackViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension FeedbackViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
