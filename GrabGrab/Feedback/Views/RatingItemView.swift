//
//  RatingItemView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 19.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class RatingItemView: UIStackView {

    private let didTap: () -> ()
    let rating: Int

    private var containerView: UIView!
    private var titleLabel: UILabel!

    var isSelected: Bool = false {
        didSet {
            updateSelectionState()
        }
    }

    init(rating: Int, didTap: @escaping () -> ()) {
        self.didTap = didTap
        self.rating = rating

        super.init(frame: .zero)
        axis = .vertical
        distribution = .fill
        alignment = .center
        spacing = 8
        addViews(rating: rating)
    }

    @available(*, unavailable)
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func updateSelectionState() {
        titleLabel.textColor = isSelected ? .primary : .labelText
        containerView.backgroundColor = isSelected ? .ratingBorder : .clear
    }

    private func addViews(rating: Int) {
        let container = UIView(frame: .zero)
        container.backgroundColor = .clear
        container.cornerRadius = 8.0
        let imageView = UIImageView(image: image(for: rating))
        imageView.contentMode = .scaleAspectFit
        container.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 45),
            imageView.heightAnchor.constraint(equalToConstant: 45),
            imageView.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 4),
            imageView.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -4),
            imageView.topAnchor.constraint(equalTo: container.topAnchor, constant: 4),
            imageView.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -4)
        ])

        let label = UILabel(frame: .zero)
        label.textColor = .labelText
        label.font = .preferredFont(forTextStyle: .footnote)
        label.text = labelText(for: rating)

        addArrangedSubview(container)
        addArrangedSubview(label)

        container.isUserInteractionEnabled = true
        container.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didItemTap)))

        containerView = container
        titleLabel = label
    }

    private func labelText(for rating: Int) -> String {
        switch rating {
        case 5:
            return LocalizedStrings.Order.Review.Rate.perfect
        case 4:
            return LocalizedStrings.Order.Review.Rate.good
        case 3:
            return LocalizedStrings.Order.Review.Rate.notReally
        default:
            return LocalizedStrings.Order.Review.Rate.bad
        }
    }

    private func image(for rating: Int) -> UIImage {
        switch rating {
        case 5:
            return #imageLiteral(resourceName: "happy")
        case 4:
            return #imageLiteral(resourceName: "good")
        case 3:
            return #imageLiteral(resourceName: "sad")
        default:
            return #imageLiteral(resourceName: "bad")
        }
    }

    @objc private func didItemTap() {
        didTap()
    }
}
