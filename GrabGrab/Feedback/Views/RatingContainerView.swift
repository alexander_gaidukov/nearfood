//
//  RatingContainerView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 19.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class RatingContainerView: UIStackView {

    var selectedRating: Int? {
        didSet {
            arrangedSubviews.forEach {
                guard let ratingItemView = $0 as? RatingItemView else { return }
                ratingItemView.isSelected = ratingItemView.rating == selectedRating
            }
        }
    }

    func configure(ratings: [Int], didTap: @escaping (Int) -> ()) {
        arrangedSubviews.forEach { $0.removeFromSuperview() }
        ratings.forEach { rating in
            let view = RatingItemView(rating: rating) { didTap(rating) }
            addArrangedSubview(view)
        }
    }
}
