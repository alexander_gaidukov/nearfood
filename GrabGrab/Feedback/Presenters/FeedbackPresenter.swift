//
//  FeedbackPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol FeedbackPresenterType {
    var state: FeedbackViewState { get }

    var availableRatings: [Int] { get }

    var title: String { get }

    func close()
    func changeNote(_ note: String)
    func changeRating(_ rating: Int)
    func send()
}

enum FeedbackAction {
    case close
    case complete(Feedback)
}

enum FeedbackOrder {
    case order(Order)
    case info(OrderInfo)

    var photos: [[Photo]] {
        switch self {
        case .order(let order):
            return order.cartItems.map(\.photos)
        case .info:
            return []
        }
    }

    var title: String {
        switch self {
        case .order(let order):
            return order.title
        case .info(let order):
            return order.title
        }
    }

    var feedbackUUID: String? {
        switch self {
        case .order(let order):
            return order.feedback?.uuid
        case .info(let order):
            return order.feedbackUuid
        }
    }
}

struct FeedbackViewState {
    @Observable
    var note: String?

    @Observable
    var rating: Int?

    @Observable
    var photos: [[Photo]]

    @Observable
    var isLoading = false

    @Observable
    var error: WebError?

    var isButtonEnabled: Observable<Bool> {
        $rating.map { $0 != nil }
    }
}

final class FeedbackPresenter: FeedbackPresenterType {
    private let completion: (FeedbackAction) -> ()

    var state: FeedbackViewState

    let availableRatings = [2, 3, 4, 5]

    let title: String
    private let order: FeedbackOrder
    private let webClient: WebClientType

    init(order: FeedbackOrder, selectedRating: Int?, webClient: WebClientType, completion: @escaping (FeedbackAction) -> ()) {
        self.completion = completion
        self.webClient = webClient
        title = order.title
        self.order = order
        state = FeedbackViewState(rating: selectedRating, photos: order.photos)
        if case let .info(info) = order {
            loadOrderDetails(info.uuid)
        }
    }

    func close() {
        completion(.close)
    }

    func changeNote(_ note: String) {
        state.note = note
    }

    func changeRating(_ rating: Int) {
        state.rating = rating
    }

    func send() {
        guard let rating = state.rating, let uuid = order.feedbackUUID else { return }
        state.isLoading = true
        let resource = ResourceBuilder.feedbackResource(
            feedbackUUID: uuid,
            rating: rating,
            note: state.note ?? ""
        )
        webClient.load(resource: resource) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let feedback):
                DispatchQueue.main.async { self?.completion(.complete(feedback)) }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    private func loadOrderDetails(_ orderUUID: String) {
        webClient.load(resource: ResourceBuilder.orderDetailsResource(orderUUID: orderUUID)) {[weak self] result in
            guard let self = self, let order = try? result.get() else { return }
            self.state.photos = order.cartItems.map(\.photos)
        }
    }
}
