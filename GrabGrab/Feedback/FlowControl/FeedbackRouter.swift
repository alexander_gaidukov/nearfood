//
//  FeedbackRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol FeedbackRouterType {
    func showFeedback(order: FeedbackOrder, selectedRating: Int?, completion: @escaping (FeedbackAction) -> ())
    func dismiss(completion: @escaping () -> ())
}

final class FeedbackRouter: NSObject, FeedbackRouterType {

    private weak var presentingViewController: UIViewController?
    private var factory: FeedbackViewControllersFactoryType

    init(presentingViewController: UIViewController?, factory: FeedbackViewControllersFactoryType) {
        self.presentingViewController = presentingViewController
        self.factory = factory
    }

    func showFeedback(order: FeedbackOrder, selectedRating: Int?, completion: @escaping (FeedbackAction) -> ()) {
        let controller = factory.feedbackViewController(order: order, selectedRating: selectedRating, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        presentingViewController?.present(controller, animated: true, completion: nil)
    }

    func dismiss(completion: @escaping () -> ()) {
        presentingViewController?.dismiss(animated: true, completion: completion)
    }
}

// MARK: - Transitioning Delegates

extension FeedbackRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
