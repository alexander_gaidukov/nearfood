//
//  FeedbackCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class FeedbackCoordinator: Coordinator {
    let completion: (Feedback?) -> ()
    let router: FeedbackRouterType
    let order: FeedbackOrder
    let selectedRating: Int?

    init(order: FeedbackOrder, selectedRating: Int?, router: FeedbackRouterType, completion: @escaping (Feedback?) -> ()) {
        self.order = order
        self.router = router
        self.selectedRating = selectedRating
        self.completion = completion
    }

    func start() {
        router.showFeedback(order: order, selectedRating: selectedRating) { action in
            self.router.dismiss {
                switch action {
                case .close:
                    self.completion(nil)
                case .complete(let feedback):
                    self.completion(feedback)
                }
            }
        }
    }
}
