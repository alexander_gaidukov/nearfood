//
//  FeedbackViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol FeedbackViewControllersFactoryType {
    func feedbackViewController(
        order: FeedbackOrder,
        selectedRating: Int?,
        completion: @escaping (FeedbackAction) -> ()
    ) -> FeedbackViewController
}

struct FeedbackViewControllersFactory: FeedbackViewControllersFactoryType {

    let webClient: WebClientType

    private let storyboard: Storyboard = .feedback

    func feedbackViewController(
        order: FeedbackOrder,
        selectedRating: Int?,
        completion: @escaping (FeedbackAction) -> ()
    ) -> FeedbackViewController {
        let presenter = FeedbackPresenter(order: order, selectedRating: selectedRating, webClient: webClient, completion: completion)
        let controller = FeedbackViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
