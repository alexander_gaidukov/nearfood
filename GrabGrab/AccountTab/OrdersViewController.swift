//
//  OrdersViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController {

    var presenter: OrdersPresenterType!

    private let disposableBag = DisposableBag()

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var emptyStateView: UIView!
    @IBOutlet private weak var emptyTitleLabel: UILabel!
    @IBOutlet private weak var emptyDescriptionLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        presenter.refresh()
    }

    private func configureViews() {
        title = LocalizedStrings.Account.ordersLabel
        emptyTitleLabel.text = LocalizedStrings.Account.Orders.Empty.title
        emptyDescriptionLabel.text = LocalizedStrings.Account.Orders.Empty.description
        collectionView.register(cellType: OrderCell.self)
        var contentInset = collectionView.contentInset
        contentInset.bottom = Constants.UI.tabBarHeight
        collectionView.contentInset = contentInset
    }

    private func configureBindings() {

        presenter.state.isCollectionViewHidden.bind(to: collectionView, keyPath: \.isHidden).addToDisposableBag(disposableBag)
        presenter.state.isEmptyViewHidden.bind(to: emptyStateView, keyPath: \.isHidden).addToDisposableBag(disposableBag)

        presenter.state.$orders.observe { [weak self] _ in self?.collectionView.reloadData() }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }
}

extension OrdersViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter.state.orders.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: OrderCell = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(with: OrderCellModel(order: presenter.state.orders[indexPath.item]))
        return cell
    }
}

extension OrdersViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.select(presenter.state.orders[indexPath.item])
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        presenter.displayOrder(at: indexPath.item)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        CGSize(width: collectionView.frame.width, height: 108)
    }
}

extension OrdersViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension OrdersViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
