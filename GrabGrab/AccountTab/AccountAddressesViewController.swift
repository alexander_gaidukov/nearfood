//
//  AccountAddressesViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class AccountAddressesViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet private weak var emptyStateView: UIView!
    @IBOutlet private weak var emptyStateTitleLabel: UILabel!
    @IBOutlet private weak var emptyStateDescriptionLabel: UILabel!
    @IBOutlet private weak var emptyStateButton: UIButton!

    var presenter: AccountAddressesPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    private func configureViews() {
        tableView.register(cellType: AccountAddressCell.self)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        tableView.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: Constants.UI.tabBarHeight, right: 0)

        emptyStateTitleLabel.text = LocalizedStrings.Account.Addreses.Empty.title
        emptyStateDescriptionLabel.text = LocalizedStrings.Account.Addreses.Empty.description
        emptyStateButton.setTitle(LocalizedStrings.Account.Addreses.Empty.button, for: .normal)

        title = LocalizedStrings.Account.Addreses.title
        emptyStateButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    private func configureBindings() {
        presenter.state.$locations.observe { [weak self] _ in self?.tableView.reloadData() }.addToDisposableBag(disposableBag)

        presenter.state.isEmpty.observe {[weak self] in
            guard let self = self else { return }
            self.tableView.isHidden = $0
            self.emptyStateView.isHidden = !$0
            let barButtonItem = UIBarButtonItem.barButtonItem(
                withTitle: LocalizedStrings.Common.add,
                target: self,
                selector: #selector(self.addTapped)
            )
            self.navigationItem.rightBarButtonItem = $0 ? nil : barButtonItem
        }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    @objc private func addTapped() {
        presenter.addNew()
    }
}

extension AccountAddressesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.state.locations.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AccountAddressCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: AccountAddressCellModel(location: presenter.state.locations[indexPath.row]))
        return cell
    }
}

extension AccountAddressesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        presenter.select(presenter.state.locations[indexPath.row])
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        guard presenter.canRemoveLocation(at: indexPath) else {
            return .none
        }
        return .delete
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard presenter.canRemoveLocation(at: indexPath) else { return }
        if editingStyle == .delete {
            presenter.remove(presenter.state.locations[indexPath.row])
        }
    }
}

extension AccountAddressesViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension AccountAddressesViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
