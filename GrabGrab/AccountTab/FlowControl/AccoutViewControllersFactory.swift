//
//  AccoutViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol AccountViewControllersFactoryType {
    func accountViewController(completion: @escaping (AccountAction) -> ()) -> AccountViewController
    func addressListViewController(completion: @escaping (AccountAddressesAction) -> ()) -> AccountAddressesViewController
    func addOrUpdateAddressViewController(
        location: Location?,
        completion: @escaping (AddOrUpdateAddressAction) -> ()
    ) -> AddOrUpdateAddressViewController
    func customerNameViewController(completion: @escaping (AccountNameAction) -> ()) -> AccountNameViewController
    func ordersViewController(completion: @escaping (OrdersAction) -> ()) -> OrdersViewController
    func aboutUsViewController(completion: @escaping (AboutUsAction) -> ()) -> AboutUsViewController
    func logoutConfirmationViewController(completion: @escaping (Bool) -> ()) -> UIAlertController
}

struct AccountViewControllersFactory: AccountViewControllersFactoryType {

    private let storyboard: Storyboard = .account

    let dataProvidersStorage: DataProvidersStorageType
    let managersStorage: ManagersStorageType
    let webClient: WebClientType

    func accountViewController(completion: @escaping (AccountAction) -> ()) -> AccountViewController {
        let presenter = AccountPresenter(
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            ordersDataProvider: dataProvidersStorage.ordersDataProvider,
            phoneCodesManager: managersStorage.phoneCodesManager,
            webClient: webClient,
            completion: completion
        )
        let controller = AccountViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func addressListViewController(completion: @escaping (AccountAddressesAction) -> ()) -> AccountAddressesViewController {
        let presenter = AccountAddressesPresenter(
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            webClient: webClient,
            completion: completion
        )
        let controller = AccountAddressesViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func addOrUpdateAddressViewController(
        location: Location?,
        completion: @escaping (AddOrUpdateAddressAction) -> ()
    ) -> AddOrUpdateAddressViewController {
        let presenter = AddOrUpdateAddressPresenter(
            location: location,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            webClient: webClient,
            completion: completion
        )
        let controller = AddOrUpdateAddressViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func customerNameViewController(completion: @escaping (AccountNameAction) -> ()) -> AccountNameViewController {
        let presenter = AccountNamePresenter(
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            webClient: webClient,
            completion: completion
        )
        let controller = AccountNameViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func ordersViewController(completion: @escaping (OrdersAction) -> ()) -> OrdersViewController {
        let presenter = OrdersPresenter(
            ordersDataProvider: dataProvidersStorage.ordersDataProvider,
            completion: completion
        )
        let controller = OrdersViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func aboutUsViewController(completion: @escaping (AboutUsAction) -> ()) -> AboutUsViewController {
        let presenter = AboutUsPresenter(
            companyInfoDataProvider: dataProvidersStorage.companyInfoDataProvider,
            phoneCodesManager: managersStorage.phoneCodesManager,
            completion: completion
        )
        let controller = AboutUsViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func logoutConfirmationViewController(completion: @escaping (Bool) -> ()) -> UIAlertController {
        let alert = UIAlertController(title: LocalizedStrings.Account.logoutConfirmationMessage, message: nil, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: LocalizedStrings.Common.cancel, style: .cancel) {_ in completion(false) }
        alert.addAction(cancelAction)

        let exitAction = UIAlertAction(title: LocalizedStrings.Account.logoutButtonTitle, style: .default) { _ in completion(true) }
        alert.addAction(exitAction)

        return alert
    }
}
