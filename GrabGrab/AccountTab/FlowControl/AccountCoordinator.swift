//
//  AccountCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class AccountCoordinator: TabCoordinator {

    private let router: AccountRouterType
    private let dataProvidersStorage: DataProvidersStorageType
    private let managersStorage: ManagersStorageType
    private let webClient: WebClientType
    private var childCoordinator: Coordinator?

    init(router: AccountRouterType,
         dataProvidersStorage: DataProvidersStorageType,
         managersStorage: ManagersStorageType,
         webClient: WebClientType
    ) {
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
        self.managersStorage = managersStorage
        self.webClient = webClient
    }

    func start() -> UIViewController & TabRootController {
        router.showAccount { action in
            switch action {
            case .select(let menuType):
                self.selectMenu(menuType)
            }
        }
    }

    private func selectMenu(_ menuType: AccountMenuType) {
        switch menuType {
        case .customerName:
            self.showCustomerName()
        case .addresses:
            self.showAddresses()
        case .orders:
            self.showOrders()
        case .paymentMethods:
            self.showPaymentMethods()
        case .aboutUs:
            self.showAboutUs()
        case .invites:
            self.showInvites()
        case .tips:
            self.showTips()
        case .logout:
            self.showLogout()
        }
    }

    private func showInvites() {
        if let coordinator = childCoordinator, coordinator is InvitationsCoordinator { return }

        let factory = InvitationsViewControllersFactory(dataProvidersStorage: dataProvidersStorage)
        let router = InvitationsRouter(factory: factory, presentingViewController: self.router.navController)
        let invitationsCoordinator = InvitationsCoordinator(router: router) { [weak self] in
            self?.childCoordinator = nil
        }
        invitationsCoordinator.start()
        childCoordinator = invitationsCoordinator
    }

    private func showCustomerName() {
        router.showCustomerName { action in
            switch action {
            case .completed:
                self.router.dismiss()
            }
        }
    }

    private func showAddresses() {
        router.showAddressList { action in
            switch action {
            case .select(let location):
                self.showAddOrUpdateAddress(location)
            case .addNew:
                self.showAddOrUpdateAddress(nil)
            }
        }
    }

    private func showAddOrUpdateAddress(_ location: Location?) {
        router.showAddOrUpdateAddress(location: location) { action in
            switch action {
            case .completed:
                self.router.pop()
            case let .selectAddress(location, completion):
                self.showAddressSelection(location: location, completion: completion)
            }
        }
    }

    private func showLogout() {
        router.showLogoutConfirmation {
            if $0 { self.dataProvidersStorage.customerDataProvider.logout() }
        }
    }

    private func showAddressSelection(location: Location, completion: @escaping (Location) -> ()) {
        if let coordinator = childCoordinator, coordinator is AddressesCoordinator { return }

        let factory = AddressesViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = AddressesRouter(presentingViewController: self.router.topViewController, factory: factory)
        let addressesCoordinator = AddressesCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            type: .new(location)
        ) {[weak self] action in
            switch action {
            case .selected(let location):
                completion(location)
            case .closed:
                break
            }
            self?.childCoordinator = nil
        }
        addressesCoordinator.start()
        childCoordinator = addressesCoordinator
    }

    private func showOrders() {
        router.showOrders { action in
            switch action {
            case .select(let order):
                self.showOrderDetails(order)
            }
        }
    }

    private func showOrderDetails(_ order: Order) {
        if let coordinator = childCoordinator, coordinator is OrderCoordinator { return }
        let factory = OrderViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = OrderRouter(presentingViewController: self.router.topViewController, factory: factory)
        let orderCoordinator = OrderCoordinator(
            orderUUID: order.uuid,
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        ) { [weak self] in
            self?.childCoordinator = nil
        }
        orderCoordinator.start()
        childCoordinator = orderCoordinator
    }

    private func showPaymentMethods() {
        if let coordinator = childCoordinator, coordinator is PaymentMethodsCoordinator { return }
        let factory = PaymentMethodsViewControllersFactory(
            webClient: webClient,
            dataProvidersStorage: dataProvidersStorage
        )
        let router = PaymentMethodsRouter(presentingViewController: self.router.topViewController, factory: factory)
        let paymentMethodsCoordinator = PaymentMethodsCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            webClient: webClient,
            markCurrentPayment: true
        ) {[weak self] _, _ in
            self?.childCoordinator = nil
        }
        paymentMethodsCoordinator.start()
        childCoordinator = paymentMethodsCoordinator
    }

    private func showAboutUs() {
        router.showAboutUs { action in
            switch action {
            case .document(let document):
                self.showDocument(document)
            }
        }
    }

    private func showTips() {
        if let coordinator = childCoordinator, coordinator is TipsCoordinator { return }

        let factory = TipsViewControllersFactory(dataProvidersStorage: dataProvidersStorage, webClient: webClient)
        let router = TipsRouter(factory: factory, presentingViewController: self.router.navController)
        let tipsCoordinator = TipsCoordinator(router: router, context: .account) { [weak self] in
            self?.childCoordinator = nil
        }
        tipsCoordinator.start()
        childCoordinator = tipsCoordinator
    }

    private func showDocument(_ document: CompanyInfo.Document) {
        router.showDocument(document)
    }
}
