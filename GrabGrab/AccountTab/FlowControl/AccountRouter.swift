//
//  AccountRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import SafariServices

protocol AccountRouterType {
    var topViewController: UIViewController? { get }
    var navController: UINavigationController! { get }
    func showAccount(completion: @escaping (AccountAction) -> ()) -> UINavigationController
    func showAddressList(completion: @escaping (AccountAddressesAction) -> ())
    func showAddOrUpdateAddress(location: Location?, completion: @escaping (AddOrUpdateAddressAction) -> ())
    func showCustomerName(completion: @escaping (AccountNameAction) -> ())
    func showOrders(completion: @escaping (OrdersAction) -> ())
    func showAboutUs(completion: @escaping (AboutUsAction) -> ())
    func showLogoutConfirmation(completion: @escaping (Bool) -> ())
    func showDocument(_ document: CompanyInfo.Document)
    func pop()
    func dismiss()
}

final class AccountRouter: NSObject, AccountRouterType {
    private let factory: AccountViewControllersFactoryType

    private weak var navigationController: UINavigationController?

    var navController: UINavigationController! {
        navigationController
    }

    init(factory: AccountViewControllersFactoryType) {
        self.factory = factory
    }

    func showAccount(completion: @escaping (AccountAction) -> ()) -> UINavigationController {
        let controller = factory.accountViewController(completion: completion)
        let navController = GGNavigationController(rootViewController: controller)
        navigationController = navController
        return navController
    }

    func showAddressList(completion: @escaping (AccountAddressesAction) -> ()) {
        let controller = factory.addressListViewController(completion: completion)
        navigationController?.pushViewController(controller, animated: true)
    }

    func showAddOrUpdateAddress(location: Location?, completion: @escaping (AddOrUpdateAddressAction) -> ()) {
        let controller = factory.addOrUpdateAddressViewController(location: location, completion: completion)
        navigationController?.pushViewController(controller, animated: true)
    }

    func showCustomerName(completion: @escaping (AccountNameAction) -> ()) {
        let controller = factory.customerNameViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showOrders(completion: @escaping (OrdersAction) -> ()) {
        let controller = factory.ordersViewController(completion: completion)
        navigationController?.pushViewController(controller, animated: true)
    }

    func showAboutUs(completion: @escaping (AboutUsAction) -> ()) {
        let controller = factory.aboutUsViewController(completion: completion)
        navigationController?.pushViewController(controller, animated: true)
    }

    func showLogoutConfirmation(completion: @escaping (Bool) -> ()) {
        let controller = factory.logoutConfirmationViewController(completion: completion)
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func pop() {
        navigationController?.popViewController(animated: true)
    }

    func dismiss() {
        topViewController?.dismiss(animated: true, completion: nil)
    }

    func showDocument(_ document: CompanyInfo.Document) {
        guard let url = URL(string: document.url) else { return }
        let controller = SFSafariViewController(url: url)
        topViewController?.present(controller, animated: true, completion: nil)
    }

    var topViewController: UIViewController? {
        navigationController
    }
}

extension AccountRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
