//
//  AccountNameViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class AccountNameViewController: UIViewController, PopupViewController, KeyboardAvoiding {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var nameTextField: GGTextField!
    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet private weak var keyboardHidingView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!

    var presenter: AccountNamePresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
        nameTextField.becomeFirstResponder()
    }

    private func configureViews() {
        nameTextField.placeholder = LocalizedStrings.Account.Name.placeholder
        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        titleLabel.text = LocalizedStrings.Account.Name.title
        descriptionLabel.text = LocalizedStrings.Account.Name.description
        saveButton.setTitle(LocalizedStrings.Common.save, for: .normal)
        cancelButton.setTitle(LocalizedStrings.Common.cancel, for: .normal)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        containerView.addGestureRecognizer(panGestureRecognizer)

        keyboardHidingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
    }

    private func configureBindings() {

        presenter.state.$name.bind(to: nameTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.nameError.bind(to: nameTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.isSaveButtonEnabled.bind(to: saveButton, keyPath: \.isEnabled).addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    @IBAction private func saveTapped() {
        presenter.save()
    }

    @IBAction private func cancelTapped() {
        presenter.close()
    }

    @objc func close() {
        if nameTextField.isFirstResponder {
            hideKeyboard()
        } else {
            presenter.close()
        }
    }

    @objc private func hideKeyboard() {
        nameTextField.resignFirstResponder()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        presenter.changeName(text)
    }

}

extension AccountNameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboardHidingView.isHidden = false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        keyboardHidingView.isHidden = true
    }
}

extension AccountNameViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension AccountNameViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
