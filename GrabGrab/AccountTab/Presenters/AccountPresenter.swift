//
//  AccountPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AccountPresenterType {
    var state: AccountViewState { get }
    var models: Observable<[TableViewSectionModel]> { get }
    func refresh()
}

enum AccountAction {
    case select(AccountMenuType)
}

struct AccountViewState {
    var phoneNumber: String?

    @Observable
    var customer: Customer?

    @Observable
    var newOrdersCount: Int

    @Observable
    var bonuses: Double?
}

final class AccountPresenter: AccountPresenterType {
    private let completion: (AccountAction) -> ()

    private let customerDataProvider: CustomerDataProviderType
    private let ordersDataProvider: OrdersDataProviderType
    private let webClient: WebClientType

    var state: AccountViewState

    var models: Observable<[TableViewSectionModel]> {
        state.$customer
            .combine(with: state.$newOrdersCount)
            .combine(with: state.$bonuses).map {
                [
                    AccountBonusesSectionModel(amount: $1 ?? 0),
                    AccountMenuSectionModel(customer: $0.0, newOrdersCount: $0.1) { [weak self] in
                        self?.select(menuType: $0)
                    }
                ]
        }
    }

    init(customerDataProvider: CustomerDataProviderType,
         ordersDataProvider: OrdersDataProviderType,
         phoneCodesManager: PhoneCodesManagerType,
         webClient: WebClientType, completion: @escaping (AccountAction) -> ()
    ) {
        self.completion = completion
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        let customer = customerDataProvider.customer
        self.ordersDataProvider = ordersDataProvider

        let phoneNumber = (customer?.phone).map {
            phoneCodesManager.phoneNumber(from: $0)
        }?.formattedNumber

        state = AccountViewState(
            phoneNumber: phoneNumber,
            customer: customer,
            newOrdersCount: ordersDataProvider.activeOrders.count,
            bonuses: customerDataProvider.customer?.bonuses
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(customerDidChange),
            name: .customerDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(currentOrderDidChange),
            name: .currentOrderDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(bonusesDidChange),
            name: .bonusesDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(customerDidChange),
            name: .tipsDidChangeNotification,
            object: nil
        )
    }

    func refresh() {
        guard let customer = customerDataProvider.customer else { return }
        webClient.load(resource: ResourceBuilder.bonusesResource(userUUID: customer.uuid)) {[weak self] result in
            guard let self = self, let balance = try? result.get() else { return }
            self.state.bonuses = balance
        }
    }

    func select(menuType: AccountMenuType) {
        completion(.select(menuType))
    }

    @objc private func customerDidChange() {
        state.customer = customerDataProvider.customer
    }

    @objc private func currentOrderDidChange() {
        state.newOrdersCount = ordersDataProvider.activeOrders.count
    }

    @objc private func bonusesDidChange() {
        state.bonuses = customerDataProvider.customer?.bonuses
    }
}
