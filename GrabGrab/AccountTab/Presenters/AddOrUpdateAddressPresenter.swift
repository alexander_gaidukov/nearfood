//
//  AddOrUpdateAddressPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AddOrUpdateAddressPresenterType {
    var state: AddorUpdateAddressViewState { get }
    func save()
    func changeTitle(_ title: String)
    func changeApartments(_ apartments: String)
    func changeEntrance(_ entrance: String)
    func changeFloor(_ floor: String)
    func changeDoorPhone(_ doorphone: String)
    func changeNote(_ note: String)
    func changeAddress()
    func close()
}

enum AddOrUpdateAddressAction {
    case completed
    case selectAddress(Location, (Location) -> ())
}

struct AddorUpdateAddressViewState {

    fileprivate var isNew: Bool {
        location.uuid.isEmpty
    }

    var header: String? {
        isNew ? LocalizedStrings.Account.Addreses.New.title : location.title
    }

    var isAddressFieldEnabled: Bool {
        isNew
    }

    @Observable
    var location: Location

    @Observable
    var isLoading = false

    @Observable
    var error: WebError?

    var title: Observable<String?> {
        $location.map(\.title)
    }

    var address: Observable<String?> {
        $location.map(\.address)
    }

    var apartments: Observable<String?> {
        $location.map(\.apartments)
    }

    var entrance: Observable<String?> {
        $location.map(\.entrance)
    }

    var floor: Observable<String?> {
        $location.map(\.floor)
    }

    var doorphone: Observable<String?> {
        $location.map(\.doorPhone)
    }

    var note: Observable<String?> {
        $location.map(\.note)
    }

    var isSaveButtonEnabled: Observable <Bool> {
        $location.map { $0.title?.isEmpty == false && $0.address?.isEmpty == false }
    }

    var titleError: Observable<String?> {
        $error.map { $0?.apiError?.title?.localizedDescription }
    }

    var addressError: Observable<String?> {
        $error.map { $0?.apiError?.address?.localizedDescription }
    }

    var apartmentsError: Observable<String?> {
        $error.map { $0?.apiError?.apartments?.localizedDescription }
    }

    var entranceError: Observable<String?> {
        $error.map { $0?.apiError?.entrance?.localizedDescription }
    }

    var floorError: Observable<String?> {
        $error.map { $0?.apiError?.floor?.localizedDescription }
    }

    var doorPhoneError: Observable<String?> {
        $error.map { $0?.apiError?.door_phone?.localizedDescription }
    }
}

final class AddOrUpdateAddressPresenter: AddOrUpdateAddressPresenterType {
    private let completion: (AddOrUpdateAddressAction) -> ()
    private let customerDataProvider: CustomerDataProviderType
    private let webClient: WebClientType

    var state: AddorUpdateAddressViewState

    init(
        location: Location?,
        customerDataProvider: CustomerDataProviderType,
        webClient: WebClientType,
        completion: @escaping (AddOrUpdateAddressAction) -> ()
    ) {
        self.completion = completion
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        self.state = AddorUpdateAddressViewState(location: location ?? Location(latitude: 0, longitude: 0))
    }

    func save() {

        guard let customer = customerDataProvider.customer else { return }

        state.isLoading = true

        let addResource =  ResourceBuilder.saveLocationResource(
            location: state.location,
            customerUUID: customer.uuid
        ).map { $0.1 }

        let updateResource = ResourceBuilder.updateLocationResource(
            location: state.location,
            customerUUID: customer.uuid
        )

        webClient.load(combinedResource: state.isNew ? addResource : updateResource) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let locations):
                self?.customerDataProvider.updateAddresses(locations)
                DispatchQueue.main.async {
                    self?.completion(.completed)
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    func changeTitle(_ title: String) {
        state.location.title = title
    }

    func changeEntrance(_ entrance: String) {
        state.location.entrance = entrance
    }

    func changeApartments(_ apartments: String) {
        state.location.apartments = apartments
    }

    func changeFloor(_ floor: String) {
        state.location.floor = floor
    }

    func changeDoorPhone(_ doorphone: String) {
        state.location.doorPhone = doorphone
    }

    func changeNote(_ note: String) {
        state.location.note = note
    }

    func changeAddress() {
        completion(.selectAddress(state.location, {[weak self] location in
            guard let self = self else { return }
            var newLocation = location
            newLocation.title = self.state.location.title
            self.state.location = newLocation
        }))
    }

    func close() {
        completion(.completed)
    }
}
