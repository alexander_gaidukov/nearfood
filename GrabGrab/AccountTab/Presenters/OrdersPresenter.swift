//
//  OrdersPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol OrdersPresenterType {
    var state: OrdersViewState { get }
    func select(_ order: Order)
    func refresh()
    func displayOrder(at index: Int)
}

enum OrdersAction {
    case select(Order)
}

struct OrdersViewState {
    @Observable
    var orders: [Order]

    @Observable
    var isLoading: Bool = false

    @Observable
    var error: WebError?

    var isEmptyViewHidden: Observable<Bool> {
        $orders.combine(with: $isLoading).map { !$0.isEmpty || $1 }
    }

    var isCollectionViewHidden: Observable<Bool> {
        $orders.map { $0.isEmpty }
    }
}

final class OrdersPresenter: OrdersPresenterType {

    var state: OrdersViewState

    private let completion: (OrdersAction) -> ()
    private let ordersDataProvider: OrdersDataProviderType

    private let offsetToLoad = 5
    private var isOrdersRefreshing = false

    init(ordersDataProvider: OrdersDataProviderType, completion: @escaping (OrdersAction) -> ()) {
        self.completion = completion
        self.ordersDataProvider = ordersDataProvider
        self.state = OrdersViewState(orders: ordersDataProvider.orders)
        NotificationCenter.default.addObserver(self, selector: #selector(ordersDidChange), name: .ordersDidChangeNotification, object: nil)
    }

    func refresh() {
        guard !isOrdersRefreshing else { return }
        isOrdersRefreshing = true
        if state.orders.isEmpty { state.isLoading = true }
        ordersDataProvider.refreshOrders {[weak self] error in
            guard let self = self else { return }
            self.isOrdersRefreshing = false
            if self.state.isLoading {
                self.state.isLoading = false
                self.state.error = error
            }
        }
    }

    func displayOrder(at index: Int) {
        guard index >= state.orders.count - offsetToLoad else { return }
        guard !isOrdersRefreshing else { return }
        isOrdersRefreshing = true
        ordersDataProvider.loadNextOrders { [weak self] _ in
            self?.isOrdersRefreshing = false
        }
    }

    func select(_ order: Order) {
        completion(.select(order))
    }

    @objc private func ordersDidChange() {
        state.orders = ordersDataProvider.orders
    }
}
