//
//  AccountNamePresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AccountNamePresenterType {
    var state: AccountNameViewState { get }
    func save()
    func close()
    func changeName(_ name: String)
}

enum AccountNameAction {
    case completed
}

struct AccountNameViewState {
    @Observable
    var isLoading = false

    @Observable
    var name: String?

    @Observable
    var error: WebError?

    var isSaveButtonEnabled: Observable<Bool> {
        $name.map { $0?.isEmpty == false }
    }

    var nameError: Observable<String?> {
        $error.map {$0?.apiError?.name?.localizedDescription}
    }
}

final class AccountNamePresenter: AccountNamePresenterType {
    private let completion: (AccountNameAction) -> ()
    private let customerDataProvider: CustomerDataProviderType
    private let webClient: WebClientType

    var state: AccountNameViewState

    init(customerDataProvider: CustomerDataProviderType, webClient: WebClientType, completion: @escaping (AccountNameAction) -> ()) {
        self.completion = completion
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        self.state = AccountNameViewState(name: customerDataProvider.customer?.name)
    }

    func save() {

        guard let name = state.name,
              !name.isEmpty,
              let customer = customerDataProvider.customer else {
            return
        }

        state.isLoading = true

        let resource = ResourceBuilder.updateCustomerResource(name: name, uuid: customer.uuid)

        webClient.load(resource: resource) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let customer):
                self?.customerDataProvider.changeName(name: customer.name)
                DispatchQueue.main.async {
                    self?.completion(.completed)
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    func changeName(_ name: String) {
        state.name = name
    }

    func close() {
        completion(.completed)
    }
}
