//
//  AboutUsPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AboutUsPresenterType {
    var state: AboutUsViewState { get }
}

enum AboutUsAction {
    case document(CompanyInfo.Document)
}

struct AboutUsViewState {
    @Observable
    var models: [TableViewSectionModel] = []

    @Observable
    var isLoading: Bool = false

    @Observable
    var error: WebError?
}

final class AboutUsPresenter: AboutUsPresenterType {
    private let completion: (AboutUsAction) -> ()
    private let companyInfoDataProvider: CompanyInfoDataProviderType
    private let phoneCodesManager: PhoneCodesManagerType

    var state = AboutUsViewState()

    init(
        companyInfoDataProvider: CompanyInfoDataProviderType,
        phoneCodesManager: PhoneCodesManagerType,
        completion: @escaping (AboutUsAction) -> ()
    ) {
        self.completion = completion
        self.companyInfoDataProvider = companyInfoDataProvider
        self.phoneCodesManager = phoneCodesManager
        state.models = configureModels()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(companyInfoDidChange),
            name: .companyInfoDidChangeNotification,
            object: nil
        )
        updateInfo()
    }

    @objc private func companyInfoDidChange() {
        state.models = configureModels()
    }

    private func updateInfo() {
        let isSilentLoading = !state.models.isEmpty
        if !isSilentLoading { state.isLoading = true }
        companyInfoDataProvider.update { [weak self] error in
            if !isSilentLoading {
                self?.state.error = error
                self?.state.isLoading = false
            }
        }
    }

    private func configureModels() -> [TableViewSectionModel] {
        guard let info = companyInfoDataProvider.info else { return [] }
        let phone = info.phone.map { phoneCodesManager.phoneNumber(from: $0) }
        let mail = info.email
        let contactsModel = InfoContactsSectionViewModel(
            phone: phone,
            email: mail,
            call: { [weak self] in self?.call(to: phone) },
            write: { [weak self] in self?.email(to: mail) }
        )
        let documentsModel = InfoDocumentsSectionViewModel(documents: info.documents) {[weak self] doc in self?.completion(.document(doc))}
        let linksModel = InfoLinksSectionModel(links: info.links) { [weak self] link in self?.moveToLink(link) }
        return [contactsModel, linksModel, documentsModel]
    }

    private func call(to phone: PhoneNumber?) {
        guard let phone = phone else { return }
        UIApplication.shared.call(to: phone)
    }

    private func email(to mail: String?) {
        guard let mail = mail else { return }
        UIApplication.shared.email(to: mail)
    }

    private func moveToLink(_ link: CompanyInfo.Link) {
        guard let url = URL(string: link.url) else { return }
        UIApplication.shared.openIfAvailable(url)
    }
}
