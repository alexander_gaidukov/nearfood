//
//  AccountAddressesPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AccountAddressesPresenterType {
    var state: AccountAddressesViewState { get }
    func select(_ location: Location)
    func remove(_ location: Location)
    func addNew()
    func canRemoveLocation(at indexPath: IndexPath) -> Bool
}

enum AccountAddressesAction {
    case select(Location)
    case addNew
}

struct AccountAddressesViewState {
    @Observable
    var isLoading = false

    @Observable
    var locations: [Location]

    @Observable
    var error: WebError?

    var isEmpty: Observable<Bool> {
        $locations.combine(with: $isLoading).map { $0.isEmpty && !$1 }
    }
}

final class AccountAddressesPresenter: AccountAddressesPresenterType {

    var state: AccountAddressesViewState

    private let completion: (AccountAddressesAction) -> ()
    private let customerDataProvider: CustomerDataProviderType
    private let webClient: WebClientType

    init(customerDataProvider: CustomerDataProviderType, webClient: WebClientType, completion: @escaping (AccountAddressesAction) -> ()) {
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        self.completion = completion
        self.state = AccountAddressesViewState(
            locations: customerDataProvider.customer?.locations?.filter { $0.title?.isEmpty == false }.reversed() ?? []
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(locationsDidChange),
            name: .locationDidChangeNotification,
            object: nil
        )
        refreshLocations()
    }

    func select(_ location: Location) {
        completion(.select(location))
    }

    func addNew() {
        completion(.addNew)
    }

    func remove(_ location: Location) {
        guard let customer = customerDataProvider.customer else { return }
        state.isLoading = true
        let resource = ResourceBuilder.removeLocationResource(location: location, customerUUID: customer.uuid)
        webClient.load(combinedResource: resource) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let locations):
                self?.customerDataProvider.updateAddresses(locations)
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    func canRemoveLocation(at indexPath: IndexPath) -> Bool {
        (customerDataProvider.customer?.locations?.count ?? 0) > 1
    }

    @objc private func locationsDidChange() {
        state.locations = customerDataProvider.customer?.locations?.filter { $0.title?.isEmpty == false }.reversed() ?? []
    }

    private func refreshLocations() {
        guard let customer = customerDataProvider.customer else { return }
        let resource = ResourceBuilder.locationsResource(customerUUID: customer.uuid)
        webClient.load(resource: resource) {[weak self] result in
            guard let locations = try? result.get() else { return }
            self?.customerDataProvider.updateAddresses(locations)
        }
    }
}
