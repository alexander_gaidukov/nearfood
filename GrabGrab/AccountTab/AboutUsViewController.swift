//
//  AboutUsViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class AboutUsViewController: UIViewController {
    var presenter: AboutUsPresenterType!

    @IBOutlet private weak var tableView: UITableView!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    private func configureViews() {
        title = LocalizedStrings.Info.title
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: Constants.UI.tabBarHeight, right: 0)
        presenter.state.models.forEach { $0.registerCells(in: tableView) }
    }

    private func configureBindings() {

        presenter.state.$models.observe { [weak self] _ in self?.tableView.reloadData() }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }
}

extension AboutUsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.state.models.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.state.models[section].numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        presenter.state.models[indexPath.section].cell(tableView: tableView, indexPath: indexPath)
    }
}

extension AboutUsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.state.models[indexPath.section].didSelect(cell: tableView.cellForRow(at: indexPath), at: indexPath)
    }
}

extension AboutUsViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension AboutUsViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
