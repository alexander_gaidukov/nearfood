//
//  AddOrUpdateAddressViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class AddOrUpdateAddressViewController: UIViewController {

    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var apartmentsLabel: UILabel!
    @IBOutlet private weak var entranceLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var floorLabel: UILabel!
    @IBOutlet private weak var doorPhoneLabel: UILabel!
    @IBOutlet private weak var noteLabel: UILabel!

    @IBOutlet private weak var titleTextField: GGTextField!
    @IBOutlet private weak var addressTextField: GGTextField!
    @IBOutlet private weak var apartmentsTextField: GGTextField!
    @IBOutlet private weak var entranceTextField: GGTextField!
    @IBOutlet private weak var floorTextField: GGTextField!
    @IBOutlet private weak var doorPhoneTextField: GGTextField!
    @IBOutlet private weak var noteTextView: GGTextView!

    var presenter: AddOrUpdateAddressPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    private func configureViews() {
        navigationItem.rightBarButtonItem = UIBarButtonItem.barButtonItem(
            withTitle: LocalizedStrings.Common.save,
            target: self,
            selector: #selector(saveTapped)
        )

        if navigationController?.viewControllers.count == 1 {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: LocalizedStrings.Common.close,
                style: .plain,
                target: self,
                action: #selector(closeTapped)
            )
        }

        titleLabel.text = LocalizedStrings.Account.Addreses.New.addressTitleLabel
        titleTextField.placeholder = LocalizedStrings.Account.Addreses.New.addressTitlePlaceholder
        addressLabel.text = LocalizedStrings.Addresses.addressLabel
        addressTextField.placeholder = LocalizedStrings.Addresses.addressPlaceholder
        apartmentsLabel.text = LocalizedStrings.Addresses.apartmentsLabel
        entranceLabel.text = LocalizedStrings.Addresses.entranceLabel
        floorLabel.text = LocalizedStrings.Addresses.floorLabel
        doorPhoneLabel.text = LocalizedStrings.Addresses.doorphoneLabel
        noteLabel.text = LocalizedStrings.Addresses.noteLabel

        noteTextView.placeholder = LocalizedStrings.Addresses.notePlaceholder
        noteTextView.didChange = { [weak self] in self?.presenter.changeNote($0) }

        addressTextField.isEnabled = presenter.state.isAddressFieldEnabled
        title = presenter.state.header
    }

    private func configureBindings() {

        presenter.state.title.bind(to: titleTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.titleError.bind(to: titleTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.address.bind(to: addressTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.addressError.bind(to: addressTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.apartments.bind(to: apartmentsTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.apartmentsError.bind(to: apartmentsTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.entrance.bind(to: entranceTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.entranceError.bind(to: entranceTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.floor.bind(to: floorTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.floorError.bind(to: floorTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.doorphone.bind(to: doorPhoneTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.doorPhoneError.bind(to: doorPhoneTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.note.bind(to: noteTextView, keyPath: \.text).addToDisposableBag(disposableBag)

        if let saveButton = self.navigationItem.rightBarButtonItem?.customView as? UIButton {
            presenter.state.isSaveButtonEnabled.bind(to: saveButton, keyPath: \.isEnabled).addToDisposableBag(disposableBag)
        }

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    @objc private func saveTapped() {
        presenter.save()
    }

    @objc private func closeTapped() {
        presenter.close()
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        if textField == titleTextField {
            presenter.changeTitle(text)
        } else if textField == apartmentsTextField {
            presenter.changeApartments(text)
        } else if textField == entranceTextField {
            presenter.changeEntrance(text)
        } else if textField == floorTextField {
            presenter.changeFloor(text)
        } else if textField == doorPhoneTextField {
            presenter.changeDoorPhone(text)
        }
    }

}

extension AddOrUpdateAddressViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == addressTextField {
            presenter.changeAddress()
            return false
        }
        return true
    }
}

extension AddOrUpdateAddressViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension AddOrUpdateAddressViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
