//
//  AccountBonusesCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct AccountBonusesCellModel {
    let amount: String
}

extension AccountBonusesCellModel {
    init(amount: Double) {
        self.amount = amount.string(with: Formatters.priceFormatter) ?? ""
    }
}

final class AccountBonusesCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Account.bonusesLabel
        amountLabel.font = .boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .title2).pointSize)
    }

    func configure(model: AccountBonusesCellModel) {
        amountLabel.text = model.amount
    }
}
