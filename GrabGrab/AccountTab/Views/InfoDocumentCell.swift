//
//  InfoDocumentCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct InfoDocumentCellModel {
    var document: CompanyInfo.Document
    var isSeparatorHidden: Bool
    var title: String {
        document.name
    }
}

class InfoDocumentCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var separatorView: UIView!

    func configure(with model: InfoDocumentCellModel) {
        titleLabel.text = model.title
        separatorView.isHidden = model.isSeparatorHidden
    }
}
