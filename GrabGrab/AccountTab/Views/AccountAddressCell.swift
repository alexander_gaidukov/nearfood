//
//  AccountAddressCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct AccountAddressCellModel {
    let title: String?
    let address: String
    var titleViewHidden: Bool {
        title == nil
    }
}

extension AccountAddressCellModel {
    init(location: Location) {
        title = location.title
        address = location.deliveryAddress
    }
}

class AccountAddressCell: UITableViewCell {

    @IBOutlet private weak var titleView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var hideTitleConstraint: NSLayoutConstraint!

    func configure(model: AccountAddressCellModel) {
        if model.titleViewHidden {
            titleView.isHidden = true
            hideTitleConstraint.priority = .defaultHigh
        } else {
            titleView.isHidden = false
            hideTitleConstraint.priority = .defaultLow
        }

        titleLabel.text = model.title
        addressLabel.text = model.address
    }

}
