//
//  OrderCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderCellModel {
    var title: String {
        order.title
    }
    var statusModel: OrderStatusModel {
        OrderStatusModel(order: order, isList: true)
    }
    var timerInfo: TimerInfoModel {
        TimerInfoModel(order: order, isCard: true)
    }

    var photos: [[Photo]] {
        order.cartItems.map(\.photos)
    }

    var order: Order
}

class OrderCell: UICollectionViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var timerLabel: UILabel!
    @IBOutlet private weak var timerView: UIView!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var stackView: ImagesStackView!
    @IBOutlet private weak var addionalDishesLabel: UILabel!

    private var isTimerRun = false

    private var model: OrderCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        isAccessibilityElement = true
        accessibilityIdentifier = Identifiers.Order.cell
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        stackView.prepareForReuse()
        isTimerRun = false
    }

    func configure(with model: OrderCellModel) {

        self.model = model

        if !self.isTimerRun && model.timerInfo.shouldTick {
            isTimerRun = true
            runTimer()
        }
    }

    private func updateUI() {
        titleLabel.text = model.title
        statusLabel.text = model.statusModel.label
        statusLabel.textColor = model.statusModel.color
        timerView.isHidden = model.timerInfo.isHidden
        configureTimer()

        let additionalCount = model.photos.count - stackView.arrangedSubviews.count
        if additionalCount > 0 {
            addionalDishesLabel.text = "+\(additionalCount)"
            addionalDishesLabel.isHidden = false
        } else {
            addionalDishesLabel.isHidden = true
        }
        stackView.configure(with: model.photos)
    }

    private func configureTimer() {
        timerView.backgroundColor = model.timerInfo.backgroundColor
        timerLabel.textColor = model.timerInfo.color
        timerLabel.text = model.timerInfo.label
    }

    private func runTimer() {
        guard isTimerRun else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            guard let self = self, self.isTimerRun else { return }
            self.configureTimer()
            self.runTimer()
        }
    }
}
