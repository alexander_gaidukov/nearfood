//
//  AccountBonusesSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct AccountBonusesSectionModel: TableViewSectionModel {
    private let model: AccountBonusesCellModel
    private let isEmpty: Bool

    init(amount: Double) {
        isEmpty = amount <= 0
        model = AccountBonusesCellModel(amount: amount)
    }

    var numberOfRows: Int {
        isEmpty ? 0 : 1
    }

    func registerCells(in tableView: UITableView) {}

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: AccountBonusesCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
