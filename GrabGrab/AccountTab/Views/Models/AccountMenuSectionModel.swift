//
//  AccountMenuSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct AccountMenuSectionModel: TableViewSectionModel {
    private let models: [AccountMenuCellModel]
    private let didSelect: (AccountMenuType) -> ()

    // swiftlint:disable:next function_body_length
    init(customer: Customer?, newOrdersCount: Int, didSelect: @escaping (AccountMenuType) -> ()) {
        self.didSelect = didSelect
        let nameModel = AccountMenuCellModel(
            type: .customerName,
            image: #imageLiteral(resourceName: "account_profile"),
            title: customer?.name ?? LocalizedStrings.Account.customerNameLabel,
            titleColor: customer?.name == nil ? .labelText : .mainText,
            infoViewType: .none,
            isSeparatorHidden: false,
            isChevronViewHidden: false,
            identifier: Identifiers.Account.name
        )

        let addressesModel = AccountMenuCellModel(
            type: .addresses,
            image: #imageLiteral(resourceName: "account_locations"),
            title: LocalizedStrings.Account.addressesLabel,
            titleColor: .mainText,
            infoViewType: .none,
            isSeparatorHidden: false,
            isChevronViewHidden: false,
            identifier: Identifiers.Account.addresses
        )

        let ordersModel = AccountMenuCellModel(
            type: .orders,
            image: #imageLiteral(resourceName: "account_orders"),
            title: LocalizedStrings.Account.ordersLabel,
            titleColor: .mainText,
            infoViewType: newOrdersCount == 0 ? .none : .badge("\(newOrdersCount)"),
            isSeparatorHidden: false,
            isChevronViewHidden: false,
            identifier: Identifiers.Account.orders
        )

        let paymentMethodsModel = AccountMenuCellModel(
            type: .paymentMethods,
            image: #imageLiteral(resourceName: "account_payment_methods"),
            title: LocalizedStrings.Account.paymentMethodsLabel,
            titleColor: .mainText,
            infoViewType: .none,
            isSeparatorHidden: false,
            isChevronViewHidden: false,
            identifier: Identifiers.Account.paymentMethods
        )

        let aboutUsModel = AccountMenuCellModel(
            type: .aboutUs,
            image: #imageLiteral(resourceName: "account_contacts"),
            title: LocalizedStrings.Account.aboutUsLabel,
            titleColor: .mainText,
            infoViewType: .none,
            isSeparatorHidden: false,
            isChevronViewHidden: false,
            identifier: Identifiers.Account.contacts
        )

        let invitesModel = AccountMenuCellModel(
            type: .invites,
            image: #imageLiteral(resourceName: "account_invitations"),
            title: LocalizedStrings.Invitations.title,
            titleColor: .mainText,
            infoViewType: .none,
            isSeparatorHidden: false,
            isChevronViewHidden: false,
            identifier: Identifiers.Account.invitations
        )

        let tipsModel = AccountMenuCellModel(
            type: .tips,
            image: #imageLiteral(resourceName: "account_tips"),
            title: LocalizedStrings.Account.tipsLabel,
            titleColor: .mainText,
            infoViewType: customer?.tipsSettings.map { .label($0.formattedValue) } ?? .none,
            isSeparatorHidden: false,
            isChevronViewHidden: false,
            identifier: Identifiers.Account.tips
        )

        let logoutModel = AccountMenuCellModel(
            type: .logout,
            image: #imageLiteral(resourceName: "account_logout"),
            title: LocalizedStrings.Account.logoutButtonTitle,
            titleColor: .btnLogout,
            infoViewType: .none,
            isSeparatorHidden: true,
            isChevronViewHidden: true,
            identifier: Identifiers.Account.logout
        )

        models = [
            nameModel,
            addressesModel,
            ordersModel,
            paymentMethodsModel,
            aboutUsModel,
            invitesModel,
            tipsModel,
            logoutModel
        ]
    }

    var numberOfRows: Int {
        models.count
    }

    func registerCells(in tableView: UITableView) {}

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: AccountMenuCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: models[indexPath.row])
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        didSelect(models[indexPath.row].type)
    }
}
