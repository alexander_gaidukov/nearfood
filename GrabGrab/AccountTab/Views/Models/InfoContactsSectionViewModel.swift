//
//  InfoContactsSectionViewModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct InfoContactsSectionViewModel: TableViewSectionModel {

    private let models: [InfoContactCellModel]

    init(phone: PhoneNumber?, email: String?, call: @escaping () -> (), write: @escaping () -> ()) {
        let phoneModel = phone.map { InfoContactCellModel(icon: #imageLiteral(resourceName: "phone"), title: $0.formattedNumber, isSeparatorHidden: false, didSelect: call) }
        let emailModel = email.map { InfoContactCellModel(icon: #imageLiteral(resourceName: "email"), title: $0, isSeparatorHidden: true, didSelect: write) }
        models = [phoneModel, emailModel].compactMap { $0 }
    }

    var numberOfRows: Int {
        models.count
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: InfoContactCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: InfoContactCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: models[indexPath.row])
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        models[indexPath.row].didSelect()
    }
}
