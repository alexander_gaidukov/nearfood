//
//  InfoLinksSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct InfoLinksSectionModel: TableViewSectionModel {

    private let model: InfoLinksCellModel

    init(links: [CompanyInfo.Link], didSelect: @escaping (CompanyInfo.Link) -> ()) {
        model = InfoLinksCellModel(links: links, didSelect: didSelect)
    }

    var numberOfRows: Int {
        1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: InfoLinksCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: InfoLinksCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model)
        return cell
    }
}
