//
//  InfoDocumentsSectionViewModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct InfoDocumentsSectionViewModel: TableViewSectionModel {

    private let models: [InfoDocumentCellModel]
    private let didSelectHandler: (CompanyInfo.Document) -> ()

    init(documents: [CompanyInfo.Document], didSelect: @escaping (CompanyInfo.Document) -> ()) {
        self.didSelectHandler = didSelect
        models = documents.enumerated().map {
            InfoDocumentCellModel(document: $0.element, isSeparatorHidden: $0.offset == documents.count - 1)
        }
    }

    var numberOfRows: Int {
        models.count
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: InfoDocumentCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: InfoDocumentCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: models[indexPath.row])
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        didSelectHandler(models[indexPath.row].document)
    }
}
