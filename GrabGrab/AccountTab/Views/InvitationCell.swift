//
//  InvitationCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 18.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct InvitationCellModel {
    let code: String
    let name: String
    let description: String
    let availableUsages: String?
    let share: () -> ()

    init(invite: Invite, share: @escaping () -> ()) {
        description = invite.description
        code = invite.code
        name = invite.name
        availableUsages = invite.availableUsages.map { LocalizedStrings.Invitations.Cell.availableUsages($0) }
        self.share = share
    }
}

class InvitationCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var usageLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var shareButton: UIButton!

    private var model: InvitationCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        shareButton.setTitle(LocalizedStrings.Invitations.Cell.share, for: .normal)
    }

    func configure(with model: InvitationCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.name
        codeLabel.text = model.code
        descriptionLabel.text = model.description
        usageLabel.text = model.availableUsages
        usageLabel.isHidden = model.availableUsages == nil
    }

    @IBAction private func shareButtonTapped() {
        model.share()
    }
}
