//
//  SocialLinkCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct SocialLinkCellModel {
    var link: CompanyInfo.Link
    var title: String? {
        link.kind.base?.icon == nil ? link.name : nil
    }
    var icon: UIImage? {
        link.kind.base?.icon
    }

    init(link: CompanyInfo.Link) {
        self.link = link
    }
}

class SocialLinkCell: UICollectionViewCell {

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.cornerRadius = imageView.frame.width / 2.0
    }

    func configure(with model: SocialLinkCellModel) {
        imageView.image = model.icon
        titleLabel.text = model.title
    }
}
