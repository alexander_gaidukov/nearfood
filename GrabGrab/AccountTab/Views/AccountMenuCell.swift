//
//  AccountMenuCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum AccountMenuType {
    case customerName
    case addresses
    case orders
    case paymentMethods
    case aboutUs
    case invites
    case tips
    case logout
}

struct AccountMenuCellModel {
    enum InfoViewType {
        case badge(String)
        case label(String)
        case none
    }

    var type: AccountMenuType
    var image: UIImage?
    var title: String
    var titleColor: UIColor
    var infoViewType: InfoViewType
    var isSeparatorHidden: Bool
    var isChevronViewHidden: Bool
    var identifier: String
}

class AccountMenuCell: UITableViewCell {
    @IBOutlet private weak var iconView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var badgeView: UIView!
    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var badgeLabel: UILabel!
    @IBOutlet private weak var separatorView: UIView!
    @IBOutlet private weak var chevronView: UIView!

    func configure(model: AccountMenuCellModel) {
        iconView.image = model.image
        titleLabel.text = model.title
        titleLabel.textColor = model.titleColor
        separatorView.isHidden = model.isSeparatorHidden
        chevronView.isHidden = model.isChevronViewHidden

        badgeView.isHidden = true
        infoLabel.isHidden = true

        switch model.infoViewType {
        case .badge(let value):
            badgeView.isHidden = false
            badgeLabel.text = value
        case .label(let value):
            infoLabel.isHidden = false
            infoLabel.text = value
        case .none:
            break
        }
        accessibilityIdentifier = model.identifier
    }
}
