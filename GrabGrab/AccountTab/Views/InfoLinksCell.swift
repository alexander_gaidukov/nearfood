//
//  InfoLinksCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 02.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct InfoLinksCellModel {
    var models: [SocialLinkCellModel]
    var didSelect: (CompanyInfo.Link) -> ()

    init(links: [CompanyInfo.Link], didSelect: @escaping (CompanyInfo.Link) -> ()) {
        models = links.map(SocialLinkCellModel.init)
        self.didSelect = didSelect
    }
}

class InfoLinksCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var collectionView: UICollectionView!

    private let itemSize = CGSize(width: 55, height: 55)

    private var model: InfoLinksCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Info.followUsLabel
        collectionView.register(cellType: SocialLinkCell.self)
    }

    func configure(with model: InfoLinksCellModel) {
        self.model = model
    }

    private func updateUI() {
        if model.models.count > 1 {
            // swiftlint:disable:next force_cast
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            let offset = layout.sectionInset.left + layout.sectionInset.right
            let spacing = (UIScreen.main.bounds.width - offset - itemSize.width * CGFloat(model.models.count))
                / CGFloat(model.models.count - 1)
            layout.minimumInteritemSpacing = max(spacing, 20.0)
            layout.minimumLineSpacing = max(spacing, 20.0)
        }

        collectionView.reloadData()
    }
}

extension InfoLinksCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        model.models.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SocialLinkCell = collectionView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model.models[indexPath.item])
        return cell
    }
}

extension InfoLinksCell: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        itemSize
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        model.didSelect(model.models[indexPath.item].link)
    }
}
