//
//  InfoContactCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct InfoContactCellModel {
    let icon: UIImage?
    let title: String
    let isSeparatorHidden: Bool
    let didSelect: () -> ()
}

class InfoContactCell: UITableViewCell {
    @IBOutlet private weak var iconView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var separatorView: UIView!

    func configure(with model: InfoContactCellModel) {
        iconView.image = model.icon
        titleLabel.text = model.title
        separatorView.isHidden = model.isSeparatorHidden
    }
}
