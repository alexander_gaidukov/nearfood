//
//  AccountViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    @IBOutlet private weak var phoneNumberLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!

    var presenter: AccountPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        presenter.refresh()
    }

    private func configureView() {
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 0, right: 0)
        tableView.register(cellType: AccountMenuCell.self)
        tableView.register(cellType: AccountBonusesCell.self)

        phoneNumberLabel.text = presenter.state.phoneNumber

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    private func configureBindings() {
        presenter.models.observe { [weak self] _ in
            self?.tableView.reloadData()
        }.addToDisposableBag(disposableBag)
    }
}

extension AccountViewController: TabRootController {
    var tabBarImage: UIImage {
        #imageLiteral(resourceName: "profile_icon")
    }

    var tabBarSelectedImage: UIImage {
        #imageLiteral(resourceName: "profile_icon_selected")
    }

    var identifier: String {
        Identifiers.TabBar.account
    }
}

extension AccountViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.models.wrappedValue.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.models.wrappedValue[section].numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        presenter.models.wrappedValue[indexPath.section].cell(tableView: tableView, indexPath: indexPath)
    }
}

extension AccountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.models.wrappedValue[indexPath.section].didSelect(
            cell: tableView.cellForRow(at: indexPath),
            at: indexPath
        )
    }
}
