//
//  PhoneConfirmationViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class PhoneConfirmationViewController: UIViewController, KeyboardAvoiding {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var codeTitleLabel: UILabel!
    @IBOutlet private weak var codeTextField: GGTextField!
    @IBOutlet private weak var phoneLabel: UILabel!

    @IBOutlet private weak var sendCodeAgainLabel: UILabel!
    @IBOutlet private weak var timerLabel: UILabel!
    @IBOutlet private weak var sendCodeAgainView: UIView!
    @IBOutlet private weak var sendCodeButton: UIButton!

    var presenter: PhoneConfirmationPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
        codeTextField.becomeFirstResponder()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
    }

    private func configureBindings() {
        presenter.state.codeError.bind(to: codeTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)
        presenter.state.isViewActive.bind(to: codeTextField, keyPath: \.isUserInteractionEnabled).addToDisposableBag(disposableBag)
        presenter.state.isSendCodeButtonHidden.bind(to: sendCodeButton, keyPath: \.isHidden).addToDisposableBag(disposableBag)
        presenter.state.isSendCodeViewHidden.bind(to: sendCodeAgainView, keyPath: \.isHidden).addToDisposableBag(disposableBag)
        presenter.state.$secondsLeft.map { $0.formattedTime }.bind(to: timerLabel, keyPath: \.text).addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe {[weak self] isLoading in
            guard let self = self else { return }
            if isLoading && self.codeTextField.isFirstResponder {
                self.codeTextField.resignFirstResponder()
            }
            if isLoading {
                self.showLoading()
            } else {
                self.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] error in
            if let error = error {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    private func configureView() {
        title = LocalizedStrings.PhoneConfirmation.navigationTitle
        codeTitleLabel.text = LocalizedStrings.PhoneConfirmation.codeTitle
        codeTextField.placeholder = LocalizedStrings.PhoneConfirmation.codePlaceholder
        sendCodeAgainLabel.text = LocalizedStrings.PhoneConfirmation.sendCodeAgainLabel
        sendCodeButton.setTitle(LocalizedStrings.PhoneConfirmation.sendCodeAgainButtonTitle, for: .normal)
        phoneLabel.text = presenter.state.phone.formattedNumber
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        presenter.changeCode(textField.text ?? "")
    }

    @IBAction private func sendCodeAgainTapped() {
        presenter.sendCodeAgain()
    }
}

extension PhoneConfirmationViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension PhoneConfirmationViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
