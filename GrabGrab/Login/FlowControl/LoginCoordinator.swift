//
//  LoginCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 29.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class LoginCoordinator: Coordinator {
    var completion: () -> ()
    private let router: LoginRouterType
    private let dataProvidersStorage: DataProvidersStorageType
    private let managersStorage: ManagersStorageType
    private let webClient: WebClientType

    private var childCoordinator: Coordinator?

    init(router: LoginRouterType,
         dataProvidersStorage: DataProvidersStorageType,
         managersStorage: ManagersStorageType,
         webClient: WebClientType,
         completion: @escaping () -> ()) {
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
        self.managersStorage = managersStorage
        self.webClient = webClient
        self.completion = completion
    }

    func start() {
        if !dataProvidersStorage.customerDataProvider.isLoggedIn {
            showLogin()
        } else {
            completion()
        }
    }

    private func showLogin() {
        router.showLogin { action in
            switch action {
            case .loggedIn(let response):
                self.showPhoneConfirmation(withResponse: response)
            case .changeCountry(let completion):
                self.showCountriesList(completion: completion)
            }
        }
    }

    private func showCountriesList(completion: @escaping (Country?) -> ()) {
        if let childCoordinator = childCoordinator, childCoordinator is CountriesCoordinator { return }

        let factory = CountriesViewControllersFactory(managersStorage: managersStorage)
        let router = CountriesRouter(
            presentingController: self.router.window.rootViewController,
            factory: factory
        )

        let coordinator = CountriesCoordinator(router: router) { [weak self] country in
            if let country = country {
                self?.managersStorage.phoneCodesManager.select(country: country)
            }
            completion(country)
            self?.childCoordinator = nil
        }

        coordinator.start()
        childCoordinator = coordinator
    }

    private func showPhoneConfirmation(withResponse response: AuthenticationResponse) {
        self.router.showPhoneConfirmation(withResponse: response) { action in
            switch action {
            case .confirmed(let response):
                self.dataProvidersStorage.customerDataProvider.login(withResponse: response)
                self.completion()
            }
        }
    }
}
