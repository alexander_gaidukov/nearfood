//
//  LoginRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol LoginRouterType {
    var window: UIWindow { get }
    func showLogin(completion: @escaping (LoginAction) -> ())
    func showPhoneConfirmation(withResponse response: AuthenticationResponse, completion: @escaping (PhoneConfirmationAction) -> ())
}

final class LoginRouter: LoginRouterType {
    let window: UIWindow
    private let factory: LoginViewControllersFactoryType

    private var navigationController: GGNavigationController! {
        window.rootViewController as? GGNavigationController
    }

    init(window: UIWindow, factory: LoginViewControllersFactoryType) {
        self.window = window
        self.factory = factory
    }

    func showLogin(completion: @escaping (LoginAction) -> ()) {
        let controller = factory.loginViewController(completion: completion)
        let navController = GGNavigationController(rootViewController: controller)
        window.rootViewController = navController
    }

    func showPhoneConfirmation(withResponse response: AuthenticationResponse, completion: @escaping (PhoneConfirmationAction) -> ()) {
        let controller = factory.phoneConfirmationViewController(withResponse: response, completion: completion)
        navigationController.pushViewController(controller, animated: true)
    }
}
