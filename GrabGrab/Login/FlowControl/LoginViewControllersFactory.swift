//
//  LoginViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 29.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol LoginViewControllersFactoryType {
    func loginViewController(completion: @escaping (LoginAction) -> ()) -> LoginViewController
    func phoneConfirmationViewController(
        withResponse response: AuthenticationResponse,
        completion: @escaping (PhoneConfirmationAction) -> ()
    ) -> PhoneConfirmationViewController
}

struct LoginViewControllersFactory: LoginViewControllersFactoryType {

    let storyboard: Storyboard = .login

    let dataProvidersStorage: DataProvidersStorageType
    let managersStorage: ManagersStorageType
    let webClient: WebClientType

    func loginViewController(completion: @escaping (LoginAction) -> ()) -> LoginViewController {
        let presenter = LoginPresenter(
            webClient: webClient,
            phoneCodesManager: managersStorage.phoneCodesManager,
            completion: completion
        )
        let controller = LoginViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func phoneConfirmationViewController(
        withResponse response: AuthenticationResponse,
        completion: @escaping (PhoneConfirmationAction) -> ()
    ) -> PhoneConfirmationViewController {
        let presenter = PhoneConfirmationPresenter(
            response: response,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            phoneCodesManager: managersStorage.phoneCodesManager,
            webClient: webClient,
            completion: completion
        )
        let controller = PhoneConfirmationViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
