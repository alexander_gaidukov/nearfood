//
//  LoginPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 29.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol LoginPresenterType {
    var state: SignInViewState { get }
    func login()
    func changeCountry()
    func changePhoneNumber(_ number: String)
}

struct SignInViewState {
    @Observable
    var phoneNumber: PhoneNumber

    @Observable
    var error: WebError?

    @Observable
    var isLoading = false

    var isLoginButtonEnabled: Observable<Bool> {
        $phoneNumber.map { $0.isValid }
    }

    var isViewActive: Observable<Bool> {
        $isLoading.map { !$0 }
    }

    var phoneError: Observable<String?> {
        $error.map { $0?.apiError?.phone?.localizedDescription }
    }

    var loginButtonTitle: Observable<String> {
        $isLoading.map { $0 == true ? "" : LocalizedStrings.Login.buttonTitle }
    }
}

enum LoginAction {
    case loggedIn(AuthenticationResponse)
    case changeCountry((Country?) -> ())
}

final class LoginPresenter: LoginPresenterType {

    private let completion: (LoginAction) -> ()
    private let webClient: WebClientType

    private(set) var state: SignInViewState

    init(webClient: WebClientType, phoneCodesManager: PhoneCodesManagerType, completion: @escaping (LoginAction) -> ()) {
        self.webClient = webClient
        self.completion = completion
        self.state = SignInViewState(phoneNumber: PhoneNumber(country: phoneCodesManager.selectedCountry))
    }

    func login() {
        guard state.phoneNumber.isValid else { return }
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.authenticationResource(phone: state.phoneNumber.rawValue)) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self?.completion(.loggedIn(response))
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    func changePhoneNumber(_ number: String) {
        state.phoneNumber.setNumber(number: number)
    }

    func changeCountry() {
        completion(.changeCountry({[weak self] in
            guard let country = $0 else { return }
            self?.state.phoneNumber.country = country
        }))
    }
}
