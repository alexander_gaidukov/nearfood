//
//  PhoneConfirmationPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol PhoneConfirmationPresenterType {
    var state: PhoneConfirmationViewState { get }
    func changeCode(_ code: String)
    func sendCodeAgain()
}

struct PhoneConfirmationViewState {
    @Observable
    var secondsLeft: Int = Constants.Intervals.smsResendInterval

    var phone: PhoneNumber

    @Observable
    var code: String = ""

    @Observable
    var error: WebError?

    @Observable
    var isLoading: Bool = false

    var codeError: Observable<String?> {
        $error.map { $0?.apiError?.code?.localizedDescription }
    }

    var isViewActive: Observable<Bool> {
        $isLoading.map { !$0 }
    }

    var isSendCodeButtonHidden: Observable<Bool> {
        $secondsLeft.map { $0 > 0 }
    }

    var isSendCodeViewHidden: Observable<Bool> {
        $secondsLeft.map { $0 <= 0 }
    }
}

enum PhoneConfirmationAction {
    case confirmed(SignInResponse)
}

final class PhoneConfirmationPresenter: PhoneConfirmationPresenterType {

    private let completion: (PhoneConfirmationAction) -> ()
    private let webClient: WebClientType
    private let customerDataProvider: CustomerDataProviderType
    private var response: AuthenticationResponse

    private(set) var state: PhoneConfirmationViewState
    private var timerStartDate: Date?
    private var timer: Timer?

    init(response: AuthenticationResponse,
         customerDataProvider: CustomerDataProviderType,
         phoneCodesManager: PhoneCodesManagerType,
         webClient: WebClientType,
         completion: @escaping (PhoneConfirmationAction) -> ()
    ) {
        self.webClient = webClient
        self.response = response
        self.customerDataProvider = customerDataProvider
        self.state = PhoneConfirmationViewState(phone: phoneCodesManager.phoneNumber(from: response.phone))
        self.completion = completion
        runTimer()
    }

    func changeCode(_ code: String) {
        state.code = String(code.prefix(4))
        if state.code.count == 4 {
            checkCode()
        }
    }

    func sendCodeAgain() {
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.authenticationResource(phone: response.phone)) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let response):
                self?.response = response
                self?.resetTimer()
                DispatchQueue.main.async {
                    self?.runTimer()
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    private func resetTimer() {
        state.secondsLeft = Constants.Intervals.smsResendInterval
    }

    private func runTimer() {
        guard timer == nil else { return }
        timerStartDate = Date()
        let timer = Timer(timeInterval: 1.0, target: self, selector: #selector(tickTimer), userInfo: nil, repeats: true)
        self.timer = timer
        RunLoop.main.add(timer, forMode: .common)
    }

    private func stopTimer() {
        timerStartDate = nil
        timer?.invalidate()
        timer = nil
    }

    @objc private func tickTimer() {
        guard let startDate = timerStartDate else { return }
        let seconds = Calendar.current.dateComponents([.second], from: startDate, to: Date()).second ?? 0

        self.state.secondsLeft = max(Constants.Intervals.smsResendInterval - seconds, 0)
        if self.state.secondsLeft == 0 {
            self.stopTimer()
        }
    }

    private func checkCode() {
        guard state.code.count == 4 else { return }
        state.isLoading = true
        let resource = ResourceBuilder.phoneConfirmationResource(
            authResponse: response,
            code: state.code,
            device: customerDataProvider.device
        )
        webClient.load(combinedResource: resource) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self?.completion(.confirmed(response))
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }
}
