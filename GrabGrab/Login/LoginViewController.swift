//
//  LoginViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 29.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, KeyboardAvoiding {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var countryCodeLabel: UILabel!
    @IBOutlet private weak var phoneTitleLabel: UILabel!
    @IBOutlet private weak var phoneTextField: GGTextField!
    @IBOutlet private weak var signInButton: UIButton!
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!

    var presenter: LoginPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        configureView()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
    }

    private func configureBindings() {
        presenter.state.$phoneNumber
            .map { $0.country.phoneCode }
            .bind(to: countryCodeLabel, keyPath: \.text)
            .addToDisposableBag(disposableBag)

        presenter.state.$phoneNumber.map { $0.formattedMask }.bind(to: phoneTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.phoneError.bind(to: phoneTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.isLoginButtonEnabled.bind(to: signInButton, keyPath: \.isEnabled).addToDisposableBag(disposableBag)
        presenter.state.isViewActive.bind(to: signInButton, keyPath: \.isUserInteractionEnabled).addToDisposableBag(disposableBag)
        presenter.state.isViewActive.bind(to: phoneTextField, keyPath: \.isUserInteractionEnabled).addToDisposableBag(disposableBag)

        presenter.state.loginButtonTitle.observe { [weak self] in
            self?.signInButton.setTitle($0, for: .normal)
        }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe {[weak self] isLoading in
            guard let self = self else { return }
            if isLoading && !self.loadingIndicator.isAnimating {
                self.loadingIndicator.startAnimating()
            } else if !isLoading && self.loadingIndicator.isAnimating {
                self.loadingIndicator.stopAnimating()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] error in
            if let error = error {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    private func configureView() {
        phoneTitleLabel.text = LocalizedStrings.Login.phoneTitle
        phoneTextField.placeholder = LocalizedStrings.Login.phonePlaceholder
    }

    @IBAction private func loginButtonTapped() {
        phoneTextField.resignFirstResponder()
        presenter.login()
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        presenter.changePhoneNumber(textField.text ?? "")
    }

    @IBAction private func changeCountryTapped() {
        phoneTextField.resignFirstResponder()
        presenter.changeCountry()
    }
}

extension LoginViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
