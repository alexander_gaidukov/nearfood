//
//  MainRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol MainRouterType {
    var topViewController: UIViewController? { get }
    func showMainFlow(completion: @escaping (MainTabBarAction) -> ())
}

final class MainRouter: MainRouterType {

    private let window: UIWindow
    private let factory: MainViewControllersFactoryType

    init(window: UIWindow, factory: MainViewControllersFactoryType) {
        self.window = window
        self.factory = factory
    }

    func showMainFlow(completion: @escaping (MainTabBarAction) -> ()) {
        window.rootViewController = factory.mainTabBarController(completion: completion)
    }

    var topViewController: UIViewController? {
        guard var topCandidate = (window.rootViewController as? GGTabBarController)?
                .selectedViewController else {
            return nil
        }
        while let controller = topCandidate.presentedViewController {
            topCandidate = controller
        }
        return topCandidate
    }
}
