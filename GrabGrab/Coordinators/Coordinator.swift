//
//  Coordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol Coordinator {
    func start()
}

protocol TabCoordinator {
    func start() -> UIViewController & TabRootController
}
