//
//  MainFlowCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class MainFlowCoordinator: Coordinator {
    var completion: () -> () = {}

    private let webClient: WebClientType
    private let router: MainRouterType
    private let dataProvidersStorage: DataProvidersStorageType
    private let managersStorage: ManagersStorageType

    private var childCoordinator: Coordinator?

    init(router: MainRouterType,
         dataProvidersStorage: DataProvidersStorageType,
         managersStorage: ManagersStorageType,
         webClient: WebClientType
    ) {
        self.router = router
        self.webClient = webClient
        self.dataProvidersStorage = dataProvidersStorage
        self.managersStorage = managersStorage
    }

    func start() {
        router.showMainFlow { action in
            switch action {
            case .showCart:
                self.showCart()
            }
        }
    }

    func handle(notification: Notification) {
        switch notification.name {
        case .newMessageNotification:
            showChat()
        case .newFeedbackNotification:
            showFeedback(notification)
        case .showOrderDetailsNotification:
            showOrder(notification)
        default:
            break
        }
    }

    private func showOrderDetails(_ order: Order) {
        if let coordinator = childCoordinator, coordinator is OrderCoordinator { return }
        let factory = OrderViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = OrderRouter(presentingViewController: self.router.topViewController, factory: factory)
        let orderCoordinator = OrderCoordinator(
            orderUUID: order.uuid,
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        ) { [weak self] in
            self?.childCoordinator = nil
        }
        orderCoordinator.start()
        childCoordinator = orderCoordinator
    }

    private func showCart() {
        guard !dataProvidersStorage.cartDataProvider.cart.isEmpty else { return }

        if let childCoordinator = self.childCoordinator, childCoordinator is CartCoordinator { return }

        let factory = CartViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = CartRouter(presentingViewController: self.router.topViewController, factory: factory)
        let cartCoordinator = CartCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        ) { [weak self] order in
            if let order = order {
                DispatchQueue.main.async {
                    self?.showOrderDetails(order)
                    self?.managersStorage.feedbackManager.orderCreated()
                }
            }
            self?.childCoordinator = nil
        }
        cartCoordinator.start()
        childCoordinator = cartCoordinator
    }

    private func showChat() {
        if self.router.topViewController is ChatViewController { return }
        let factory = ChatViewControllersFactory(
            order: nil,
            dataProvidersStorage: dataProvidersStorage
        )
        let router = ChatRouter(presentingViewController: self.router.topViewController, factory: factory)
        let coordinator = ChatCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            webClient: webClient
        )
        coordinator.completion = { [weak self] in self?.childCoordinator = nil }
        let _: Void = coordinator.start()
        childCoordinator = coordinator
    }

    private func showOrder(_ notification: Notification) {
        guard let orderUUID = notification.userInfo?["uuid"] as? String else { return }
        if let topViewController = self.router.topViewController as? OrderDetailsViewController,
           topViewController.presenter.orderUUID == orderUUID { return }
        let factory = OrderViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = OrderRouter(presentingViewController: self.router.topViewController, factory: factory)
        let coordinator = OrderCoordinator(
            orderUUID: orderUUID,
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        ) { [weak self] in
            self?.childCoordinator = nil
        }
        coordinator.start()
        childCoordinator = coordinator
    }

    private func showFeedback(_ notification: Notification) {

        if self.router.topViewController is FeedbackViewController { return }

        guard let json = notification.userInfo,
            let data = try? JSONSerialization.data(withJSONObject: json, options: []),
            let orderInfo = try? Coders.decoder.decode(OrderInfo.self, from: data) else { return }

        let factory = FeedbackViewControllersFactory(webClient: webClient)
        let router = FeedbackRouter(presentingViewController: self.router.topViewController, factory: factory)
        let coordinator = FeedbackCoordinator(
            order: .info(orderInfo),
            selectedRating: nil,
            router: router
        ) {[weak self] _ in
            self?.childCoordinator = nil
        }
        coordinator.start()
        childCoordinator = coordinator
    }
}
