//
//  MainViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol MainViewControllersFactoryType {
    func mainTabBarController(completion: @escaping (MainTabBarAction) -> ()) -> MainTabBarController
}

struct MainViewControllersFactory: MainViewControllersFactoryType {
    let tabCoordinators: [TabCoordinator]
    let dataProvidersStorage: DataProvidersStorageType

    func mainTabBarController(completion: @escaping (MainTabBarAction) -> ()) -> MainTabBarController {
        let presenter = MainTabBarPresenter(
            tabCoordinators: tabCoordinators,
            cartDataProvider: dataProvidersStorage.cartDataProvider,
            messagesDataProvider: dataProvidersStorage.messagesDataProvider,
            completion: completion
        )
        return MainTabBarController(presenter: presenter)
    }
}
