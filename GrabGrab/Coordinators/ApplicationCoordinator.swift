//
//  ApplicationCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import WidgetKit

// swiftlint:disable:next type_body_length
final class ApplicationCoordinator: Coordinator {

    private let window: UIWindow
    private let webClient: WebClientType
    private let dataProvidersStorage: DataProvidersStorageType
    private let managersStorage: ManagersStorageType
    private let logger: LoggerType

    private var childCoordinator: Coordinator?
    private var postpondNotification: Notification?

    var completion: () -> () = {}

    init(window: UIWindow,
         webClient: WebClientType,
         dataProvidersStorage: DataProvidersStorageType,
         managersStorage: ManagersStorageType,
         logger: LoggerType) {
        self.window = window
        self.webClient = webClient
        self.dataProvidersStorage = dataProvidersStorage
        self.managersStorage = managersStorage
        self.logger = logger
        dataProvidersStorage.customerDataProvider.delegate = self

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleNotification(_:)),
            name: .newMessageNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleNotification(_:)),
            name: .newFeedbackNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleNotification(_:)),
            name: .showOrderDetailsNotification,
            object: nil
        )
    }

    func start() {
        showWelcomeFlow()
    }

    func registerDeviceForPushNotifications(with deviceToken: Data) {
        managersStorage
            .pushNotificationsManager
            .applicationDidRegisterForRemoteNotifcation(withDeviceToken: deviceToken)
    }

    func handleSilentPushNotification(
        with userInfo: [AnyHashable: Any],
        completion: @escaping (UIBackgroundFetchResult) -> Void
    ) {
        if (userInfo["kind"] as? String) == "orders.updated" {
            if #available(iOS 14.0, *) {
                WidgetCenter.shared.reloadTimelines(ofKind: Constants.Identifiers.widgetKind)
            }
        }
        managersStorage.pushNotificationsManager.handleSilent(userInfo) {
            completion(.newData)
        }
    }

    func handlePushNotification(
        with userInfo: [AnyHashable: Any],
        completion: @escaping () -> Void
    ) {
        managersStorage.pushNotificationsManager.handle(userInfo, completion: completion)
    }

    func handleDeeplink(_ url: URL) -> Bool {
        managersStorage.deeplinksManager.handle(url: url)
    }

    func refreshApplicationData() {
        guard dataProvidersStorage.customerDataProvider.isActive else { return }
        dataProvidersStorage.cartDataProvider.checkCartState { _ in }
        dataProvidersStorage.appVersionDataProvider.update {}
        dataProvidersStorage.ordersDataProvider.updateActiveOrders(notifyWidget: false)
    }

    func refreshUserData() {
        guard dataProvidersStorage.customerDataProvider.isLoggedIn else { return }
        webClient.load(combinedResource: ResourceBuilder.currentUserResource()) {[weak self] result in
            guard let customer = try? result.get() else { return }
            self?.dataProvidersStorage.customerDataProvider.updateCustomer(customer)
        }
    }

    func showDebug() {
        let env = (Bundle.main.infoDictionary?["BuildType"] as? String).flatMap(Environment.init)
        guard env == .development else { return }
        guard let presentingVC = window.rootViewController else { return }
        if let coordinator = childCoordinator, coordinator is DebugCoordinator { return }
        let factory = DebugViewControllersFactory()
        let router = DebugRouter(factory: factory, presentingViewController: presentingVC)
        let coordinator = DebugCoordinator(router: router, logger: logger) { [weak self] env in
            self?.updateAPIConfiguration(env)
            self?.childCoordinator = nil
        }
        coordinator.start()
        self.childCoordinator = coordinator
    }

    private func registerForAppVersionUpdates() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appVersionStatusDidChange),
            name: .appVersionStateDidChangeNotification,
            object: nil
        )
    }

    private func updateAPIConfiguration(_ env: Environment?) {
        guard let environment = env else { return }
        Constants.Environments.api = environment
        logger.clearAPILogs()
        showWelcomeFlow()
    }

    @objc private func handleNotification(_ notification: Notification) {
        guard let mainCoordinator = childCoordinator as? MainFlowCoordinator else {
            postpondNotification = notification
            return
        }

        mainCoordinator.handle(notification: notification)
    }

    private func startUserFlow() {
        if dataProvidersStorage.appVersionDataProvider.state == .unsupported {
            showUnsupportedVersion()
        } else if dataProvidersStorage.introDataProvider.shouldShowIntro {
            showIntroFlow()
        } else if !dataProvidersStorage.customerDataProvider.isLoggedIn {
            showLogin()
        } else if !dataProvidersStorage.customerDataProvider.isActive {
            showActivation()
        } else if !dataProvidersStorage.customerDataProvider.isCustomerHasAddress {
            showOnboarding()
        } else {
            dataProvidersStorage.onboardingDataProvider.isCompleted { isCompleted in
                DispatchQueue.main.async {
                    if isCompleted {
                        self.showMainFlow()
                    } else {
                        self.showOnboarding()
                    }
                }
            }
        }
    }

    private func showWelcomeFlow() {
        if let coordinator = childCoordinator, coordinator is WelcomeCoordinator { return }
        let factory = WelcomeViewControllersFactory(
            webClient: webClient,
            dataProvidersStorage: dataProvidersStorage
        )
        let router = WelcomeRouter(window: window, factory: factory)
        let coordinator = WelcomeCoordinator(router: router) {[weak self] in
            guard let self = self else { return }
            self.registerForAppVersionUpdates()
            self.refreshApplicationData()
            if
                self.dataProvidersStorage.introDataProvider.shouldShowIntro
                && self.dataProvidersStorage.customerDataProvider.isLoggedIn
            { // Customer is already signed in and just reinstall the app
                self.dataProvidersStorage.introDataProvider.introDidFinish()
            }
            self.startUserFlow()
        }
        childCoordinator = coordinator
        coordinator.start()
    }

    private func showIntroFlow() {
        if let coordinator = childCoordinator, coordinator is IntroCoordinator { return }
        let factory = IntroViewControllersFactory()
        let router = IntroRouter(window: window, factory: factory)
        childCoordinator = IntroCoordinator(router: router, dataProvidersStorage: dataProvidersStorage) { [weak self] in
            self?.startUserFlow()
        }
        childCoordinator?.start()
    }

    private func showMainFlow() {
        if let coordinator = childCoordinator, coordinator is MainFlowCoordinator { return }
        let factory = MainViewControllersFactory(
            tabCoordinators: [foodCoordinator(), accountCoordinator(), chatCoordinator()],
            dataProvidersStorage: dataProvidersStorage
        )
        let router = MainRouter(window: window, factory: factory)
        let mainCoordinator = MainFlowCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        mainCoordinator.start()
        childCoordinator = mainCoordinator
        guard let notification = postpondNotification else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            mainCoordinator.handle(notification: notification)
            self.postpondNotification = nil
        }
    }

    private func showLogin() {
        if let coordinator = childCoordinator, coordinator is LoginCoordinator { return }
        let factory = LoginViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = LoginRouter(window: window, factory: factory)
        childCoordinator = LoginCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        ) { [weak self] in self?.startUserFlow() }
        childCoordinator?.start()
    }

    private func showActivation() {
        if let coordinator = childCoordinator, coordinator is ActivationCoordinator { return }
        let factory = ActivationViewControllersFactory(
            webClient: webClient,
            dataProvidersStorage: dataProvidersStorage
        )
        let router = ActivationRouter(window: window, factory: factory)
        childCoordinator = ActivationCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage

        ) { [weak self] in self?.startUserFlow() }
        childCoordinator?.start()
    }

    private func showOnboarding() {
        if let coordinator = childCoordinator, coordinator is OnboardingCoordinator { return }
        let addressesFactory = AddressesViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let factory = OnboardingViewControllersFactory(
            managersStorage: managersStorage,
            addressesViewControllersFactory: addressesFactory
        )
        let router = OnboardingRouter(window: window, factory: factory)
        childCoordinator = OnboardingCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage
        ) { [weak self] in self?.startUserFlow() }
        childCoordinator?.start()
    }

    private func showUnsupportedVersion() {
        if let coordinator = childCoordinator, coordinator is AppVersionsCoordinator { return }
        let factory = AppVersionsViewControllersFactory()
        let router = AppVersionsRouter(window: window, factory: factory)
        childCoordinator = AppVersionsCoordinator(router: router, dataProvidersStorage: dataProvidersStorage)
        childCoordinator?.start()
    }

    private func foodCoordinator() -> FoodCoordinator {
        let factory = FoodViewControllersFactory(dataProvidersStorage: dataProvidersStorage)
        let router = FoodRouter(factory: factory)
        return FoodCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
    }

    private func accountCoordinator() -> AccountCoordinator {
        let factory = AccountViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = AccountRouter(factory: factory)
        return AccountCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
    }

    private func chatCoordinator() -> ChatCoordinator {
        let factory = ChatViewControllersFactory(
            order: nil,
            dataProvidersStorage: dataProvidersStorage
        )
        let router = ChatRouter(presentingViewController: nil, factory: factory)
        return ChatCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            webClient: webClient
        )
    }

    @objc private func appVersionStatusDidChange() {
        startUserFlow()
    }
}

extension ApplicationCoordinator: CustomerDataProviderDelegate {

    func willLogout() {
        webClient.load(resource: ResourceBuilder.logoutResource(deviceUUID: dataProvidersStorage.customerDataProvider.device.uuid)) { _ in }
    }

    func didLogout() {
        showLogin()
    }

    func didLogin() {
        dataProvidersStorage.cartDataProvider.checkCartState { _ in }
        managersStorage.pushNotificationsManager.registerNotificationToken()
        refreshApplicationData()
    }

    func didActivate() {
        managersStorage.pushNotificationsManager.registerNotificationToken()
        refreshApplicationData()
    }
}
