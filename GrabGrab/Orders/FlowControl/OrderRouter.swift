//
//  OrderRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import SafariServices

protocol OrderRouterType {
    var topViewController: UIViewController? { get }
    func showOrderDetails(orderUUID: String, completion: @escaping (OrderDetailsAction) -> ())
    func showEditAddress(location: Location, completion: @escaping (AddOrUpdateAddressAction) -> ())
    func showContactlessInfo(completion: @escaping (ContactlessInfoAction) -> ())
    func showCancellationFees(fees: Fees, completion: @escaping (OrderCancellationFeesAction) -> ())
    func showReceipt(_ receipt: Receipt)
    func dismiss()
}

final class OrderRouter: NSObject, OrderRouterType {

    private weak var presentingViewController: UIViewController?
    private let factory: OrderViewControllersFactoryType

    init(presentingViewController: UIViewController?, factory: OrderViewControllersFactoryType) {
        self.presentingViewController = presentingViewController
        self.factory = factory
        super.init()
    }

    func showOrderDetails(orderUUID: String, completion: @escaping (OrderDetailsAction) -> ()) {
        let controller = factory.orderDetailsViewController(orderUUID: orderUUID, completion: completion)
        controller.modalPresentationStyle = .fullScreen
        presentingViewController?.present(controller, animated: true, completion: nil)
    }

    func showEditAddress(location: Location, completion: @escaping (AddOrUpdateAddressAction) -> ()) {
        let controller = factory.editAddressViewController(location: location, completion: completion)
        let navController = GGPresentedNavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        topViewController?.present(navController, animated: true, completion: nil)
    }

    func showContactlessInfo(completion: @escaping (ContactlessInfoAction) -> ()) {
        let controller = factory.contactlessInfoViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showCancellationFees(fees: Fees, completion: @escaping (OrderCancellationFeesAction) -> ()) {
        let controller = factory.orderCancellationFeesViewController(fees: fees, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showReceipt(_ receipt: Receipt) {
        guard let url = URL(string: receipt.url) else { return }
        let controller = SFSafariViewController(url: url)
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func dismiss() {
        topViewController?.dismiss(animated: true, completion: nil)
    }

    var topViewController: UIViewController? {
        guard var topCandidate = presentingViewController else { return nil }
        while let controller = topCandidate.presentedViewController {
            topCandidate = controller
        }
        return topCandidate
    }
}

extension OrderRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
