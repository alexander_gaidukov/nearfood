//
//  OrderCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class OrderCoordinator: Coordinator {
    private let completion: () -> ()
    private let orderUUID: String
    private let router: OrderRouterType

    private let dataProvidersStorage: DataProvidersStorageType
    private let managersStorage: ManagersStorageType

    private let webClient: WebClientType

    private var childCoordinator: Coordinator?

    init(
        orderUUID: String,
        router: OrderRouterType,
        dataProvidersStorage: DataProvidersStorageType,
        managersStorage: ManagersStorageType,
        webClient: WebClientType,
        completion: @escaping () -> ()
    ) {
        self.orderUUID = orderUUID
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
        self.managersStorage = managersStorage
        self.webClient = webClient
        self.completion = completion
    }

    func start() {
        router.showOrderDetails(orderUUID: orderUUID) { action in
            switch action {
            case .completed:
                self.router.dismiss()
                self.completion()
            case .writeToSupport(let order):
                self.writeToSupport(order: order)
            case .changePaymentMethod(let completion):
                self.changePaymentMethod(completion: completion)
            case .contactlessInfo:
                self.showContactlessInfo()
            case .saveAddress(let location):
                self.showSaveAddress(location: location)
            case let .feedback(order, completion):
                self.showFeedback(order: order, completion: completion)
            case let .fees(fees, completion):
                self.showCancellationFees(fees: fees, completion: completion)
            case let .cartItem(cartItem, placeholders):
                self.showCartItem(cartItem, placeholders: placeholders)
            case let .comment(note, completion):
                self.showEditComment(note: note, completion: completion)
            case let .applePay(order, completion):
                self.showApplePay(order: order, completion: completion)
            case let .receipt(receipt):
                self.showReceipt(receipt)
            case let .changeRecipient(recipient, complention):
                self.addOrUpdateRecipient(recipient, completion: complention)
            case .otherTipOption(let completion):
                self.showOtherTipsOption(completion: completion)
            case .tipsInfo:
                self.showTipsInfo()
            }
        }
    }

    private func showOtherTipsOption(completion: @escaping (OrderTips?) -> ()) {
        if let coordinator = childCoordinator, coordinator is TipsCoordinator { return }

        let factory = TipsViewControllersFactory(dataProvidersStorage: dataProvidersStorage, webClient: webClient)
        let router = TipsRouter(factory: factory, presentingViewController: self.router.topViewController)
        let tipsCoordinator = TipsCoordinator(router: router, orderUUID: orderUUID) { [weak self] orderTips in
            completion(orderTips)
            self?.childCoordinator = nil
        }
        tipsCoordinator.start()
        childCoordinator = tipsCoordinator
    }

    private func showTipsInfo() {
        if let coordinator = childCoordinator, coordinator is TipsCoordinator { return }

        let factory = TipsViewControllersFactory(dataProvidersStorage: dataProvidersStorage, webClient: webClient)
        let router = TipsRouter(factory: factory, presentingViewController: self.router.topViewController)
        let tipsCoordinator = TipsCoordinator(router: router, context: .info) { [weak self] in
            self?.childCoordinator = nil
        }
        tipsCoordinator.start()
        childCoordinator = tipsCoordinator
    }

    private func showReceipt(_ receipt: Receipt) {
        router.showReceipt(receipt)
    }

    private func showApplePay(order: Order, completion: @escaping (String?) -> ()) {
        if let coordinator = childCoordinator, coordinator is PaymentSystemsCoordinator { return }

        let factory = PaymentSystemsViewControllersFactory()
        let router = PaymentSystemsRouter(presentingViewController: self.router.topViewController, factory: factory)

        let paymentSystemsCoordinator = PaymentSystemsCoordinator(
            router: router,
            paymentSystem: .applePay,
            order: order
        ) {[weak self] token in
            completion(token)
            self?.childCoordinator = nil
        }

        paymentSystemsCoordinator.start()
        childCoordinator = paymentSystemsCoordinator
    }

    private func writeToSupport(order: Order) {
        if let coordinator = childCoordinator, coordinator is ChatCoordinator { return }
        let factory = ChatViewControllersFactory(
            order: order,
            dataProvidersStorage: dataProvidersStorage
        )
        let router = ChatRouter(presentingViewController: self.router.topViewController, factory: factory)
        let chatCoordinator = ChatCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            webClient: webClient
        )

        chatCoordinator.completion = {[weak self] in
            self?.childCoordinator = nil
        }
        let _: Void = chatCoordinator.start()
        childCoordinator = chatCoordinator
    }

    private func showContactlessInfo() {
        router.showContactlessInfo { action in
            switch action {
            case .complete:
                self.router.dismiss()
            }
        }
    }

    private func showFeedback(order: Order, completion: @escaping (Feedback?) -> ()) {
        if let coordinator = childCoordinator, coordinator is FeedbackCoordinator { return }
        let factory = FeedbackViewControllersFactory(webClient: webClient)
        let router = FeedbackRouter(presentingViewController: self.router.topViewController, factory: factory)
        let coordinator = FeedbackCoordinator(order: .order(order), selectedRating: nil, router: router) {[weak self] feedback in
            completion(feedback)
            self?.childCoordinator = nil
        }
        coordinator.start()
        childCoordinator = coordinator
    }

    private func showSaveAddress(location: Location) {
        router.showEditAddress(location: location) { action in
            switch action {
            case .completed:
                self.router.dismiss()
            case .selectAddress:
                break // This branch is unaccessible in this scenario
            }
        }
    }

    private func changePaymentMethod(completion: @escaping (PaymentMethod?, WebError?) -> ()) {
        if let coordinator = childCoordinator, coordinator is PaymentMethodsCoordinator { return }
        let factory = PaymentMethodsViewControllersFactory(
            webClient: webClient,
            dataProvidersStorage: dataProvidersStorage
        )
        let router = PaymentMethodsRouter(presentingViewController: self.router.topViewController, factory: factory)
        let paymentMethodsCoordinator = PaymentMethodsCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            webClient: webClient,
            markCurrentPayment: false
        ) { [weak self] method, error in
            completion(method, error)
            self?.childCoordinator = nil
        }
        paymentMethodsCoordinator.start()
        childCoordinator = paymentMethodsCoordinator
    }

    private func showCartItem(_ cartItem: CartItem, placeholders: [UIImage?]) {
        if let coordinator = childCoordinator, coordinator is DishDetailCoordinator {
            return
        }
        let factory = DishDetailViewControllersFactory()
        let router = DishDetailRouter(factory: factory, presentingController: self.router.topViewController)
        let coordinator = DishDetailCoordinator(
            cartItem: cartItem,
            imagePlaceholders: placeholders,
            orderable: false,
            router: router) {[weak self] _ in
            self?.childCoordinator = nil
        }
        coordinator.start()
        childCoordinator = coordinator
    }

    private func showCancellationFees(fees: Fees, completion: @escaping (Bool) -> ()) {
        router.showCancellationFees(fees: fees) { action in
            self.router.dismiss()
            switch action {
            case .confirm:
                completion(true)
            case .cancel:
                completion(false)
            }
        }
    }

    private func showEditComment(note: String?, completion: @escaping (String?) -> ()) {

        guard let controller = router.topViewController else {
            return
        }

        if let coordinator = childCoordinator, coordinator is CommentCoordinator { return }

        let factory = CommentViewControllersFactory()
        let router = CommentRouter(factory: factory, presentingViewController: controller)
        let coordinator = CommentCoordinator(note: note, router: router) {[weak self] comment in
            completion(comment)
            self?.childCoordinator = nil
        }

        coordinator.start()

        childCoordinator = coordinator
    }

    private func addOrUpdateRecipient(_ recipient: Recipient?, completion: @escaping (Recipient?) -> ()) {
        if let coordinator = childCoordinator, coordinator is RecipientsCoordinator { return }

        let factory = RecipientsViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage
        )
        let router = RecipientsRouter(
            presentingViewController: self.router.topViewController,
            factory: factory
        )

        let coordinator = RecipientsCoordinator(
            recipient: recipient,
            router: router,
            managersStorage: managersStorage
        ) { [weak self] recipient in
            completion(recipient)
            self?.childCoordinator = nil
        }

        coordinator.start()
        childCoordinator = coordinator
    }
}
