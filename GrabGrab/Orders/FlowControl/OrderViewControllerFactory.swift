//
//  OrderViewControllerFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol OrderViewControllersFactoryType {
    func orderDetailsViewController(orderUUID: String, completion: @escaping (OrderDetailsAction) -> ()) -> OrderDetailsViewController

    func editAddressViewController(
        location: Location,
        completion: @escaping (AddOrUpdateAddressAction) -> ()
    ) -> AddOrUpdateAddressViewController

    func contactlessInfoViewController(completion: @escaping (ContactlessInfoAction) -> ()) -> ContactlessInfoViewController

    func orderCancellationFeesViewController(
        fees: Fees,
        completion: @escaping (OrderCancellationFeesAction) -> ()
    ) -> OrderCancellationFeesViewConroller
}

struct OrderViewControllersFactory: OrderViewControllersFactoryType {

    private let storyboard: Storyboard = .orders

    let dataProvidersStorage: DataProvidersStorageType
    let managersStorage: ManagersStorageType
    let webClient: WebClientType

    func orderDetailsViewController(
        orderUUID: String,
        completion: @escaping (OrderDetailsAction) -> ()
    ) -> OrderDetailsViewController {
        let presenter = OrderDetailsPresenter(
            orderUUID: orderUUID,
            ordersDataProvider: dataProvidersStorage.ordersDataProvider,
            addressesDataProvider: dataProvidersStorage.addressDataProvider,
            phoneCodesManager: managersStorage.phoneCodesManager,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            webClient: webClient,
            completion: completion
        )
        let controller = OrderDetailsViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func editAddressViewController(
        location: Location,
        completion: @escaping (AddOrUpdateAddressAction) -> ()
    ) -> AddOrUpdateAddressViewController {
        let presenter = AddOrUpdateAddressPresenter(
            location: location,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            webClient: webClient,
            completion: completion
        )
        let controller = AddOrUpdateAddressViewController.instance(storyboard: .account)
        controller.presenter = presenter
        return controller
    }

    func contactlessInfoViewController(completion: @escaping (ContactlessInfoAction) -> ()) -> ContactlessInfoViewController {
        let presenter = ContactlessInfoPresenter(completion: completion)
        let controller = ContactlessInfoViewController.instance(storyboard: .cart)
        controller.presenter = presenter
        return controller
    }

    func orderCancellationFeesViewController(
        fees: Fees,
        completion: @escaping (OrderCancellationFeesAction) -> ()
    ) -> OrderCancellationFeesViewConroller {
        let presenter = OrderCancellationFeesPresenter(fees: fees, completion: completion)
        let conroller = OrderCancellationFeesViewConroller.instance(storyboard: storyboard)
        conroller.presenter = presenter
        return conroller
    }
}
