//
//  OrderDetailsMapViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import YandexMapKit
import CoreLocation

final class OrderDetailsMapViewController: UIViewController {
    @IBOutlet private weak var mapView: YMKMapView!

    var objects: [(position: GeoPosition, isCurrent: Bool)] = [] {
        didSet {
            if isViewLoaded {
                placeObjects()
            }
        }
    }

    var location: Location? {
        didSet {
            if isViewLoaded {
                if oldValue?.latitude != location?.latitude || oldValue?.longitude != location?.longitude {
                    self.move(location: location)
                }
            }
        }
    }

    var courierPolylineResponse: PolylineResponse? {
        didSet {
            if isViewLoaded {
                if oldValue?.polyline != courierPolylineResponse?.polyline {
                    self.updateCourierPosition()
                }
            }
        }
    }

    var vehicleType: Courier.VehicleType? {
        didSet {
            if isViewLoaded {
                if oldValue != vehicleType {
                    self.updateCourierPosition()
                }
            }
        }
    }

    private var objectsPlacemarks: [YMKMapObject] = []
    private var courierPlacemark: YMKMapObject?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        move(location: location)
        updateCourierPosition()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                configureMapViewStyle()
            }
        }
    }

    private func configureViews() {
        configureMapViewStyle()
        placeObjects()
    }

    private func configureMapViewStyle() {
        if #available(iOS 13.0, *) {
            mapView.mapWindow.map.isNightModeEnabled = traitCollection.userInterfaceStyle == .dark
        } else {
            mapView.mapWindow.map.isNightModeEnabled = false
        }
    }

    private func move(location: Location?) {

        guard let location = location else { return }

        let newPosition = YMKCameraPosition(target: YMKPoint(latitude: location.latitude, longitude: location.longitude),
                                            zoom: max(mapView.mapWindow.map.cameraPosition.zoom, 15),
                                            azimuth: 0,
                                            tilt: 0)

        mapView.mapWindow.map.move(with: newPosition,
                                   animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 0.5)) {_ in }
    }

    private func updateCourierPosition() {
        if let courierPlacemark = courierPlacemark {
            mapView.mapWindow.map.mapObjects.remove(with: courierPlacemark)
        }

        courierPlacemark = nil

        let response: PolylineResponse?
        if ProcessInfo().isInTestMode {
            response = PolylineResponse(
                polyline: "",
                position: Constants.UITests.Order.courierCoordinates
            )
        } else {
            response = courierPolylineResponse
        }

        guard let position = response?.position else { return }

        let point = YMKPoint(latitude: position.latitude, longitude: position.longitude)
        let courierPlacemark = self.mapView.mapWindow.map.mapObjects.addPlacemark(
            with: point,
            image: vehicleType?.mapIcon ?? #imageLiteral(resourceName: "car_big"),
            style: YMKIconStyle(
                anchor: CGPoint(x: 0.5, y: 0.5) as NSValue,
                rotationType: YMKRotationType.rotate.rawValue as NSNumber,
                zIndex: 1,
                flat: true,
                visible: true,
                scale: 1,
                tappableArea: nil
            )
        )
        courierPlacemark.direction = position.course ?? 0
        self.courierPlacemark = courierPlacemark
    }

    private func placeObjects() {
        objectsPlacemarks.forEach {
            mapView.mapWindow.map.mapObjects.remove(with: $0)
        }
        objectsPlacemarks.removeAll()

        objects.enumerated().forEach {
            let point = YMKPoint(
                latitude: $0.element.position.latitude,
                longitude: $0.element.position.longitude
            )

            if $0.element.isCurrent {
                objectsPlacemarks.append(mapView.mapWindow.map.mapObjects.addPlacemark(with: point, image: #imageLiteral(resourceName: "user")))
            } else {
                let placemark = mapView.mapWindow.map.mapObjects.addPlacemark(with: point)
                let view = placemarkView(for: $0.offset + 1)
                if let viewProvider = YRTViewProvider(uiView: view) {
                    placemark.setViewWithView(viewProvider)
                }
                objectsPlacemarks.append(placemark)
            }
        }
    }

    private func placemarkView(for position: Int) -> UIView {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        view.isOpaque = false
        view.backgroundColor = UIColor.white.withAlphaComponent(0)

        let innerView = UIView(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        innerView.backgroundColor = .background
        innerView.borderColor = .mainText
        innerView.borderWidth = 3
        innerView.cornerRadius = 14
        view.addSubview(innerView)

        let font = UIFont.preferredFont(forTextStyle: .footnote)

        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        label.backgroundColor = .clear
        label.font = font
        label.textColor = .mainText
        label.textAlignment = .center
        label.text = "\(position)"
        innerView.addSubview(label)

        return view
    }
}
