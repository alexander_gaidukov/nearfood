//
//  OrderDetailsViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import Pulley

final class OrderDetailsViewController: UIViewController {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var timerLabel: UILabel!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var timerView: UIView!
    @IBOutlet private weak var saveAddressView: UIView!
    @IBOutlet private weak var hideTimerConstraint: NSLayoutConstraint!

    @IBOutlet private weak var saveAddressLabel: UILabel!
    @IBOutlet private weak var saveAddressButton: UIButton!
    @IBOutlet private weak var dismissAddressPromptButton: UIButton!

    @IBOutlet private weak var backButton: UIButton!

    var presenter: OrderDetailsPresenterType!
    private let disposableBag = DisposableBag()

    private var pulleyController: PulleyViewController!
    private var mapVC: OrderDetailsMapViewController!
    private var infoVC: OrderInfoViewController!

    private var isTimerRun: Bool = false {
        didSet {
            if isTimerRun {
                runTimer()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    private func configureTimer(_ timerInfo: TimerInfoModel?) {
        guard let timerInfo = timerInfo else { return }
        timerLabel.text = timerInfo.label
        timerView.backgroundColor = timerInfo.backgroundColor
        timerLabel.textColor = timerInfo.color
        timerView.isHidden = timerInfo.isHidden
        hideTimerConstraint.priority = timerInfo.isHidden ? .defaultHigh : .defaultLow
        if isTimerRun != timerInfo.shouldTick {
            self.isTimerRun.toggle()
        }
    }

    private func configureStatus(_ statusModel: OrderStatusModel?) {
        statusLabel.text = statusModel?.label
        statusLabel.textColor = statusModel?.color ?? .clear
    }

    private func runTimer() {
        guard isTimerRun else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            guard let self = self, self.isTimerRun else { return }
            self.configureTimer(self.presenter.state.timerInfo.wrappedValue)
            self.runTimer()
        }
    }

    private func configureViews() {

        backButton.accessibilityIdentifier = Identifiers.Common.backButton

        saveAddressLabel.text = LocalizedStrings.Order.Address.savePrompt
        saveAddressButton.setTitle(LocalizedStrings.Common.yes, for: .normal)
        dismissAddressPromptButton.setTitle(LocalizedStrings.Common.no, for: .normal)

        mapVC = OrderDetailsMapViewController.instance(storyboard: .orders)
        infoVC = OrderInfoViewController.instance(storyboard: .orders)

        infoVC.isScrollable = false
        let drawer = infoVC.embededInDrawer()

        pulleyController = PulleyViewController(contentViewController: mapVC, drawerViewController: drawer)
        pulleyController.drawerTopInset = 76.0
        pulleyController.backgroundDimmingColor = .clear
        pulleyController.drawerBackgroundVisualEffectView = nil
        pulleyController.initialDrawerPosition = .partiallyRevealed
        pulleyController.delegate = self
        insertViewController(pulleyController, toView: containerView)
        containerView.bringSubviewToFront(saveAddressView)

        NSLayoutConstraint.activate([
            drawer.view.topAnchor.constraint(equalTo: saveAddressView.bottomAnchor, constant: 12)
        ])
    }

    private func configureBindings() {

        presenter.state.isEmpty.bind(to: infoVC.view, keyPath: \.isHidden).addToDisposableBag(disposableBag)
        presenter.models.bind(to: infoVC, keyPath: \.models).addToDisposableBag(disposableBag)

        presenter.state.location.bind(to: mapVC, keyPath: \.location).addToDisposableBag(disposableBag)
        presenter.state.bundleItems.bind(to: mapVC, keyPath: \.objects).addToDisposableBag(disposableBag)
        presenter.state.vehicleType.bind(to: mapVC, keyPath: \.vehicleType).addToDisposableBag(disposableBag)

        if !ProcessInfo().isInTestMode {
            presenter.state.$courierPolyline.bind(to: mapVC, keyPath: \.courierPolylineResponse).addToDisposableBag(disposableBag)
            presenter.state.shouldHideSaveAddress.bind(to: saveAddressView, keyPath: \.isHidden).addToDisposableBag(disposableBag)
        }

        presenter.state.timerInfo.observe { [weak self] in self?.configureTimer($0) }.addToDisposableBag(disposableBag)

        presenter.state.statusModel.observe { [weak self] in self?.configureStatus($0) }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe {[weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$tipsSelectionMessage.observe {[weak self] in
            if let message = $0 {
                self?.showBanner(withType: .success(message))
            }
        }.addToDisposableBag(disposableBag)
    }

    @IBAction private func backTapped() {
        presenter.dismiss()
    }

    @IBAction private func saveAddressTapped() {
        presenter.saveAddress()
    }

    @IBAction private func dismissAddressPromptTapped() {
        presenter.dismissAddressPrompt()
    }
}

extension OrderDetailsViewController: PulleyDelegate {
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        infoVC.isScrollable = drawer.drawerPosition == .open
    }
}

extension OrderDetailsViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension OrderDetailsViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
