//
//  OrderCancellationFeesViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.10.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class OrderCancellationFeesViewConroller: UIViewController, PopupViewController {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var confirmButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!

    var presenter: OrderCancellationFeesPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        titleLabel.text = LocalizedStrings.Order.Cancellation.Fees.title
        descriptionLabel.attributedText = presenter.description
        confirmButton.setTitle(LocalizedStrings.Order.Cancellation.Fees.confirm, for: .normal)
        cancelButton.setTitle(LocalizedStrings.Order.Cancellation.Fees.cancel, for: .normal)
    }

    func close() {}

    @IBAction private func confirmTapped() {
        presenter.confirm()
    }

    @IBAction private func cancelTapped() {
        presenter.cancel()
    }

}
