//
//  OrderDetailsRecipientCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsRecipientCellModel {
    var title: String
    let isEditable: Bool

    init(recipient: (name: String, phone: String)?, isEditable: Bool) {
        if let recipient = recipient {
            title = "\(recipient.name)\n\(recipient.phone)"
        } else {
            title = LocalizedStrings.Recipient.empty
        }
        self.isEditable = isEditable
    }
}

final class OrderDetailsRecipientCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var arrowImageView: UIImageView!

    func configure(with model: OrderDetailsRecipientCellModel) {
        titleLabel.text = model.title
        arrowImageView.isHidden = !model.isEditable
    }
}
