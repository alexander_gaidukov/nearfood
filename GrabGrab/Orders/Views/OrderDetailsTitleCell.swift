//
//  OrderDetailsTitleCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsTitleCellModel {
    var title: String?
}

final class OrderDetailsTitleCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!

    func configure(model: OrderDetailsTitleCellModel) {
        titleLabel.text = model.title
    }
}
