//
//  OrderDetailsBillCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class OrderDetailsBillCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Order.receipt
    }

}
