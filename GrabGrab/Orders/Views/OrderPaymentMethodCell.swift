//
//  OrderPaymentMethodCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderPaymentMethodCellModel {
    var image: UIImage?
    var title: String
}

extension OrderPaymentMethodCellModel {
    init(paymentMethod: PaymentMethod) {
        image = paymentMethod.image
        title = paymentMethod.title
    }
}

class OrderPaymentMethodCell: UITableViewCell {
    @IBOutlet private weak var paymentMethodLabel: UILabel!
    @IBOutlet private weak var cardImageView: UIImageView!

    func configure(model: OrderPaymentMethodCellModel) {
        paymentMethodLabel.text = model.title
        cardImageView.image = model.image
    }
}
