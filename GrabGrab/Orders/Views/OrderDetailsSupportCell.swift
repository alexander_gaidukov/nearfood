//
//  OrderDetailsSupportCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsSupportCellModel {
    var shouldHideSeparator: Bool
}

final class OrderDetailsSupportCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var separatorView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Order.support
    }

    func configure(with model: OrderDetailsSupportCellModel) {
        separatorView.isHidden = model.shouldHideSeparator
    }
}
