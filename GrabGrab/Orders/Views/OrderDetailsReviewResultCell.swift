//
//  OrderDetailsReviewResultCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsReviewResultCellModel {
    let image: UIImage?
    let note: String?
}

extension OrderDetailsReviewResultCellModel {
    init(feedback: Feedback) {
        image = feedback.icon
        note = feedback.note
    }
}

class OrderDetailsReviewResultCell: UITableViewCell {

    @IBOutlet private weak var iconView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var noteLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Order.Review.completeTitle
    }

    func configure(model: OrderDetailsReviewResultCellModel) {
        iconView.image = model.image
        noteLabel.text = model.note
        noteLabel.isHidden = model.note?.isEmpty != false
    }
}
