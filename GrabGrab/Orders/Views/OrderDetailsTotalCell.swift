//
//  OrderDetailsTotalCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsTotalCellModel {
    var price: String
}

extension OrderDetailsTotalCellModel {
    init(price: Double) {
        self.price = price.string(with: Formatters.priceFormatter) ?? ""
    }
}

final class OrderDetailsTotalCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        priceLabel.font = UIFont.systemFont(ofSize: UIFont.preferredFont(forTextStyle: .callout).pointSize, weight: .bold)
        titleLabel.text = LocalizedStrings.Order.total
    }

    func configure(model: OrderDetailsTotalCellModel) {
        priceLabel.text = model.price
    }
}
