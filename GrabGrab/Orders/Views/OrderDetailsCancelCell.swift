//
//  OrderDetailsCancelCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsCancelCellModel {
    var didSelect: () -> ()
}

final class OrderDetailsCancelCell: UITableViewCell {
    @IBOutlet private weak var cancelButton: UIButton!

    private var model: OrderDetailsCancelCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        cancelButton.setTitle(LocalizedStrings.Order.cancel, for: .normal)
    }

    func configure(model: OrderDetailsCancelCellModel) {
        self.model = model
    }

    private func updateUI() {}

    @IBAction private func didSelect() {
        model.didSelect()
    }
}
