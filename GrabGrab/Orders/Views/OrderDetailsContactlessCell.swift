//
//  OrderDetailsContactlessCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsContactlessCellModel {
    var isEnabled: Bool
    var isOn: Bool
    var didSwitch: (Bool) -> ()
}

final class OrderDetailsContactlessCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var switchView: UISwitch!

    private var model: OrderDetailsContactlessCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Cart.deliveryTitle
    }

    func configure(model: OrderDetailsContactlessCellModel) {
        self.model = model
    }

    private func updateUI() {
        switchView.isOn = model.isOn
        switchView.isEnabled = model.isEnabled
    }

    @IBAction private func switchDidChange() {
        model.didSwitch(switchView.isOn)
    }
}
