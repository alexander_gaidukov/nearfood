//
//  OrderDetailsAddressCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsAddressCellModel {
    var location: Location?
    var address: String? { location?.deliveryAddress }
    var note: String? { location?.note }
    var isNoteHidden: Bool { location?.note?.isEmpty != false }
}

final class OrderDetailsAddressCell: UITableViewCell {
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var noteLabel: UILabel!

    func configure(model: OrderDetailsAddressCellModel) {
        addressLabel.text = model.address
        noteLabel.text = model.note
        noteLabel.isHidden = model.isNoteHidden
    }
}
