//
//  OrderDetailsTipsOptionsCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsTipsOptionsCellModel {
    let itemModels: [OrderTipsItemCellModel]

    private let didSelect: (TipsOption?) -> ()
    private let options: [TipsOption]

    var selectedIndex: Int? {
        itemModels.firstIndex(where: \.isSelected)
    }

    var isCollectionViewInteractable: Bool {
        !options.isEmpty
    }

    init(
        tip: OrderTips,
        options: [TipsOption],
        didSelect:  @escaping (TipsOption?) -> ()
    ) {
        self.didSelect = didSelect
        self.options = options
        if options.isEmpty {
            let count = Int((UIScreen.main.bounds.width / OrderTipsItemCell.skeletonItemsWidth).rounded(.up))
            itemModels = Array(
                repeating: OrderTipsItemCellModel(amount: "", percent: nil, isSelected: false),
                count: count
            )
        } else {
            var models = options.map {
                OrderTipsItemCellModel(
                    amount: $0.amount.flatMap { $0.string(with: Formatters.priceFormatter) } ?? "",
                    percent: $0.kind == .percent ? $0.formattedValue : nil,
                    isSelected: fabs(($0.amount ?? 0) - tip.amount) <= .ulpOfOne
                )
            }
            models.append(
                OrderTipsItemCellModel(
                    amount: LocalizedStrings.Tips.Options.Fixed.otherAmount,
                    percent: nil,
                    isSelected: false
                )
            )
            self.itemModels = models
        }
    }

    func didSelectOption(at index: Int) {
        guard !options.isEmpty else { return }
        didSelect(options.get(at: index))
    }
}

final class OrderDetailsTipsOptionsCell: UITableViewCell {
    @IBOutlet private weak var collectionView: UICollectionView!

    private var model: OrderDetailsTipsOptionsCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(cellType: OrderTipsItemCell.self)
    }

    func configure(with model: OrderDetailsTipsOptionsCellModel) {
        self.model = model
    }

    private func updateUI() {
        collectionView.reloadData()
        collectionView.isUserInteractionEnabled = model.isCollectionViewInteractable
        if let index = model.selectedIndex {
            DispatchQueue.main.async {
                self.collectionView.scrollToItem(
                    at: IndexPath(item: index, section: 0),
                    at: .centeredHorizontally,
                    animated: true
                )
            }
        }
    }
}

extension OrderDetailsTipsOptionsCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        model.itemModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: OrderTipsItemCell = collectionView.dequeue(forIndexPath: indexPath)
        if let model = model.itemModels.get(at: indexPath.item) {
            cell.configure(with: model)
        }
        return cell
    }
}

extension OrderDetailsTipsOptionsCell: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        guard let model = model.itemModels.get(at: indexPath.item) else { return .zero }
        return CGSize(width: OrderTipsItemCell.width(for: model), height: collectionView.bounds.size.height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        model.didSelectOption(at: indexPath.item)
    }
}
