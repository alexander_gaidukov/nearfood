//
//  OrderDetailsDishCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsDishCellModel {
    let photos: [ImageAttachment]
    let count: String
    let name: String
    let price: String?
    let isCounterHidden: Bool
}

extension OrderDetailsDishCellModel {
    // swiftlint:disable:next function_body_length
    init(charge: EstimationCharge, cartItem: CartItem?) {
        var delay: Int?
        switch charge.reason {
        case .dish(let metadata):
            photos = cartItem?.photos.map { .remote($0) } ?? []
            count = "\(metadata.count)"
            name = metadata.title
            isCounterHidden = false
        case .combo(let metadata):
            photos = cartItem?.photos.map { .remote($0) } ?? []
            count = "\(metadata.count)"
            name = metadata.title
            isCounterHidden = false
        case .surge(let metadata):
            photos = [.local(#imageLiteral(resourceName: "surge"))]
            count = ""
            name = metadata.title
            isCounterHidden = true
            delay = metadata.delay
        case .bonuses(let metadata):
            photos = [.local(#imageLiteral(resourceName: "bonuses"))]
            count = ""
            name = metadata.title
            isCounterHidden = true
        case .coupon(let metadata):
            photos = [.local(#imageLiteral(resourceName: "discount"))]
            count = ""
            name = metadata.coupon.name
            isCounterHidden = true
        case .area(let metadata):
            photos = [.local(#imageLiteral(resourceName: "area"))]
            count = ""
            name = metadata.title
            isCounterHidden = true
            delay = metadata.delay
        case .unknown(let reason, let metadata):
            photos = UIImage(named: metadata.category ?? reason).map { [.local($0)] } ?? []
            count = ""
            name = metadata.title
            isCounterHidden = true
            delay = metadata.delay
        }

        price = .priceAndDelay(
            from: charge.amount,
            delay: delay,
            pricePlusPreposition: false
        )
    }
}

final class OrderDetailsDishCell: UITableViewCell {
    @IBOutlet private weak var photosContainerView: UIView!
    @IBOutlet private weak var countLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var counterView: UIView!
    @IBOutlet private weak var hideCounterConstraint: NSLayoutConstraint!

    var images: [UIImage?] {
        photosView.images
    }

    private let photosView = ComboPhotosView.fromNib()

    override func awakeFromNib() {
        super.awakeFromNib()
        photosView.overlapping = 11.0
        photosContainerView.fill(with: photosView)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        photosView.prepareForReuse()
    }

    func configure(model: OrderDetailsDishCellModel) {
        photosView.configure(with: model.photos)
        counterView.isHidden = model.isCounterHidden
        hideCounterConstraint.priority = model.isCounterHidden ? .defaultHigh : .defaultLow
        countLabel.text = model.count
        nameLabel.text = model.name
        priceLabel.text = model.price
    }
}
