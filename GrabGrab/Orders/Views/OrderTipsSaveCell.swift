//
//  OrderTipsSaveCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderTipsSaveCellModel {
    let message: String
    let confirm: () -> ()
    let cancel: () -> ()
}

extension OrderTipsSaveCellModel {
    init(option: TipsOption, confirm: @escaping () -> (), cancel: @escaping () -> ()) {
        self.confirm = confirm
        self.cancel = cancel
        message = LocalizedStrings.Order.Tips.saveAsDefault(amount: option.formattedValue)
    }
}

class OrderTipsSaveCell: UITableViewCell {

    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var confirmButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!

    private var model: OrderTipsSaveCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        confirmButton.setTitle(LocalizedStrings.Common.yes, for: .normal)
        cancelButton.setTitle(LocalizedStrings.Common.no, for: .normal)
    }

    func configure(with model: OrderTipsSaveCellModel) {
        self.model = model
    }

    private func updateUI() {
        messageLabel.text = model.message
    }

    @IBAction private func confirmButtonTapped() {
        model.confirm()
    }

    @IBAction private func cancelButtonTapped() {
        model.cancel()
    }
}
