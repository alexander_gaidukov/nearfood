//
//  OrderDetailsTipCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsTipCellModel {
    let didSelect: () -> ()
    let emojie: String?
    let title: String?
    let isReceiptLabelHidden: Bool

    var isEmojieHidden: Bool { emojie == nil }

    init(tip: OrderTips, didSelect: @escaping () -> ()) {
        self.didSelect = didSelect
        let amount = tip.amount.string(with: Formatters.priceFormatter) ?? ""

        switch tip.paymentStatus {
        case .completed:
            self.emojie = tip.amount > .ulpOfOne ? "👍" : nil
            self.title = tip.amount > .ulpOfOne
                ? LocalizedStrings.Order.Tips.completed(amount: amount)
                : LocalizedStrings.Order.Tips.noTips
        case .rejected:
            self.emojie = "😔"
            self.title = LocalizedStrings.Order.Tips.rejected
        case .pending:
            self.emojie = "⏳"
            self.title = LocalizedStrings.Order.Tips.processing(amount: amount)
        case .cancelled:
            self.emojie = nil
            self.title = LocalizedStrings.Order.Tips.cancelled
        }

        self.isReceiptLabelHidden = tip.receipt == nil
    }
}

final class OrderDetailsTipCell: UITableViewCell {
    @IBOutlet private weak var imageLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var receiptLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        receiptLabel.text = LocalizedStrings.Order.Tips.receipt
    }

    func configure(with model: OrderDetailsTipCellModel) {
        imageLabel.text = model.emojie
        imageLabel.isHidden = model.isEmojieHidden
        titleLabel.text = model.title
        receiptLabel.isHidden = model.isReceiptLabelHidden
    }
}
