//
//  OrderTipsItemCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import SkeletonView

struct OrderTipsItemCellModel {
    let amount: String
    let percent: String?
    let isSelected: Bool

    var bgColor: UIColor {
        isSelected ? .primary : .backgroundDisabled
    }

    var amountTextColor: UIColor {
        isSelected ? .white : .mainText
    }

    var percentTextColor: UIColor {
        isSelected ? .white : .labelText
    }

    var isSkeleton: Bool {
        amount.isEmpty
    }
}

final class OrderTipsItemCell: UICollectionViewCell {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var amountLabel: UILabel!
    @IBOutlet private weak var percentLabel: UILabel!

    static let skeletonItemsWidth: CGFloat = 54

    static func width(for model: OrderTipsItemCellModel) -> CGFloat {
        if model.isSkeleton {
            return skeletonItemsWidth
        }

        var result = (model.amount as NSString).boundingRect(
            with: CGSize(width: .greatestFiniteMagnitude, height: 40.0),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.systemFont(ofSize: 16.0)],
            context: nil
        ).width

        if let percent = model.percent {
            result += (percent as NSString).boundingRect(
                with: CGSize(width: .greatestFiniteMagnitude, height: 40.0),
                options: [.usesLineFragmentOrigin],
                attributes: [.font: UIFont.systemFont(ofSize: 16.0)],
                context: nil
            ).width + 4.0
        }

        return result + 32.0
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.isSkeletonable = true
        containerView.skeletonCornerRadius = 20.0
    }

    func configure(with model: OrderTipsItemCellModel) {
        containerView.backgroundColor = model.bgColor
        amountLabel.text = model.amount
        amountLabel.textColor = model.amountTextColor

        if let percent = model.percent {
            percentLabel.text = percent
            percentLabel.textColor = model.percentTextColor
            percentLabel.isHidden = false
        } else {
            percentLabel.isHidden = true
        }

        if model.isSkeleton && !containerView.sk.isSkeletonActive {
            containerView.showAnimatedGradientSkeleton()
        } else if !model.isSkeleton && containerView.sk.isSkeletonActive {
            containerView.hideSkeleton()
        }
    }
}
