//
//  OrderPaymentMethodSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderPaymentMethodSectionModel: TableViewSectionModel {
    let model: OrderPaymentMethodCellModel?

    init(paymentMethod: PaymentMethod?) {
        model = paymentMethod.map(OrderPaymentMethodCellModel.init)
    }

    var numberOfRows: Int {
        model == nil ? 0 : 1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderPaymentMethodCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderPaymentMethodCell = tableView.dequeue(forIndexPath: indexPath)
        if let model = model {
            cell.configure(model: model)
        }
        return cell
    }
}
