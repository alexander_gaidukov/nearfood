//
//  OrderDetailsTotalSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsTotalSectionModel: TableViewSectionModel {

    private let model: OrderDetailsTotalCellModel

    init(price: Double) {
        model = OrderDetailsTotalCellModel(price: price)
    }

    var numberOfRows: Int {
        1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsTotalCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsTotalCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
