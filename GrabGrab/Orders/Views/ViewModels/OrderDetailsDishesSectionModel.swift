//
//  OrderDetailsDishesSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsDishesSectionModel: TableViewSectionModel {

    private let models: [OrderDetailsDishCellModel]
    private let items: [(charge: EstimationCharge, cartItem: CartItem?)]
    private let selectionHandler: (CartItem, [UIImage?]) -> ()

    init(items:[(charge: EstimationCharge, cartItem: CartItem?)], didSelect: @escaping (CartItem, [UIImage?]) -> ()) {
        self.items = items
        selectionHandler = didSelect
        models = items.map { item in OrderDetailsDishCellModel(charge: item.charge, cartItem: item.cartItem) }
    }

    var numberOfRows: Int {
        models.count + 1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsDishCell.self)
        tableView.register(cellType: OrderDetailsHeaderCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row > 0 else {
            let cell: OrderDetailsHeaderCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(title: LocalizedStrings.Cart.Dishes.label)
            return cell
        }
        let cell: OrderDetailsDishCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: models[indexPath.row - 1])
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        guard indexPath.row != 0 else { return }
        if let cartItem = items[indexPath.row - 1].cartItem {
            let placeholders = (cell as? OrderDetailsDishCell)?.images ?? []
            selectionHandler(cartItem, placeholders)
        }
    }
}
