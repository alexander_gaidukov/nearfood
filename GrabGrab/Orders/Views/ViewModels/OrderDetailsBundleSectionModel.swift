//
//  OrderDetailsBundleSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsBundleSectionModel: TableViewSectionModel {
    let model: OrderDetailsBundleCellModel
    let isEnabled: Bool

    init(shouldShow: Bool, position: Int?, bundleSize: Int?, vehicleIcon: UIImage?) {
        model = OrderDetailsBundleCellModel(positionInBundle: position ?? 0, vehicleIcon: vehicleIcon)
        self.isEnabled = shouldShow && position != nil && (bundleSize ?? 0) > 1
    }

    var numberOfRows: Int {
        isEnabled ? 1 : 0
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsBundleCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsBundleCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
