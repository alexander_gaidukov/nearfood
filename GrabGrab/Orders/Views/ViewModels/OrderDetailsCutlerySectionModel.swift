//
//  OrderDetailsCutlerySectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsCutlerySectionModel: TableViewSectionModel {

    private let count: Int

    init(count: Int) {
        self.count = count
    }

    var numberOfRows: Int {
        1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsCutleryCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsCutleryCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(count: count)
        return cell
    }
}
