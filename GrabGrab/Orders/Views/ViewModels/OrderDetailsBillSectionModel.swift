//
//  OrderDetailsBillSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsBillSectionModel: TableViewSectionModel {
    private let selectionHandler: () -> ()
    private let receipt: Receipt?

    init(receipt: Receipt?, didSelect: @escaping () -> ()) {
        self.selectionHandler = didSelect
        self.receipt = receipt
    }

    var numberOfRows: Int {
        receipt == nil ? 0 : 1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsBillCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsBillCell = tableView.dequeue(forIndexPath: indexPath)
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        selectionHandler()
    }
}
