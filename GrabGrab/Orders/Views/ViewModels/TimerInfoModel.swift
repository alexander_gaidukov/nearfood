//
//  TimerInfoModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct TimerInfoModel {

    var label: String {
        guard !ProcessInfo().isInTestMode else { return Constants.UITests.Order.timer }
        let interval = scheduledAt.timeIntervalSince(arrivedAt ?? Date())
        return Formatters.timerFormatter.string(from: fabs(interval)) ?? "00:00"
    }

    var color: UIColor {

        guard !ProcessInfo().isInTestMode else { return .mainText }

        let interval = scheduledAt.timeIntervalSince(arrivedAt ?? Date())

        if interval < 0 {
            return .errorLabel
        } else if arrivedAt != nil {
            return .timerGreen
        } else {
            return .mainText
        }
    }

    var backgroundColor: UIColor {

        let defaultColor: UIColor = isCard ? .timerBackground : .inputBackground

        guard !ProcessInfo().isInTestMode else { return defaultColor }

        let interval = scheduledAt.timeIntervalSince(arrivedAt ?? Date())
        if interval < 0 {
            return .errorBackground
        } else if arrivedAt != nil {
            return .greenBackground
        } else {
            return defaultColor
        }
    }

    var shouldTick: Bool {
        !ProcessInfo().isInTestMode && !isHidden && arrivedAt == nil
    }

    var isHidden: Bool {
        !ProcessInfo().isInTestMode && ![.paid, .cooking, .ready].contains(status)
    }

    private let arrivedAt: Date?
    private let scheduledAt: Date
    private let status: Order.Status
    private let isCard: Bool

}

extension TimerInfoModel {
    init(order: Order, isCard: Bool) {
        status = order.status
        arrivedAt = order.arrivedAt
        scheduledAt = order.scheduledAt
        self.isCard = isCard
    }
}
