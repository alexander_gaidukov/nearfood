//
//  OrderDetailsTipsSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsTipsSectionModel: TableViewSectionModel {

    private enum CellType {
        case pending(OrderDetailsTipsOptionsCellModel)
        case completed(OrderDetailsTipCellModel)
    }

    private let cellType: CellType?
    private let saveCellModel: OrderTipsSaveCellModel?
    private let shouldShowInfo: () -> ()

    init(
        tips: OrderTips?,
        options: [TipsOption],
        showSaveTips: Bool,
        didSelectOption: @escaping (TipsOption?) -> (),
        didSelect: @escaping () -> (),
        shouldShowInfo: @escaping () -> (),
        saveAsDefault: @escaping () -> (),
        cancelSaveAsDefault: @escaping () -> ()
    ) {
        self.shouldShowInfo = shouldShowInfo

        guard let tips = tips else {
            cellType = nil
            saveCellModel = nil
            return
        }

        if tips.paymentStatus == .pending, !tips.isProcessed {
            cellType = .pending(OrderDetailsTipsOptionsCellModel(
                tip: tips,
                options: options,
                didSelect: didSelectOption
            ))

            if showSaveTips {
                saveCellModel = OrderTipsSaveCellModel(
                    option: tips.option,
                    confirm: saveAsDefault,
                    cancel: cancelSaveAsDefault
                )
            } else {
                saveCellModel = nil
            }

        } else {
            cellType = .completed(OrderDetailsTipCellModel(tip: tips, didSelect: didSelect))
            saveCellModel = nil
        }
    }

    var numberOfRows: Int {
        if cellType == nil {
            return 0
        }

        return saveCellModel == nil ? 2 : 3
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsTipCell.self)
        tableView.register(cellType: OrderDetailsTipsOptionsCell.self)
        tableView.register(cellType: OrderTipsSaveCell.self)
        tableView.register(cellType: OrderDetailsHeaderCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cellType = cellType, indexPath.row > 0 else {
            let cell: OrderDetailsHeaderCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(title: LocalizedStrings.Account.tipsLabel, isInfoHidden: false)
            return cell
        }

        if let model = saveCellModel, indexPath.row == 2 {
            let cell: OrderTipsSaveCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(with: model)
            return cell
        }

        switch cellType {
        case .pending(let model):
            let cell: OrderDetailsTipsOptionsCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(with: model)
            return cell
        case .completed(let model):
            let cell: OrderDetailsTipCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(with: model)
            return cell
        }
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        if indexPath.row == 0 {
            shouldShowInfo()
        } else if indexPath.row == 1, case .completed(let model) = cellType {
            model.didSelect()
        }
    }
}
