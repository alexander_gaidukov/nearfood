//
//  OrderDetailsContactlessSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsContactlessSectionModel: TableViewSectionModel {
    var model: OrderDetailsContactlessCellModel
    private let selectionHandler: () -> ()

    init(isEnabled: Bool, isOn: Bool, didSwitch: @escaping (Bool) -> (), didSelect: @escaping () -> ()) {
        self.selectionHandler = didSelect
        model = OrderDetailsContactlessCellModel(isEnabled: isEnabled, isOn: isOn, didSwitch: didSwitch)
    }

    var numberOfRows: Int {
       1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsContactlessCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsContactlessCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        selectionHandler()
    }
}
