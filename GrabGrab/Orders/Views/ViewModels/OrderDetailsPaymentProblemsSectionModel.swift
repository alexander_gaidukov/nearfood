//
//  OrderDetailsPaymentProblemsSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsPaymentProblemsSectionModel: TableViewSectionModel {
    let isEnabled: Bool
    private let selectionHandler: () -> ()

    init(isEnabled: Bool, didSelect: @escaping () -> ()) {
        self.isEnabled = isEnabled
        self.selectionHandler = didSelect
    }

    var numberOfRows: Int {
        isEnabled ? 1 : 0
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsPaymentProblemCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsPaymentProblemCell = tableView.dequeue(forIndexPath: indexPath)
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        selectionHandler()
    }
}
