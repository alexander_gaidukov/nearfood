//
//  OrderCommentSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.10.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderCommentSectionModel: TableViewSectionModel {
    private let model: OrderCommentCellModel
    private let didSelect: () -> ()
    private let isEnable: Bool

    init(comment: String?, isEditable: Bool, didSelect: @escaping () -> ()) {
        model = OrderCommentCellModel(message: comment ?? "", isEditable: isEditable)
        self.didSelect = didSelect
        isEnable = (isEditable || comment?.isEmpty == false)
    }

    var numberOfRows: Int {
        isEnable ? 1 : 0
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderCommentCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderCommentCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        didSelect()
    }
}
