//
//  OrderDetailsReviewSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsReviewSectionModel: TableViewSectionModel {
    private let feedback: Feedback?
    private let selectionHandler: () -> ()

    init(feedback: Feedback?, didSelect: @escaping () -> ()) {
        self.feedback = feedback
        self.selectionHandler = didSelect
    }

    var numberOfRows: Int {
        feedback == nil ? 0 : 1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsReviewCell.self)
        tableView.register(cellType: OrderDetailsReviewResultCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        if let feedback = feedback, feedback.isCompleted {
            let cell: OrderDetailsReviewResultCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(model: OrderDetailsReviewResultCellModel(feedback: feedback))
            return cell
        }

        let cell: OrderDetailsReviewCell = tableView.dequeue(forIndexPath: indexPath)
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        if let feedback = feedback, feedback.isCompleted { return }
        selectionHandler()
    }
}
