//
//  OrderDetailsCancelSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsCancelSectionModel: TableViewSectionModel {
    let model: OrderDetailsCancelCellModel
    let isEnabled: Bool

    init(isEnabled: Bool, didSelect: @escaping () -> ()) {
        model = OrderDetailsCancelCellModel(didSelect: didSelect)
        self.isEnabled = isEnabled
    }

    var numberOfRows: Int {
        isEnabled ? 1 : 0
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsCancelCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsCancelCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
