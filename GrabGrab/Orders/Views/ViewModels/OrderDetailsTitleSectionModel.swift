//
//  OrderDetailsTitleSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsTitleSectionModel: TableViewSectionModel {

    private let model: OrderDetailsTitleCellModel

    init(title: String?) {
        model = OrderDetailsTitleCellModel(title: title)
    }

    var numberOfRows: Int {
        1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsTitleCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsTitleCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
