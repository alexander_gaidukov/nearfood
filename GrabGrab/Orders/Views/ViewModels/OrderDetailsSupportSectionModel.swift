//
//  OrderDetailsSupportSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsSupportSectionModel: TableViewSectionModel {
    private let selectionHandler: () -> ()
    private let model: OrderDetailsSupportCellModel

    init(shouldHideSeparator: Bool, didSelect: @escaping () -> ()) {
        self.selectionHandler = didSelect
        model = OrderDetailsSupportCellModel(shouldHideSeparator: shouldHideSeparator)
    }

    var numberOfRows: Int {
       1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsSupportCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsSupportCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model)
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        selectionHandler()
    }
}
