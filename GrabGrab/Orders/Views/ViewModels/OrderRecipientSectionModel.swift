//
//  OrderRecipientSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct OrderRecipientSectionModel: TableViewSectionModel {
    private let model: OrderDetailsRecipientCellModel
    private let isEditable: Bool
    private let didSelect: () -> ()

    init(recipient: (name: String, phone: String)?, isEditable: Bool, didSelect: @escaping () -> ()) {
        model = OrderDetailsRecipientCellModel(
            recipient: recipient,
            isEditable: isEditable
        )
        self.isEditable = isEditable
        self.didSelect = didSelect
    }

    var numberOfRows: Int {
        1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsRecipientCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderDetailsRecipientCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model)
        return cell
    }

    func didSelect(cell: UITableViewCell?, at indexPath: IndexPath) {
        if isEditable { didSelect() }
    }
}
