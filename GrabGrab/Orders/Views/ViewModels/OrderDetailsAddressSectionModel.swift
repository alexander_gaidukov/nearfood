//
//  OrderDetailsAddressSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsAddressSectionModel: TableViewSectionModel {

    private let model: OrderDetailsAddressCellModel

    init(location: Location?) {
        model = OrderDetailsAddressCellModel(location: location)
    }

    var numberOfRows: Int {
        2
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: OrderDetailsAddressCell.self)
        tableView.register(cellType: OrderDetailsHeaderCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row > 0 else {
            let cell: OrderDetailsHeaderCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(title: LocalizedStrings.Cart.Address.label)
            return cell
        }
        let cell: OrderDetailsAddressCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
