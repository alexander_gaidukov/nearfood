//
//  OrderStatusModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 08.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderStatusModel {
    let label: String
    let color: UIColor
}

extension OrderStatusModel {
    init(order: Order, isList: Bool) {

        guard !ProcessInfo().isInTestMode else {
            label = LocalizedStrings.Order.Status.ontheway
            color = .mainText
            return
        }

        switch order.status {
        case .pending:
            label = LocalizedStrings.Order.Status.pending
        case .unpaid:
            label = LocalizedStrings.Order.Status.unpaid
        case .paid, .cooking:
            label = LocalizedStrings.Order.Status.preparing
        case .ready:
            if order.isArrived {
                label = LocalizedStrings.Order.Status.arrived
            } else if order.bundle?.status == .inProgress {
                label = LocalizedStrings.Order.Status.ontheway
            } else {
                label = LocalizedStrings.Order.Status.preparing
            }
        case .completed:
            label = isList ? LocalizedStrings.Order.Status.completedList : LocalizedStrings.Order.Status.completed(in: order.deliveryTime)
        case .cancelled:
            label = LocalizedStrings.Order.Status.cancelled
        }

        color = order.status.messageColor
    }
}
