//
//  OrderCommentCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.10.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderCommentCellModel {
    let message: String
    let isEditable: Bool

    var isChangeHidden: Bool { !isEditable }
    var textColor: UIColor { message.isEmpty ? .labelText : .secondaryText }

    var displayNote: String { message.isEmpty ? LocalizedStrings.Order.Comment.empty : message }
}

class OrderCommentCell: UITableViewCell {

    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var changeLabel: UILabel!
    @IBOutlet private weak var hideChangeConstraint: NSLayoutConstraint!

    func configure(model: OrderCommentCellModel) {
        messageLabel.text = model.displayNote
        changeLabel.isHidden = model.isChangeHidden
        changeLabel.text = LocalizedStrings.Common.change
        hideChangeConstraint.priority = model.isChangeHidden ? .defaultHigh : .defaultLow
        messageLabel.numberOfLines = 0
        messageLabel.textColor = model.textColor
    }

}
