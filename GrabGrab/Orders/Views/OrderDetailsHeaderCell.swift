//
//  OrderDetailsHeaderCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class OrderDetailsHeaderCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var infoImageView: UIImageView!

    func configure(title: String, isInfoHidden: Bool = true) {
        titleLabel.text = title
        infoImageView.isHidden = isInfoHidden
    }
}
