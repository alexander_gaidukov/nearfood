//
//  OrderDetailsBundleCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct OrderDetailsBundleCellModel {
    var positionInBundle: Int
    var vehicleIcon: UIImage?

    var message: NSAttributedString {
        let result = NSMutableAttributedString(
            string: LocalizedStrings.Order.Bundle.message(for: positionInBundle + 1),
            attributes: [.font: UIFont.preferredFont(forTextStyle: .footnote), .foregroundColor: UIColor.mainText]
        )

        // swiftlint:disable:next force_try
        let expression = try! NSRegularExpression(pattern: "№[1-9]+", options: [])
        let range = expression.rangeOfFirstMatch(
            in: result.string,
            options: [],
            range: NSRange(location: 0, length: result.string.utf16.count)
        )
        result.addAttribute(
            .font,
            value: UIFont.systemFont(ofSize: UIFont.preferredFont(forTextStyle: .footnote).pointSize, weight: .bold),
            range: range
        )
        return result
    }
}

final class OrderPositionView: UIView {

    private let innerView: UIView

    init(position: Int) {
        innerView = UIView(frame: .zero)
        innerView.backgroundColor = .background
        innerView.translatesAutoresizingMaskIntoConstraints = false
        innerView.cornerRadius = 14
        innerView.borderColor = .mainText
        innerView.borderWidth = 3
        super.init(frame: .zero)
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(innerView)

        let label = UILabel(frame: .zero)
        label.font = .preferredFont(forTextStyle: .footnote)
        label.backgroundColor = .clear
        label.textColor = .mainText
        label.text = "\(position)"
        label.translatesAutoresizingMaskIntoConstraints = false
        innerView.addSubview(label)

        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: 36.0),
            heightAnchor.constraint(equalToConstant: 36.0),
            innerView.widthAnchor.constraint(equalToConstant: 28.0),
            innerView.heightAnchor.constraint(equalToConstant: 28.0),
            innerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            innerView.centerYAnchor.constraint(equalTo: centerYAnchor),
            label.centerXAnchor.constraint(equalTo: innerView.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: innerView.centerYAnchor)
        ])
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                innerView.borderColor = .mainText
            }
        }
    }
}

final class OrderDetailsBundleCell: UITableViewCell {
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var stackView: UIStackView!

    func configure(model: OrderDetailsBundleCellModel) {
        messageLabel.attributedText = model.message
        if stackView.arrangedSubviews.count > 1 {
            for index in (1..<stackView.arrangedSubviews.count).reversed() {
                stackView.arrangedSubviews[index].removeFromSuperview()
            }
        }

        for index in 0...model.positionInBundle {
            stackView.addArrangedSubview(arrowView())
            stackView.addArrangedSubview(circleView(position: index, isCurrent: index == model.positionInBundle))
        }

        if let vehicleImageView = stackView.arrangedSubviews.first as? UIImageView {
            vehicleImageView.image = model.vehicleIcon ?? #imageLiteral(resourceName: "car")
        }
    }

    private func arrowView() -> UIView {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "arrow_long"))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .barTint
        return imageView
    }

    private func circleView(position: Int, isCurrent: Bool) -> UIView {
        isCurrent ? UIImageView(image: #imageLiteral(resourceName: "user")) : OrderPositionView(position: position + 1)
    }
}
