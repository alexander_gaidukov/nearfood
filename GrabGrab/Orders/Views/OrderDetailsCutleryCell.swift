//
//  OrderDetailsCutleryCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class OrderDetailsCutleryCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var countLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Cart.cutleryTitle
    }

    func configure(count: Int) {
        countLabel.text = "\(count)"
    }
}
