//
//  OrderInfoViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class OrderInfoViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    var models: [TableViewSectionModel] = [] {
        didSet {
            if isViewLoaded { reloadData() }
        }
    }

    var isScrollable: Bool = false {
        didSet {
            tableView?.isScrollEnabled = isScrollable
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isScrollEnabled = isScrollable
    }

    private func reloadData() {
        configureCells()
        tableView.reloadData()
    }

    private func configureCells() {
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: UIApplication.shared.safeAreaInsets.bottom + 25, right: 0)
        models.forEach {
            $0.registerCells(in: tableView)
        }
    }
}

extension OrderInfoViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        models.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        models[section].numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        models[indexPath.section].cell(tableView: tableView, indexPath: indexPath)
    }
}

extension OrderInfoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        models[indexPath.section].didSelect(cell: tableView.cellForRow(at: indexPath), at: indexPath)
    }
}
