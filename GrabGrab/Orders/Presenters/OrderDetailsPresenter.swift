//
//  OrderDetailsPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

// swiftlint:disable file_length

protocol OrderDetailsPresenterType {
    var orderUUID: String { get }
    var models: Observable<[TableViewSectionModel]> { get }
    var state: OrderDetailsViewState { get }
    func dismiss()
    func saveAddress()
    func dismissAddressPrompt()
}

struct OrderDetailsViewState {
    @Observable
    var order: Order?

    var isEmpty: Observable<Bool> {
        $order.map { $0 == nil }
    }

    @Observable
    var isLoading: Bool = false

    @Observable
    var courierPolyline: PolylineResponse?

    @Observable
    var saveAddressAlreadyAsked: Bool

    @Observable
    var error: WebError?

    @Observable
    var tipsSelectionMessage: String?

    @Observable
    var tipsOptions: [TipsOption] = []

    var location: Observable<Location?> {
        $order.map { $0?.deliveryLocation }
    }

    var statusModel: Observable<OrderStatusModel?> {
        $order.map { $0.map { OrderStatusModel(order: $0, isList: false) } }
    }

    var timerInfo: Observable<TimerInfoModel?> {
        $order.map { $0.map { TimerInfoModel(order: $0, isCard: false) } }
    }

    var shouldHideSaveAddress: Observable<Bool> {
        $order.combine(with: $saveAddressAlreadyAsked) { $0?.deliveryLocation.title != nil || $1 }
    }

    var bundleItems: Observable<[(position: GeoPosition, isCurrent: Bool)]> {
        $order.map {
            guard let order = $0 else { return [] }

            guard let position = order.positionInBundle,
                  let bundle = order.bundle,
                  let orders = bundle.activeOrders,
                  position < orders.count,
                  order.isTrackingAvailable else { return [(order.deliveryLocation.position, true)] }

            return (0...position).map {
                let item = orders[$0]
                return (item.position, item.uuid == order.uuid)
            }
        }
    }

    var vehicleType: Observable<Courier.VehicleType?> {
        $order.map { $0?.bundle?.courier?.vehicleType }
    }

    fileprivate var cartItems:[(charge: EstimationCharge, cartItem: CartItem?)] {
        (order?.estimation.calculation.charges?.elements ?? []).map { charge in
            let itemUUID = charge.dishMetadata?.dishUuid ?? charge.comboMetadata?.comboUuid
            let cartItem = order?.cartItems.first { $0.uuid == itemUUID }
            return (charge, cartItem)
        }
    }
}

enum OrderDetailsAction {
    case completed
    case contactlessInfo
    case changePaymentMethod((PaymentMethod?, WebError?) -> ())
    case saveAddress(Location)
    case writeToSupport(Order)
    case feedback(Order, (Feedback?) -> ())
    case fees(Fees, (Bool) -> ())
    case cartItem(CartItem, [UIImage?])
    case comment(String?, (String?) -> ())
    case applePay(Order, (String?) -> ())
    case receipt(Receipt)
    case otherTipOption((OrderTips?) -> ())
    case tipsInfo
    case changeRecipient(Recipient?, (Recipient?) -> ())
}

// swiftlint:disable:next type_body_length
final class OrderDetailsPresenter: OrderDetailsPresenterType {

    var state: OrderDetailsViewState

    private let completion: (OrderDetailsAction) -> ()
    private let addressesDataProvider: AddressesDataProviderType
    private let ordersDataProvider: OrdersDataProviderType
    private let phoneCodesManager: PhoneCodesManagerType
    private let customerDataProvider: CustomerDataProviderType
    private let webClient: WebClientType

    private var isOrderTrackingInProgress: Bool = false
    let orderUUID: String
    private let disposableBag = DisposableBag()

    var models = Observable<[TableViewSectionModel]>(wrappedValue: [])

    private var didCancelDefaultTips = false

    init(
        orderUUID: String,
        ordersDataProvider: OrdersDataProviderType,
        addressesDataProvider: AddressesDataProviderType,
        phoneCodesManager: PhoneCodesManagerType,
        customerDataProvider: CustomerDataProviderType,
        webClient: WebClientType,
        completion: @escaping (OrderDetailsAction) -> ()
    ) {
        self.ordersDataProvider = ordersDataProvider
        self.customerDataProvider =  customerDataProvider
        self.completion = completion
        self.webClient = webClient
        self.addressesDataProvider = addressesDataProvider
        self.phoneCodesManager = phoneCodesManager
        self.orderUUID = orderUUID

        let order = ordersDataProvider.orders.first { $0.uuid == orderUUID }
            ?? ordersDataProvider.activeOrders.first { $0.uuid == orderUUID }

        state = OrderDetailsViewState(
            order: order,
            saveAddressAlreadyAsked: addressesDataProvider.askedForSaveLocationIds.contains(order?.deliveryLocation.uuid ?? "")
        )
        models.wrappedValue = configureModels()
        state.$order.observe { [weak self] _ in
            self?.models.wrappedValue = self?.configureModels() ?? []
        }.addToDisposableBag(disposableBag)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(shouldUpdateOrder(_:)),
            name: .shouldUpdateOrdersNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(shouldUpdateFeedback(_:)),
            name: .shouldUpdateFeedbackNotification,
            object: nil
        )

        startOrStopOrderTracking()
        scheduleOrderUpdate()
        loadTipsOptions()
    }

    func dismiss() {
        completion(.completed)
    }

    func saveAddress() {
        guard let location = state.order?.deliveryLocation else { return }
        dismissAddressPrompt()
        completion(.saveAddress(location))
    }

    func dismissAddressPrompt() {
        guard let location = state.order?.deliveryLocation else { return }
        addressesDataProvider.locationDidAskedForSave(locationId: location.uuid)
        state.saveAddressAlreadyAsked = true
    }

    // swiftlint:disable:next function_body_length
    private func configureModels() -> [TableViewSectionModel] {
        let titleModel = OrderDetailsTitleSectionModel(title: state.order?.title)
        let addressModel = OrderDetailsAddressSectionModel(location: state.order?.deliveryLocation)
        let showSaveTips = !didCancelDefaultTips
            && (state.order?.tip?.amount ?? 0) > .ulpOfOne
            && (customerDataProvider.customer?.tipsSettings?.props.value ?? 0) <= .ulpOfOne

        let tipsModel = OrderDetailsTipsSectionModel(
            tips: state.order?.tip,
            options: state.tipsOptions,
            showSaveTips: showSaveTips,
            didSelectOption: { [weak self] in
                guard let self = self else { return }
                if let option = $0 {
                    self.updateTipsOption(option)
                } else {
                    self.selectOtherTipsOption()
                }
            },
            didSelect: { [weak self] in self?.showTipsReceipt() },
            shouldShowInfo: { [weak self] in self?.completion(.tipsInfo) },
            saveAsDefault: { [weak self] in self?.saveTipsAsDefault() },
            cancelSaveAsDefault: { [weak self] in self?.cancelTipsSaving() }
        )

        let dishesModel = OrderDetailsDishesSectionModel(
            items: state.cartItems,
            didSelect: { [weak self] cartItem, placeholders in self?.completion(.cartItem(cartItem, placeholders)) }
        )
        let cutleryModel = OrderDetailsCutlerySectionModel(count: state.order?.cutleryCount ?? 0)
        let totalModel = OrderDetailsTotalSectionModel(price: state.order?.estimation.calculation.total ?? 0)
        let contactlessModel = OrderDetailsContactlessSectionModel(
            isEnabled: ![.cancelled, .completed].contains(state.order?.status),
            isOn: state.order?.contactless == true,
            didSwitch: {[weak self] isOn in
                self?.changeContactless(isOn)
            },
            didSelect: {[weak self] in
                self?.completion(.contactlessInfo)
            }
        )

        let receiptModel = OrderDetailsBillSectionModel(receipt: state.order?.receipt) { [weak self] in
            self?.showReceipt()
        }

        let supportModel = OrderDetailsSupportSectionModel(shouldHideSeparator: receiptModel.numberOfRows == 0) { [weak self] in
            self?.writeToSupport()
        }

        let cancelModel = OrderDetailsCancelSectionModel(isEnabled: state.order?.isCancellable == true) {[weak self] in
            self?.cancelOrder()
        }

        let paymentProblemsModel = OrderDetailsPaymentProblemsSectionModel(isEnabled: state.order?.status == .unpaid) {[weak self] in
            self?.changePaymentMethod()
        }

        let bundleModel = OrderDetailsBundleSectionModel(
            shouldShow: state.order?.isTrackingAvailable == true,
            position: state.order?.positionInBundle,
            bundleSize: state.order?.bundle?.orders?.count,
            vehicleIcon: state.order?.bundle?.courier?.vehicleType.icon
        )

        let reviewModel = OrderDetailsReviewSectionModel(feedback: state.order?.feedback, didSelect: {[weak self] in self?.showFeedback() })

        let paymentModel = OrderPaymentMethodSectionModel(paymentMethod: state.order?.paymentMethod)

        let commentModel = OrderCommentSectionModel(
            comment: state.order?.note,
            isEditable: ![.cancelled, .completed].contains(state.order?.status)
        ) { [weak self] in
            self?.editOrderComment()
        }

        let recipient = state.order?.recipient.map {
            (
                name: $0.name,
                phone: self.phoneCodesManager.phoneNumber(from: $0.phone).formattedNumber
            )
        }

        let recipientModel = OrderRecipientSectionModel(
            recipient: recipient,
            isEditable: ![.cancelled, .completed].contains(state.order?.status)
        ) { [weak self] in
            self?.editRecipient()
        }

        var models: [TableViewSectionModel] = [
            titleModel,
            bundleModel,
            paymentProblemsModel,
            addressModel,
            tipsModel,
            dishesModel,
            cutleryModel,
            totalModel,
            commentModel,
            recipientModel,
            paymentModel,
            contactlessModel
        ]

        if !ProcessInfo().isInTestMode {
            if [.completed, .cancelled].contains(state.order?.status) {
                models.insert(reviewModel, at: 1)
            } else {
                models.append(reviewModel)
            }
        }

        models.append(supportModel)
        models.append(receiptModel)
        models.append(cancelModel)

        return models
    }

    private func saveTipsAsDefault() {
        guard let tips = state.order?.tip?.option, let customer = customerDataProvider.customer else {
            return
        }

        let resource = ResourceBuilder.updateTipsSettingsResource(tips, customerUUID: customer.uuid)
        state.isLoading = true

        webClient.load(resource: resource) { [weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success(let customer):
                self.customerDataProvider.updateTipsSettings(tips: customer.tipsSettings)
                self.models.wrappedValue = self.configureModels()
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    private func cancelTipsSaving() {
        didCancelDefaultTips = true
        models.wrappedValue = configureModels()
    }

    private func showTipsReceipt() {
        guard let receipt = state.order?.tip?.receipt else { return }
        completion(.receipt(receipt))
    }

    private func editRecipient() {
        completion(.changeRecipient(state.order?.recipient, {[weak self] recipient in
            guard let self = self else { return }
            if let recipient = recipient {
                self.updateRecipient(recipient)
            } else {
                self.removeRecipient()
            }
        }))
    }

    private func removeRecipient() {
        guard
            let order = state.order,
            let recipient = order.recipient
        else { return }

        let resource = ResourceBuilder.removeRecipientResource(recipient, from: order)

        state.isLoading = true
        webClient.load(combinedResource: resource) { [weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let order):
                self?.updateState(with: order)
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    private func updateRecipient(_ recipient: Recipient) {
        guard let order = state.order else { return }

        guard let currentRecipient = order.recipient else {
            addRecipient(recipient, to: order)
            return
        }

        guard
            currentRecipient.name != recipient.name
            || currentRecipient.phone != recipient.phone
        else {
            return
        }

        let newRecipient = Recipient(
            uuid: currentRecipient.uuid,
            phone: recipient.phone,
            name: recipient.name
        )

        let resource = ResourceBuilder.updateRecipientResource(newRecipient, in: order)

        state.isLoading = true
        webClient.load(combinedResource: resource) { [weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let order):
                self?.updateState(with: order)
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    private func addRecipient(_ recipient: Recipient, to order: Order) {
        let resource = ResourceBuilder.addRecipientResource(recipient, to: order)

        state.isLoading = true
        webClient.load(combinedResource: resource) { [weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let order):
                self?.updateState(with: order)
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    private func changeContactless(_ isOn: Bool) {
        updateOrder(contactless: isOn)
    }

    private func writeToSupport() {
        guard let order = state.order else { return }
        completion(.writeToSupport(order))
    }

    private func cancelOrder() {
        guard let order = state.order else { return }
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.cancelOrderFeesResource(order: order)) {[weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success(let fees):
                DispatchQueue.main.async {
                    self.completion(.fees(fees, {[weak self] isConfirmed in
                        if isConfirmed { self?.confirmOrderCancellation() }
                    }))
                }
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    private func showReceipt() {
        guard let receipt = state.order?.receipt else { return }
        completion(.receipt(receipt))
    }

    private func showFeedback() {
        guard let order = state.order else { return }
        completion(.feedback(order, {[weak self] feedback in
            guard let feedback = feedback else { return }
            self?.state.order?.feedback = feedback
        }))
    }

    private func confirmOrderCancellation() {
        guard let order = state.order else { return }
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.cancelOrderResource(order: order)) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let order):
                self?.state.order = order
                DispatchQueue.main.async {
                    self?.ordersDataProvider.updateActiveOrders(notifyWidget: true)
                    self?.dismiss()
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    private func changePaymentMethod() {
        completion(.changePaymentMethod({[weak self] method, error in
            if let error = error {
                self?.state.error = error
            }

            if let method = method {
                self?.updateOrder(paymentMethod: method)
            }
        }))
    }

    private func updateOrder(paymentMethod: PaymentMethod? = nil, contactless: Bool? = nil, note: String? = nil) {

        guard let order = state.order else { return }

        if case let .applePay(token) = paymentMethod, token == nil {
            DispatchQueue.main.async {
                self.completion(.applePay(order, { [weak self] token in
                    guard let token = token else { return }
                    self?.updateOrder(paymentMethod: .applePay(token), contactless: contactless, note: note)
                }))
            }
            return
        }

        if let contactless = contactless {
            state.order?.contactless = contactless
        }

        state.isLoading = true
        let resource = ResourceBuilder.updateOrderResource(
            order: order,
            isContactless: contactless,
            paymentMethod: paymentMethod,
            note: note
        )
        webClient.load(resource: resource) { [weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let order):
                self?.updateState(with: order)
            case .failure(let error):
                if let contactless = contactless {
                    self?.state.order?.contactless = !contactless
                }
                self?.state.error = error
            }
        }
    }

    private func scheduleOrderUpdate() {
        loadOrderDetails()
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.Intervals.ordersUpdateInterval) {[weak self] in
            guard let self = self, self.state.order?.status != .cancelled && self.state.order?.status != .completed else { return }
            self.scheduleOrderUpdate()
        }
    }

    private func loadOrderDetails() {
        self.ordersDataProvider.orderDetails(for: orderUUID) { [weak self] order in
            guard let order = order else { return }
            self?.updateState(with: order)
        }
    }

    private func updateState(with order: Order) {
        state.order = order
        state.saveAddressAlreadyAsked = addressesDataProvider.askedForSaveLocationIds.contains(order.deliveryLocation.uuid)
        startOrStopOrderTracking()
    }

    private func startOrStopOrderTracking() {
        guard let order = state.order else { return }
        if order.isTrackingAvailable && !isOrderTrackingInProgress {
            isOrderTrackingInProgress = true
            scheduleOrderTracking()
        } else if !order.isTrackingAvailable && isOrderTrackingInProgress {
            isOrderTrackingInProgress = false
            state.courierPolyline = nil
        }
    }

    private func loadTipsOptions() {
        let resource = ResourceBuilder.tipsOptionsResource(orderUUID: orderUUID)
        webClient.load(resource: resource) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let options):
                self.state.tipsOptions = options
                self.models.wrappedValue = self.configureModels()
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    private func selectOtherTipsOption() {
        completion(.otherTipOption({ [weak self] orderTips in
            guard let self = self, let orderTips = orderTips else { return }
            if !self.state.tipsOptions.contains(where: { fabs(($0.amount ?? 0) - orderTips.amount) <= .ulpOfOne }) {
                var newOptions = self.state.tipsOptions
                let additionalOption = TipsOption(
                    amount: orderTips.amount,
                    kind: orderTips.kind,
                    props: orderTips.props
                )
                newOptions.append(additionalOption)
                newOptions.sort { ($0.amount ?? 0) < ($1.amount ?? 0) }
                self.state.tipsOptions = newOptions
            }

            self.state.order?.tip = orderTips
            self.tipsValueDidChange()
            self.loadTipsOptions()
        }))
    }

    private func tipsValueDidChange() {
        guard let tips = state.order?.tip else { return }
        var tipsValue = tips.amount.string(with: Formatters.priceFormatter) ?? ""
        let option = tips.option
        if option.kind == .percent {
            tipsValue += " (\(option.formattedValue))"
        }
        state.tipsSelectionMessage = LocalizedStrings.Tips.Options.orderOptionConfirmation(value: tipsValue)
    }

    private func updateTipsOption(_ option: TipsOption) {
        let resource = ResourceBuilder.updateTipsSettingsResource(option, orderUUID: orderUUID)
        state.isLoading = true
        webClient.load(resource: resource) { [weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success(let tips):
                self.state.order?.tip = tips
                self.tipsValueDidChange()
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    private func scheduleOrderTracking() {
        loadOrderTracking()
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.Intervals.orderTrackingInterval) {[weak self] in
            guard let self = self, self.isOrderTrackingInProgress else { return }
            self.scheduleOrderTracking()
        }
    }

    private func editOrderComment() {
        guard ![.cancelled, .completed].contains(state.order?.status) else { return }
        completion(.comment(state.order?.note, { [weak self] note in
            if let note = note {
                self?.updateOrder(note: note)
            }
        }))
    }

    private func loadOrderTracking() {
        guard let order = state.order else { return }
        webClient.load(resource: ResourceBuilder.tracks(for: order)) {[weak self] result in
            guard self?.isOrderTrackingInProgress == true, let polylineResponse = try? result.get() else { return }
            self?.state.courierPolyline = polylineResponse
        }
    }

    @objc private func shouldUpdateOrder(_ notification: Notification) {
        guard let uuid = notification.userInfo?["uuid"] as? String, uuid == orderUUID else { return }
        loadOrderDetails()
    }

    @objc private func shouldUpdateFeedback(_ notification: Notification) {
        guard let json = notification.userInfo,
            let data = try? JSONSerialization.data(withJSONObject: json, options: []),
            let feedback = try? Coders.decoder.decode(Feedback.self, from: data) else { return }
        state.order?.feedback = feedback
    }
}
