//
//  OrderCancellationFeesPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.10.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol OrderCancellationFeesPresenterType {
    var description: NSAttributedString { get }
    func confirm()
    func cancel()
}

enum OrderCancellationFeesAction {
    case confirm
    case cancel
}

final class OrderCancellationFeesPresenter: OrderCancellationFeesPresenterType {
    private let completion: (OrderCancellationFeesAction) -> ()

    let description: NSAttributedString

    init(fees: Fees, completion: @escaping (OrderCancellationFeesAction) -> ()) {
        self.completion = completion

        if fees.fee == 0 {
            description = NSAttributedString(
                string: LocalizedStrings.Order.cancelConfirmation,
                attributes: [.font: UIFont.preferredFont(forTextStyle: .subheadline), .foregroundColor: UIColor.mainText]
            )
        } else {
            let feesString = fees.fee.string(with: Formatters.priceFormatter) ?? ""
            let refundString = fees.refund.string(with: Formatters.priceFormatter) ?? ""

            let description = NSMutableAttributedString(
                string: String(format: LocalizedStrings.Order.Cancellation.Fees.description, feesString, refundString),
                attributes: [.font: UIFont.preferredFont(forTextStyle: .subheadline), .foregroundColor: UIColor.mainText]
            )

            if let range = description.string.ranges(of: feesString).first {
                let feesRange = NSRange(range, in: description.string)
                description.addAttribute(
                    .font,
                    value: UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .subheadline).pointSize),
                    range: feesRange
                )
            }

            if let range = description.string.ranges(of: refundString).last {
                let refundRange = NSRange(range, in: description.string)

                description.addAttribute(
                    .font,
                    value: UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .subheadline).pointSize),
                    range: refundRange
                )
            }

            self.description = description
        }
    }

    func confirm() {
        completion(.confirm)
    }

    func cancel() {
        completion(.cancel)
    }
}
