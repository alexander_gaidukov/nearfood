//
//  IntroPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol IntroPresenterType {
    var state: IntroViewState { get }
    func next()
}

enum IntroStep: Equatable, CaseIterable {
    case taste
    case delivery
    case transparancy
    case quality

    var next: IntroStep? {
        let values = IntroStep.allCases
        guard index < values.count - 1 else { return nil }
        return values[index + 1]
    }

    var index: Int {
        // swiftlint:disable:next force_unwrapping
        IntroStep.allCases.firstIndex(of: self)!
    }
}

enum IntroAction {
    case next
}

struct IntroViewState {

    enum ButtonState {
        case next
        case complete

        var title: String {
            switch self {
            case .complete:
                return LocalizedStrings.Intro.completeButton
            case .next:
                return LocalizedStrings.Intro.nextButton
            }
        }

        var textColor: UIColor {
            switch self {
            case .complete:
                return UIColor.white
            case .next:
                return .secondaryText
            }
        }

        var backgroundColor: UIColor {
            switch self {
            case .complete:
                return .primary
            case .next:
                return .inputBackground
            }
        }
    }

    var selectedIndex: Int
    var title: String
    var description: String
    var image: UIImage
    var buttonState: ButtonState

    init(step: IntroStep) {
        switch step {
        case .taste:
            title = LocalizedStrings.Intro.Taste.title
            description = LocalizedStrings.Intro.Taste.description
            image = #imageLiteral(resourceName: "intro_taste")
        case .delivery:
            title = LocalizedStrings.Intro.Delivery.title
            description = LocalizedStrings.Intro.Delivery.description
            image = #imageLiteral(resourceName: "intro_delivery")
        case .transparancy:
            title = LocalizedStrings.Intro.Transparancy.title
            description = LocalizedStrings.Intro.Transparancy.description
            image = #imageLiteral(resourceName: "intro_transparancy")
        case .quality:
            title = LocalizedStrings.Intro.Quality.title
            description = LocalizedStrings.Intro.Quality.description
            image = #imageLiteral(resourceName: "intro_quality")
        }

        selectedIndex = step.index
        buttonState = step.next == nil ? .complete : .next
    }
}

final class IntroPresenter: IntroPresenterType {
    private let completion: (IntroAction) -> ()
    private(set) var state: IntroViewState

    init(step: IntroStep, completion: @escaping (IntroAction) -> ()) {
        self.completion = completion
        self.state = IntroViewState(step: step)
    }

    func next() {
        completion(.next)
    }
}
