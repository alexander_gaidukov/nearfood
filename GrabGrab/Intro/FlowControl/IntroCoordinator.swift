//
//  IntroCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class IntroCoordinator: Coordinator {
    var completion: () -> ()
    private let router: IntroRouterType
    private let dataProvidersStorage: DataProvidersStorageType
    // swiftlint:disable:next force_unwrapping
    private var step: IntroStep = IntroStep.allCases.first!

    init(router: IntroRouter,
         dataProvidersStorage: DataProvidersStorageType,
         completion: @escaping () -> ()) {
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
        self.completion = completion
    }

    func start() {
        guard dataProvidersStorage.introDataProvider.shouldShowIntro else {
            completion()
            return
        }
        showIntro()
    }

    private func showIntro() {
        router.showIntroStep(step: step) { action in
            switch action {
            case .next:
                self.showNext()
            }
        }
    }

    private func showNext() {
        guard let nextStep = step.next else {
            dataProvidersStorage.introDataProvider.introDidFinish()
            completion()
            return
        }
        step = nextStep
        showIntro()
    }
}
