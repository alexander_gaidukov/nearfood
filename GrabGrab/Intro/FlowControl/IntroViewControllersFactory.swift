//
//  IntroViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit
protocol IntroViewControllersFactoryType {
    func introViewController(step: IntroStep, completion: @escaping (IntroAction) -> ()) -> IntroViewController
}

struct IntroViewControllersFactory: IntroViewControllersFactoryType {

    private let storyboard: Storyboard = .intro

    func introViewController(step: IntroStep, completion: @escaping (IntroAction) -> ()) -> IntroViewController {
        let presenter = IntroPresenter(step: step, completion: completion)
        let controller = IntroViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
