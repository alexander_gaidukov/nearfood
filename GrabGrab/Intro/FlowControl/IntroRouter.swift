//
//  IntroRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol IntroRouterType {
    func showIntroStep(step: IntroStep, completion: @escaping (IntroAction) -> ())
}

final class IntroRouter: IntroRouterType {
    private let window: UIWindow
    private let factory: IntroViewControllersFactoryType

    private var navigationController: GGNavigationController? {
        window.rootViewController as? GGNavigationController
    }

    init(window: UIWindow, factory: IntroViewControllersFactoryType) {
        self.window = window
        self.factory = factory
    }

    func showIntroStep(step: IntroStep, completion: @escaping (IntroAction) -> ()) {
        let controller = factory.introViewController(step: step, completion: completion)
        pushOrShow(viewController: controller)
    }

    private func pushOrShow(viewController: UIViewController) {
        if let navController = navigationController {
            navController.pushViewController(viewController, animated: true)
            return
        }

        let navController = GGNavigationController(rootViewController: viewController)
        window.rootViewController = navController
    }
}
