//
//  IntroViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class IntroViewController: UIViewController {
    @IBOutlet private weak var progressStackView: UIStackView!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var nextButton: UIButton!

    var presenter: IntroPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    private func configureViews() {
        progressStackView.arrangedSubviews.enumerated().forEach { index, item in
            item.backgroundColor = index == self.presenter.state.selectedIndex ? .progressBackgroundSelected : .progressBackground
        }
        imageView.image = presenter.state.image
        titleLabel.text = presenter.state.title
        descriptionLabel.text = presenter.state.description
        nextButton.setTitle(presenter.state.buttonState.title, for: .normal)
        nextButton.setTitleColor(presenter.state.buttonState.textColor, for: .normal)
        nextButton.backgroundColor = presenter.state.buttonState.backgroundColor
    }

    @IBAction private func buttonDidTap() {
        presenter.next()
    }
}
