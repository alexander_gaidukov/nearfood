//
//  ContactlessInfoViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class ContactlessInfoViewController: UIViewController, PopupViewController {
    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!

    var presenter: ContactlessInfoPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        titleLabel.text = LocalizedStrings.Cart.Contactless.title
        descriptionLabel.text = LocalizedStrings.Cart.Contactless.description
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        contentView.addGestureRecognizer(panGestureRecognizer)
    }

    @objc func close() {
        presenter.close()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }
}
