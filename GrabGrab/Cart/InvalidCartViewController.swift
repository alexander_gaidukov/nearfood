//
//  InvalidCartViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class InvalidCartViewController: UIViewController, PopupViewController {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var priceTitle: UILabel!
    @IBOutlet private weak var orderButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var imagesStackView: ImagesStackView!
    @IBOutlet private weak var hideImagesConstraint: NSLayoutConstraint!

    var presenter: InvalidCartPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        configureImages()
        titleLabel.text = LocalizedStrings.Error.commonTitle
        descriptionLabel.text = presenter.description
        priceTitle.attributedText = presenter.priceTitle
        orderButton.setTitle(LocalizedStrings.Cart.Invalid.orderButtonTitle, for: .normal)
        cancelButton.setTitle(LocalizedStrings.Cart.Invalid.cancelButtonTitle, for: .normal)
    }

    private func configureImages() {
        guard !presenter.photos.isEmpty else {
            imagesStackView.isHidden = true
            hideImagesConstraint.priority = .defaultHigh
            return
        }

        imagesStackView.configure(with: presenter.photos)
    }

    func close() {}

    @IBAction private func orderTapped() {
        presenter.order()
    }

    @IBAction private func cancelTapped() {
        presenter.cancel()
    }

}
