//
//  PaymentProcessingViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class PaymentProcessingViewController: UIViewController, PopupViewController {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!

    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var loadingImageView: UIImageView!
    @IBOutlet private weak var loadingTitleLabel: UILabel!

    @IBOutlet private weak var errorView: UIView!
    @IBOutlet private weak var errorTitleLabel: UILabel!
    @IBOutlet private weak var errorDescriptionLabel: UILabel!

    @IBOutlet private weak var paymentTitleLabel: UILabel!
    @IBOutlet private weak var paymentImageView: UIImageView!
    @IBOutlet private weak var paymentNameLabel: UILabel!
    @IBOutlet private weak var changePaymentLabel: UILabel!

    @IBOutlet private weak var paymentButton: UIButton!
    @IBOutlet private weak var closeButton: UIButton!

    var presenter: PaymentProcessingPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    private func configureViews() {
        loadingTitleLabel.text = LocalizedStrings.Cart.PaymentProcessing.loadingTitle
        errorTitleLabel.text = LocalizedStrings.Cart.PaymentProcessing.errorTitle
        errorDescriptionLabel.text = LocalizedStrings.Cart.PaymentProcessing.errorDescription
        paymentTitleLabel.text = LocalizedStrings.Cart.PaymentMethod.label
        changePaymentLabel.text = LocalizedStrings.Common.change
        closeButton.setTitle(LocalizedStrings.Common.close, for: .normal)
        paymentButton.setTitle(LocalizedStrings.Cart.PaymentProcessing.repeatButton, for: .normal)
    }

    private func configureBindings() {
        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$paymentMethod.observe { [weak self] in
            self?.configurePaymentMethod($0)
        }.addToDisposableBag(disposableBag)

        presenter.state.$kind.observe { [weak self] in
            self?.updateView(with: $0)
        }.addToDisposableBag(disposableBag)

        presenter.state.isCloseButtonHidden.bind(to: closeButton, keyPath: \.isHidden).addToDisposableBag(disposableBag)
    }

    private func updateView(with kind: PaymentProcessingViewState.ViewKind) {
        loadingView.isHidden = kind != .loading
        errorView.isHidden = kind != .unpaid
        UIView.animate(withDuration: 0.3) { self.contentView.layoutIfNeeded() }
    }

    private func configurePaymentMethod(_ paymentMethod: PaymentMethod?) {
        paymentImageView.image = paymentMethod?.image
        paymentNameLabel.text = paymentMethod?.title
    }

    private func startLoadingImageViewAnimation() {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")

        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = Float.pi * 2.0
        rotationAnimation.duration = 0.8
        rotationAnimation.repeatCount = Float.infinity

        loadingImageView.layer.add(rotationAnimation, forKey: "com.grabgrab.rotationanimationkey")
    }

    func close() {}

    func animationDidFinish() {
        startLoadingImageViewAnimation()
        presenter.viewDidAppear()
    }

    @IBAction private func changePaymentTapped() {
        presenter.changePaymentMethod()
    }

    @IBAction private func paymentButtonTapped() {
        presenter.repeatPayment()
    }

    @IBAction private func closeButtonTapped() {
        presenter.close()
    }
}

extension PaymentProcessingViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
