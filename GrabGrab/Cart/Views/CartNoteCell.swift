//
//  CartNoteCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartNoteCellModel {
    let note: String
    let change: () -> ()

    var displayNote: String { note.isEmpty ? LocalizedStrings.Order.Comment.empty : note }
    var textColor: UIColor { note.isEmpty ? .labelText : .secondaryText }

}

class CartNoteCell: UITableViewCell {

    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var changeLabel: UILabel!

    var model: CartNoteCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(model: CartNoteCellModel) {
        self.model = model
    }

    private func updateUI() {
        messageLabel.text = model.displayNote
        changeLabel.text = LocalizedStrings.Common.change
        messageLabel.numberOfLines = 0
        messageLabel.textColor = model.textColor
    }

    @IBAction private func changeTapped() {
        model.change()
    }
}
