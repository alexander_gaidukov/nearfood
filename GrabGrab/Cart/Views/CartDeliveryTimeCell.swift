//
//  CartDeliveryTimeCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartDeliveryTimeCellModel {
    var didSelect: () -> ()
    var title: String

    init(didSelect: @escaping () -> ()) {
        title = LocalizedStrings.Cart.DeliveryTime.asap
        self.didSelect = didSelect
    }
}

final class CartDeliveryTimeCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!

    private var model: CartDeliveryTimeCellModel! {
        didSet {
            updateUI()
        }
    }

    func configure(with model: CartDeliveryTimeCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.title
    }

    @IBAction private func cellDidTap() {
        model.didSelect()
    }
}
