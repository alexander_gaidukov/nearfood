//
//  CartRecipientCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartRecipientCellModel {
    var didSelect: () -> ()
    var title: String

    init(recipient: (name: String, phone: String)?, didSelect: @escaping () -> ()) {
        if let recipient = recipient {
            title = "\(recipient.name)\n\(recipient.phone)"
        } else {
            title = LocalizedStrings.Recipient.empty
        }
        self.didSelect = didSelect
    }
}

final class CartRecipientCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!

    private var model: CartRecipientCellModel! {
        didSet {
            updateUI()
        }
    }

    func configure(with model: CartRecipientCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.title
    }

    @IBAction private func cellDidTap() {
        model.didSelect()
    }
}
