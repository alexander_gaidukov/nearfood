//
//  CartOrderCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartOrderCellModel {
    var isEnabled: Bool
    var storefrontState: Storefront.State
    var buttonTitle: String
    var orderClick: () -> ()
}

extension CartOrderCellModel {
    init(price: Double, isEnabled: Bool, isEmpty: Bool, storefrontState: Storefront.State, orderClick: @escaping () -> ()) {
        self.isEnabled = isEnabled
        self.storefrontState = storefrontState

        switch storefrontState {
        case .closed:
            self.buttonTitle = LocalizedStrings.Menu.Closed.title
        default:
            if isEmpty {
                self.buttonTitle = LocalizedStrings.Cart.Empty.buttonTitle
            } else if price == 0 {
                self.buttonTitle = LocalizedStrings.Cart.freeOrderButtonTitle
            } else {
                self.buttonTitle = "\(LocalizedStrings.Cart.orderButtonTitle) \(price.string(with: Formatters.priceFormatter) ?? "")"
            }
        }

        self.orderClick = orderClick
    }
}

class CartOrderCell: UITableViewCell {
    @IBOutlet private weak var orderButton: UIButton!

    private var model: CartOrderCellModel! {
        didSet {
            updateUI()
        }
    }

    func configure(model: CartOrderCellModel) {
        self.model = model
    }

    private func updateUI() {
        self.orderButton.setTitle(model.buttonTitle, for: .normal)
        self.orderButton.isEnabled = model.isEnabled

        switch model.storefrontState {
        case .surge:
            orderButton.setImage(#imageLiteral(resourceName: "surge_button"), for: .normal)
        default:
            orderButton.setImage(nil, for: .normal)
        }
    }

    @IBAction private func orderButtonTapped() {
        model.orderClick()
    }
}
