//
//  CartCouponCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartCouponCellModel {
    let amount: String?
    let percent: String?
    let description: String?
    let title: String
    let note: String?
    let didSelect: () -> ()
    let didRemove: () -> ()
}

extension CartCouponCellModel {
    init(amount: Double, coupon: Coupon, note: String?, didSelect: @escaping () -> (), didRemove: @escaping () -> ()) {
        self.didSelect = didSelect
        self.didRemove = didRemove
        self.note = note
        title = coupon.name
        switch coupon.kind {
        case .discountFixed:
            self.amount = abs(amount).string(with: Formatters.priceFormatter).map { "-\($0)" }
            percent = nil
            description = nil
        case .discountPercent:
            self.amount = abs(amount).string(with: Formatters.priceFormatter).map { "-\($0)" }
            percent = "\(Int(coupon.props.value * 100))%"
            description = coupon.props.maxAmount?.string(with: Formatters.priceFormatter).map {
                LocalizedStrings.Cart.Coupons.maxDiscount($0)
            }
        case .cashbackFixed:
            self.amount = nil
            percent = nil
            description = abs(amount).string(with: Formatters.priceFormatter).map {
                LocalizedStrings.Cart.Coupons.cashback($0)
            }
        case .cashbackPercent:
            self.amount = nil
            self.percent = nil

            var value = "\(Int(coupon.props.value * 100))%"
            if let amount = abs(amount).string(with: Formatters.priceFormatter) {
                value += "(\(amount))"
            }

            var couponDescription = LocalizedStrings.Cart.Coupons.cashback(value)
            if let maxAmount = coupon.props.maxAmount?.string(with: Formatters.priceFormatter) {
                couponDescription += "\n\(LocalizedStrings.Cart.Coupons.maxCashback(maxAmount))"
            }

            description = couponDescription
        }
    }
}

final class CartCouponCell: UITableViewCell {

    @IBOutlet private var detailsLabel: UILabel!
    @IBOutlet private var amountStackView: UIStackView!
    @IBOutlet private var amountLabel: UILabel!
    @IBOutlet private var percentLabel: UILabel!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!

    @IBOutlet private weak var noteView: UIView!
    @IBOutlet private weak var noteLabel: UILabel!

    private var model: CartCouponCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        detailsLabel.text = LocalizedStrings.Common.details
    }

    func configure(with model: CartCouponCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.title
        descriptionLabel.text = model.description
        descriptionLabel.isHidden = model.description?.isEmpty != false
        amountLabel.text = model.amount
        amountLabel.isHidden = model.amount?.isEmpty != false
        percentLabel.text = model.percent
        percentLabel.isHidden = model.percent?.isEmpty != false
        amountStackView.isHidden = model.amount?.isEmpty != false && model.percent?.isEmpty != false

        noteLabel.text = model.note
        noteView.isHidden = model.note?.isEmpty != false
    }

    @IBAction private func cellTapped() {
        model.didSelect()
    }
}
