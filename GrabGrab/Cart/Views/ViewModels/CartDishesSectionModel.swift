//
//  CartDishesSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartDishesSectionModel: TableViewSectionModel {

    private let models: [CartItemCellModel]

    init(
        items: [StoredCartItem]?,
        plus: @escaping (StoredCartItem) -> Int,
        minus: @escaping (StoredCartItem) -> Int,
        remove: @escaping (StoredCartItem) -> ()
    ) {
        guard let items = items else {
            models = []
            return
        }

        models = items.map { item in
            CartItemCellModel(
                cartItem: item,
                plus: { plus(item) },
                minus: { minus(item) },
                remove: { remove(item) }
            )
        }
    }

    var numberOfRows: Int {
        if models.isEmpty {
            return 2
        }
        return models.count + 1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartItemCell.self)
        tableView.register(cellType: CartUnavailableItemCell.self)
        tableView.register(cellType: CartHeaderCell.self)
        tableView.register(cellType: EmptyCartCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        guard indexPath.row > 0 else {
            let cell: CartHeaderCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(title: LocalizedStrings.Cart.Dishes.label)
            return cell
        }

        guard !models.isEmpty else {
            let cell: EmptyCartCell = tableView.dequeue(forIndexPath: indexPath)
            return cell
        }

        let model = models[indexPath.row - 1]

        guard model.isAvailable else {
            let cell: CartUnavailableItemCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(model: model)
            return cell
        }

        let cell: CartItemCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }

    func allowEditing(at indexPath: IndexPath) -> Bool {
        !models.isEmpty && indexPath.row != 0
    }

    func commit(editingStyle: UITableViewCell.EditingStyle, at indexPath: IndexPath) {
        guard !models.isEmpty && indexPath.row != 0 else { return }
        if case .delete = editingStyle {
            models[indexPath.row - 1].remove()
        }
    }
}
