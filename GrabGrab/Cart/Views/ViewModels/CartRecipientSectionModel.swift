//
//  CartRecipientSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct CartRecipientSectionModel: TableViewSectionModel {

    var model: CartRecipientCellModel

    init(recipient: (name: String, phone: String)?, didSelect: @escaping () -> ()) {
        model = CartRecipientCellModel(recipient: recipient, didSelect: didSelect)
    }

    var numberOfRows: Int {
        1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartRecipientCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: CartRecipientCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model)
        return cell
    }
}
