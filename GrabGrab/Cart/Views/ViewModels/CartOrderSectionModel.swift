//
//  CartOrderSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartOrderSectionModel: TableViewSectionModel {

    var model: CartOrderCellModel

    init(isEmpty: Bool, price: Double, isEnabled: Bool, storefrontState: Storefront.State, didClick: @escaping () -> ()) {
        model = CartOrderCellModel(
            price: price,
            isEnabled: isEnabled,
            isEmpty: isEmpty,
            storefrontState: storefrontState,
            orderClick: didClick
        )
    }

    var numberOfRows: Int {
       1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartOrderCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        let cell: CartOrderCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
