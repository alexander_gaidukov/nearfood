//
//  CartPaymentMethodSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartPaymentMethodSectionModel: TableViewSectionModel {

    var model: CartPaymentMethodCellModel

    init(paymentMethod: PaymentMethod?, didSelect: @escaping () -> ()) {
        model = CartPaymentMethodCellModel(paymentMethod: paymentMethod, didSelect: didSelect)
    }

    var numberOfRows: Int {
       2
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartPaymentMethodCell.self)
        tableView.register(cellType: CartHeaderCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {

        guard indexPath.row > 0 else {
            let cell: CartHeaderCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(title: LocalizedStrings.Cart.PaymentMethod.label)
            return cell
        }

        let cell: CartPaymentMethodCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
