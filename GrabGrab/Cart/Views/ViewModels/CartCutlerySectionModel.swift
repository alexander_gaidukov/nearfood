//
//  CartCutlerySectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartCutlerySectionModel: TableViewSectionModel {

    var model: CartCutleryCellModel
    private let isEmpty: Bool

    init(isEmpty: Bool, count: Int, plus: @escaping () -> Int, minus: @escaping () -> Int) {
        self.isEmpty = isEmpty
        model = CartCutleryCellModel(count: count, plus: plus, minus: minus)
    }

    var numberOfRows: Int {
        isEmpty ? 0 : 1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartCutleryCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: CartCutleryCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
