//
//  CartAddressSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartAddressSectionModel: TableViewSectionModel {

    private let model: CartAddressCellModel

    init(location: Location?, changeAddress: @escaping () -> ()) {
        model = CartAddressCellModel(location: location, changeAddress: changeAddress)
    }

    var numberOfRows: Int {
        2
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartAddressCell.self)
        tableView.register(cellType: CartHeaderCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: CartHeaderCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(title: LocalizedStrings.Cart.Address.label)
            return cell
        }
        let cell: CartAddressCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
