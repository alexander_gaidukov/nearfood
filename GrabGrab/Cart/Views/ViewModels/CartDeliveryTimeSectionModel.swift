//
//  CartDeliveryTimeSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartDeliveryTimeSectionModel: TableViewSectionModel {

    var model: CartDeliveryTimeCellModel

    init(didSelect: @escaping () -> ()) {
        model = CartDeliveryTimeCellModel(didSelect: didSelect)
    }

    var numberOfRows: Int {
        1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartDeliveryTimeCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: CartDeliveryTimeCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model)
        return cell
    }
}
