//
//  CartDeliverySectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartDeliverySectionModel: TableViewSectionModel {

    var model: CartDeliveryCellModel

    init(isOn: Bool, didSwitch: @escaping (Bool) -> (), didSelect: @escaping () -> ()) {
        model = CartDeliveryCellModel(isOn: isOn, didSwitch: didSwitch, didSelect: didSelect)
    }

    var numberOfRows: Int {
       1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartDeliveryCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: CartDeliveryCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
