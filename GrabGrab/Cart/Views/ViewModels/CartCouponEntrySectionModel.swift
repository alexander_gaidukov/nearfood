//
//  CartCouponEntrySectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct CartCouponEntrySectionModel: TableViewSectionModel {

    private let model: CartCouponEntryCellModel
    private let isPresented: Bool

    init(
        isPresented: Bool,
        entry: String?,
        errorMessage: String?,
        didChange: @escaping (String?) -> (),
        apply: @escaping () -> ()
    ) {
        self.isPresented = isPresented
        model = CartCouponEntryCellModel(
            entry: entry,
            errorMessage: errorMessage,
            didChange: didChange,
            apply: apply
        )
    }

    var numberOfRows: Int {
        isPresented ? 1 : 0
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartCouponEntryCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: CartCouponEntryCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(with: model)
        return cell
    }
}
