//
//  CartCommentSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartNoteSectionModel: TableViewSectionModel {

    var model: CartNoteCellModel

    init(note: String, change: @escaping () -> ()) {
        model = CartNoteCellModel(note: note, change: change)
    }

    var numberOfRows: Int {
       1
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartNoteCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell: CartNoteCell = tableView.dequeue(forIndexPath: indexPath)
        cell.configure(model: model)
        return cell
    }
}
