//
//  CartChargesSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartChargesSectionModel: TableViewSectionModel {

    private enum ModelType {
        case coupon(CartCouponCellModel)
        case other(CartChargeCellModel)
    }

    private let models: [ModelType]

    init(
        charges: [EstimationCharge],
        didSelectSurge: @escaping () -> (),
        didSelectCoupon: @escaping (Coupon) -> (),
        didRemoveCoupon: @escaping (Coupon) -> ()
    ) {
        models = charges.compactMap {
            switch $0.reason {
            case .dish, .combo:
                return nil
            case .coupon(let metadata):
                return .coupon(CartCouponCellModel(
                    amount: $0.amount,
                    coupon: metadata.coupon,
                    note: metadata.note,
                    didSelect: { didSelectCoupon(metadata.coupon) },
                    didRemove: { didRemoveCoupon(metadata.coupon) }
                ))
            case .surge(let metadata):
                return .other(CartChargeCellModel(
                    reason: EstimationCharge.Reason.surgeReason,
                    amount: $0.amount,
                    metadata: metadata,
                    detailsTitle: LocalizedStrings.Cart.Surge.info,
                    didSelect: didSelectSurge
                ))
            case .bonuses(let metadata):
                return .other(CartChargeCellModel(
                    reason: EstimationCharge.Reason.bonusesReason,
                    amount: $0.amount,
                    metadata: metadata
                ))
            case .area(let metadata):
                return .other(CartChargeCellModel(
                    reason: EstimationCharge.Reason.areaReason,
                    amount: $0.amount,
                    metadata: metadata)
                )
            case .unknown(let reason, let metadata):
                return .other(CartChargeCellModel(
                    reason: reason,
                    amount: $0.amount,
                    metadata: metadata)
                )
            }
        }
    }

    var numberOfRows: Int {
        models.count
    }

    func registerCells(in tableView: UITableView) {
        tableView.register(cellType: CartChargeCell.self)
        tableView.register(cellType: CartCouponCell.self)
    }

    func cell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let modelType = models.get(at: indexPath.row) else { return UITableViewCell() }

        switch modelType {
        case .coupon(let model):
            let cell: CartCouponCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(with: model)
            return cell
        case .other(let model):
            let cell: CartChargeCell = tableView.dequeue(forIndexPath: indexPath)
            cell.configure(model: model)
            return cell
        }
    }

    func allowEditing(at indexPath: IndexPath) -> Bool {
        guard let modelType = models.get(at: indexPath.row) else { return false }
        if case .coupon = modelType { return true }
        return false
    }

    func commit(editingStyle: UITableViewCell.EditingStyle, at indexPath: IndexPath) {
        guard let modelType = models.get(at: indexPath.row) else { return }
        guard case let .coupon(model) = modelType else { return }
        if case .delete = editingStyle {
            model.didRemove()
        }
    }
}
