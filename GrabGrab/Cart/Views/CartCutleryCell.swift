//
//  CartCutleryCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartCutleryCellModel {
    var count: Int
    var plus: () -> Int
    var minus: () -> Int

    var isEmpty: Bool { count == 0 }
}

class CartCutleryCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var countLabel: UILabel!
    @IBOutlet private weak var minusButton: UIButton!

    private var model: CartCutleryCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Cart.cutleryTitle
    }

    func configure(model: CartCutleryCellModel) {
        self.model = model
    }

    private func updateUI() {
        countLabel.text = "\(model.count)"
        minusButton.isHidden = model.isEmpty
    }

    @IBAction private func plusTapped() {
        model.count = model.plus()
    }

    @IBAction private func minusTapped() {
        model.count = model.minus()
    }
}
