//
//  CartPaymentMethodCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartPaymentMethodCellModel {
    var hasPaymentMethod: Bool
    var image: UIImage?
    var title: String
    var isConfirmed: Bool
    var didSelect: () -> ()
}

extension CartPaymentMethodCellModel {
    init(paymentMethod: PaymentMethod?, didSelect: @escaping () -> ()) {
        self.didSelect = didSelect
        hasPaymentMethod = paymentMethod != nil
        image = paymentMethod?.image
        title = paymentMethod?.title ?? ""
        isConfirmed = paymentMethod?.isEnabled == true
    }
}

class CartPaymentMethodCell: UITableViewCell {
    @IBOutlet private weak var addPaymentMethodLabel: UILabel!
    @IBOutlet private weak var changePaymentMethodLabel: UILabel!
    @IBOutlet private weak var paymentMethodView: UIView!
    @IBOutlet private weak var paymentMethodLabel: UILabel!
    @IBOutlet private weak var unconfirmedView: UIView!
    @IBOutlet private weak var unconfirmedLabel: UILabel!
    @IBOutlet private weak var cardImageView: UIImageView!
    @IBOutlet private weak var paymentButton: UIButton!

    private var model: CartPaymentMethodCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        addPaymentMethodLabel.text = LocalizedStrings.Cart.PaymentMethod.addNew
        unconfirmedLabel.text = LocalizedStrings.PaymentMethod.notConfirmed
        changePaymentMethodLabel.text = LocalizedStrings.Common.change
        paymentButton.accessibilityIdentifier = Identifiers.Cart.PaymentMethod.changeButton
    }

    func configure(model: CartPaymentMethodCellModel) {
        self.model = model
    }

    private func updateUI() {
        if model.hasPaymentMethod {
            addPaymentMethodLabel.isHidden = true
            paymentMethodView.isHidden = false
            paymentMethodLabel.text = model.title
            unconfirmedView.isHidden = model.isConfirmed
            cardImageView.image = model.image
        } else {
            addPaymentMethodLabel.isHidden = false
            paymentMethodView.isHidden = true
        }
    }

    @IBAction private func cellTapped() {
        model.didSelect()
    }
}
