//
//  CartCouponEntryCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartCouponEntryCellModel {
    var entry: String?
    var errorMessage: String?
    var didChange: (String?) -> ()
    var apply: () -> ()
}

final class CartCouponEntryCell: UITableViewCell {
    @IBOutlet private var textField: GGTextField!
    @IBOutlet private var applyButton: MainButton!

    private var model: CartCouponEntryCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        textField.placeholder = LocalizedStrings.Cart.Coupons.placeholder
        applyButton.setTitle(LocalizedStrings.Common.apply, for: .normal)
    }

    func configure(with model: CartCouponEntryCellModel) {
        self.model = model
    }

    private func updateUI() {
        textField.text = model.entry
        textField.errorMessage = model.errorMessage
        applyButton.isEnabled = model.entry?.isEmpty == false
    }

    @IBAction private func applyButtonTapped() {
        model.apply()
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        applyButton.isEnabled = textField.text?.isEmpty == false
        model.didChange(textField.text)
    }
}

extension CartCouponEntryCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
