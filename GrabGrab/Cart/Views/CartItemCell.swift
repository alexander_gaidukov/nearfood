//
//  CartItemCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartItemCellModel {
    var photoUrls: [String]
    var name: String
    var count: Int
    var quantity: String?
    var price: String

    var isAvailable: Bool
    var isEmpty: Bool {
        count == 0
    }

    var plus: () -> Int
    var minus: () -> Int
    var remove: () -> ()
}

extension CartItemCellModel {
    init(cartItem: StoredCartItem, plus: @escaping () -> Int, minus: @escaping () -> Int, remove: @escaping () -> ()) {
        self.plus = plus
        self.minus = minus
        self.remove = remove
        switch cartItem {
        case .combo(let combo):
            photoUrls = combo.photoURLs
            name = combo.name
            count = max(combo.count, 0)
            price = combo.price.string(with: Formatters.priceFormatter) ?? ""
            isAvailable = combo.isAvailable
            quantity = nil
        case .dish(let dish):
            photoUrls = dish.photoUrl.map { [$0] } ?? []
            name = dish.name
            count = max(dish.count, 0)
            price = dish.price.string(with: Formatters.priceFormatter) ?? ""
            isAvailable = dish.isAvailable
            quantity = "\(dish.quantity) \(dish.unit.shortName)"
        }
    }
}

final class CartItemCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var countLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var minusButton: UIButton!
    @IBOutlet private weak var photosContainerView: UIView!
    @IBOutlet private weak var quantityLabel: UILabel!

    private let photosView = ComboPhotosView.fromNib()

    private var model: CartItemCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        photosView.overlapping = 10
        photosContainerView.fill(with: photosView)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        photosView.prepareForReuse()
    }

    func configure(model: CartItemCellModel) {
        self.model = model
    }

    private func updateUI() {
        photosView.grayscaled = model.isEmpty
        photosView.configure(with: model.photoUrls)
        nameLabel.textColor = model.isEmpty ? .labelText : .mainText
        nameLabel.text = model.name
        countLabel.text = "\(model.count)"
        quantityLabel.text = model.quantity
        priceLabel.text = model.price
        minusButton.isHidden = model.isEmpty
    }

    @IBAction private func plusTapped() {
        model.count = model.plus()
    }

    @IBAction private func minusTapped() {
        model.count = model.minus()
    }

}
