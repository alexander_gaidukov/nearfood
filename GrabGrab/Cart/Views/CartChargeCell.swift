//
//  CartUnknownChargeCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartChargeCellModel {
    let title: String
    let amount: String?
    let note: String?
    let detailsTitle: String?
    let icon: UIImage?
    let didSelect: () -> ()
}

extension CartChargeCellModel {
    init(
        reason: String,
        amount: Double,
        metadata: CartChargeMetadata,
        detailsTitle: String? = nil,
        didSelect: @escaping () -> () = {}
    ) {
        self.title = metadata.title
        self.note = metadata.note
        self.amount = .priceAndDelay(
            from: amount,
            delay: metadata.delay,
            pricePlusPreposition: false
        )
        self.icon = UIImage(named: metadata.category ?? reason)
        self.detailsTitle = detailsTitle
        self.didSelect = didSelect
    }
}

final class CartChargeCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!

    @IBOutlet private weak var noteView: UIView!
    @IBOutlet private weak var noteLabel: UILabel!
    @IBOutlet private weak var detailsLabel: UILabel!

    private var model: CartChargeCellModel! {
        didSet {
            updateUI()
        }
    }

    func configure(model: CartChargeCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.title
        amountLabel.text = model.amount
        iconImageView.image = model.icon
        noteLabel.text = model.note
        noteView.isHidden = model.note?.isEmpty != false
        detailsLabel.text = model.detailsTitle
    }

    @IBAction private func didTap() {
        model.didSelect()
    }
}
