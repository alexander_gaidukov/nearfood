//
//  CartAddressCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartAddressCellModel {
    var location: Location?
    var changeAddress: () -> ()

    var address: String? {
        location?.deliveryAddress
    }

    var note: String? {
        location?.note
    }

    var isNoteHidden: Bool {
       location?.note?.isEmpty != false
    }
}

class CartAddressCell: UITableViewCell {

    @IBOutlet private weak var changeAddressLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var noteLabel: UILabel!

    private var model: CartAddressCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        changeAddressLabel.text = LocalizedStrings.Common.change
    }

    func configure(model: CartAddressCellModel) {
        self.model = model
    }

    private func updateUI() {
        addressLabel.text = model.address
        noteLabel.text = model.note
        noteLabel.isHidden = model.isNoteHidden
    }

    @IBAction private func changeAddressTapped() {
        model.changeAddress()
    }
}
