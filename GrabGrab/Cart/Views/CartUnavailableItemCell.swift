//
//  CartUnavailableItemCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 17.04.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CartUnavailableItemCell: UITableViewCell {
    @IBOutlet private weak var stopListLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var photosContainerView: UIView!

    private let photosView = ComboPhotosView.fromNib()

    private var model: CartItemCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        photosView.overlapping = 10
        photosView.grayscaled = true
        photosContainerView.fill(with: photosView)
        stopListLabel.text = LocalizedStrings.Cart.Dishes.stopList
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        photosView.prepareForReuse()
    }

    func configure(model: CartItemCellModel) {
        self.model = model
    }

    private func updateUI() {
        photosView.configure(with: model.photoUrls)
        nameLabel.text = model.name
    }
}
