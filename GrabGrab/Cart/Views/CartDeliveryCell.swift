//
//  CartDeliveryCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 13.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct CartDeliveryCellModel {
    var isOn: Bool
    var didSwitch: (Bool) -> ()
    var didSelect: () -> ()
}

class CartDeliveryCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var switchView: UISwitch!

    private var model: CartDeliveryCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.Cart.deliveryTitle
    }

    func configure(model: CartDeliveryCellModel) {
        self.model = model
    }

    private func updateUI() {
        switchView.isOn = model.isOn
    }

    @IBAction private func switchDidChange() {
        let isOn = switchView.isOn
        model.didSwitch(isOn)
    }

    @IBAction private func infoSelected() {
        model.didSelect()
    }
}
