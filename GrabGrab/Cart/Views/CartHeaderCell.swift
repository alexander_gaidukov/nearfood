//
//  CartHeaderCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CartHeaderCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!

    func configure(title: String) {
        titleLabel.text = title
    }
}
