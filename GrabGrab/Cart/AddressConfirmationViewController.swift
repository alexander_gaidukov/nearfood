//
//  AddressConfirmationViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class AddressConfirmationViewController: UIViewController, PopupViewController {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var confirmButton: UIButton!
    @IBOutlet private weak var declineButton: UIButton!

    private let disposableBag = DisposableBag()

    var presenter: AddressConfirmationPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        titleLabel.text = LocalizedStrings.Cart.AddressVerification.title
        descriptionLabel.text = presenter.state.description
        confirmButton.setTitle(LocalizedStrings.Cart.AddressVerification.confirm, for: .normal)
        declineButton.setTitle(LocalizedStrings.Cart.AddressVerification.decline, for: .normal)
    }

    func close() {}

    @IBAction private func confirmTapped() {
        presenter.confirm()
    }

    @IBAction private func declineTapped() {
        presenter.decline()
    }

}
