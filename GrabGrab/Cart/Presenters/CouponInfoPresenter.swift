//
//  CouponInfoPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CouponInfoPresenterType {
    var state: CouponInfoViewState { get }

    func close()
}

enum CouponInfoAction {
    case close
}

struct CouponInfoViewState {
    let title: String
    let code: String
    let description: String
    let maxAmount: String?
    let showCashbackInfo: Bool
}

extension CouponInfoViewState {
    init(coupon: Coupon) {
        title = coupon.name
        code = coupon.code
        description = coupon.description
        switch coupon.kind {
        case .discountFixed:
            maxAmount = nil
            showCashbackInfo = false
        case .discountPercent:
            maxAmount = coupon.props.maxAmount?.string(with: Formatters.priceFormatter).map {
                LocalizedStrings.Cart.Coupons.maxDiscount($0)
            }
            showCashbackInfo = false
        case .cashbackFixed:
            maxAmount = nil
            showCashbackInfo = true
        case .cashbackPercent:
            maxAmount = coupon.props.maxAmount?.string(with: Formatters.priceFormatter).map {
                LocalizedStrings.Cart.Coupons.maxCashback($0)
            }
            showCashbackInfo = true
        }
    }
}

final class CouponInfoPresenter: CouponInfoPresenterType {
    private let completion: (CouponInfoAction) -> ()

    private(set) var state: CouponInfoViewState

    init(coupon: Coupon, completion: @escaping (CouponInfoAction) -> ()) {
        self.completion = completion

        self.state = CouponInfoViewState(coupon: coupon)
    }

    func close() {
        completion(.close)
    }
}
