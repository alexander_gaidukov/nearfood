//
//  CartDeliveryTimeInfoPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CartDeliveryTimeInfoPresenterType {
    func close()
}

enum CartDeliveryTimeAction {
    case complete
}

final class CartDeliveryTimeInfoPresenter: CartDeliveryTimeInfoPresenterType {
    private let completion: (CartDeliveryTimeAction) -> ()

    init(completion: @escaping (CartDeliveryTimeAction) -> ()) {
        self.completion = completion
    }

    func close() {
        completion(.complete)
    }
}
