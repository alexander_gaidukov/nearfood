//
//  InvalidCartPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol InvalidCartPresenterType {
    var photos: [[String]] { get }
    var description: String { get }
    var priceTitle: NSAttributedString { get }
    func order()
    func cancel()
}

enum InvalidCartAction {
    case order
    case cancel
}

final class InvalidCartPresenter: InvalidCartPresenterType {
    private let completion: (InvalidCartAction) -> ()

    let photos: [[String]]
    let description: String
    let priceTitle: NSAttributedString

    init(cartDataProvider: CartDataProviderType, completion: @escaping (InvalidCartAction) -> ()) {
        self.completion = completion

        let unavailableDishes = cartDataProvider.cart.unavailableDishes
        let unavailableCombos = cartDataProvider.cart.unavailableCombos

        let isEmpty = unavailableCombos.isEmpty && unavailableDishes.isEmpty

        photos = unavailableCombos.map(\.photoURLs)
            + unavailableDishes.compactMap { $0.photoUrl.map { [$0] } }

        description = isEmpty ? LocalizedStrings.Cart.Invalid.newPriceDescription
            : LocalizedStrings.Cart.Invalid.description

        let priceTitle = NSMutableAttributedString(
            string: LocalizedStrings.Cart.Invalid.priceTitle,
            attributes: [.font: UIFont.preferredFont(forTextStyle: .subheadline), .foregroundColor: UIColor.mainText]
        )

        let price = cartDataProvider.cart.totalPrice
        let priceString = price == 0 ?
            LocalizedStrings.Common.free :
            (price.string(with: Formatters.priceFormatter) ?? "")

        priceTitle.append(
            NSAttributedString(
                string: " " + priceString,
                attributes: [
                    .font: UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .subheadline).pointSize),
                    .foregroundColor: UIColor.mainText
                ]
            )
        )
        self.priceTitle = priceTitle
    }

    func order() {
        completion(.order)
    }

    func cancel() {
        completion(.cancel)
    }
}
