//
//  PaymentProcessingPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol PaymentProcessingPresenterType {
    var state: PaymentProcessingViewState { get }
    func viewDidAppear()
    func close()
    func changePaymentMethod()
    func repeatPayment()
}

enum PaymentProcessingAction {
    case completed(Order?)
    case changePaymentMethod((PaymentMethod?, WebError?) -> ())
    case applePay(Order, (String?) -> ())
}

struct PaymentProcessingViewState {

    enum ViewKind {
        case loading
        case unpaid
    }

    @Observable
    var kind: ViewKind

    @Observable
    var error: WebError?

    @Observable
    var paymentMethod: PaymentMethod?

    @Observable
    fileprivate var isTimerOver: Bool

    var isCloseButtonHidden: Observable<Bool> {
        $kind.combine(with: $isTimerOver) { $0 == .loading && !$1 }
    }

    init(order: Order) {
        switch order.status {
        case .unpaid:
            kind = .unpaid
        default:
            kind = .loading
        }
        error = nil
        paymentMethod = order.paymentMethod
        isTimerOver = false
    }
}

final class PaymentProcessingPresenter: PaymentProcessingPresenterType {
    private var order: Order {
        didSet {
            updateState()
        }
    }

    private(set) var state: PaymentProcessingViewState
    private let webClient: WebClientType
    private let ordersDataProvider: OrdersDataProviderType
    private let completion: (PaymentProcessingAction) -> ()

    private var token: String?

    init(
        order: Order,
        ordersDataProvider: OrdersDataProviderType,
        webClient: WebClientType,
        completion: @escaping (PaymentProcessingAction) -> ()
    ) {
        self.order = order
        self.webClient = webClient
        self.ordersDataProvider = ordersDataProvider
        self.completion = completion
        state = PaymentProcessingViewState(order: order)
    }

    func viewDidAppear() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(shouldUpdateOrder(_:)),
            name: .shouldUpdateOrdersNotification,
            object: nil
        )
        scheduleOrderUpdate()
        startCloseButtonTimer()
    }

    func changePaymentMethod() {
        completion(.changePaymentMethod({ [weak self] method, error in
            if let paymentMethod = method {
                self?.state.paymentMethod = paymentMethod
            }
            self?.state.error = error
        }))
    }

    func repeatPayment() {
        guard let paymentMethod = state.paymentMethod else { return }
        updateOrder(paymentMethod: paymentMethod)
    }

    func close() {
        let resource = ResourceBuilder.cancelOrderResource(order: order)
        webClient.load(resource: resource) { _ in }
        completion(.completed(nil))
    }

    private func startCloseButtonTimer() {
        state.isTimerOver = false
        let token = UUID().uuidString
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.Intervals.paymentProcessingLoadingInterval) { [weak self] in
            if self?.token == token {
                self?.state.isTimerOver = true
            }
        }
        self.token = token
    }

    private func updateState() {
        switch order.status {
        case .pending:
            if state.kind == .unpaid { startCloseButtonTimer() }
            state.kind = .loading
        case .unpaid:
            state.kind = .unpaid
        default:
            DispatchQueue.main.async {
                self.completion(.completed(self.order))
            }
        }
    }

    private func scheduleOrderUpdate() {
        loadOrderDetails()
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.Intervals.paymentProcessingUpdateInterval) {[weak self] in
            guard let self = self, self.order.status == .pending else { return }
            self.scheduleOrderUpdate()
        }
    }

    private func loadOrderDetails() {
        self.ordersDataProvider.orderDetails(for: order.uuid) { [weak self] order in
            guard let order = order else { return }
            self?.order = order
        }
    }

    private func updateOrder(paymentMethod: PaymentMethod) {

        let order = self.order

        if case let .applePay(token) = paymentMethod, token == nil {
            DispatchQueue.main.async {
                self.completion(.applePay(order, { [weak self] token in
                    guard let token = token else { return }
                    self?.updateOrder(paymentMethod: .applePay(token))
                }))
            }
            return
        }

        state.kind = .loading

        let resource = ResourceBuilder.updateOrderResource(order: order, paymentMethod: paymentMethod)

        webClient.load(resource: resource) { [weak self] result in
            switch result {
            case .success(let order):
                self?.order = order
                self?.scheduleOrderUpdate()
            case .failure(let error):
                self?.state.kind = .unpaid
                self?.state.error = error
            }
        }
    }

    @objc private func shouldUpdateOrder(_ notification: Notification) {
        guard let uuid = notification.userInfo?["uuid"] as? String, uuid == order.uuid else { return }
        loadOrderDetails()
    }
}
