//
//  CartPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import CoreLocation

protocol CartPresenterType {
    var state: CartViewState { get }
    var sectionModels: Observable<[TableViewSectionModel]> { get }
    func close(isDismissed: Bool)
}

enum CartAction {
    case close(Bool)
    case changeAddress
    case contactlessInfo
    case showInvalid((Bool) -> ())
    case showAddressConfirmation(Location, (Bool) -> ())
    case changeComment(String, (String?) -> ())
    case changePaymentMethod((PaymentMethod?, WebError?) -> ())
    case completed(Order)
    case surgeInfo
    case couponInfo(Coupon)
    case surgeConfirmation((Bool) -> ())
    case applePay((String?) -> ())
    case deliveryTime
    case addOrUpdateRecipient(Recipient?, (Recipient?) -> ())
}

struct CartState {
    private var cart: Cart?

    var isEmpty: Bool {
        cart?.isEmpty != false
    }

    var totalPrice: Double {
        cart?.totalPrice ?? 0
    }

    var location: Location?

    var recipient: Recipient? {
        cart?.recipient
    }

    var note: String {
        cart?.note ?? ""
    }

    var coupon: Coupon? {
        cart?.coupon
    }

    var charges: [EstimationCharge] {
        cart?.charges ?? []
    }

    var items: [StoredCartItem]? {
        let dishes: [StoredCartItem] = cart?.dishes.map { .dish($0) } ?? []
        let combos: [StoredCartItem] = cart?.combos.map { .combo($0) } ?? []
        return combos + dishes
    }

    var paymentMethod: PaymentMethod?

    var isOrderable: Bool {
        cart?.isOrderable == true
    }

    init(cart: Cart?, location: Location?, paymentMethod: PaymentMethod?) {
        self.cart = cart
        self.location = location
        self.paymentMethod = paymentMethod
    }

    mutating func update(with cart: Cart?) {
        self.cart = cart
    }
}

struct CartViewState {
    enum CouponApplyingState {
        case none
        case adding
        case added(String)
        case removing(String)
        case removed(String)
    }

    @Observable
    var eta: Int?

    @Observable
    var cartState: CartState

    @Observable
    var storefrontState: Storefront.State

    @Observable
    var isLoading: Bool = false

    @Observable
    var error: WebError?

    @Observable
    var couponApplyingState: CouponApplyingState = .none

    fileprivate var couponDraft: String?

    @Observable
    fileprivate var couponErrorMessage: String?

    var title: Observable<String?> {
        $storefrontState.combine(with: $eta).map {
            switch $0.0 {
            case .closed:
                return LocalizedStrings.Menu.Closed.title
            default:
                return $0.1.map { LocalizedStrings.Cart.title(for: $0) }
            }
        }
    }

    var isOrderButtonEnabled: Bool {
        guard cartState.isOrderable, let paymentMethod = cartState.paymentMethod, paymentMethod.isEnabled else { return false }
        return storefrontState != .closed
    }
}

// swiftlint:disable:next type_body_length
final class CartPresenter: CartPresenterType {
    private let completion: (CartAction) -> ()
    private let cartDataProvider: CartDataProviderType
    private let customerDataProvider: CustomerDataProviderType
    private let phoneCodesManager: PhoneCodesManagerType
    private let locationManager: LocationManagerType
    private let webClient: WebClientType

    private var invalidItemsConfirmed = false
    private var surgeConfirmed = false
    private var isAddressConfirmed = false

    private(set) var sectionModels = Observable<[TableViewSectionModel]>(wrappedValue: [])
    private let disposableBag = DisposableBag()

    private(set) var state: CartViewState

    init(
        cartDataProvider: CartDataProviderType,
        customerDataProvider: CustomerDataProviderType,
        locationManager: LocationManagerType,
        phoneCodesManager: PhoneCodesManagerType,
        webClient: WebClientType,
        completion: @escaping (CartAction) -> ()
    ) {
        self.cartDataProvider = cartDataProvider
        self.customerDataProvider = customerDataProvider
        self.locationManager = locationManager
        self.phoneCodesManager = phoneCodesManager
        self.webClient = webClient
        self.completion = completion

        let cartState = CartState(
            cart: cartDataProvider.cart,
            location: customerDataProvider.currentLocation,
            paymentMethod: customerDataProvider.paymentMethod
        )

        self.state = CartViewState(eta: cartDataProvider.cart.eta,
                                   cartState: cartState,
                                   storefrontState: cartDataProvider.storefrontState)
        sectionModels.wrappedValue = configureSections()
        state.$couponErrorMessage.combine(with: state.$cartState).combine(with: state.$storefrontState).observe { [weak self] _, _ in
            self?.sectionModels.wrappedValue = self?.configureSections() ?? []
        }.addToDisposableBag(disposableBag)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(cartDidChange),
            name: .cartDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(locationDidChange),
            name: .locationDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(paymentMethodDidChange),
            name: .paymentMethodDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(storefrontStateDidChange),
            name: .storefrontStateDidChangeNotification,
            object: nil
        )

        cartDataProvider.checkCartState {_ in}

        locationManager.start()
    }

    deinit {
        locationManager.stop()
    }

    func close(isDismissed: Bool) {
        cartDataProvider.clearEmptyDishes()
        completion(.close(isDismissed))
    }

    func changeAddress() {
        completion(.changeAddress)
    }

    @objc private func locationDidChange() {
        state.cartState.location = customerDataProvider.currentLocation
    }

    @objc private func paymentMethodDidChange() {
        state.cartState.paymentMethod = customerDataProvider.paymentMethod
    }

    @objc private func storefrontStateDidChange() {
        state.storefrontState = cartDataProvider.storefrontState
    }

    @objc private func cartDidChange() {
        var cartState = self.state.cartState
        let cart  = cartDataProvider.cart
        let coupon = cart.coupon

        cartState.update(with: cart)
        state.cartState = cartState
        state.eta = cartDataProvider.cart.eta

        switch state.couponApplyingState {
        case .adding where coupon != nil:
            state.couponApplyingState = .added(coupon!.code) // swiftlint:disable:this force_unwrapping
        case .removing(let code) where  coupon == nil:
            state.couponApplyingState = .removed(code)
        default:
            break
        }
    }

    private func startOrderFlow() {
        guard !state.isLoading else { return }
        invalidItemsConfirmed = false
        surgeConfirmed = false
        confirmAddress { [weak self] confirmed in
            if confirmed {
                self?.state.isLoading = true
                self?.makeOrder()
            } else {
                self?.changeAddress()
            }
        }
    }

    private func confirmAddress(completion: @escaping (Bool) -> ()) {
        guard
            !isAddressConfirmed,
            let userLocation = locationManager.currentLocation,
            let orderLocation = state.cartState.location,
            userLocation.distance(
                from: CLLocation(latitude: orderLocation.latitude, longitude: orderLocation.longitude)
            ) > Constants.Cart.maxAutomaticallyVerifiedDistanceToOrder
        else {
            completion(true)
            return
        }

        self.completion(.showAddressConfirmation(orderLocation, { [weak self] in
            completion($0)
            self?.isAddressConfirmed = true
        }))
    }

    private func makeOrder() {

        let currentTotal = cartDataProvider.cart.totalPrice

        cartDataProvider.checkCartState {[weak self] error in
            guard let self = self else { return }
            guard error == nil else {
                self.state.isLoading = false
                self.state.error = error
                return
            }
            guard self.invalidItemsConfirmed
                    || (!self.cartDataProvider.cart.hasInvalidItems && self.cartDataProvider.cart.totalPrice == currentTotal) else {
                self.state.isLoading = false
                DispatchQueue.main.async {
                    self.showInvalidState()
                }
                return
            }

            guard self.surgeConfirmed || self.cartDataProvider.cart.surge == nil else {
                self.state.isLoading = false
                DispatchQueue.main.async {
                    self.showSurgeConfirmation()
                }
                return
            }

            self.makeOrderRequest()
        }
    }

    private func makeOrderRequest(method: PaymentMethod? = nil) {

        guard let paymentMethod = method ?? customerDataProvider.paymentMethod else { return }

        if case let .applePay(token) = paymentMethod, token == nil {
            state.isLoading = false
            DispatchQueue.main.async {
                self.completion(.applePay({ [weak self] token in
                    guard let token = token else { return }
                    self?.makeOrderRequest(method: .applePay(token))
                }))
            }
            return
        }

        if !state.isLoading { state.isLoading = true }

        let resource = ResourceBuilder.makeOrderResource(
            cartUUID: cartDataProvider.cart.uuid,
            paymentMethod: paymentMethod,
            cutlery: max(cartDataProvider.cart.cutlery, 0),
            contactless: cartDataProvider.cart.leaveAtDoor,
            recipient: cartDataProvider.cart.recipient,
            note: cartDataProvider.cart.note
        )

        webClient.load(resource: resource) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let order):
                DispatchQueue.main.async {
                    self?.completion(.completed(order))
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    private func showInvalidState() {
        completion(.showInvalid({[weak self] confirmed in
            guard confirmed else { return }
            self?.invalidItemsConfirmed = true
            self?.state.isLoading = true
            self?.makeOrder()
        }))
    }

    private func showSurgeConfirmation() {
        completion(.surgeConfirmation({[weak self] confirmed in
            guard confirmed else { return }
            self?.surgeConfirmed = true
            self?.state.isLoading = true
            self?.makeOrder()
        }))
    }

    private func changeComment() {
        completion(.changeComment(state.cartState.note, { [weak self] comment in
            guard let comment = comment else { return }
            self?.cartDataProvider.setNote(comment)
        }))
    }

    private func changeRecipient() {
        self.completion(.addOrUpdateRecipient(state.cartState.recipient) { [weak self] recipient in
            guard let self = self else { return }
            if let recipient = recipient {
                self.cartDataProvider.addRecipient(recipient)
            } else {
                self.cartDataProvider.removeRecipient()
            }
        })
    }

    private func applyCoupon() {
        guard
            let customer = customerDataProvider.customer,
            let code = state.couponDraft, !code.isEmpty
        else { return }
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.validateCoupon(code, customerUUID: customer.uuid)) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.cartDataProvider.updateDraftCoupon(to: code)
                self.state.couponApplyingState = .adding
                self.cartDataProvider.checkCartState { [weak self] error in
                    guard let self = self else { return }
                    self.state.isLoading = false
                    self.state.error = error
                    if error == nil {
                        self.state.couponErrorMessage = nil
                        self.state.couponDraft = nil
                    }
                }
            case .failure(let error):
                self.state.isLoading = false
                self.state.error = error
                let codeError = error.apiError?.code
                self.state.couponErrorMessage = codeError?.localizedDescription
            }
        }
    }

    private func removeCoupon(_ coupon: Coupon) {
        state.isLoading = true
        cartDataProvider.updateDraftCoupon(to: nil)
        state.couponApplyingState = .removing(coupon.code)
        cartDataProvider.checkCartState { [weak self] error in
            guard let self = self else { return }
            self.state.isLoading = false
            self.state.error = error
        }
    }

    private func showCouponInfo(_ coupon: Coupon) {
        completion(.couponInfo(coupon))
    }

    private func couponEntryDidChange(_ entry: String?) {
        state.couponDraft = entry
    }

    // swiftlint:disable:next function_body_length
    private func configureSections() -> [TableViewSectionModel] {

        let addressSection = CartAddressSectionModel(location: state.cartState.location,
                                                     changeAddress: { [weak self] in self?.changeAddress() })
        let deliveryTimeSection = CartDeliveryTimeSectionModel { [weak self] in
            self?.completion(.deliveryTime)
        }

        let recipient = state.cartState.recipient.map {
            (name: $0.name, phone: phoneCodesManager.phoneNumber(from: $0.phone).formattedNumber)
        }

        let recipientSection = CartRecipientSectionModel(recipient: recipient) { [weak self] in
            guard let self = self else { return }
            self.changeRecipient()
        }

        let couponEntrySection = CartCouponEntrySectionModel(
            isPresented: state.cartState.coupon == nil,
            entry: state.couponDraft,
            errorMessage: state.couponErrorMessage,
            didChange: { [weak self] in self?.couponEntryDidChange($0) },
            apply: { [weak self] in self?.applyCoupon() }
        )

        let dishesSection = CartDishesSectionModel(
            items: state.cartState.items,
            plus: { [weak self] item in self?.cartDataProvider.changeCount(of: item, by: 1) ?? 0},
            minus: { [weak self] item in self?.cartDataProvider.changeCount(of: item, by: -1) ?? 0},
            remove: { [weak self] item in self?.cartDataProvider.removeCartItem(item) }
        )

        let cutlerySection = CartCutlerySectionModel(isEmpty: state.cartState.isEmpty,
                                                     count: max(cartDataProvider.cart.cutlery, 0),
                                                     plus: { [weak self] in self?.cartDataProvider.changeCuntleryCount(by: 1) ?? 0 },
                                                     minus: { [weak self] in self?.cartDataProvider.changeCuntleryCount(by: -1) ?? 0 })

        let deliverySection = CartDeliverySectionModel(
            isOn: cartDataProvider.cart.leaveAtDoor,
            didSwitch: { [weak self] in self?.cartDataProvider.leaveAtDoor(isOn: $0) },
            didSelect: { [weak self] in self?.completion(.contactlessInfo) }
        )

        let noteSection = CartNoteSectionModel(note: state.cartState.note) { [weak self] in self?.changeComment() }

        let paymentMethodAction = CartAction.changePaymentMethod { [weak self] _, error in
            self?.state.error = error
        }

        let paymentMethodSection = CartPaymentMethodSectionModel(
            paymentMethod: state.cartState.paymentMethod,
            didSelect: { [weak self] in self?.completion(paymentMethodAction) }
        )

        let orderSection = CartOrderSectionModel(isEmpty: state.cartState.isEmpty,
                                                 price: state.cartState.totalPrice,
                                                 isEnabled: state.isOrderButtonEnabled,
                                                 storefrontState: state.storefrontState,
                                                 didClick: { [weak self] in self?.startOrderFlow() })

        let chargesModel = CartChargesSectionModel(
            charges: state.cartState.charges,
            didSelectSurge: { [weak self] in self?.completion(.surgeInfo) },
            didSelectCoupon: { [weak self] in self?.showCouponInfo($0) },
            didRemoveCoupon: { [weak self] in self?.removeCoupon($0) }
        )

        return [
            addressSection,
            deliveryTimeSection,
            recipientSection,
            couponEntrySection,
            dishesSection,
            chargesModel,
            cutlerySection,
            deliverySection,
            noteSection,
            paymentMethodSection,
            orderSection
        ]
    }
} // swiftlint:disable:this file_length
