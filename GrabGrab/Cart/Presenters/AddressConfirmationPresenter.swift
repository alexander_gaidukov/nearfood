//
//  AddressConfirmationPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AddressConfirmationPresenterType {
    var state: AddressConfirmationViewState { get }
    func confirm()
    func decline()
}

enum AddressConfirmationAction {
    case confirm
    case decline
}

struct AddressConfirmationViewState {
    let description: String
}

final class AddressConfirmationPresenter: AddressConfirmationPresenterType {
    private let completion: (AddressConfirmationAction) -> ()
    private(set) var state: AddressConfirmationViewState

    init(location: Location, completion: @escaping (AddressConfirmationAction) -> ()) {
        self.completion = completion
        self.state = AddressConfirmationViewState(
            description: LocalizedStrings.Cart.AddressVerification.description(for: location.fullAddress)
        )
    }

    func confirm() {
        completion(.confirm)
    }

    func decline() {
        completion(.decline)
    }
}
