//
//  ContactlessInfoPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol ContactlessInfoPresenterType {
    func close()
}

enum ContactlessInfoAction {
    case complete
}

final class ContactlessInfoPresenter: ContactlessInfoPresenterType {
    private let completion: (ContactlessInfoAction) -> ()

    init(completion: @escaping (ContactlessInfoAction) -> ()) {
        self.completion = completion
    }

    func close() {
        completion(.complete)
    }
}
