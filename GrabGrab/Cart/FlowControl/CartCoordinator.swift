//
//  CartCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class CartCoordinator: Coordinator {

    private let router: CartRouterType
    private var completion: (Order?) -> ()
    private var childCoordinator: Coordinator?
    private let dataProvidersStorage: DataProvidersStorageType
    private let managersStorage: ManagersStorageType
    private let webClient: WebClientType

    init(router: CartRouterType,
         dataProvidersStorage: DataProvidersStorageType,
         managersStorage: ManagersStorageType,
         webClient: WebClientType,
         completion: @escaping (Order?) -> ()) {
        self.router = router
        self.completion = completion
        self.dataProvidersStorage = dataProvidersStorage
        self.managersStorage = managersStorage
        self.webClient = webClient
    }

    func start() {
        router.showCart { action in
            switch action {
            case .close(let dismissed):
                if dismissed {
                    self.completion(nil)
                } else {
                    self.router.dismiss {
                        self.completion(nil)
                    }
                }
            case .changeAddress:
                self.showAddressSelection()
            case .changePaymentMethod(let completion):
                self.showPaymentMethodSelection(completion: completion)
            case .showInvalid(let completion):
                self.showInvalidCart(completion: completion)
            case let .showAddressConfirmation(location, completion):
                self.showAddressConfirmation(location, completion: completion)
            case .contactlessInfo:
                self.showContactlessInfo()
            case .surgeInfo:
                self.showSurgeInfo(showButtons: false) { _ in }
            case .surgeConfirmation(let completion):
                self.showSurgeInfo(showButtons: true, completion: completion)
            case .applePay(let completion):
                self.showApplePay(order: nil, completion: completion)
            case let .changeComment(note, completion):
                self.changeComment(note: note, completion: completion)
            case .deliveryTime:
                self.showDeliveryTimeInfo()
            case let .addOrUpdateRecipient(recipient, completion):
                self.addOrUpdateRecipient(recipient, completion: completion)
            case .completed(let order):
                self.processOrder(order)
            case let .couponInfo(coupon):
                self.showCouponInfo(coupon)
            }
        }
    }

    private func showDeliveryTimeInfo() {
        router.showDeliveryTimeInfo { action in
            switch action {
            case .complete:
                self.router.dismiss {}
            }
        }
    }

    private func processOrder(_ order: Order) {
        guard order.status == .pending else {
            finish(with: order)
            return
        }

        router.showPaymentProcessing(order: order) { action in
            switch action {
            case .completed(let order):
                self.router.dismiss {
                    if let order = order {
                        self.finish(with: order)
                    }
                }
            case .changePaymentMethod(let completion):
                self.showPaymentMethodSelection(completion: completion)
            case let .applePay(order, completion):
                self.showApplePay(order: order, completion: completion)
            }
        }
    }

    private func finish(with order: Order) {
        dataProvidersStorage.cartDataProvider.clear()
        dataProvidersStorage.ordersDataProvider.updateActiveOrders(notifyWidget: true)
        router.dismiss {
            self.completion(order)
        }
    }

    private func changeComment(note: String, completion: @escaping (String?) -> ()) {
        guard let controller = router.topViewController else { return }
        if let coordinator = childCoordinator, coordinator is CommentCoordinator { return }
        let factory = CommentViewControllersFactory()
        let router = CommentRouter(factory: factory, presentingViewController: controller)
        let coordinator = CommentCoordinator(note: note, router: router) { [weak self] comment in
            completion(comment)
            self?.childCoordinator = nil
        }

        coordinator.start()
        childCoordinator = coordinator
    }

    private func showApplePay(order: Order?, completion: @escaping (String?) -> ()) {
        if let coordinator = childCoordinator, coordinator is PaymentSystemsCoordinator { return }

        let factory = PaymentSystemsViewControllersFactory()
        let router = PaymentSystemsRouter(presentingViewController: self.router.topViewController, factory: factory)

        let paymentSystemsCoordinator: PaymentSystemsCoordinator
        if let order = order {
            paymentSystemsCoordinator = PaymentSystemsCoordinator(
                router: router,
                paymentSystem: .applePay,
                order: order
            ) { [weak self] token in
                completion(token)
                self?.childCoordinator = nil
            }
        } else {
            paymentSystemsCoordinator = PaymentSystemsCoordinator(
                router: router,
                paymentSystem: .applePay,
                cart: dataProvidersStorage.cartDataProvider.cart
            ) {[weak self] token in
                completion(token)
                self?.childCoordinator = nil
            }
        }

        paymentSystemsCoordinator.start()
        childCoordinator = paymentSystemsCoordinator
    }

    private func showContactlessInfo() {
        router.showContactlessInfo { action in
            switch action {
            case .complete:
                self.router.dismiss {}
            }
        }
    }

    private func showAddressSelection() {
        if let coordinator = childCoordinator, coordinator is AddressesCoordinator { return }

        let factory = AddressesViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage,
            webClient: webClient
        )
        let router = AddressesRouter(presentingViewController: self.router.topViewController, factory: factory)

        let addressesCoordinator = AddressesCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            type: .selection
        ) {[weak self] action in
            switch action {
            case .selected(let location):
                self?.dataProvidersStorage.customerDataProvider.selectAddress(location)
            case .closed:
                break
            }
            self?.childCoordinator = nil
        }
        addressesCoordinator.start()
        childCoordinator = addressesCoordinator
    }

    private func showPaymentMethodSelection(completion: @escaping (PaymentMethod?, WebError?) -> ()) {
        if let coordinator = childCoordinator, coordinator is PaymentMethodsCoordinator { return }
        let factory = PaymentMethodsViewControllersFactory(
            webClient: webClient,
            dataProvidersStorage: dataProvidersStorage
        )

        let router = PaymentMethodsRouter(presentingViewController: self.router.topViewController, factory: factory)
        let paymentMethodsCoordinator = PaymentMethodsCoordinator(
            router: router,
            dataProvidersStorage: dataProvidersStorage,
            webClient: webClient,
            markCurrentPayment: true
        ) {[weak self] method, error in
            completion(method, error)
            self?.childCoordinator = nil
        }
        paymentMethodsCoordinator.start()
        childCoordinator = paymentMethodsCoordinator
    }

    private func showInvalidCart(completion: @escaping (Bool) -> ()) {
        router.showInvalidCart { action in
            self.router.dismiss {
                switch action {
                case .order:
                    completion(true)
                case .cancel:
                    completion(false)
                }
            }
        }
    }

    private func showAddressConfirmation(_ location: Location, completion: @escaping (Bool) -> ()) {
        router.showAddressConfirmation(location: location) { action in
            self.router.dismiss {
                switch action {
                case .confirm:
                    completion(true)
                case .decline:
                    completion(false)
                }
            }
        }
    }

    private func showSurgeInfo(showButtons: Bool, completion: @escaping (Bool) -> ()) {
        if let childCoordinator = self.childCoordinator, childCoordinator is SurgeCoordinator { return }
        guard let surge = dataProvidersStorage.cartDataProvider.cart.surge else { return }
        let factory = SurgeViewControllersFactory()
        let router = SurgeRouter(presentingViewController: self.router.topViewController, factory: factory)

        let coordinator = SurgeCoordinator(surge: surge, showButtons: showButtons, router: router) {[weak self] isConfirmed in
            completion(isConfirmed)
            self?.childCoordinator = nil
        }
        coordinator.start()
        childCoordinator = coordinator
    }

    private func showCouponInfo(_ coupon: Coupon) {
        router.showCouponInfoViewController(coupon: coupon) { action in
            switch action {
            case .close:
                self.router.dismiss {}
            }
        }
    }

    private func addOrUpdateRecipient(_ recipient: Recipient?, completion: @escaping (Recipient?) -> ()) {
        if let coordinator = childCoordinator, coordinator is RecipientsCoordinator { return }

        let factory = RecipientsViewControllersFactory(
            dataProvidersStorage: dataProvidersStorage,
            managersStorage: managersStorage
        )

        let router = RecipientsRouter(
            presentingViewController: self.router.topViewController,
            factory: factory
        )

        let coordinator = RecipientsCoordinator(
            recipient: recipient,
            router: router,
            managersStorage: managersStorage
        ) { [weak self] recipient in
            completion(recipient)
            self?.childCoordinator = nil
        }

        coordinator.start()
        childCoordinator = coordinator
    }
}
