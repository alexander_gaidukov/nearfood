//
//  CartRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol CartRouterType {
    var topViewController: UIViewController? { get }
    func showCart(completion: @escaping (CartAction) -> ())
    func showInvalidCart(completion: @escaping (InvalidCartAction) -> ())
    func showContactlessInfo(completion: @escaping (ContactlessInfoAction) -> ())
    func showDeliveryTimeInfo(completion: @escaping (CartDeliveryTimeAction) -> ())
    func showPaymentProcessing(order: Order, completion: @escaping (PaymentProcessingAction) -> ())
    func showAddressConfirmation(location: Location, completion: @escaping (AddressConfirmationAction) -> ())
    func showCouponInfoViewController(coupon: Coupon, completion: @escaping (CouponInfoAction) -> ())
    func dismiss(completion: @escaping () -> ())
}

final class CartRouter: NSObject, CartRouterType {
    private weak var presentingViewController: UIViewController?
    private let factory: CartViewControllersFactoryType
    init(presentingViewController: UIViewController?, factory: CartViewControllersFactoryType) {
        self.presentingViewController = presentingViewController
        self.factory = factory
        super.init()
    }

    func showCart(completion: @escaping (CartAction) -> ()) {
        let controller = factory.cartViewController(completion: completion)
        let navController = GGNavigationController(rootViewController: controller)
        presentingViewController?.present(navController, animated: true, completion: nil)
    }

    func showInvalidCart(completion: @escaping (InvalidCartAction) -> ()) {
        let controller = factory.invalidCartViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showContactlessInfo(completion: @escaping (ContactlessInfoAction) -> ()) {
        let controller = factory.contactlessInfoViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showDeliveryTimeInfo(completion: @escaping (CartDeliveryTimeAction) -> ()) {
        let controller = factory.deliveryTimeInfoViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showPaymentProcessing(order: Order, completion: @escaping (PaymentProcessingAction) -> ()) {
        let controller = factory.paymentProcessingViewController(order: order, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showAddressConfirmation(location: Location, completion: @escaping (AddressConfirmationAction) -> ()) {
        let controller = factory.addressConfirmationViewController(location: location, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showCouponInfoViewController(coupon: Coupon, completion: @escaping (CouponInfoAction) -> ()) {
        let controller = factory.couponInfoViewController(coupon: coupon, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func dismiss(completion: @escaping () -> ()) {
        topViewController?.dismiss(animated: true, completion: completion)
    }

    var topViewController: UIViewController? {
        guard var topCandidate = presentingViewController else { return nil }
        while let controller = topCandidate.presentedViewController {
            topCandidate = controller
        }
        return topCandidate
    }
}

// MARK: - Transitioning Delegates

extension CartRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
