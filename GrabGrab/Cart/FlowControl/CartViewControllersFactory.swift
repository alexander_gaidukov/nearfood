//
//  CartViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol CartViewControllersFactoryType {
    func cartViewController(completion: @escaping (CartAction) -> ()) -> CartViewController

    func invalidCartViewController(completion: @escaping (InvalidCartAction) -> ()) -> InvalidCartViewController

    func contactlessInfoViewController(completion: @escaping (ContactlessInfoAction) -> ()) -> ContactlessInfoViewController

    func deliveryTimeInfoViewController(completion: @escaping (CartDeliveryTimeAction) -> ()) -> CartDeliveryTimeInfoViewController

    func paymentProcessingViewController(
        order: Order,
        completion: @escaping (PaymentProcessingAction) -> ()
    ) -> PaymentProcessingViewController

    func addressConfirmationViewController(
        location: Location,
        completion: @escaping (AddressConfirmationAction) -> ()
    ) -> AddressConfirmationViewController

    func couponInfoViewController(
        coupon: Coupon,
        completion: @escaping (CouponInfoAction) -> ()
    ) -> CouponInfoViewController
}

struct CartViewControllersFactory: CartViewControllersFactoryType {
    private let storyboard: Storyboard = .cart

    let dataProvidersStorage: DataProvidersStorageType
    let managersStorage: ManagersStorageType
    let webClient: WebClientType

    func cartViewController(completion: @escaping (CartAction) -> ()) -> CartViewController {
        let presenter = CartPresenter(
            cartDataProvider: dataProvidersStorage.cartDataProvider,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            locationManager: managersStorage.locationManager,
            phoneCodesManager: managersStorage.phoneCodesManager,
            webClient: webClient,
            completion: completion
        )
        let controller = CartViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func invalidCartViewController(completion: @escaping (InvalidCartAction) -> ()) -> InvalidCartViewController {
        let presenter = InvalidCartPresenter(
            cartDataProvider: dataProvidersStorage.cartDataProvider,
            completion: completion
        )
        let controller = InvalidCartViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func contactlessInfoViewController(completion: @escaping (ContactlessInfoAction) -> ()) -> ContactlessInfoViewController {
        let presenter = ContactlessInfoPresenter(completion: completion)
        let controller = ContactlessInfoViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func deliveryTimeInfoViewController(completion: @escaping (CartDeliveryTimeAction) -> ()) -> CartDeliveryTimeInfoViewController {
        let presenter  = CartDeliveryTimeInfoPresenter(completion: completion)
        let controller = CartDeliveryTimeInfoViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func paymentProcessingViewController(
        order: Order,
        completion: @escaping (PaymentProcessingAction) -> ()
    ) -> PaymentProcessingViewController {
        let presenter = PaymentProcessingPresenter(
            order: order,
            ordersDataProvider: dataProvidersStorage.ordersDataProvider,
            webClient: webClient,
            completion: completion
        )

        let controller = PaymentProcessingViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func addressConfirmationViewController(
        location: Location,
        completion: @escaping (AddressConfirmationAction) -> ()
    ) -> AddressConfirmationViewController {
        let presenter = AddressConfirmationPresenter(location: location, completion: completion)

        let controller = AddressConfirmationViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func couponInfoViewController(
        coupon: Coupon,
        completion: @escaping (CouponInfoAction) -> ()
    ) -> CouponInfoViewController {
        let presenter = CouponInfoPresenter(coupon: coupon, completion: completion)
        let controller = CouponInfoViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
