//
//  CartDeliveryTimeInfoViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CartDeliveryTimeInfoViewController: UIViewController, PopupViewController {
    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!

    var presenter: CartDeliveryTimeInfoPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        titleLabel.text = LocalizedStrings.Cart.DeliveryTime.title
        let descriptionFontSize = UIFont.preferredFont(forTextStyle: .subheadline).pointSize
        descriptionLabel.attributedText = LocalizedStrings.Cart.DeliveryTime
            .description(fontSize: Double(descriptionFontSize))
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        contentView.addGestureRecognizer(panGestureRecognizer)
    }

    @objc func close() {
        presenter.close()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }
}
