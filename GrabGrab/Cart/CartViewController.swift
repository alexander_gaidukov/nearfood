//
//  CartViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    var presenter: CartPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    private func configureView() {
        presenter.sectionModels.wrappedValue.forEach { $0.registerCells(in: self.tableView) }
        tableView.tableFooterView = UIView(frame: .zero)
        if #available(iOS 13.0, *) {
            navigationController?.presentationController?.delegate = self
        } else {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(closeTapped))
        }
    }

    private func configureBindings() {
        presenter.state.title.bind(to: self, keyPath: \.title).addToDisposableBag(disposableBag)

        presenter.sectionModels.observe { [weak self] _ in self?.tableView.reloadData() }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
                self?.view.endEditing(true)
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$couponApplyingState.observe { [weak self] in
            switch $0 {
            case .added(let code):
                self?.showBanner(withType: .success(LocalizedStrings.Cart.Coupons.applyConfirmation(code)))
            case .removed(let code):
                self?.showBanner(withType: .success(LocalizedStrings.Cart.Coupons.removeConfirmation(code)))
            default:
                break
            }
        }.addToDisposableBag(disposableBag)
    }

    @objc private func closeTapped() {
        view.endEditing(true)
        DispatchQueue.main.async {
            self.presenter.close(isDismissed: false)
        }
    }
}

extension CartViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.sectionModels.wrappedValue.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.sectionModels.wrappedValue[section].numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        presenter.sectionModels.wrappedValue[indexPath.section].cell(tableView: tableView, indexPath: indexPath)
    }
}

extension CartViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if presenter.sectionModels.wrappedValue[indexPath.section].allowEditing(at: indexPath) { return .delete }
        return .none
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        presenter.sectionModels.wrappedValue[indexPath.section].commit(editingStyle: editingStyle, at: indexPath)
    }
}

extension CartViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}

extension CartViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension CartViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        presenter.close(isDismissed: true)
    }
}
