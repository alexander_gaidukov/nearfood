//
//  CouponInfoViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class CouponInfoViewController: UIViewController, PopupViewController {
    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var maxAmountLabel: UILabel!
    @IBOutlet private weak var cashbackLabel: UILabel!
    @IBOutlet private weak var cashbackContainerView: UIView!

    var presenter: CouponInfoPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        cashbackLabel.text = LocalizedStrings.Cart.Coupons.cashbackInfo
        titleLabel.text = presenter.state.title
        codeLabel.text = presenter.state.code
        descriptionLabel.text = presenter.state.description
        maxAmountLabel.text = presenter.state.maxAmount
        maxAmountLabel.isHidden = presenter.state.maxAmount?.isEmpty != false
        cashbackContainerView.isHidden = !presenter.state.showCashbackInfo

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        contentView.addGestureRecognizer(panGestureRecognizer)
    }

    @objc func close() {
        presenter.close()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }
}
