//
//  GGUIMarker.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class GGUIMarkerView: UIView {

    private let label: UILabel

    var value: Int = 0 {
        didSet {
            updateUI()
        }
    }

    override init(frame: CGRect) {

        label = UILabel(frame: .zero)

        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .primary
        clipsToBounds = true
        borderWidth = 2
        borderColor = .tabbarBackground

        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .systemFont(ofSize: 11)
        label.backgroundColor = .clear
        addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])

        updateUI()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                borderColor = .tabbarBackground
            }
        }
    }

    private func updateUI() {
        if value > 9 {
            label.text = "9+"
        } else {
            label.text = "\(value)"
        }
        isHidden = value == 0
    }
}

struct GGUIMarker {
    static func makeLabel(
        withText text: String? = nil,
        font: UIFont = .systemFont(ofSize: 11),
        color: UIColor = .black,
        numberOfLines: Int = 1,
        alignment: NSTextAlignment = .center
    ) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.font = font
        label.textColor = color
        label.numberOfLines = numberOfLines
        label.textAlignment = alignment
        return label
    }

    static func makeImageView(withImage image: UIImage? = nil, contentMode: UIView.ContentMode = .top) -> UIImageView {
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = contentMode
        imageView.clipsToBounds = true
        return imageView
    }

    static func makeMarker() -> GGUIMarkerView {
        GGUIMarkerView(frame: .zero)
    }
}
