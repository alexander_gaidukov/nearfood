//
//  GGTabBarViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum GGTabBarItem {
    case selection(GGTabBarSelectionItem)
    case action(GGTabBarActionItem)

    var selectionItem: GGTabBarSelectionItem? {
        guard case let .selection(item) = self else { return nil }
        return item
    }

    var view: UIView {
        switch self {
        case let .selection(view):
            return view
        case let .action(view):
            return view
        }
    }

    func setMarkerValue(_ value: Int) {
        guard case let .selection(item) = self else { return }
        item.setMarkerValue(value)
    }
}

final class GGTabBarActionItem: UIView {
    init(customButton: UIButton) {
        super.init(frame: .zero)
        configure(withButton: customButton)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure(withButton button: UIButton) {
        translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: topAnchor, constant: 4.0),
            button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0)
        ])
    }
}

final class GGTabBarSelectionItem: UIButton {

    var color: UIColor = .lightGray {
        didSet {
            textLabel.textColor = color
        }
    }

    private let iconImageView = GGUIMarker.makeImageView()
    private let textLabel = GGUIMarker.makeLabel()
    private let marker = GGUIMarker.makeMarker()

    private var icon: UIImage?
    private var selectedIcon: UIImage?

    private(set)var tabIndex: Int = 0

    var isSelectedTab: Bool = false {
        didSet {
            iconImageView.image = isSelectedTab ? selectedIcon : icon
        }
    }

    convenience init(icon: UIImage,
                     selectedIcon: UIImage,
                     title: String? = nil,
                     font: UIFont = .systemFont(ofSize: 11),
                     tabIndex: Int,
                     markerValue: Int
    ) {
        self.init(type: .custom)
        self.icon = icon
        self.selectedIcon = selectedIcon
        self.tabIndex = tabIndex
        translatesAutoresizingMaskIntoConstraints = false
        iconImageView.image = icon
        textLabel.text = title
        textLabel.font = font
        marker.cornerRadius = 10
        marker.value = markerValue
        clipsToBounds = false
        configureViews()
    }

    func setMarkerValue(_ value: Int) {
        marker.value = value
    }

    private func configureViews() {
        addSubview(iconImageView)
        addSubview(marker)

        NSLayoutConstraint.activate([
            iconImageView.topAnchor.constraint(equalTo: topAnchor, constant: 10.0),
            iconImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            iconImageView.widthAnchor.constraint(equalTo: iconImageView.heightAnchor),
            marker.widthAnchor.constraint(equalToConstant: 20),
            marker.heightAnchor.constraint(equalToConstant: 20),
            marker.topAnchor.constraint(equalTo: iconImageView.topAnchor, constant: -5),
            marker.trailingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 5)
        ])

        if textLabel.text?.isEmpty == false {
            addSubview(textLabel)
            NSLayoutConstraint.activate([
                textLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2),
                textLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
            ])
        }
    }
}

final class GGTabBar: UIView {
    var ggItems: [GGTabBarItem] = []

    convenience init(items: [GGTabBarItem]) {
        self.init()
        ggItems = items
        translatesAutoresizingMaskIntoConstraints = false
        configureViews()
    }

    override var tintColor: UIColor! {
        didSet {
            ggItems.forEach { $0.selectionItem?.color = tintColor }
        }
    }

    private func configureViews() {
        if ggItems.isEmpty { return }

        let line = UIView()
        line.translatesAutoresizingMaskIntoConstraints = false
        line.backgroundColor = .tabbarSeparator
        addSubview(line)
        NSLayoutConstraint.activate([
            line.leadingAnchor.constraint(equalTo: leadingAnchor),
            line.trailingAnchor.constraint(equalTo: trailingAnchor),
            line.topAnchor.constraint(equalTo: topAnchor),
            line.heightAnchor.constraint(equalToConstant: 0.5)
        ])

        ggItems.forEach { $0.selectionItem?.color = tintColor }

        for (index, item) in ggItems.enumerated() {
            addSubview(item.view)
            NSLayoutConstraint.activate([
                item.view.topAnchor.constraint(equalTo: topAnchor),
                item.view.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])

            if index == 0 {
                NSLayoutConstraint.activate([
                    item.view.leadingAnchor.constraint(equalTo: leadingAnchor)
                ])
                continue
            }

            let multiplier: CGFloat
            if case .selection = item {
                multiplier = 1.0
            } else {
                multiplier = 2.0
            }

            NSLayoutConstraint.activate([
                item.view.leadingAnchor.constraint(equalTo: ggItems[index - 1].view.trailingAnchor),
                item.view.widthAnchor.constraint(equalTo: ggItems[index - 1].view.widthAnchor, multiplier: multiplier)
            ])

            if index == ggItems.count - 1 {
                NSLayoutConstraint.activate([
                    item.view.trailingAnchor.constraint(equalTo: trailingAnchor)
                ])
            }
        }
    }
}

class GGTabBarController: UITabBarController {
    var ggTabBar: GGTabBar!
    var selectedColor = UIColor.darkGray
    let animator = UIViewPropertyAnimator(duration: 0.5, curve: .linear)
    var normalColor = UIColor.lightGray {
        didSet {
            ggTabBar.tintColor = normalColor
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.isHidden = true
        configureViews()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        animator.stopAnimation(true)
        if animator.state != .inactive {
            animator.finishAnimation(at: .current)
        }
    }

    func configureViews() {}

    func showMarker(count: Int, index: Int) {
        ggTabBar.ggItems[index].setMarkerValue(count)
    }

    func setTabBar(withItems items: [GGTabBarItem], height: CGFloat = Constants.UI.tabBarHeight) {
        guard !items.isEmpty else { return }
        ggTabBar = GGTabBar(items: items)
        ggTabBar.tintColor = normalColor
        ggTabBar.backgroundColor = UIColor.tabbarBackground.withAlphaComponent(0.94)

        let blurEffectView = UIVisualEffectView(effect: nil)
        blurEffectView.frame = ggTabBar.bounds
        blurEffectView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        ggTabBar.insertSubview(blurEffectView, at: 0)

        view.addSubview(ggTabBar)
        selectItem(at: 0)

        NSLayoutConstraint.activate([
            ggTabBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            ggTabBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            ggTabBar.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ggTabBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -height)
        ])

        items.forEach {
            guard let selectionItem = $0.selectionItem else { return }
            selectionItem.addTarget(self, action: #selector(switchTab(_:)), for: .touchUpInside)
        }

        animator.addAnimations {
            blurEffectView.effect = UIBlurEffect(style: .regular)
        }
        animator.fractionComplete = 0.3
    }

    @objc private func switchTab(_ sender: GGTabBarSelectionItem) {
        deselectItem(at: selectedIndex)
        selectItem(at: sender.tabIndex)
        selectedIndex = sender.tabIndex
    }

    private func selectItem(at index: Int) {
        ggTabBar.ggItems[index].selectionItem?.isSelectedTab = true
        ggTabBar.ggItems[index].selectionItem?.color = selectedColor
    }

    private func deselectItem(at index: Int) {
        ggTabBar.ggItems[index].selectionItem?.isSelectedTab = false
        ggTabBar.ggItems[index].selectionItem?.color = normalColor
    }
}
