//
//  OrderCommentPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 23.10.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol OrderCommentPresenterType {
    var state: OrderCommentViewState { get }
    func close()
    func changeNote(_ note: String)
    func save()
}

enum OrderCommentAction {
    case close
    case save(String)
}

struct OrderCommentViewState {
    @Observable
    var note: String?
}

final class OrderCommentPresenter: OrderCommentPresenterType {
    private let completion: (OrderCommentAction) -> ()
    var state: OrderCommentViewState
    private let originalNote: String?

    init(note: String?, completion: @escaping (OrderCommentAction) -> ()) {
        self.completion = completion
        state = OrderCommentViewState(note: note)
        originalNote = note
    }

    func close() {
        completion(.close)
    }

    func changeNote(_ note: String) {
        state.note = note
    }

    func save() {
        if let note = state.note, note != originalNote {
            completion(.save(note))
        } else {
            completion(.close)
        }

    }
}
