//
//  OrderCommentViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 23.10.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class OrderCommentViewController: UIViewController, PopupViewController, KeyboardAvoiding {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet private weak var textView: GGTextView!
    @IBOutlet private weak var keyboardHidingView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!

    var presenter: OrderCommentPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
        textView.becomeFirstResponder()
    }

    private func configureView() {

        containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        titleLabel.text = LocalizedStrings.Order.Comment.title
        descriptionLabel.text = LocalizedStrings.Order.Comment.description
        saveButton.setTitle(LocalizedStrings.Common.save, for: .normal)
        cancelButton.setTitle(LocalizedStrings.Common.cancel, for: .normal)

        configureToolbar()

        textView.placeholder = LocalizedStrings.Order.Comment.placeholder
        textView.didBeginEditing = { [weak self] in
            self?.keyboardHidingView.isHidden = false
        }
        textView.didEndEditing = { [weak self] in self?.keyboardHidingView.isHidden = true }
        textView.didChange = { [weak self] in self?.presenter.changeNote($0) }

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        containerView.addGestureRecognizer(panGestureRecognizer)

        keyboardHidingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
    }

    private func configureBindings() {
        presenter.state.$note.bind(to: textView, keyPath: \.text).addToDisposableBag(disposableBag)
    }

    private func configureToolbar() {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50.0))
        toolbar.barStyle = .default
        toolbar.barTintColor = .tabbarBackground
        toolbar.tintColor = .presentedNavigationTint
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideKeyboard))
        ]
        toolbar.sizeToFit()
        textView.inputAccessoryView = toolbar
    }

    @IBAction private func saveTapped() {
        textView.resignFirstResponder()
        presenter.save()
    }

    @IBAction private func cancelTapped() {
        textView.resignFirstResponder()
        presenter.close()
    }

    @objc func close() {
        if textView.isFirstResponder {
            hideKeyboard()
        } else {
            presenter.close()
        }
    }

    @objc private func hideKeyboard() {
        textView.resignFirstResponder()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }

}
