//
//  CommentCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 18.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class CommentCoordinator: Coordinator {
    let completion: (String?) -> ()
    let router: CommentRouterType
    let note: String?

    init(note: String?, router: CommentRouterType, completion: @escaping (String?) -> ()) {
        self.completion = completion
        self.router = router
        self.note = note
    }

    func start() {
        router.showOrderComment(note: note) { action in
            self.router.dismiss()
            switch action {
            case .close:
                self.completion(nil)
            case .save(let comment):
                self.completion(comment)
            }
        }
    }
}
