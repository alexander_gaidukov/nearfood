//
//  CommentViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 18.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol CommentViewControllersFactoryType {
    func orderCommentViewController(note: String?, completion: @escaping (OrderCommentAction) -> ()) -> OrderCommentViewController
}

struct CommentViewControllersFactory: CommentViewControllersFactoryType {
    private let storyboard: Storyboard = .comment

    func orderCommentViewController(note: String?, completion: @escaping (OrderCommentAction) -> ()) -> OrderCommentViewController {
        let presenter = OrderCommentPresenter(note: note, completion: completion)
        let controller = OrderCommentViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
