//
//  CommentRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 18.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol CommentRouterType {
    func showOrderComment(note: String?, completion: @escaping (OrderCommentAction) -> ())
    func dismiss()
}

final class CommentRouter: NSObject, CommentRouterType {
    private let presentingViewController: UIViewController
    private let factory: CommentViewControllersFactoryType

    init(factory: CommentViewControllersFactoryType, presentingViewController: UIViewController) {
        self.factory = factory
        self.presentingViewController = presentingViewController
    }

    func showOrderComment(note: String?, completion: @escaping (OrderCommentAction) -> ()) {
        let controller = factory.orderCommentViewController(note: note, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        presentingViewController.present(controller, animated: true, completion: nil)
    }

    func dismiss() {
        presentingViewController.dismiss(animated: true, completion: nil)
    }
}

extension CommentRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
