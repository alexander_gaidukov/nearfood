//
//  WelcomeViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    var presenter: WelcomePresenterType!

    @IBOutlet private weak var loadingView: PhotosLoadingIndicator!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.configure(with: presenter.state.images)
        configureBindings()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.start()
    }

    private func configureBindings() {
        presenter.state.$isLoading.observe {[weak self] isLoading in
            guard let self = self else { return }
            if isLoading {
                self.loadingView.start()
            } else {
                self.loadingView.stop()
            }
        }.addToDisposableBag(disposableBag)
    }
}
