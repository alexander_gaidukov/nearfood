//
//  WelcomeViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol WelcomeViewControllersFactoryType {
    func welcomeViewController(completion: @escaping (WelcomeAction) -> ()) -> WelcomeViewController
}

struct WelcomeViewControllersFactory: WelcomeViewControllersFactoryType {
    private let storyboard: Storyboard = .welcome
    let webClient: WebClientType
    let dataProvidersStorage: DataProvidersStorageType

    func welcomeViewController(completion: @escaping (WelcomeAction) -> ()) -> WelcomeViewController {
        let presenter = WelcomePresenter(
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            appVersionDataProvider: dataProvidersStorage.appVersionDataProvider,
            webClient: webClient,
            completion: completion
        )
        let controller = WelcomeViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
