//
//  WelcomeRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol WelcomeRouterType {
    func showWelcome(completion: @escaping (WelcomeAction) -> ())
}

final class WelcomeRouter: WelcomeRouterType {

    let window: UIWindow
    private let factory: WelcomeViewControllersFactoryType

    init(window: UIWindow, factory: WelcomeViewControllersFactoryType) {
        self.window = window
        self.factory = factory
    }

    func showWelcome(completion: @escaping (WelcomeAction) -> ()) {
        let controller = factory.welcomeViewController(completion: completion)
        window.rootViewController = controller
    }
}
