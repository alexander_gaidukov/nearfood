//
//  WelcomeCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class WelcomeCoordinator: Coordinator {
    var completion: () -> ()
    private let router: WelcomeRouterType

    init(router: WelcomeRouterType,
         completion: @escaping () -> ()) {
        self.router = router
        self.completion = completion
    }

    func start() {
        showWelcome()
    }

    private func showWelcome() {
        router.showWelcome { action in
            switch action {
            case .complete:
                self.completion()
            }
        }
    }
}
