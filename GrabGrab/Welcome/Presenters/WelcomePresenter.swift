//
//  WelcomePresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol WelcomePresenterType {
    var state: WelcomeViewState { get }
    func start()
}

struct WelcomeViewState {
    @Observable
    var isLoading = false
    var images = [#imageLiteral(resourceName: "splash1"), #imageLiteral(resourceName: "splash2"), #imageLiteral(resourceName: "splash3"), #imageLiteral(resourceName: "splash4"), #imageLiteral(resourceName: "splash5")]
}

enum WelcomeAction {
    case complete
}

final class WelcomePresenter: WelcomePresenterType {
    private(set) var state = WelcomeViewState()
    private let customerDataProvider: CustomerDataProviderType
    private let appVersionDataProvider: AppVersionDataProviderType
    private let webClient: WebClientType
    private let completion: (WelcomeAction) -> ()

    init(
        customerDataProvider: CustomerDataProviderType,
        appVersionDataProvider: AppVersionDataProviderType,
        webClient: WebClientType,
        completion: @escaping (WelcomeAction) -> ()) {
        self.customerDataProvider = customerDataProvider
        self.appVersionDataProvider = appVersionDataProvider
        self.webClient = webClient
        self.completion = completion
    }

    func start() {
        guard customerDataProvider.isLoggedIn else {
            completion(.complete)
            return
        }
        state.isLoading = true

        let group = DispatchGroup()
        group.enter()

        appVersionDataProvider.update { group.leave() }

        if customerDataProvider.isLoggedIn {
            group.enter()
            webClient.load(combinedResource: ResourceBuilder.currentUserResource()) {[weak self] result in
                if let customer = try? result.get() {
                    self?.customerDataProvider.updateCustomer(customer)
                }
                group.leave()
            }
        }

        group.notify(queue: .main) {
            self.state.isLoading = false
            self.completion(.complete)
        }
    }
}
