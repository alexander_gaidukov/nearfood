//
//  AddressesDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import YandexMapKitSearch

struct AddressSuggestionItem {
    var displayText: String
    var searchText: String
    var isLeaf: Bool
    var latitude: Double = 0
    var longitude: Double = 0
}

struct SearchWindow {
    let southWest: (lat: Double, lon: Double)
    let northEast: (lat: Double, lon: Double)
}

protocol AddressesDataProviderType {
    var askedForSaveLocationIds: Set<String> { get }

    func locationDidAskedForSave(locationId: String)

    func address(
        from latitude: Double,
        longitude: Double,
        completion: @escaping (String?) -> ()
    )

    func location(
        for address: String,
        window: SearchWindow,
        completion: @escaping(Double?, Double?) -> ()
    )

    func suggestAddresses(
        for searchText: String,
        window: SearchWindow,
        completion: @escaping ([AddressSuggestionItem]) -> ()
    )
}

final class AddressesDataProvider: AddressesDataProviderType {

    private let searchManager = YMKSearch.sharedInstance().createSearchManager(with: .combined)
    private var searchSession: YMKSearchSession?
    private lazy var suggestSession: YMKSearchSuggestSession = {
        searchManager.createSuggestSession()
    }()

    @UserDefaultsItem(key: StorageKey("grab-grab.locations.askedForSave.key"), defaultValue: [])
    var askedForSaveLocationIds: Set<String>

    func locationDidAskedForSave(locationId: String) {
        askedForSaveLocationIds.insert(locationId)
    }

    func address(from latitude: Double, longitude: Double, completion: @escaping (String?) -> ()) {
        let options = YMKSearchOptions()
        options.searchTypes = .geo
        searchSession = searchManager.submit(
            with: YMKPoint(latitude: latitude, longitude: longitude),
            zoom: nil,
            searchOptions: options
        ) { response, _ in
            completion(response?.collection.children.first?.obj?.name)
        }
    }

    func location(for address: String, window: SearchWindow, completion: @escaping(Double?, Double?) -> ()) {
        let options = YMKSearchOptions()
        options.searchTypes = .geo
        searchSession = searchManager.submit(
            withText: address,
            geometry: YMKGeometry(
                boundingBox: YMKBoundingBox(
                    southWest: YMKPoint(latitude: window.southWest.lat, longitude: window.southWest.lon),
                    northEast: YMKPoint(latitude: window.northEast.lat, longitude: window.northEast.lon)
                )
            ),
            searchOptions: options
        ) { response, _ in
            guard let point = response?.collection.children.first?.obj?.geometry.first?.point else {
                completion(nil, nil)
                return
            }
            completion(point.latitude, point.longitude)
        }
    }

    func suggestAddresses(
        for searchText: String,
        window: SearchWindow,
        completion: @escaping ([AddressSuggestionItem]) -> ()
    ) {
        guard !searchText.isEmpty else {
            completion([])
            return
        }
        let options = YMKSuggestOptions()
        options.suggestTypes = .geo
        suggestSession.suggest(
            withText: searchText,
            window: YMKBoundingBox(
                southWest: YMKPoint(latitude: window.southWest.lat, longitude: window.southWest.lon),
                northEast: YMKPoint(latitude: window.northEast.lat, longitude: window.northEast.lon)
            ),
            suggestOptions: options
        ) { items, _ in
            guard let items = items else {
                completion([])
                return
            }

            completion(items.map {
                AddressSuggestionItem(
                    displayText: $0.displayText ?? $0.searchText,
                    searchText: $0.searchText,
                    isLeaf: $0.action == .search
                )
            })
        }
    }
}
