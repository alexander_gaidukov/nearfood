//
//  CompanyInfoDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.12.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CompanyInfoDataProviderType {
    var info: CompanyInfo? { get }
    func update(completion: @escaping (WebError?) -> ())
}

final class CompanyInfoDataProvider: CompanyInfoDataProviderType {

    private let webClient: WebClientType

    init(webClient: WebClientType) {
        self.webClient = webClient
        update(completion: { _ in })
    }

    @UserDefaultsItem(key: StorageKey("com.grab-grab.companyInfo.key"))
    var info: CompanyInfo? {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .companyInfoDidChangeNotification, object: nil)
            }
        }
    }

    func update(completion: @escaping (WebError?) -> ()) {
        webClient.load(resource: ResourceBuilder.companyInfoResource()) { [weak self] result in
            switch result {
            case .success(let info):
                self?.info = info
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
}
