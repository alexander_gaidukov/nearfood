//
//  OrdersDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 18.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import WidgetKit

protocol OrdersDataProviderType {
    var orders: [Order] { get }
    var activeOrders: [Order] { get }
    func refreshOrders(completion: @escaping (WebError?) -> ())
    func loadNextOrders(completion: @escaping (WebError?) -> ())
    func updateActiveOrders(notifyWidget: Bool)
    func orderDetails(for orderUUID: String, completion: @escaping (Order?) -> ())
}

final class OrdersDataProvider: OrdersDataProviderType {

    private let webClient: WebClientType
    private let customerDataProvider: CustomerDataProviderType

    private let limit: Int = 25
    private var offset: Int {
        orders.count
    }
    private var isEOF = false

    init(webClient: WebClientType, customerDataProvider: CustomerDataProviderType) {
        self.webClient = webClient
        self.customerDataProvider = customerDataProvider
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(shouldUpdateOrders),
            name: .shouldUpdateOrdersNotification,
            object: nil
        )
        scheduleActiveOrdersUpdates()
    }

    private(set) var orders: [Order] = [] {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .ordersDidChangeNotification, object: nil)
            }
        }
    }
    private(set) var activeOrders: [Order] = [] {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .currentOrderDidChangeNotification, object: nil)
            }
        }
    }

    func refreshOrders(completion: @escaping (WebError?) -> ()) {
        guard let customer = customerDataProvider.customer else { return }
        let resource = ResourceBuilder.ordersResource(
            customerUUID: customer.uuid,
            limit: limit,
            offset: 0
        )

        webClient.load(resource: resource) {[weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let orders):
                self.merge(orders)
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }

    func loadNextOrders(completion: @escaping (WebError?) -> ()) {
        guard !isEOF, let customer = customerDataProvider.customer else {
            completion(nil)
            return
        }

        let resource = ResourceBuilder.ordersResource(
            customerUUID: customer.uuid,
            limit: limit,
            offset: offset
        )

        webClient.load(resource: resource) {[weak self] result in

            guard let self = self else { return }

            switch result {
            case .success(let orders):
                self.isEOF = orders.count < self.limit
                self.orders.append(contentsOf: orders)
                completion(nil)
            case .failure(let error):
                completion(error)
            }

        }
    }

    func updateActiveOrders(notifyWidget: Bool = false) {
        if #available(iOS 14.0, *) {
            WidgetCenter.shared.reloadTimelines(ofKind: Constants.Identifiers.widgetKind)
        }
        guard let customer = customerDataProvider.customer, customer.isActive else { return }
        let resource = ResourceBuilder.activeOrdersResource(customerUUID: customer.uuid)
        webClient.load(resource: resource) { [weak self] result in
            guard let orders = try? result.get() else { return }
            self?.activeOrders = orders
        }
    }

    func orderDetails(for orderUUID: String, completion: @escaping (Order?) -> ()) {
        webClient.load(resource: ResourceBuilder.orderDetailsResource(orderUUID: orderUUID)) { result in
            guard let order = try? result.get() else { return }
            completion(order)
        }
    }

    private func scheduleActiveOrdersUpdates() {
        updateActiveOrders()
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.Intervals.ordersUpdateInterval) {[weak self] in
            self?.scheduleActiveOrdersUpdates()
        }
    }

    private func merge(_ orders: [Order]) {
        guard let firstUUID = self.orders.first?.uuid else {
            self.orders = orders
            return
        }

        guard let index = orders.firstIndex(where: { $0.uuid == firstUUID }) else {
            self.orders.insert(contentsOf: orders, at: 0)
            return
        }

        self.orders.replaceSubrange(0..<(orders.count - index), with: orders)
    }

    @objc private func shouldUpdateOrders() {
        updateActiveOrders()
        refreshOrders(completion: { _ in })
    }
}
