//
//  ContactsDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.09.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import ContactsUI

protocol ContactsDataProviderType {
    var isEnabled: Bool { get }
    func fetchContacts(completion: @escaping([Recipient]) -> ())
}

final class ContactsDataProvider: ContactsDataProviderType {
    func fetchContacts(completion: @escaping ([Recipient]) -> ()) {
        authorizeIfNeeded {
            guard $0 else {
                completion([])
                return
            }
            self.fetchCNContacts(completion: completion)
        }
    }

    var isEnabled: Bool {
        CNContactStore.authorizationStatus(for: .contacts) != .denied
    }

    private func fetchCNContacts(completion: @escaping ([Recipient]) -> ()) {
        let contactStore = CNContactStore()
        var contacts = [CNContact]()

        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey
        ] as [Any]

        // swiftlint:disable:next force_cast
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])

        do {
            try contactStore.enumerateContacts(with: request) { contact, _ in
                contacts.append(contact)
            }
        } catch {

        }

        let recipients = contacts.flatMap { contact -> [Recipient] in
            let name = [contact.givenName, contact.familyName].filter { !$0.isEmpty }.joined(separator: " ")
            return contact.phoneNumbers.map { number in
                let availableValues = "+0123456789"
                let unavailableCharactersSet = CharacterSet(charactersIn: availableValues).inverted
                var phone = number.value.stringValue.removing(charactersFrom: unavailableCharactersSet)
                if !phone.hasPrefix("+") { phone = "+" + phone }

                return Recipient(uuid: "", phone: phone, name: name)
            }
        }

        completion(recipients)
    }

    private func authorizeIfNeeded(completion: @escaping (Bool) -> ()) {
        let authStatus = CNContactStore.authorizationStatus(for: .contacts)

        guard authStatus == .notDetermined else {
            completion(authStatus == .authorized)
            return
        }

        CNContactStore().requestAccess(for: .contacts) { granted, _ in
            completion(granted)
        }
    }
}
