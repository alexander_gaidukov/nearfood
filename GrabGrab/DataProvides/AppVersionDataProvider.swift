//
//  AppVersionDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 26.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

enum AppVersionState {
    case upToDate
    case outdated
    case unsupported
}

protocol AppVersionDataProviderType {
    var state: AppVersionState { get }
    var currentVersion: String? { get }
    func update(completion: @escaping () -> ())
    func moveToAppStore()
}

final class AppVersionDataProvider: AppVersionDataProviderType {

    var state: AppVersionState = .upToDate {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .appVersionStateDidChangeNotification, object: nil)
            }
        }
    }

    var currentVersion: String? {
        Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }

    private let webClient: WebClientType

    init(webClient: WebClientType) {
        self.webClient = webClient
    }

    func update(completion: @escaping () -> ()) {
        fetchAppVersions { [weak self] version in
            self?.updateState(version: version)
            completion()
        }
    }

    func moveToAppStore() {
        // swiftlint:disable:next force_unwrapping
        let url = URL(string: "itms-apps://apple.com/app/\(Constants.Identifiers.appStoreId)")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    private func fetchAppVersions(completion: @escaping (AppVersion?) -> ()) {
        webClient.load(resource: ResourceBuilder.appVersionResource()) { result in
            completion(try? result.get())
        }
    }

    private func isVersion(_ version: String, isGreaterThen anotherVersion: String) -> Bool {
        let versionComponents = version.components(separatedBy: ".").map { Int($0) ?? 0 }
        let anotherVersionComponents = anotherVersion.components(separatedBy: ".").map { Int($0) ?? 0 }
        for index in 0..<min(versionComponents.count, anotherVersionComponents.count) {
            if versionComponents[index] > anotherVersionComponents[index] { return true }
            if versionComponents[index] < anotherVersionComponents[index] { return false }
        }
        return versionComponents.count > anotherVersionComponents.count
    }

    private func updateState(version: AppVersion?) {
        guard let currentVersion = currentVersion else {
            return
        }

        if let minVersion = version?.minVersion, isVersion(minVersion, isGreaterThen: currentVersion) {
            if state != .unsupported { state = .unsupported }
            return
        }

        if let storeVersion = version?.appstoreVersion, isVersion(storeVersion, isGreaterThen: currentVersion) {
            if state != .outdated { state = .outdated }
            return
        }

        if state != .upToDate { state = .upToDate }
    }
}
