//
//  StorefrontDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol StorefrontDataProviderType {
    var storefront: Storefront? { get }
    func refresh(completion: @escaping (WebError?) -> ())
}

final class StorefrontDataProvider: StorefrontDataProviderType {

    private var downloadTask: DownloadTask?
    private var uuid: String?

    private(set) var storefront: Storefront? {
        didSet {
//            if let storefront = storefront, oldValue?.kitchen.uuid != storefront.kitchen.uuid {
//                subscribeForKitchenUpdates(kitchen: storefront.kitchen)
//            }

            let stateDidChange = storefront?.state != oldValue?.state

            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .storefrontDidChangeNotification, object: nil)
                if stateDidChange {
                    NotificationCenter.default.post(name: .storefrontStateDidChangeNotification, object: nil)
                }
            }
        }
    }
    private let webClient: WebClientType
    private let customerDataProvider: CustomerDataProviderType

    init(webClient: WebClientType, customerDataProvider: CustomerDataProviderType) {
        self.webClient = webClient
        self.customerDataProvider = customerDataProvider
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(surgeDidChange(_:)),
            name: .shouldUpdateSurgeNotification,
            object: nil
        )
    }

    func refresh(completion: @escaping (WebError?) -> ()) {
        downloadTask?.cancel()
        let uuid = UUID().uuidString
        self.uuid = uuid
        scheduleStorefrontUpdate(uuid: uuid, force: true, completion: completion)
    }

    private func scheduleStorefrontUpdate(
        uuid: String,
        force: Bool = false,
        completion: @escaping (WebError?) -> () = { _ in }
    ) {
        guard uuid == self.uuid else { return }
        load(force: force, completion: completion)
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.Intervals.storefrontUpdateInterval) {[weak self] in
            self?.scheduleStorefrontUpdate(uuid: uuid)
        }
    }

    private func load(force: Bool = false, completion: @escaping (WebError?) -> () = { _ in }) {
        guard let customer = customerDataProvider.customer,
              let location = customerDataProvider.currentLocation else {
            completion(nil)
            return
        }
        let resource = ResourceBuilder.storefrontResource(userUUID: customer.uuid, locationUUID: location.uuid)
        downloadTask = webClient.load(resource: resource) {[weak self] result in
            switch result {
            case .success(let storefront):
                self?.storefront = storefront
                completion(nil)
            case .failure(let error):
                if force { self?.storefront = nil }
                completion(error)
            }
        }
    }

    private func subscribeForKitchenUpdates(kitchen: Kitchen) {
        let resource = ResourceBuilder.subscribeDevice(
            deviceUUID: customerDataProvider.device.uuid,
            forKitchen: kitchen.uuid
        )
        webClient.load(resource: resource) { _ in }
    }

    @objc private func surgeDidChange(_ notification: Notification) {
        guard let kitchenUUID = notification.userInfo?["kitchen_uuid"] as? String,
              storefront?.kitchen.uuid == kitchenUUID else { return }
        load()
    }
}
