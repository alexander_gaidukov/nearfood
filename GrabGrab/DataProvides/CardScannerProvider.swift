//
//  CardScannerProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 18.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CardScannerProviderType: AnyObject {
    var cardScanningViewController: UIViewController { get }
    var completion: ((String?) -> ())? { get set }
}

final class CardScannerProvider: NSObject, CardScannerProviderType {
    var completion: ((String?) -> ())?

    var cardScanningViewController: UIViewController {
        // swiftlint:disable:next force_unwrapping
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)!
        cardIOVC.collectCardholderName = false
        cardIOVC.collectCVV = false
        cardIOVC.collectExpiry = false
        cardIOVC.modalPresentationStyle = .fullScreen
        cardIOVC.disableManualEntryButtons = true
        cardIOVC.hideCardIOLogo = true
        return cardIOVC
    }
}

extension CardScannerProvider: CardIOPaymentViewControllerDelegate {
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        completion?(nil)
    }

    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        completion?(cardInfo.cardNumber)
    }
}
