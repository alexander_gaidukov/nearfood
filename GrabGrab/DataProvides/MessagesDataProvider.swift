//
//  MessagesDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol MessagesDataProviderType {
    var newMessagesCount: Int { get }
    var messages: [Message] { get }
    func messageDidRead(_ message: Message)
    func refresh(completion: @escaping (WebError?) -> ())
    func loadNextPage(completion: @escaping (WebError?) -> ())
    func send(text: String, order: Order?)
    func send(image: UIImage, order: Order?)
    func remove(message: Message)
    func resend(message: Message)
    func updateFeedback(_ feedback: Feedback, for message: Message)
}

final class MessagesDataProvider: MessagesDataProviderType {

    private var serverMessages: [Message] = [] {
        didSet {
            NotificationCenter.default.post(name: .messagesDidChangeNotification, object: nil)
        }
    }

    private var localMessages: [Message] = [] {
        didSet {
            NotificationCenter.default.post(name: .messagesDidChangeNotification, object: nil)
        }
    }

    var messages: [Message] {
        (serverMessages + localMessages).sorted { $0.createdAt > $1.createdAt }
    }

    private let refreshMessageQueue = DispatchQueue(label: "grab-grab.chat.messages.queue")
    private let limit = 25
    private var hasNextPage = true
    private var isNextPageLoading = false
    private let customerDataProvider: CustomerDataProviderType
    private let webClient: WebClientType

    @UserDefaultsItem(key: StorageKey("com.grab-grab.messages.new.key"), defaultValue: 0)
    private(set) var newMessagesCount: Int {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .newMessagesDidArriveNotification, object: nil)
            }
        }
    }

    @UserDefaultsItem(key: StorageKey("com.grab-grab.messages.last.key"))
    private var lastReadMessageDate: Date? {
        didSet {
            updateNewMessagesCount()
        }
    }

    init(customerDataProvider: CustomerDataProviderType, webClient: WebClientType) {
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(shouldUpdateMessages),
            name: .shouldUpdateMessagesNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(shouldUpdateFeedback(_:)),
            name: .shouldUpdateFeedbackNotification,
            object: nil
        )
    }

    func refresh(completion: @escaping (WebError?) -> ()) {
        guard  let customer = customerDataProvider.customer else {
            completion(nil)
            return
        }

        let resource = ResourceBuilder.messagesResource(
            customerUUID: customer.uuid,
            date: messages.first?.createdAt,
            direction: .next,
            limit: limit
        )

        webClient.load(resource: resource) {[weak self] response in
            guard let self = self else { return }
            switch response {
            case .success(let messages):
                self.refreshMessageQueue.async { self.refreshMessages(with: messages) }
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }

    func loadNextPage(completion: @escaping (WebError?) -> ()) {
        guard hasNextPage, !isNextPageLoading, let customer = customerDataProvider.customer else {
            completion(nil)
            return
        }

        isNextPageLoading = true
        let resource = ResourceBuilder.messagesResource(
            customerUUID: customer.uuid,
            date: messages.last?.createdAt,
            direction: .prev,
            limit: limit
        )
        webClient.load(resource: resource) {[weak self] result in
            guard let self = self else { return }
            defer {
                self.isNextPageLoading = false
            }
            guard let messages = try? result.get() else { return }
            self.refreshMessageQueue.async { self.addOlderMessaages(messages) }
            self.hasNextPage = messages.count >= self.limit
        }
    }

    func send(text: String, order: Order?) {
        guard let customer = customerDataProvider.customer else { return }
        let message = Message(customer: customer, body: text, order: order)
        localMessages.append(message)
        sendMessage(message: message)
    }

    func send(image: UIImage, order: Order?) {
        guard let customer = customerDataProvider.customer else { return }
        let message = Message(customer: customer, image: image, order: order)
        localMessages.append(message)
        sendMessage(message: message)
    }

    func remove(message: Message) {
        guard let index = localMessages.firstIndex(of: message) else { return }
        localMessages.remove(at: index)
    }

    func resend(message: Message) {
        guard let index = localMessages.firstIndex(of: message) else { return }
        localMessages[index].sendingState = .inProgress
        sendMessage(message: localMessages[index])
    }

    func updateFeedback(_ feedback: Feedback, for message: Message) {
        guard let index = serverMessages.firstIndex(of: message) else { return }
        serverMessages[index].properties?.order?.feedback = feedback
    }

    func messageDidRead(_ message: Message) {
        guard message.author?.uuid != customerDataProvider.customer?.uuid else { return }
        let date = lastReadMessageDate
        // swiftlint:disable:next force_unwrapping
        if date == nil || date! < message.createdAt {
            lastReadMessageDate = message.createdAt
        }
    }

    @objc private func shouldUpdateMessages() {
        refresh { _ in }
    }

    @objc private func shouldUpdateFeedback(_ notification: Notification) {
        guard let json = notification.userInfo,
            let data = try? JSONSerialization.data(withJSONObject: json, options: []),
            let feedback = try? Coders.decoder.decode(Feedback.self, from: data),
            let index = serverMessages
                .firstIndex(where: {
                    $0.kind == .feedback && $0.properties?.order?.feedback?.uuid == feedback.uuid
                }) else {
            return
        }
        serverMessages[index].properties?.order?.feedback = feedback
    }

    private func sendMessage(message: Message) {
        guard let author = message.author else { return }
        let resource: CombinedResource<[Message]>
        if let attachment = message.attachments?.first, case let .local(image) = attachment {
            resource = ResourceBuilder.sendImageMessageResource(
                customerUUID: author.uuid,
                image: image,
                orderUUID: message.properties?.order?.uuid,
                lastMessageDate: serverMessages.first?.createdAt,
                limit: limit
            )
        } else if let body = message.body, !body.isEmpty {
            resource = ResourceBuilder.sendMessageResource(
                customerUUID: author.uuid,
                body: body,
                attachment: nil,
                orderUUID: message.properties?.order?.uuid,
                lastMessageDate: serverMessages.first?.createdAt,
                limit: limit
            )
        } else {
            return
        }

        webClient.load(combinedResource: resource) {[weak self] result in
            guard let self = self else { return }
            let messageIndex = self.localMessages.firstIndex(of: message)
            if let messages = try? result.get() {
                if let index = messageIndex {
                    self.localMessages.remove(at: index)
                }
                self.refreshMessages(with: messages)
            } else {
                if let index = messageIndex {
                    self.localMessages[index].sendingState = .error
                }
            }
        }
    }

    private func refreshMessages(with newMessages: [Message]) {

        defer { updateNewMessagesCount() }

        guard !newMessages.isEmpty else { return }

        guard let firstMessage = messages.first else {
            serverMessages = newMessages
            return
        }

        guard let index = newMessages.lastIndex(where: { $0.createdAt > firstMessage.createdAt }) else {
            return
        }

        serverMessages.insert(contentsOf: newMessages[...index], at: 0)
    }

    private func updateNewMessagesCount() {
        guard let lastReadMessageDate = lastReadMessageDate else {
            newMessagesCount = 0
            return
        }

        let newReceivedMessages = serverMessages.filter {
            $0.author?.uuid != customerDataProvider.customer?.uuid && $0.createdAt > lastReadMessageDate
        }
        if newMessagesCount != newReceivedMessages.count {
            newMessagesCount = newReceivedMessages.count
        }
    }

    private func addOlderMessaages(_ olderMessages: [Message]) {
        guard !olderMessages.isEmpty else { return }

        guard let lastMessage = messages.last else {
            serverMessages = olderMessages
            return
        }

        guard let index = olderMessages.firstIndex(where: { $0.createdAt < lastMessage.createdAt }) else {
            return
        }
        serverMessages.append(contentsOf: olderMessages[index...])
    }
}
