//
//  InvitationsDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation
protocol InvitationsDataProviderType {
    var receivedInvitation: String? { get set }
    var invitations: [Invite] { get }
    var shouldShowInvitesOnMenu: Bool { get }
    func updateInvitations(completion: @escaping (WebError?) -> ())
    func didCloseInvitationsOnMenu()
}

final class InvitationsDataProvider: InvitationsDataProviderType {

    @UserDefaultsItem(key: StorageKey("com.grabgrab.menu.invitations.closed"), defaultValue: false)
    private var didCloseInvitations: Bool {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .invitesVisibilityDidChangeNotification, object: nil)
            }
        }
    }
    var shouldShowInvitesOnMenu: Bool {
        !didCloseInvitations
    }

    private(set) var invitations: [Invite] = [] {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .invitesDidChangeNotification, object: nil, userInfo: nil)
            }
        }
    }
    private let webClient: WebClientType
    private let customerDataProvider: CustomerDataProviderType

    init(webClient: WebClientType, customerDataProvider: CustomerDataProviderType) {
        self.webClient = webClient
        self.customerDataProvider = customerDataProvider
    }

    @UserDefaultsItem(key: StorageKey("com.grabgrab.receivedInvitation"))
    var receivedInvitation: String? {
        didSet {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .receivedInvitationDidChangeNotification, object: nil)
            }
        }
    }

    func updateInvitations(completion: @escaping (WebError?) -> ()) {
        guard let customer = customerDataProvider.customer else {
            completion(nil)
            return
        }
        webClient.load(resource: ResourceBuilder.invitesResource(customerUUID: customer.uuid)) { [weak self] result in
            switch result {
            case .success(let invitations):
                self?.invitations = invitations
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }

    func didCloseInvitationsOnMenu() {
        didCloseInvitations = true
    }

}
