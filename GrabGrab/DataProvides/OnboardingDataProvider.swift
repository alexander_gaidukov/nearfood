//
//  OnboardingDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum OnboardingService: String, CaseIterable {
    case location
    case notifications
}

protocol OnboardingDataProviderType {
    func isAuthorizationNeeded(for service: OnboardingService, completion: @escaping (Bool) -> ())
}

extension OnboardingDataProviderType {
    func nextNotCompletedService(completion: @escaping (OnboardingService?) -> ()) {
        OnboardingService.allCases.asyncFirst(
            where: { service, handler in
                self.isAuthorizationNeeded(for: service, completion: handler)
            },
            completion: completion
        )
    }

    func isCompleted(completion: @escaping (Bool) -> ()) {
        nextNotCompletedService { completion($0 == nil) }
    }
}

struct OnboardingDataProvider: OnboardingDataProviderType {
    func isAuthorizationNeeded(for service: OnboardingService, completion: @escaping (Bool) -> ()) {
        guard !ProcessInfo().isInTestMode else {
            completion(false)
            return
        }
        switch service {
        case .location:
            completion(LocationManager.isAuthorizationNeeded)
        case .notifications:
            return PushNotificationsManager.isAuthorizationNeeded(completion: completion)
        }
    }
}
