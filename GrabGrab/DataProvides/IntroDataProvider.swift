//
//  IntroDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 22.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation
protocol IntroDataProviderType {
    var shouldShowIntro: Bool { get }
    func introDidFinish()
}

final class IntroDataProvider: IntroDataProviderType {

    @UserDefaultsItem(key: StorageKey("com.grab-grab.intro.shown"), defaultValue: false)
    private var introWasShown: Bool

    var shouldShowIntro: Bool {
        !introWasShown
    }

    func introDidFinish() {
        introWasShown = true
    }
}
