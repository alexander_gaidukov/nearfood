//
//  CustomerDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import WidgetKit
import PassKit

protocol CustomerDataProviderDelegate: AnyObject {
    func willLogout()
    func didLogout()
    func didLogin()
    func didActivate()
}

protocol CustomerDataProviderType: AnyObject {
    var delegate: CustomerDataProviderDelegate? { get set }

    var customer: Customer? { get }
    var device: Device { get }
    var isLoggedIn: Bool { get }
    var isActive: Bool { get }
    var paymentMethods: [PaymentMethod] { get }

    var isCustomerHasAddress: Bool { get }

    var currentLocation: Location? { get }
    var paymentMethod: PaymentMethod? { get }

    func updateAddresses(_ addresses: [Location])
    func selectAddress(_ address: Location)

    func updatePaymentMethods(_ paymentMethods: [BankCard])
    func addPaymentMethod(_ paymentMethod: BankCard)
    func removePaymentMethod(_ paymentMethod: BankCard)
    func selectPaymentMethod(_ paymentMethod: PaymentMethod)

    func updateBonuses(bonuses: Double?)
    func updateTipsSettings(tips: TipsOption?)

    func changeName(name: String?)
    func activate(activationDate: Date, invite: Invite?)
    func updateCustomer(_ customer: Customer)
    func login(withResponse response: SignInResponse)
    func logout()
}

final class CustomerDataProvider: CustomerDataProviderType {
    private let accessTokenProvider: AccessTokenProviderType
    private let appVersionDataProvider: AppVersionDataProviderType

    init(accessTokenProvider: AccessTokenProviderType, appVersionDataProvider: AppVersionDataProviderType) {
        self.accessTokenProvider = accessTokenProvider
        self.appVersionDataProvider = appVersionDataProvider
    }

    weak var delegate: CustomerDataProviderDelegate?

    @KeychainItem(key: StorageKey("com.grabgrab.customer", domainSpecific: true))
    private(set) var customer: Customer? {
        didSet {
            accessTokenProvider.setCustomerUUID(customer?.uuid)
        }
    }

    var isCustomerHasAddress: Bool {
        customer?.locations?.isEmpty == false
    }

    @LazyKeychainItem(key: StorageKey("com.grabgrab.deviceUUID"), emptyValue: UUID().uuidString)
    private var deviceUUID: String

    var device: Device {
        Device(
            uuid: deviceUUID,
            model: UIDevice.current.modelName,
            osVersion: UIDevice.current.systemVersion,
            appVersion: appVersionDataProvider.currentVersion ?? ""
        )
    }

    var isLoggedIn: Bool {
        accessTokenProvider.isLoggedIn
    }

    var isActive: Bool {
        customer?.isActive == true
    }

    func login(withResponse response: SignInResponse) {
        customer = response.owner
        accessTokenProvider.setToken(response.token)
        delegate?.didLogin()
        reloadWidget()
    }

    func logout() {
        delegate?.willLogout()
        customer = nil
        accessTokenProvider.removeToken()
        delegate?.didLogout()
        reloadWidget()
    }

    private func reloadWidget() {
        if #available(iOS 14.0, *) {
            WidgetCenter.shared.reloadTimelines(ofKind: Constants.Identifiers.widgetKind)
        }
    }

    func updateCustomer(_ customer: Customer) {
        self.customer = customer
        NotificationCenter.default.post(name: .paymentMethodDidChangeNotification, object: nil)
        NotificationCenter.default.post(name: .locationDidChangeNotification, object: nil)
        NotificationCenter.default.post(name: .customerDidChangeNotification, object: nil)
        NotificationCenter.default.post(name: .bonusesDidChangeNotification, object: nil)
        NotificationCenter.default.post(name: .tipsDidChangeNotification, object: nil)
    }

    func changeName(name: String?) {
        var customer = self.customer
        customer?.name = name
        self.customer = customer
        NotificationCenter.default.post(name: .customerDidChangeNotification, object: nil)
    }

    func activate(activationDate: Date, invite: Invite?) {
        var customer = self.customer
        customer?.activatedAt = activationDate
        customer?.invite = invite
        self.customer = customer
        NotificationCenter.default.post(name: .customerDidChangeNotification, object: nil)
        delegate?.didActivate()
    }

    // MARK: - Locations

    @UserDefaultsItem(key: StorageKey("com.grabgrab.currentlocation", domainSpecific: true))
    private var currentLocationUUID: String?

    private(set) var currentLocation: Location? {
        get {
            if let savedLocationId = currentLocationUUID,
               let location = customer?.locations?.first(where: { $0.uuid == savedLocationId }) {
                return location
            }

            if let lastUserLocation = customer?.locations?.first {
                currentLocationUUID = lastUserLocation.uuid
                return lastUserLocation
            }

            return nil
        }

        set {
            currentLocationUUID = newValue?.uuid
            NotificationCenter.default.post(name: .locationDidChangeNotification, object: nil)
        }
    }

    func updateAddresses(_ addresses: [Location]) {
        guard var customer = self.customer else { return }
        customer.locations = addresses
        self.customer = customer
        NotificationCenter.default.post(name: .locationDidChangeNotification, object: nil)
    }

    func selectAddress(_ address: Location) {
        currentLocation = address
    }

    // MARK: - PaymentMethods

    private var isApplePayAllowed: Bool {
        false // For now Apple Pay doesn't work in Russia
//        PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: [.masterCard, .visa])
    }

    var paymentMethods: [PaymentMethod] {
        let bankCards: [PaymentMethod] = customer?.paymentMethods?.map { .card($0) } ?? []
        return isApplePayAllowed ? [.applePay(nil)] + bankCards : bankCards
    }

    @UserDefaultsItem(key: StorageKey("com.grabgrab.paymentMethod", domainSpecific: true))
    private var paymentMethodUUID: String?

    private(set) var paymentMethod: PaymentMethod? {
        get {
            let savedPaymentMethodId = paymentMethodUUID

            if savedPaymentMethodId == PaymentMethod.applePay(nil).identifier && isApplePayAllowed {
                return .applePay(nil)
            }

            if let savedCard = customer?.paymentMethods?.first(where: { $0.uuid == savedPaymentMethodId }) {
                return .card(savedCard)
            }

            if let paymentMethod = paymentMethods.first {
                paymentMethodUUID = paymentMethod.identifier
                return paymentMethod
            }

            return nil
        }

        set {
            paymentMethodUUID = newValue?.identifier
            NotificationCenter.default.post(name: .paymentMethodDidChangeNotification, object: nil)
        }
    }

    func updatePaymentMethods(_ paymentMethods: [BankCard]) {
        guard var customer = self.customer else { return }
        customer.paymentMethods = paymentMethods
        self.customer = customer
        NotificationCenter.default.post(name: .paymentMethodDidChangeNotification, object: nil)
    }

    func addPaymentMethod(_ paymentMethod: BankCard) {
        guard var customer = self.customer else { return }
        var paymentMethods = customer.paymentMethods ?? []
        paymentMethods.append(paymentMethod)
        customer.paymentMethods = paymentMethods
        self.customer = customer
        NotificationCenter.default.post(name: .paymentMethodDidChangeNotification, object: nil)
    }

    func removePaymentMethod(_ paymentMethod: BankCard) {
        guard var customer = self.customer else { return }
        guard let index = customer.paymentMethods?.firstIndex(where: { $0.uuid == paymentMethod.uuid }) else { return }
        customer.paymentMethods?.remove(at: index)
        self.customer = customer
        NotificationCenter.default.post(name: .paymentMethodDidChangeNotification, object: nil)
    }

    func updateTipsSettings(tips: TipsOption?) {
        self.customer?.tipsSettings = tips
        NotificationCenter.default.post(name: .tipsDidChangeNotification, object: nil)
    }

    func selectPaymentMethod(_ paymentMethod: PaymentMethod) {
        self.paymentMethod = paymentMethod
    }

    // MARK: - Bonuses
    func updateBonuses(bonuses: Double?) {
        var customer = self.customer
        customer?.bonuses = bonuses
        self.customer = customer
        NotificationCenter.default.post(name: .bonusesDidChangeNotification, object: nil)
    }
}
