//
//  CartDataProvider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CartDataProviderType {
    var cart: Cart { get }
    var storefrontState: Storefront.State { get }
    func add(cartItem: CartItem, count: Int)
    func addRecipient(_ recipient: Recipient)
    func removeRecipient()
    func clear()
    func clearEmptyDishes()
    func changeCount(of cartItem: StoredCartItem, by count: Int) -> Int
    func removeCartItem(_ cartItem: StoredCartItem)
    func changeCuntleryCount(by count: Int) -> Int
    func leaveAtDoor(isOn: Bool)
    func setNote(_ note: String)
    func updateDraftCoupon(to coupon: String?)
    func checkCartState(completion: @escaping (WebError?) -> ())
}
// swiftlint:disable:next type_body_length
final class CartDataProvider: CartDataProviderType {
    private let customerDataProvider: CustomerDataProviderType
    private let storefrontDataProvider: StorefrontDataProviderType
    private let webClient: WebClientType

    private var task: DownloadTask?

    private let _cart: ThreadSafe<Cart>

    private let cartSaveQueue = DispatchQueue(label: "grabgrab.cart.sync.queue", qos: .userInitiated)

    private lazy var countRefresher: DelayRefresher = {
        DelayRefresher(delay: 0.5) {[weak self] in
            self?.notify()
            self?.checkCartState()
        }
    }()

    private lazy var cartSaveRefresher: DelayRefresher = {
        DelayRefresher(delay: 1.0) { [weak self] in
            self?.saveCart()
        }
    }()

    init(
        customerDataProvider: CustomerDataProviderType,
        storefrontDataProvider: StorefrontDataProviderType,
        webClient: WebClientType
    ) {
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        self.storefrontDataProvider = storefrontDataProvider
        _cart = ThreadSafe(CartDataProvider.loadCart() ?? Cart())

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(locationDidChange),
            name: .locationDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(bonusesDidChange),
            name: .bonusesDidChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(surgeDidChange(_:)),
            name: .shouldUpdateSurgeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(storefrontStateDidChange),
            name: .storefrontStateDidChangeNotification,
            object: nil
        )
    }

    var storefrontState: Storefront.State {
        storefrontDataProvider.storefront?.state ?? .normal
    }

    var cart: Cart {
        _cart.value
    }

    private func notify(delay: TimeInterval = 0.5) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            NotificationCenter.default.post(name: .cartDidChangeNotification, object: nil)
        }
    }

    func add(cartItem: CartItem, count: Int) {
        _cart.atomically { $0.addOrUpdate(cartItem, count: count) }

        notify()
        checkCartState()
    }

    func changeCount(of cartItem: StoredCartItem, by count: Int) -> Int {

        let itemsCount: Int

        switch cartItem {
        case .dish(let cartDish):
            itemsCount = changeCount(of: cartDish, by: count)
        case .combo(let cartCombo):
            itemsCount = changeCount(of: cartCombo, by: count)
        }

        countRefresher.refresh()

        return itemsCount
    }

    private func changeCount(of cartDish: StoredCartDish, by count: Int) -> Int {
        _cart.atomically {
            guard let dishIndex = $0.dishes.firstIndex(where: { $0.uuid == cartDish.uuid }) else { return }
            guard $0.dishes[dishIndex].count + count >= 0 else { return }
            var dish = $0.dishes[dishIndex]
            $0.totalPrice += Double(count) * dish.price
            $0.amount += Double(count) * dish.price
            dish.count += count
            $0.dishes[dishIndex] = dish

        }.dishes.first(where: { $0.uuid == cartDish.uuid })?.count ?? 0
    }

    private func changeCount(of cartCombo: StoredCartCombo, by count: Int) -> Int {
        _cart.atomically {
            guard let comboIndex = $0.combos.firstIndex(where: { $0.uuid == cartCombo.uuid }) else { return }
            guard $0.combos[comboIndex].count + count >= 0 else { return }
            var combo = $0.combos[comboIndex]
            $0.totalPrice += Double(count) * combo.price
            $0.amount += Double(count) * combo.price
            combo.count += count
            $0.combos[comboIndex] = combo
        }.combos.first(where: { $0.uuid == cartCombo.uuid })?.count ?? 0
    }

    func changeCuntleryCount(by count: Int) -> Int {
        let result = _cart.atomically {
            guard $0.cutlery + count >= 0 else { return }
            $0.cutlery += count
        }.cutlery

        cartSaveRefresher.refresh()

        return result
    }

    func updateDraftCoupon(to coupon: String?) {
        _cart.atomically {
            $0.draftCoupon = coupon
        }

        cartSaveRefresher.refresh()
    }

    func addRecipient(_ recipient: Recipient) {
        _cart.atomically {
            $0.recipient = recipient
        }

        notify(delay: 0)
    }

    func removeRecipient() {
        _cart.atomically {
            $0.recipient = nil
        }
        notify(delay: 0)
    }

    func removeCartItem(_ cartItem: StoredCartItem) {
        switch cartItem {
        case .dish(let cartDish):
           removeCartDish(cartDish)
        case .combo(let cartCombo):
           removeCartCombo(cartCombo)
        }

        notify(delay: 0)
        checkCartState()
    }

    private func removeCartDish(_ cartDish: StoredCartDish) {
        _cart.atomically {
            guard let dishIndex = $0.dishes.firstIndex(where: { $0.uuid == cartDish.uuid }) else { return }
            let dish = $0.dishes[dishIndex]
            $0.totalPrice -= Double(dish.count) * dish.price
            $0.amount -= Double(dish.count) * dish.price
            $0.dishes.remove(at: dishIndex)
        }
    }

    private func removeCartCombo(_ cartCombo: StoredCartCombo) {
        _cart.atomically {
            guard let comboIndex = $0.combos.firstIndex(where: { $0.uuid == cartCombo.uuid }) else { return }
            let combo = $0.combos[comboIndex]
            $0.totalPrice -= Double(combo.count) * combo.price
            $0.amount -= Double(combo.count) * combo.price
            $0.combos.remove(at: comboIndex)
        }
    }

    func clearEmptyDishes() {
        _cart.atomically {
            $0.dishes.removeAll { $0.count <= 0 }
            $0.combos.removeAll { $0.count <= 0 }
        }
        cartSaveRefresher.refresh()
    }

    func clear() {
        _cart.atomically {
            $0.clear()
        }

        notify(delay: 0)
        checkCartState()
    }

    func leaveAtDoor(isOn: Bool) {
        _cart.atomically { $0.leaveAtDoor = isOn }
        cartSaveRefresher.refresh()
    }

    func setNote(_ note: String) {
        _cart.atomically { $0.note = note }
        notify(delay: 0)
        cartSaveRefresher.refresh()
    }

    func checkCartState(completion: @escaping (WebError?) -> () = { _ in }) {
        guard customerDataProvider.isLoggedIn,
              let customer = customerDataProvider.customer,
              let location = customerDataProvider.currentLocation else {
            completion(nil)
            return
        }

        task?.cancel()
        task = nil

        var dishes: [String: Int] = [:]
        if !cart.existingDishes.isEmpty {
            cart.existingDishes.forEach {
                dishes[$0.uuid] = $0.count
            }
        }

        var combos: [String: Int] = [:]
        if !cart.existingCombos.isEmpty {
            cart.existingCombos.forEach {
                combos[$0.uuid] = $0.count
            }
        }

        let resource = ResourceBuilder.estimateCartResource(
            userUUID: customer.uuid,
            locationUUID: location.uuid,
            dishes: dishes,
            combos: combos,
            coupon: cart.draftCoupon
        )

        task = webClient.load(resource: resource) {[weak self] result in
            switch result {
            case .success(let response):
                self?.updateCart(with: response, completion: completion)
            case .failure(let error):
                self?.cartSaveRefresher.refresh()
                self?.notify(delay: 0)
                DispatchQueue.main.async {
                    completion(error)
                }
            }
        }
    }

    @objc private func locationDidChange() {
        checkCartState()
    }

    @objc private func bonusesDidChange() {
        checkCartState()
    }

    @objc private func surgeDidChange(_ notification: Notification) {
        guard let kitchenUUID = notification.userInfo?["kitchen_uuid"] as? String,
              storefrontDataProvider.storefront?.kitchen.uuid == kitchenUUID else {
            return
        }
        checkCartState()
    }

    @objc private func storefrontStateDidChange() {
        switch storefrontDataProvider.storefront?.state {
        case .normal, .surge:
            checkCartState()
        default:
            break
        }
    }

    // swiftlint:disable:next function_body_length
    private func updateCart(with response: Estimation, completion: @escaping (WebError?) -> ()) {
        _cart.atomically { crt in
            crt.uuid = response.uuid
            crt.eta = response.eta / 60
            crt.totalPrice = response.calculation.total
            crt.amount = response.calculation.amount
            let dishes = crt.dishes.map { (dish: StoredCartDish) -> StoredCartDish in
                var result = dish
                result.isAvailable = response.calculation.problems?[result.uuid] == nil
                guard let charge = response
                        .calculation
                        .charges?
                        .first(where: { $0.dishMetadata?.dishUuid == result.uuid })
                else {
                    return result
                }
                result.totalPrice = charge.amount
                // swiftlint:disable:next force_unwrapping
                result.price = charge.dishMetadata!.price
                // swiftlint:disable:next force_unwrapping
                result.count = charge.dishMetadata!.count
                return result
            }
            let combos = crt.combos.map { (combo: StoredCartCombo) -> StoredCartCombo in
                var result = combo
                result.isAvailable = response.calculation.problems?[result.uuid] == nil
                guard let charge = response
                        .calculation
                        .charges?
                        .first(where: { $0.comboMetadata?.comboUuid == result.uuid })
                else {
                    return result
                }

                result.totalPrice = charge.amount
                // swiftlint:disable:next force_unwrapping
                result.price = charge.comboMetadata!.price
                // swiftlint:disable:next force_unwrapping
                result.count = charge.comboMetadata!.count
                return result
            }
            crt.dishes = dishes
            crt.combos = combos
            crt.charges = response.calculation.charges?.filter {
                switch $0.reason {
                case .dish, .combo:
                    return false
                default:
                    return true
                }
            } ?? []
        }
        cartSaveRefresher.refresh()
        notify(delay: 0)
        completion(nil)
    }

    private static let filename: String = "cart.json"

    private static var fileURL: URL {
        guard !ProcessInfo().isInTestMode else {
            // swiftlint:disable:next force_unwrapping
            return Bundle.main.url(forResource: "cart", withExtension: "json")!
        }

        // swiftlint:disable:next force_unwrapping
        var url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        url.appendPathComponent(filename)
        return url
    }

    private static func loadCart() -> Cart? {
        guard let data = try? Data(contentsOf: fileURL) else { return nil }
        return try? Coders.decoder.decode(Cart.self, from: data)
    }

    private func saveCart() {
        let crt = cart
        cartSaveQueue.async {
            guard let data = try? Coders.encoder.encode(crt) else { return }
            try? data.write(to: CartDataProvider.fileURL)
        }
    }
} // swiftlint:disable:this file_length
