//
//  AccessTokenprovider.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 21.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AccessTokenProviderType {
    var token: String? { get }
    var customerUUID: String? { get }
    func setToken(_ token: String)
    func setCustomerUUID(_ uuid: String?)
    func removeToken()
}

extension AccessTokenProviderType {
    var isLoggedIn: Bool {
        token != nil
    }
}

final class AccessTokenProvider: AccessTokenProviderType {

    static let shared = AccessTokenProvider()

    @KeychainItem(key: StorageKey("com.grabgrab.token", domainSpecific: true))
    private var realToken: String?

    var token: String? {
        ProcessInfo().isInTestMode ? Constants.UITests.accessToken : realToken
    }

    @KeychainItem(key: StorageKey("com.grabgrab.customer.uuid", domainSpecific: true))
    private(set) var customerUUID: String?

    func setCustomerUUID(_ uuid: String?) {
        self.customerUUID = uuid
    }

    func setToken(_ token: String) {
        self.realToken = token
    }

    func removeToken() {
        self.realToken = nil
    }
}
