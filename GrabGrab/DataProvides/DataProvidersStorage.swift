//
//  DataProvidersStorage.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol DataProvidersStorageType {
    var customerDataProvider: CustomerDataProviderType { get }
    var onboardingDataProvider: OnboardingDataProviderType { get }
    var addressDataProvider: AddressesDataProviderType { get }
    var cartDataProvider: CartDataProviderType { get }
    var ordersDataProvider: OrdersDataProviderType { get }
    var messagesDataProvider: MessagesDataProviderType { get }
    var cardScannerProvider: CardScannerProviderType { get }
    var storefrontDataProvider: StorefrontDataProviderType { get }
    var appVersionDataProvider: AppVersionDataProviderType { get }
    var companyInfoDataProvider: CompanyInfoDataProviderType { get }
    var invitationsDataProvider: InvitationsDataProviderType { get }
    var introDataProvider: IntroDataProviderType { get }
    var contactsDataProvider: ContactsDataProviderType { get }
}

final class DataProvidersStorage: DataProvidersStorageType {

    let customerDataProvider: CustomerDataProviderType

    let onboardingDataProvider: OnboardingDataProviderType

    let addressDataProvider: AddressesDataProviderType

    let cartDataProvider: CartDataProviderType

    let ordersDataProvider: OrdersDataProviderType

    let messagesDataProvider: MessagesDataProviderType

    let cardScannerProvider: CardScannerProviderType

    let storefrontDataProvider: StorefrontDataProviderType

    let appVersionDataProvider: AppVersionDataProviderType

    let companyInfoDataProvider: CompanyInfoDataProviderType

    let invitationsDataProvider: InvitationsDataProviderType

    let introDataProvider: IntroDataProviderType

    let contactsDataProvider: ContactsDataProviderType

    init(webClient: WebClientType, accessTokenProvider: AccessTokenProviderType) {

        let appVersionDataProvider = AppVersionDataProvider(webClient: webClient)
        self.appVersionDataProvider = appVersionDataProvider

        let customerDataProvider = CustomerDataProvider(
            accessTokenProvider: accessTokenProvider,
            appVersionDataProvider: appVersionDataProvider
        )
        self.customerDataProvider = customerDataProvider

        onboardingDataProvider = OnboardingDataProvider()

        addressDataProvider = AddressesDataProvider()

        let storefrontDataProvider = StorefrontDataProvider(
            webClient: webClient,
            customerDataProvider: customerDataProvider
        )
        self.storefrontDataProvider = storefrontDataProvider

        cartDataProvider = CartDataProvider(
            customerDataProvider: customerDataProvider,
            storefrontDataProvider: storefrontDataProvider,
            webClient: webClient
        )

        ordersDataProvider = OrdersDataProvider(
            webClient: webClient,
            customerDataProvider: customerDataProvider
        )

        messagesDataProvider = MessagesDataProvider(
            customerDataProvider: customerDataProvider,
            webClient: webClient
        )

        cardScannerProvider = CardScannerProvider()

        companyInfoDataProvider = CompanyInfoDataProvider(webClient: webClient)

        invitationsDataProvider = InvitationsDataProvider(
            webClient: webClient,
            customerDataProvider: customerDataProvider
        )

        contactsDataProvider = ContactsDataProvider()

        introDataProvider = IntroDataProvider()
    }

}
