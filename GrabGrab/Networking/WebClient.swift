//
//  WebClient.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol DownloadTask {
    func cancel()
}

extension URLSessionDataTask: DownloadTask {}

extension Error {
    var webError: WebError {
        if let error = self as? WebError { return error }
        return .other(0, self)
    }
}

extension URL {
    init<A>(resource: Resource<A>) {
        // swiftlint:disable:next force_unwrapping
        var components = URLComponents(url: resource.baseURL, resolvingAgainstBaseURL: false)!
        components.path = Path(components.path).appending(path: resource.path).absolutePath

        switch resource.method {
        case .get, .delete:
            components.queryItems = resource.params.map {
                URLQueryItem(name: $0.key, value: String(describing: $0.value))
            }
        default:
            break
        }

        // swiftlint:disable:next force_unwrapping
        self = components.url!
    }
}

extension URLRequest {
    init<A>(resource: Resource<A>) {
        let url = URL(resource: resource)
        self.init(url: url)
        httpMethod = resource.method.rawValue
        cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        timeoutInterval = 10
        resource.headers.merging(
            [
                "Accept": "application/json",
                "Content-Type": resource.contentType
            ],
            uniquingKeysWith: { $1 }
        ).forEach {
            setValue($0.value, forHTTPHeaderField: $0.key)
        }
        switch resource.method {
        case .post, .put, .patch:
            httpBody = resource.body
        default:
            break
        }
    }
}

extension Result {
    init(value: Success?, or error: Failure) {
        if let value = value {
            self = .success(value)
        } else {
            self = .failure(error)
        }
    }
}

protocol WebClientType {
    @discardableResult
    func load<A>(resource: Resource<A>, completion: @escaping (Result<A, WebError>) ->()) -> DownloadTask?

    @discardableResult
    func load<A>(
        combinedResource: CombinedResource<A>,
        completion: @escaping (Result<A, WebError>) -> ()
    ) -> DownloadTask?
}

final class WebClient: WebClientType {

    private let accessTokenProvider: AccessTokenProviderType
    private let logger: LoggerType

    init(accessTokenProvider: AccessTokenProviderType, logger: LoggerType) {
        self.accessTokenProvider = accessTokenProvider
        self.logger = logger
    }

    @discardableResult
    func load<A>(resource: Resource<A>, completion: @escaping (Result<A, WebError>) ->()) -> DownloadTask? {
        load(combinedResource: resource.retry(count: Constants.Intervals.retriesCount), completion: completion)
    }

    @discardableResult
    // swiftlint:disable:next function_body_length
    private func performLoad<A>(resource: Resource<A>, completion: @escaping (Result<A, WebError>) ->()) -> URLSessionDataTask? {
        var commonHeaders: HTTPHeaders = [API.Headers.language.rawValue: Locale.current.languageCode ?? "en"]
        if let token = accessTokenProvider.token {
            commonHeaders[API.Headers.accessToken.rawValue] = token
        }
        var newResouce = resource
        newResouce.headers = commonHeaders.merging(resource.headers) { $1 }

        let request = URLRequest(resource: newResouce)

        let task = URLSession.shared.dataTask(with: request) {[weak self] data, response, error in

            guard let self = self else { return }

            guard error == nil else {
                switch (error as? URLError)?.code {
                case .cancelled?:
                    break
                case .notConnectedToInternet?, .networkConnectionLost?:
                    completion(.failure(.noInternetConnection))
                default:
                    completion(.failure(.serverUnavailable))
                }
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(.serverUnavailable))
                return
            }

            #if DEBUG
            print("\(request.url?.absoluteString ?? "")")
            print("\(httpResponse.statusCode)")
            print(data.flatMap { String(data: $0, encoding: .utf8) } ?? "no data")
            #endif

            if (200..<300) ~= httpResponse.statusCode {
                let value  = data.flatMap(resource.parse)

                if value == nil && resource.shouldDebug {
                    self.logAPIError(
                        self.logger.logAPIError(
                            url: request.url?.absoluteString ?? "",
                            params: resource.params,
                            response: data,
                            code: httpResponse.statusCode
                        )
                    )
                }

                completion(Result(value: value, or: .wrongDataFormat))
            } else {
                let err: WebError = data
                    .flatMap { try? Coders.decoder.decode(APIError.self, from: $0) }
                    .map { .custom(httpResponse.statusCode, $0) }
                    ?? .other(httpResponse.statusCode, error)

                if resource.shouldDebug {
                    self.logAPIError(
                        self.logger.logAPIError(
                            url: request.url?.absoluteString ?? "",
                            params: resource.params,
                            response: data,
                            code: httpResponse.statusCode
                        )
                    )
                }

                completion(.failure(err))
            }
        }

        task.resume()

        return task

    }

    private func logAPIError(_ item: APIErrorItem) {
        #if !DEBUG
        guard Constants.Environments.api == .production else { return }
        let resource = Resource<EmptyResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "ios/report",
            method: .post,
            params: item.json,
            shouldDebug: false
        )
        load(resource: resource) { _ in }
        #endif
    }
}

extension WebClient {

    @discardableResult
    private func load<A>(
        resource: Resource<A>,
        atempt: Int,
        retries: Int,
        completion: @escaping (Result<A, WebError>) ->()
    ) -> URLSessionDataTask? {
        performLoad(resource: resource) {[weak self] result in
            switch result {
            case .success:
                completion(result)
            case .failure(let error):
                if atempt < retries && error.errorCode != 422 { // we don't need to retry for 422 error
                    DispatchQueue.global(qos: .userInitiated)
                        .asyncAfter(deadline: .now() + Constants.Intervals.retriesDelay) {
                            self?.load(
                                resource: resource,
                                atempt: atempt + 1,
                                retries: retries,
                                completion: completion
                            )
                    }
                } else {
                    completion(result)
                }
            }
        }
    }

    @discardableResult
    func load<A>(
        combinedResource: CombinedResource<A>,
        completion: @escaping (Result<A, WebError>) -> ()
    ) -> DownloadTask? {
        switch combinedResource {
        case let .value(value):
            completion(.success(value))
            return nil
        case let .single(resource):
            return load(resource: resource, completion: completion)
        case let .retry(resource, count):
            return load(resource: resource, atempt: 1, retries: count, completion: completion)
        case let .sequence(resource, transform):
            return load(combinedResource: resource) { result in
                do {
                    let value = try result.get()
                    self.load(combinedResource: transform(value), completion: completion)
                } catch {
                    completion(.failure(error.webError))
                }
            }
        case let .zipped(left, right, transform):
            let group = DispatchGroup()
            var resultA: Result<Any, WebError>!
            var resultB: Result<Any, WebError>!
            group.enter()
            load(combinedResource: left) { result in
                resultA = result
                group.leave()
            }
            group.enter()
            load(combinedResource: right) { result in
                resultB = result
                group.leave()
            }

            group.notify(queue: .global()) {
                do {
                    let valueA = try resultA.get()
                    let valueB = try resultB.get()
                    completion(.success(transform(valueA, valueB)))
                } catch {
                    completion(.failure(error.webError))
                }
            }
            return nil
        }
    }
}
