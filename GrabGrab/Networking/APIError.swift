//
//  APIError.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

@dynamicMemberLookup
final class APIError: Decodable, Error {
    var messages: [String]?
    var attributes: [String: APIError]

    private enum CodingKeys: String, CodingKey {
        case messages
        case attributes
    }

    private struct AttributesCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }

        var intValue: Int?
        init?(intValue: Int) {
            nil
        }
    }

    init(message: String) {
        messages = [message]
        attributes = [:]
    }

    init(from decoder: Decoder) throws {
        let containter = try decoder.container(keyedBy: CodingKeys.self)
        let messages = try? containter.decode([String].self, forKey: .messages)
        var attributes: [String: APIError] = [:]
        if let attributesContainer = try? containter.nestedContainer(
            keyedBy: AttributesCodingKeys.self,
            forKey: .attributes
        ) {
            for key in attributesContainer.allKeys {
                attributes[key.stringValue] = try attributesContainer.decode(
                    APIError.self,
                    forKey: AttributesCodingKeys(stringValue: key.stringValue)! // swiftlint:disable:this force_unwrapping
                )
            }
        }

        if messages == nil && attributes.isEmpty {
            throw DecodingError.keyNotFound(
                CodingKeys.messages,
                DecodingError.Context(codingPath: [], debugDescription: "")
            )
        }

        self.messages = messages
        self.attributes = attributes
    }

    subscript(dynamicMember key: String) -> APIError? {
        attributes[key]
    }
}

extension APIError: LocalizedError {
    public var errorDescription: String? {
        messages?.joined(separator: ". ") ?? ""
    }
}
