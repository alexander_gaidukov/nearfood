//
//  API.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct API {
    static var baseURL: URL {
        switch Constants.Environments.api {
        case .development:
            // swiftlint:disable:next force_unwrapping
            return URL(string: "https://api.grab-grab.ru")!
        case .production:
            // swiftlint:disable:next force_unwrapping
            return URL(string: "https://api.nearfood.ru")!
        }
    }

    static var cdnURL: URL {
        // swiftlint:disable:next force_unwrapping
        URL(string: baseURL.absoluteString.replacingOccurrences(of: "api", with: "cdn"))!
    }

    enum Headers: String {
        case accessToken = "X-Access-Token"
        case language = "Content-Language"
    }
}
