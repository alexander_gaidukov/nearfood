//
//  WebError.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum WebError: Error {
    case noInternetConnection
    case serverUnavailable
    case custom(Int, APIError)
    case wrongDataFormat
    case other(Int, Error?)
}

extension WebError {
    var errorCode: Int? {
        switch self {
        case let .custom(code, _), let .other(code, _):
            return code
        default:
            return nil
        }
    }
    var apiError: APIError? {
        guard case let .custom(_, error) = self else { return nil }
        return error
    }
}

extension WebError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noInternetConnection:
            return LocalizedStrings.Error.noInternetDescription
        case .serverUnavailable:
            return LocalizedStrings.Error.commonDescription
        case .wrongDataFormat:
            return LocalizedStrings.Error.commonDescription
        case .other:
            return LocalizedStrings.Error.commonDescription
        case let .custom(_, apiError):
            return apiError.localizedDescription
        }
    }
}

extension WebError {
    var title: String {
        if case .noInternetConnection = self {
            return LocalizedStrings.Error.noInternetTitle
        }
        return LocalizedStrings.Error.commonTitle
    }
}
