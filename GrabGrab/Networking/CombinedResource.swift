//
//  CombinedResource.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

indirect enum CombinedResource<A> {
    case value(A)
    case single(Resource<A>)
    case retry(Resource<A>, Int)
    case sequence(CombinedResource<Any>, (Any) -> CombinedResource<A>)
    case zipped(CombinedResource<Any>, CombinedResource<Any>, (Any, Any) -> A)
}

extension CombinedResource {
    var asAny: CombinedResource<Any> {
        switch self {
        case let .single(resource):
            return .single(resource.map { $0 })
        case let .retry(resource, count):
            return .retry(resource.map { $0 }, count)
        case let .sequence(resource, transform):
            return .sequence(resource, { transform($0).asAny })
        case let .zipped(left, right, transform):
            return .zipped(left, right, { transform($0, $1) })
        case let .value(value):
            return .value(value as Any)
        }
    }

    func flatMap<B>(_ transform: @escaping (A) -> CombinedResource<B>) -> CombinedResource<B> {
        // swiftlint:disable:next force_cast
        return CombinedResource<B>.sequence(self.asAny, { transform($0 as! A) })
    }

    func map<B>(_ transform: @escaping (A) -> B) -> CombinedResource<B> {
        switch self {
        case let .value(value):
            return .value(transform(value))
        case let .single(resource):
            return .single(resource.map(transform))
        case let .retry(resource, count):
            return .retry(resource.map(transform), count)
        case let .sequence(resource, modifier):
            return .sequence(resource, {
                modifier($0).map(transform)
            })
        case let .zipped(left, right, modifier):
            return .zipped(left, right, {
                transform(modifier($0, $1))
            })
        }
    }

    func zipWith<B, C>(_ other: CombinedResource<B>, _ combine: @escaping (A, B) -> C) -> CombinedResource<C> {
        CombinedResource<C>.zipped(self.asAny, other.asAny, {
            // swiftlint:disable:next force_cast
            combine($0 as! A, $1 as! B)
        })
    }

    func zip<B>(_ other: CombinedResource<B>) -> CombinedResource<(A, B)> {
        zipWith(other) { ($0, $1) }
    }
}
