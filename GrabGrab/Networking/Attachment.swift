//
//  Attachment.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.

import Foundation
import MobileCoreServices

struct Attachment {
    var data: Data
    var mimeType: String
    var name: String
}

extension Attachment {

    private static func mimeType(for pathExtension: String) -> String {
        guard
            let uti = UTTypeCreatePreferredIdentifierForTag(
                kUTTagClassFilenameExtension,
                pathExtension as NSString,
                nil
            )?.takeRetainedValue(),
            let mimeType = UTTypeCopyPreferredTagWithClass(
                uti,
                kUTTagClassMIMEType
            )?.takeRetainedValue() else {
            return "application/octet-stream"
        }

        return mimeType as String
    }

    init(path: String) throws {
        let url = URL(fileURLWithPath: path)
        data = try Data(contentsOf: url)
        name = url.lastPathComponent
        mimeType = Attachment.mimeType(for: url.pathExtension)
    }
}
