//
//  Coders.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum DateError: String, Error {
    case invalidDate
}

enum Coders {
    static let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .custom({
            let container = try $0.singleValueContainer()
            var value = try container.decode(String.self)
            // remove milliseconds
            value = value.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
            guard let date = Formatters.isoDateFormatter.date(from: value) else {
                throw DateError.invalidDate
            }
            return date
        })
        return decoder
    }()

    static let encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        encoder.dateEncodingStrategy = .custom({
            let value = Formatters.isoDateFormatter.string(from: $0)
            var container = $1.singleValueContainer()
            try container.encode(value)
        })
        return encoder
    }()
}
