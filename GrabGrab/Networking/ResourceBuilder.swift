//
//  ResourceBuilder.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 31.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import Kingfisher

// swiftlint:disable file_length
// swiftlint:disable:next type_body_length
struct ResourceBuilder {

    // MARK: - Login

    static func authenticationResource(phone: String) -> Resource<AuthenticationResponse> {
        Resource<AuthenticationResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/authenticate",
            method: .post,
            params: ["phone": phone]
        )
    }

    static func phoneConfirmationResource(
        authResponse: AuthenticationResponse,
        code: String,
        device: Device
    ) ->  CombinedResource<SignInResponse> {
        let signInResource = Resource<SignInResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/login",
            method: .post,
            params: ["phone": authResponse.phone, "uuid": authResponse.uuid, "code": code, "device": device.params]
        )
        return signInResource.combined.flatMap { response in
            guard response.owner.isActive else { return .value(response) }
            return ResourceBuilder.customerInfoResource(
                customerUUID: response.owner.uuid,
                accessToken: response.token
            ).map { info in
                var newResponse = response
                newResponse.owner.locations = info.locations
                newResponse.owner.paymentMethods = info.bankCards
                newResponse.owner.bonuses = info.bonuses
                return newResponse
            }
        }
    }

    static func logoutResource(deviceUUID: String) -> Resource<EmptyResponse> {
        Resource(baseURL: API.baseURL, path: "devices/\(deviceUUID)/logout", method: .post) { _ in EmptyResponse() }
    }

    static func currentUserResource() -> CombinedResource<Customer> {
        let customerResource = Resource<SignInResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "me"
        ).map(\.owner)
        return customerResource.combined.flatMap { customer in
            guard customer.isActive else { return .value(customer) }
            return ResourceBuilder.customerInfoResource(
                customerUUID: customer.uuid
            ).map { info in
                var newCustomer = customer
                newCustomer.locations = info.locations
                newCustomer.paymentMethods = info.bankCards
                newCustomer.bonuses = info.bonuses
                return newCustomer
            }
        }
    }

    static func customerInfoResource(
        customerUUID: String,
        accessToken: String? = nil
    ) -> CombinedResource<CustomerInfo> {
        let locationResource = ResourceBuilder.locationsResource(
            customerUUID: customerUUID,
            accessToken: accessToken
        ).combined
        let paymentMethodsResource = ResourceBuilder.paymentMethodsResource(
            userUUID: customerUUID,
            accessToken: accessToken
        ).combined
        let bonusesResource = ResourceBuilder.bonusesResource(
            userUUID: customerUUID,
            accessToken: accessToken
        ).combined
        return locationResource.zip(paymentMethodsResource).zip(bonusesResource).map {
            CustomerInfo(locations: $0.0, bankCards: $0.1, bonuses: $1)
        }
    }

    static func phoneCountryCodesResource() -> Resource<CountriesResponse> {
        Resource<CountriesResponse>(jsonDecoder: Coders.decoder, baseURL: API.baseURL, path: "phone_country_codes")
    }

    static func updateCustomerResource(name: String, uuid: String) -> Resource<Customer> {
        Resource<Customer>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(uuid)",
            method: .patch,
            params: ["name": name]
        )
    }

    static func citiesResource() -> Resource<CitiesResponse> {
        Resource<CitiesResponse>(jsonDecoder: Coders.decoder, baseURL: API.baseURL, path: "cities")
    }

    static func activateUserResource(uuid: String, invite: String?, accessToken: String? = nil) -> Resource<Customer> {
        var headers: HTTPHeaders = [:]
        if let token = accessToken {
            headers[API.Headers.accessToken.rawValue] = token
        }

        let params: JSON = invite.map { ["code": $0] } ?? [:]

        return Resource<SignInResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(uuid)/activate",
            method: .post,
            params: params,
            headers: headers
        ).map { $0.owner }
    }

    // MARK: - Push Notifications
    static func registerDeviceForPNResource(
        customerUUID: String,
        device: Device,
        token: String,
        environment: String
    ) -> Resource<EmptyResponse> {
        var params = device.params
        params["token"] = token
        params["environment"] = environment
        return Resource(
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/devices",
            method: .post,
            params: params
        ) { _ in EmptyResponse() }
    }

    static func subscribeDevice(deviceUUID: String, forKitchen kitchenUUID: String) -> Resource<EmptyResponse> {
        Resource(
            baseURL: API.baseURL,
            path: "/kitchens/\(kitchenUUID)/subscribe",
            method: .post,
            params: ["device_uuid": deviceUUID]
        )
    }

    // MARK: - Addresses

    static func checkLocationResource(
        latitude: Double,
        longitude: Double,
        customerUUID: String
    ) -> Resource<LocationCheckResponse> {
        Resource<LocationCheckResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "locations/check",
            method: .post,
            params: ["latitude": latitude, "longitude": longitude]
        )
    }

    static func availableAreasResource() -> Resource<[Area]> {
        Resource<AvailableAreasResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "kitchen_availability_areas/all"
        ).map(\.areas)
    }

    static func saveLocationResource(
        location: Location,
        customerUUID: String
    ) -> CombinedResource<(Location, [Location])> {
        let saveResource = Resource<Location>(
            jsonDecoder: Coders.decoder,
            jsonEncoder: Coders.encoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/locations",
            method: .post,
            params: location
        )
        let loadResource = locationsResource(customerUUID: customerUUID)
        return saveResource.combined.flatMap { location in loadResource.combined.map { (location, $0) }}
    }

    static func locationsResource(customerUUID: String, accessToken: String? = nil) -> Resource<[Location]> {
        var headers: HTTPHeaders = [:]
        if let token = accessToken {
            headers[API.Headers.accessToken.rawValue] = token
        }
        return Resource<LocationsResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/locations",
            headers: headers
        ).map { $0.locations.elements }
    }

    static func removeLocationResource(location: Location, customerUUID: String) -> CombinedResource<[Location]> {
        let removeResource = Resource(
            baseURL: API.baseURL,
            path: "locations/\(location.uuid)",
            method: .delete
        )
        let loadResource = locationsResource(customerUUID: customerUUID)
        return removeResource.combined.flatMap {_ in loadResource.combined}
    }

    static func updateLocationResource(location: Location, customerUUID: String) -> CombinedResource<[Location]> {
        let updateResource = Resource<Location>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "locations/\(location.uuid)",
            method: .patch,
            params: location.updateParams
        )
        let loadResource = locationsResource(customerUUID: customerUUID)
        return updateResource.combined.flatMap {_ in loadResource.combined}
    }

    // MARK: - Food

    static func storefrontResource(userUUID: String, locationUUID: String) -> Resource<Storefront> {
        Resource<Storefront>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(userUUID)/storefront",
            method: .post,
            params: ["location_uuid": locationUUID]
        )
    }

    // MARK: - Cart

    static func estimateCartResource(
        userUUID: String,
        locationUUID: String,
        dishes: [String: Int],
        combos: [String: Int],
        coupon: String?
    ) -> Resource<Estimation> {
        var items: JSON = [:]
        for (key, value) in dishes {
            items[key] = ["count": value, "kind": "dish"]
        }

        for (key, value) in combos {
            items[key] = ["count": value, "kind": "combo"]
        }

        var params: JSON = ["location_uuid": locationUUID, "cart": items]

        if let coupon = coupon, !coupon.isEmpty {
            params["coupon"] = coupon
        }

        return Resource<Estimation>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(userUUID)/estimate",
            method: .post,
            params: params
        )
    }

    // MARK: - Coupons
    static func validateCoupon(_ code: String, customerUUID: String) -> Resource<EmptyResponse> {
        Resource<EmptyResponse>(
            baseURL: API.baseURL,
            path: "/customers/\(customerUUID)/coupons/validate",
            method: .post,
            params: ["code": code]
        ) { _ in EmptyResponse() }
    }

    // MARK: - Bonuses
    static func bonusesResource(userUUID: String, accessToken: String? = nil) -> Resource<Double> {
        var headers: HTTPHeaders = [:]
        if let token = accessToken {
            headers[API.Headers.accessToken.rawValue] = token
        }
        return Resource<BonusesResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(userUUID)/balance",
            headers: headers
        ).map(\.amount)
    }

    // MARK: - Payments
    static func paymentMethodsResource(userUUID: String, accessToken: String? = nil) -> Resource<[BankCard]> {
        var headers: HTTPHeaders = [:]
        if let token = accessToken {
            headers[API.Headers.accessToken.rawValue] = token
        }
        return Resource<PaymentMethodsResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(userUUID)/bank_cards",
            headers: headers
        ).map { $0.cards.elements }
    }

    static func addNewPaymentMethodsResource(
        userUUID: String,
        token: String,
        name: String
    ) -> Resource<AddCardResponse> {
        Resource<AddCardResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(userUUID)/bank_cards",
            method: .post,
            params: ["payment_token": token, "name": name]
        )
    }

    // swiftlint:disable:next identifier_name
    static func confirmNewPaymentMethodsResource(md: String, response: String) -> Resource<BankCard> {
        Resource<BankCard>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "bank_cards/confirm_3ds",
            method: .post,
            params: ["md": md, "payment_response": response]
        )
    }

    static func removePaymentMethodResource(paymentMethodUUID: String) -> Resource<EmptyResponse> {
        Resource(
            baseURL: API.baseURL,
            path: "/bank_cards/\(paymentMethodUUID)",
            method: .delete
        )
    }

    // MARK: - Orders
    // swiftlint:disable:next function_parameter_count
    static func makeOrderResource(
        cartUUID: String,
        paymentMethod: PaymentMethod,
        cutlery: Int,
        contactless: Bool,
        recipient: Recipient?,
        note: String
    ) -> Resource<Order> {
        var paymentMethodParams: JSON = ["kind": paymentMethod.kind]

        if let data = paymentMethod.data {
            paymentMethodParams["data"] = data
        }

        var params: JSON = [
            "estimation_uuid": cartUUID,
            "payment_method": paymentMethodParams,
            "contactless": contactless,
            "cutlery_count": cutlery,
            "note": note
        ]

        if let recipient = recipient {
            params["recipient"] = recipient.json
        }

        return Resource<Order>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "orders",
            method: .post,
            params: params
        )
    }

    static func activeOrdersResource(customerUUID: String) -> Resource<[Order]> {
        Resource<OrdersResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/orders/active"
        ).map { $0.orders.elements }
    }

    static func ordersResource(customerUUID: String, limit: Int, offset: Int) -> Resource<[Order]> {
        Resource<OrdersResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/orders/all",
            params: ["limit": limit, "offset": offset]
        ).map { $0.orders.elements }
    }

    static func orderDetailsResource(orderUUID: String) -> Resource<Order> {
        Resource<Order>(jsonDecoder: Coders.decoder, baseURL: API.baseURL, path: "orders/\(orderUUID)")
    }

    static func cancelOrderResource(order: Order) -> Resource<Order> {
        Resource<Order>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "orders/\(order.uuid)/cancel",
            method: .post
        )
    }

    static func cancelOrderFeesResource(order: Order) -> Resource<Fees> {
        Resource<Fees>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "orders/\(order.uuid)/cancel",
            method: .post,
            params: ["estimate": true]
        )
    }

    static func updateOrderResource(
        order: Order,
        isContactless: Bool? = nil,
        paymentMethod: PaymentMethod? = nil,
        note: String? = nil
    ) -> Resource<Order> {
        var params: JSON = [:]
        if let isContactless = isContactless {
            params["contactless"] = isContactless
        }

        if let paymentMethod = paymentMethod {
            var paymentMethodParams: JSON = ["kind": paymentMethod.kind]
            if let data = paymentMethod.data {
                paymentMethodParams["data"] = data
            }
            params["payment_method"] = paymentMethodParams
        }

        if let note = note {
            params["note"] = note
        }

        return Resource<Order>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "orders/\(order.uuid)",
            method: .patch,
            params: params
        )
    }

    static func tracks(for order: Order) -> Resource<PolylineResponse> {
        Resource<PolylineResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "orders/\(order.uuid)/tracks"
        )
    }

    // MARK: - Chat

    enum MessagesDirection: String {
        case next
        case prev
    }

    static func messagesResource(
        customerUUID: String,
        date: Date?,
        direction: MessagesDirection?,
        limit: Int
    ) -> Resource<[Message]> {
        var params: JSON = ["limit": limit]
        if let date = date, let direction = direction {
            let interval = direction == .next ? floor(date.timeIntervalSince1970) : ceil(date.timeIntervalSince1970)
            params["at"] = Int(interval)
            params["direction"] = direction.rawValue
        }

        return Resource<MessagesResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/messages",
            params: params
        ).map { $0.messages.elements }
    }

    // swiftlint:disable:next function_parameter_count
    static func sendMessageResource(
        customerUUID: String,
        body: String?,
        attachment: Photo?,
        orderUUID: String?,
        lastMessageDate: Date?,
        limit: Int
    ) -> CombinedResource<[Message]> {
        var sendParams: JSON = [:]

        if let body = body {
            sendParams["body"] = body
        }

        if let attachment = attachment {
            sendParams["attachments"] = [attachment.digest]
        }

        if let orderUUID = orderUUID {
            sendParams["properties"] = ["order_uuid": orderUUID]
        }
        let sendResource = Resource(
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/messages",
            method: .post,
            params: sendParams
        )
        let loadResource = messagesResource(
            customerUUID: customerUUID,
            date: lastMessageDate,
            direction: .next,
            limit: limit
        )
        return sendResource.combined.flatMap { _ in loadResource.combined }
    }

    static func sendImageMessageResource(
        customerUUID: String,
        image: UIImage,
        orderUUID: String?,
        lastMessageDate: Date?,
        limit: Int
    ) -> CombinedResource<[Message]> {
        let attachment = Attachment(
            data: image.jpegData(compressionQuality: 1.0)!, // swiftlint:disable:this force_unwrapping
            mimeType: "image/jpeg",
            name: "image.jpeg"
        )
        let uploadResource = Resource<Photo>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "files",
            method: .post,
            attachments: ["file": attachment]
        )
        return uploadResource.combined.flatMap {
            ResourceBuilder.sendMessageResource(
                customerUUID: customerUUID,
                body: nil,
                attachment: $0,
                orderUUID: orderUUID,
                lastMessageDate: lastMessageDate,
                limit: limit
            )
        }
    }

    // MARK: - Review
    static func feedbackResource(feedbackUUID: String, rating: Int, note: String) -> Resource<Feedback> {
        Resource<Feedback>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "feedbacks/\(feedbackUUID)",
            method: .patch,
            params: ["rating": rating, "note": note]
        )
    }

    // MARK: - App Version

    static func appVersionResource() -> Resource<AppVersion> {
        Resource<AppVersion>(jsonDecoder: Coders.decoder, baseURL: API.baseURL, path: "ios/versions")
    }

    // MARK: - Company Info
    static func companyInfoResource() -> Resource<CompanyInfo> {
        Resource<CompanyInfo>(jsonDecoder: Coders.decoder, baseURL: API.baseURL, path: "about")
    }

    // MARK: - Invites
    static func invitesResource(customerUUID: String) -> Resource<[Invite]> {
        Resource<InvitationsResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/invites"
        ).map { $0.invites.elements }
    }

    // MARK: - Recipients
    static func addRecipientResource(_ recipient: Recipient, to order: Order) -> CombinedResource<Order> {
        let orderResource = orderDetailsResource(orderUUID: order.uuid)
        let addResource = Resource<EmptyResponse>(
            baseURL: API.baseURL,
            path: "orders/\(order.uuid)/recipient",
            method: .post,
            params: recipient.json) { _ in EmptyResponse() }
        return addResource.combined.flatMap { _ in orderResource.combined }
    }

    static func updateRecipientResource(_ recipient: Recipient, in order: Order) -> CombinedResource<Order> {
        let orderResource = orderDetailsResource(orderUUID: order.uuid)
        let updateResource = Resource<EmptyResponse>(
            baseURL: API.baseURL,
            path: "recipients/\(recipient.uuid)",
            method: .patch,
            params: recipient.json) { _ in EmptyResponse() }
        return updateResource.combined.flatMap { _ in orderResource.combined }
    }

    static func removeRecipientResource(_ recipient: Recipient, from order: Order) -> CombinedResource<Order> {
        let orderResource = orderDetailsResource(orderUUID: order.uuid)
        let updateResource = Resource<EmptyResponse>(
            baseURL: API.baseURL,
            path: "recipients/\(recipient.uuid)",
            method: .delete) { _ in EmptyResponse() }
        return updateResource.combined.flatMap { _ in orderResource.combined }
    }

    // MARK: - Tips
    static func updateTipsSettingsResource(_ tips: TipsOption, customerUUID: String) -> Resource<Customer> {
        Resource(
            jsonDecoder: Coders.decoder,
            jsonEncoder: Coders.encoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/tips/settings",
            method: .post,
            params: tips
        )
    }

    static func tipsOptionsResource(customerUUID: String) -> Resource<[TipsOption]> {
        Resource<TipsOptionsResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "customers/\(customerUUID)/tips/options"
        ).map(\.options)
    }

    static func updateTipsSettingsResource(_ tips: TipsOption, orderUUID: String) -> Resource<OrderTips> {
        Resource(
            jsonDecoder: Coders.decoder,
            jsonEncoder: Coders.encoder,
            baseURL: API.baseURL,
            path: "orders/\(orderUUID)/tips/settings",
            method: .post,
            params: tips
        )
    }

    static func tipsOptionsResource(orderUUID: String) -> Resource<[TipsOption]> {
        Resource<TipsOptionsResponse>(
            jsonDecoder: Coders.decoder,
            baseURL: API.baseURL,
            path: "orders/\(orderUUID)/tips/options"
        ).map(\.options)
    }
}
