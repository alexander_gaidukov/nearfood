//
//  Resource.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum EncodingError: Error {
    case wrongObject
}

extension Encodable {
    func params(encoder: JSONEncoder)throws -> JSON {
        let data = try encoder.encode(self)
        guard let obj = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? JSON else {
            throw EncodingError.wrongObject
        }
        return obj
    }
}

struct Resource<A> {
    let baseURL: URL
    let path: Path
    let method: RequestMethod
    var params: JSON
    var headers: HTTPHeaders
    var attachments: [String: Attachment]
    let shouldDebug: Bool
    private var boundary: String = {
        "Boundary-\(UUID().uuidString)"
    }()
    let parse: (Data) -> A?
}

extension Resource {
    init(baseURL: URL,
         path: String,
         method: RequestMethod = .get,
         params: JSON = [:],
         headers: HTTPHeaders = [:],
         attachments: [String: Attachment] = [:],
         shouldDebug: Bool = true,
         parse: @escaping (Data) -> A?) {

        self.baseURL = baseURL
        self.path = Path(path)
        self.method = method
        self.params = params
        self.headers = headers
        self.attachments = attachments
        self.parse = parse
        self.shouldDebug = shouldDebug
    }
}

extension Resource where A: Decodable {
    init(jsonDecoder: JSONDecoder,
         baseURL: URL,
         path: String,
         method: RequestMethod = .get,
         params: JSON = [:],
         headers: HTTPHeaders = [:],
         attachments: [String: Attachment] = [:],
         shouldDebug: Bool = true) {

        self.baseURL = baseURL
        self.path = Path(path)
        self.method = method
        self.params = params
        self.headers = headers
        self.attachments = attachments
        self.shouldDebug = shouldDebug
        self.parse = {
            try? jsonDecoder.decode(A.self, from: $0)
        }
    }

    init(jsonDecoder: JSONDecoder,
         jsonEncoder: JSONEncoder,
         baseURL: URL,
         path: String,
         method: RequestMethod = .get,
         params: Encodable,
         headers: HTTPHeaders = [:],
         attachments: [String: Attachment] = [:],
         shouldDebug: Bool = true) {

        self.init(
            jsonDecoder: jsonDecoder,
            baseURL: baseURL,
            path: path,
            method: method,
            params: (try? params.params(encoder: jsonEncoder)) ?? [:],
            headers: headers,
            attachments: attachments,
            shouldDebug: shouldDebug
        )
    }
}

extension Resource {

    func map<B>(_ transform: @escaping (A) -> B) -> Resource<B> {
        Resource<B>(
            baseURL: baseURL,
            path: path,
            method: method,
            params: params,
            headers: headers,
            attachments: attachments,
            shouldDebug: shouldDebug,
            boundary: boundary
        ) { self.parse($0).map(transform) }
    }

    func compactMap<B>(_ transform: @escaping (A) -> B?) -> Resource<B> {
        Resource<B>(
            baseURL: baseURL,
            path: path,
            method: method,
            params: params,
            headers: headers,
            attachments: attachments,
            shouldDebug: shouldDebug,
            boundary: boundary
        ) { self.parse($0).flatMap(transform) }
    }
}

extension Resource {
    var combined: CombinedResource<A> {
        .single(self)
    }

    func retry(count: Int) -> CombinedResource<A> {
        .retry(self, count)
    }
}

extension Resource {
    var body: Data? {
        guard !attachments.isEmpty else {
            return try? JSONSerialization.data(withJSONObject: params, options: [])
        }

        var body = Data()

        // swiftlint:disable force_unwrapping
        params.forEach {
            body.append("--\(boundary)\r\n".data(using: .utf8)!)
            body.append("Content-Disposition: form-data; name=\"\($0.key)\"\r\n\r\n".data(using: .utf8)!)
            body.append("\($0.value)\r\n".data(using: .utf8)!)
        }

        attachments.forEach { element in
            let attachment = element.value
            body.append("--\(boundary)\r\n".data(using: .utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(element.key)\"; filename=\"\(attachment.name)\"\r\n".data(using: .utf8)!)
            body.append("Content-Type: \(attachment.mimeType)\r\n\r\n".data(using: .utf8)!)
            body.append(attachment.data)
            body.append("\r\n".data(using: .utf8)!)
        }

        body.append("--\(boundary)--\r\n".data(using: .utf8)!)
        // swiftlint:enable force_unwrapping
        return body
    }

    var contentType: String {
        if attachments.isEmpty {
            return "application/json"
        }
        return "multipart/form-data; boundary=\(boundary)"
    }
}
