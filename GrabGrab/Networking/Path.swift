//
//  Path.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

struct Path {
    private var components: [String]

    var absolutePath: String {
        "/" + components.joined(separator: "/")
    }

    init(_ path: String) {
        components = path.components(separatedBy: "/").filter({ !$0.isEmpty })
    }

    mutating func append(path: Path) {
        components += path.components
    }

    func appending(path: Path) -> Path {
        var copy = self
        copy.append(path: path)
        return copy
    }
}
