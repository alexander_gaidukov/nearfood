//
//  NewPaymentMethodCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class NewPaymentMethodCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = LocalizedStrings.PaymentMethod.addNew
    }
}
