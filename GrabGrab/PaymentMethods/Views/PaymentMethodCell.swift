//
//  PaymentMethodCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct PaymentMethodCellModel {
    var image: UIImage?
    var title: String
    var isConfirmed: Bool
    var isSelected: Bool
}

extension PaymentMethodCellModel {
    init(paymentMethod: PaymentMethod, isSelected: Bool) {
        self.isSelected = isSelected
        self.title = paymentMethod.title
        self.image = paymentMethod.image
        self.isConfirmed = paymentMethod.isEnabled
    }
}

class PaymentMethodCell: UITableViewCell {
    @IBOutlet private weak var cardImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var unconfirmedView: UIView!
    @IBOutlet private weak var unconfirmedLabel: UILabel!
    @IBOutlet private weak var selectionImageView: UIImageView!

    private var model: PaymentMethodCellModel! {
        didSet {
            updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        unconfirmedLabel.text = LocalizedStrings.PaymentMethod.notConfirmed
    }

    func configure(model: PaymentMethodCellModel) {
        self.model = model
    }

    private func updateUI() {
        titleLabel.text = model.title
        unconfirmedView.isHidden = model.isConfirmed
        cardImageView.image = model.image

        selectionImageView.isHidden = !model.isConfirmed
        selectionImageView.image = model.isSelected ? #imageLiteral(resourceName: "checkbox_selected") : #imageLiteral(resourceName: "checkbox")
    }
}
