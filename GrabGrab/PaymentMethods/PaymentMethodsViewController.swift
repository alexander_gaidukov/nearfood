//
//  PaymentMethodsViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class PaymentMethodsViewController: UIViewController, PopupViewController {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var contentView: UIView!

    var presenter: PaymentMethodsPresenterType!
    private let disposableBag = DisposableBag()

    private let cellHeight: CGFloat = 56
    private let maxHeight: CGFloat = 500
    private let minHeight: CGFloat = 250

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    private func configureViews() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        panGestureRecognizer.delegate = self
        contentView.addGestureRecognizer(panGestureRecognizer)

        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)

        tableView.tableFooterView = UIView(frame: .zero)
        tableView.contentInset = contentInset
        tableView.register(cellType: PaymentMethodCell.self)
        tableView.register(cellType: NewPaymentMethodCell.self)

        let tableViewContentHeight = tableView.contentInset.top
            + tableView.contentInset.bottom
            + cellHeight * CGFloat(presenter.state.paymentMethods.count + 1)
        tableView.isScrollEnabled = tableViewContentHeight > maxHeight

        heightConstraint.constant = max(minHeight, min(maxHeight, tableViewContentHeight))
    }

    private func configureBindings() {
        presenter.state.$paymentMethods.observe { [weak self] _ in self?.tableView.reloadData() }.addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    private func removePaymentMethod(_ paymentMethod: BankCard) {
        let alert = UIAlertController(
            title: LocalizedStrings.PaymentMethod.removalConfrimation(for: paymentMethod.title),
            message: "",
            preferredStyle: .alert
        )

        let cancelAction = UIAlertAction(title: LocalizedStrings.Common.cancel, style: .cancel, handler: nil)
        alert.addAction(cancelAction)

        let removeAction = UIAlertAction(title: LocalizedStrings.Common.remove, style: .destructive) {_ in
            self.presenter.deleteMethod(paymentMethod)
        }
        alert.addAction(removeAction)

        present(alert, animated: true, completion: nil)
    }

    @objc func close() {
        presenter.close()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }
}

extension PaymentMethodsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.state.paymentMethods.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < presenter.state.paymentMethods.count else {
            let cell: NewPaymentMethodCell = tableView.dequeue(forIndexPath: indexPath)
            return cell
        }
        let cell: PaymentMethodCell = tableView.dequeue(forIndexPath: indexPath)
        let paymentMethod = presenter.state.paymentMethods[indexPath.row]
        let model = PaymentMethodCellModel(paymentMethod: paymentMethod.method, isSelected: paymentMethod.isSelected)
        cell.configure(model: model)
        return cell
    }
}

extension PaymentMethodsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard indexPath.row < presenter.state.paymentMethods.count else {
            presenter.addNew()
            return
        }
        presenter.selectMethod(presenter.state.paymentMethods[indexPath.row].method)
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        guard indexPath.row < presenter.state.paymentMethods.count else { return .none }
        let paymentMethod = presenter.state.paymentMethods[indexPath.row]
        if case .card = paymentMethod.method { return .delete }
        return .none
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete, case let .card(card) = presenter.state.paymentMethods[indexPath.row].method {
            removePaymentMethod(card)
        }
    }
}

extension PaymentMethodsViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(
        _ gestureRecognizer: UIGestureRecognizer,
        shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer
    ) -> Bool {
        true
    }
}

extension PaymentMethodsViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension PaymentMethodsViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
