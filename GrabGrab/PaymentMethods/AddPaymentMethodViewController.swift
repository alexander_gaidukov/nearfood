//
//  AddPaymentMethodViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 23.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class AddPaymentMethodViewController: UIViewController, PopupViewController, KeyboardAvoiding {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var keyboardHidingView: UIView!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var scrollView: UIScrollView!

    @IBOutlet private weak var cardNumberTextField: GGTextField!
    @IBOutlet private weak var holderNameTextField: GGTextField!
    @IBOutlet private weak var expirationDateTextField: GGTextField!
    @IBOutlet private weak var cvvTextField: GGTextField!

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!

    var presenter: AddPaymentMethodPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
    }

    private func configureView() {

        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        sendButton.setTitle(LocalizedStrings.PaymentMethod.New.button, for: .normal)
        titleLabel.text = LocalizedStrings.PaymentMethod.New.title
        descriptionLabel.text = LocalizedStrings.PaymentMethod.New.description
        cardNumberTextField.placeholder = LocalizedStrings.PaymentMethod.New.cardNumberPlaceholder
        holderNameTextField.placeholder = LocalizedStrings.PaymentMethod.New.holderNamePlaceholder
        expirationDateTextField.placeholder = LocalizedStrings.PaymentMethod.New.expirationDatePlaceholder
        cvvTextField.placeholder = LocalizedStrings.PaymentMethod.New.cvvPlaceholder

        configureToolbar()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        contentView.addGestureRecognizer(panGestureRecognizer)

        keyboardHidingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
    }

    private func configureBindings() {

        presenter.state.$cardNumber.bind(to: cardNumberTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$cardNumberError.bind(to: cardNumberTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.$holderName.bind(to: holderNameTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$holderNameError.bind(to: holderNameTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.$expirationDate.bind(to: expirationDateTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$expirationDateError.bind(to: expirationDateTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.$cvv.bind(to: cvvTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.$cvvError.bind(to: cvvTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    private func configureToolbar() {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50.0))
        toolbar.barStyle = .default
        toolbar.barTintColor = .tabbarBackground
        toolbar.tintColor = .presentedNavigationTint
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideKeyboard))
        ]
        toolbar.sizeToFit()
        cardNumberTextField.inputAccessoryView = toolbar
        cvvTextField.inputAccessoryView = toolbar
        expirationDateTextField.inputAccessoryView = toolbar
    }

    private func hasFirstResponder() -> Bool {
        cardNumberTextField.isFirstResponder
            || holderNameTextField.isFirstResponder
            || expirationDateTextField.isFirstResponder
            || cvvTextField.isFirstResponder
    }

    @IBAction private func sendTapped() {
        view.endEditing(true)
        presenter.send()
    }

    @IBAction private func scannerTapped() {
        view.endEditing(true)
        presenter.scanCard()
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        if textField == cardNumberTextField {
            presenter.changeCardNumber(text)
        } else if textField == holderNameTextField {
            presenter.changeHolderName(text)
        } else if textField == expirationDateTextField {
            presenter.changeExpirationDate(text)
        } else if textField == cvvTextField {
            presenter.changeCVV(text)
        }
    }

    @objc func close() {
        if hasFirstResponder() {
            hideKeyboard()
        } else {
            presenter.close()
        }
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }

}

extension AddPaymentMethodViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension AddPaymentMethodViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}

extension AddPaymentMethodViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboardHidingView.isHidden = false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        keyboardHidingView.isHidden = true
    }
}
