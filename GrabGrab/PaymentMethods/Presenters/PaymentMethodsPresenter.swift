//
//  PaymentMethodsPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol PaymentMethodsPresenterType {
    var state: PaymentMethodsViewState { get }
    func selectMethod(_ paymentMethod: PaymentMethod)
    func deleteMethod(_ paymentMethod: BankCard)
    func addNew()
    func close()
}

enum PaymentMethodsAction {
    case select(PaymentMethod)
    case addNew
    case close
}

struct PaymentMethodsViewState {
    @Observable
    var paymentMethods: [(method: PaymentMethod, isSelected: Bool)]

    @Observable
    var isLoading: Bool = false

    @Observable
    var error: WebError?
}

final class PaymentMethodsPresenter: PaymentMethodsPresenterType {

    var state: PaymentMethodsViewState

    private let completion: (PaymentMethodsAction) -> ()
    private let customerDataProvider: CustomerDataProviderType
    private let webClient: WebClientType
    private let markCurrentPayment: Bool

    init(
        customerDataProvider: CustomerDataProviderType,
        markCurrentPayment: Bool,
        webClient: WebClientType,
        completion: @escaping (PaymentMethodsAction) -> ()
    ) {
        let selectedPaymentMethod = markCurrentPayment ? customerDataProvider.paymentMethod : nil
        state = PaymentMethodsViewState(
            paymentMethods: customerDataProvider.paymentMethods.map {
                ($0, $0.identifier == selectedPaymentMethod?.identifier)
            }
        )
        self.completion = completion
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        self.markCurrentPayment = markCurrentPayment
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(paymentMethodsDidChange),
            name: .paymentMethodDidChangeNotification,
            object: nil
        )
        refreshPaymentMethods()
    }

    @objc private func paymentMethodsDidChange() {
        let selectedPaymentMethod = markCurrentPayment ? customerDataProvider.paymentMethod : nil
        state.paymentMethods = customerDataProvider.paymentMethods.map { ($0, $0.identifier == selectedPaymentMethod?.identifier) }
    }

    func addNew() {
        completion(.addNew)
    }

    func selectMethod(_ paymentMethod: PaymentMethod) {
        guard paymentMethod.isEnabled else { return }
        completion(.select(paymentMethod))
    }

    func deleteMethod(_ paymentMethod: BankCard) {
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.removePaymentMethodResource(paymentMethodUUID: paymentMethod.uuid)) {[weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success:
                self.customerDataProvider.removePaymentMethod(paymentMethod)
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    func close() {
        completion(.close)
    }

    private func refreshPaymentMethods() {
        guard let customer = customerDataProvider.customer else { return }
        let resource = ResourceBuilder.paymentMethodsResource(userUUID: customer.uuid)
        webClient.load(resource: resource) {[weak self] result in
            guard let methods = try? result.get() else { return }
            self?.customerDataProvider.updatePaymentMethods(methods)
        }
    }
}
