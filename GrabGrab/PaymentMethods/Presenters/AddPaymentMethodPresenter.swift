//
//  AddPaymentMethodPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 23.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol AddPaymentMethodPresenterType {
    var state: AddPaymentMethodViewState { get }
    func send()
    func close()
    func changeCardNumber(_ cardNumber: String)
    func changeHolderName(_ holderName: String)
    func changeExpirationDate(_ expirationDate: String)
    func scanCard()
    func changeCVV(_ cvv: String)
}

struct AddPaymentMethodViewState {
    @Observable
    var isLoading = false

    @Observable
    var error: WebError?

    @Observable
    var cardNumberError: String?

    @Observable
    var holderNameError: String?

    @Observable
    var expirationDateError: String?

    @Observable
    var cvvError: String?

    @Observable
    var cardNumber: String?

    @Observable
    var holderName: String?

    @Observable
    var expirationDate: String?

    @Observable
    var cvv: String?

    fileprivate var rawCardNumber: String {
        cardNumber.map { $0.components(separatedBy: " ").joined() } ?? ""
    }
}

enum AddPaymentMethodAction {
    case close
    case added(BankCard)
    case cardScanner((String?) -> ())
    case secure3d(Secure3d, (Result<(String, String), WebError>) -> ())
}

final class AddPaymentMethodPresenter: AddPaymentMethodPresenterType {

    var state = AddPaymentMethodViewState()
    private let completion: (AddPaymentMethodAction) -> ()

    private let webClient: WebClientType
    private let customerDataProvider: CustomerDataProviderType

    init(webClient: WebClientType, customerDataProvider: CustomerDataProviderType, completion: @escaping (AddPaymentMethodAction) -> ()) {
        self.completion = completion
        self.webClient = webClient
        self.customerDataProvider = customerDataProvider
    }

    func send() {

        guard let customer = customerDataProvider.customer else { return }

        guard validateInput() else { return }
        let date = state.expirationDate ?? ""
        let cvv = state.cvv ?? ""
        let name = state.holderName ?? ""

        let card = Card()
        let cryptogram = card.makeCryptogramPacket(
            state.rawCardNumber,
            andExpDate: date,
            andCVV: cvv,
            andMerchantPublicID: Constants.Identifiers.cloudPaymentsMerchantId
        )

        guard let token = cryptogram else {
            return
        }

        state.isLoading = true
        let resource = ResourceBuilder.addNewPaymentMethodsResource(userUUID: customer.uuid, token: token, name: name)
        webClient.load(resource: resource) {[weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self.handleResponse(response)
                }
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    func close() {
        completion(.close)
    }

    func changeHolderName(_ holderName: String) {
        state.holderName = holderName
    }

    func changeCVV(_ cvv: String) {
        state.cvv = cvv
    }

    func changeCardNumber(_ cardNumber: String) {
        let separator = " "
        let number = cardNumber.components(separatedBy: separator).joined()
        state.cardNumber = number.separate(each: 4, with: separator)
    }

    func changeExpirationDate(_ expirationDate: String) {
        let separator = "/"
        let rawValue = String(expirationDate.components(separatedBy: separator).joined().prefix(4))
        state.expirationDate = rawValue.separate(each: 2, with: separator)
    }

    func scanCard() {
        completion(.cardScanner({[weak self] number in
            if let number = number {
                self?.changeCardNumber(number)
            }
        }))
    }

    private func handleResponse(_ response: AddCardResponse) {
        if let bankCard = response.bankCard {
            proceedWithCard(bankCard)
        } else if let secure3d = response.secure3D {
            handle3dSecure(secure3d)
        }
    }

    private func handle3dSecure(_ secure3d: Secure3d) {
        completion(.secure3d(secure3d, { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.sendConfirmation(md: response.0, response: response.1)
            case .failure(let error):
                self.state.error = error
            }
        }))
    }

    // swiftlint:disable:next identifier_name
    private func sendConfirmation(md: String, response: String) {
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.confirmNewPaymentMethodsResource(md: md, response: response)) { [weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success(let card):
                DispatchQueue.main.async {
                    self.proceedWithCard(card)
                }
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    private func proceedWithCard(_ bankCard: BankCard) {
        customerDataProvider.addPaymentMethod(bankCard)
        customerDataProvider.selectPaymentMethod(.card(bankCard))
        completion(.added(bankCard))
    }

    private func validateInput() -> Bool {
        var isValid = true
        if Card.isCardNumberValid(state.rawCardNumber) {
            state.cardNumberError = nil
        } else {
            state.cardNumberError = LocalizedStrings.PaymentMethod.New.cardNumberError
            isValid = false
        }

        let holderName = state.holderName?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""

        if holderName.isEmpty {
            state.holderNameError = LocalizedStrings.PaymentMethod.New.holderNameError
            isValid = false
        } else {
            state.holderNameError = nil
        }

        if Card.isExpDateValid(state.expirationDate ?? "") {
            state.expirationDateError = nil
        } else {
            state.expirationDateError = LocalizedStrings.PaymentMethod.New.expirationDateError
            isValid = false
        }

        let cvv = state.cvv ?? ""
        if cvv.isEmpty {
            state.cvvError = LocalizedStrings.PaymentMethod.New.cvvError
            isValid = false
        } else {
            state.cvvError = nil
        }

        return isValid
    }
}
