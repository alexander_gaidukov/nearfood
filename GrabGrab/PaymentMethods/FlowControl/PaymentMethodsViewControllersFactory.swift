//
//  PaymenMethodsViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol PaymentMethodsViewControllersFactoryType {
    func paymentMethodsViewController(
        markCurrentPayment: Bool,
        completion: @escaping (PaymentMethodsAction) -> ()
    ) -> PaymentMethodsViewController

    func addNewPaymentViewController(completion: @escaping (AddPaymentMethodAction) -> ()) -> UIViewController

    func cardScannerViewController(completion: @escaping (String?) -> ()) -> UIViewController
}

struct PaymentMethodsViewControllersFactory: PaymentMethodsViewControllersFactoryType {

    private let storyboard: Storyboard = .payments

    let webClient: WebClientType
    let dataProvidersStorage: DataProvidersStorageType

    func paymentMethodsViewController(
        markCurrentPayment: Bool,
        completion: @escaping (PaymentMethodsAction) -> ()
    ) -> PaymentMethodsViewController {
        let presenter = PaymentMethodsPresenter(
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            markCurrentPayment: markCurrentPayment,
            webClient: webClient,
            completion: completion
        )
        let controller = PaymentMethodsViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func addNewPaymentViewController(completion: @escaping (AddPaymentMethodAction) -> ()) -> UIViewController {
        let presenter = AddPaymentMethodPresenter(
            webClient: webClient,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            completion: completion
        )
        let controller = AddPaymentMethodViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func cardScannerViewController(completion: @escaping (String?) -> ()) -> UIViewController {
        dataProvidersStorage.cardScannerProvider.completion = completion
        return dataProvidersStorage.cardScannerProvider.cardScanningViewController
    }
}
