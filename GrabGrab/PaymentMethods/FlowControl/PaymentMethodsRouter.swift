//
//  PaymentMethodsRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol PaymentMethodsRouterType {
    func showPaymentMethods(markCurrentPayment: Bool, completion: @escaping (PaymentMethodsAction) -> ())
    func showAddNewPayment(completion: @escaping (AddPaymentMethodAction) -> ())
    func showCardScanner(completion: @escaping (String?) -> ())
    func start3DSecure(_ secure3d: Secure3d, completion: @escaping (Result<(String, String), WebError>) -> ())
    func dismiss(completion: @escaping () -> ())
}

final class PaymentMethodsRouter: NSObject, PaymentMethodsRouterType {

    private weak var presentingViewController: UIViewController?
    private var factory: PaymentMethodsViewControllersFactoryType

    private let d3ds = D3DS()
    private var d3dsCompletion: ((Result<(String, String), WebError>) -> ())?

    init(presentingViewController: UIViewController?, factory: PaymentMethodsViewControllersFactoryType) {
        self.presentingViewController = presentingViewController
        self.factory = factory
        super.init()
    }

    func showAddNewPayment(completion: @escaping (AddPaymentMethodAction) -> ()) {
        let controller = factory.addNewPaymentViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showPaymentMethods(markCurrentPayment: Bool, completion: @escaping (PaymentMethodsAction) -> ()) {
        let controller = factory.paymentMethodsViewController(markCurrentPayment: markCurrentPayment, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showCardScanner(completion: @escaping (String?) -> ()) {
        let controller = factory.cardScannerViewController(completion: completion)
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func start3DSecure(_ secure3d: Secure3d, completion: @escaping (Result<(String, String), WebError>) -> ()) {
        guard let controller = topViewController else { return }
        d3dsCompletion = completion
        d3ds.make3DSPayment(
            with: controller,
            delegate: self,
            andAcsURLString: secure3d.acsUrl,
            andPaReqString: secure3d.paymentRequest,
            andTransactionIdString: secure3d.md
        )
    }

    func dismiss(completion: @escaping () -> ()) {
        topViewController?.dismiss(animated: true, completion: completion)
    }

    private var topViewController: UIViewController? {
        var candidate = presentingViewController
        while let controller = candidate?.presentedViewController {
            candidate = controller
        }
        return candidate
    }
}

// MARK: - Transitioning Delegates

extension PaymentMethodsRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}

// MARK: - D3DSDelegate

extension PaymentMethodsRouter: D3DSDelegate {
    // swiftlint:disable:next identifier_name
    func authorizationCompleted(withMD md: String!, andPares paRes: String!) {
        d3dsCompletion?(.success((md, paRes)))
        d3dsCompletion = nil
    }

    func authorizationFailed(withHtml html: String!) {
        let error = APIError(message: html)
        d3dsCompletion?(.failure(.custom(401, error)))
        d3dsCompletion = nil
    }
}
