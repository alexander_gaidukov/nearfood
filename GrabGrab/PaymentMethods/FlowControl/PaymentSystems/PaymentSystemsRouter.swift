//
//  PaymentSystemsRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import PassKit

protocol PaymentSystemsRouterType {
    func showApplePayViewController(cart: Cart, completion: @escaping (String?) -> ())
    func showApplePayViewController(order: Order, completion: @escaping (String?) -> ())
}

final class PaymentSystemsRouter: NSObject, PaymentSystemsRouterType {

    private weak var presentingViewController: UIViewController?
    private var factory: PaymentSystemsViewControllersFactoryType
    private var applePayCompletion: ((String?) -> ())?

    init(presentingViewController: UIViewController?, factory: PaymentSystemsViewControllersFactoryType) {
        self.presentingViewController = presentingViewController
        self.factory = factory
        super.init()
    }

    func showApplePayViewController(cart: Cart, completion: @escaping (String?) -> ()) {
        let controller = factory.applePayViewController(cart: cart, delegate: self)
        applePayCompletion = completion
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showApplePayViewController(order: Order, completion: @escaping (String?) -> ()) {
        let controller = factory.applePayViewController(order: order, delegate: self)
        applePayCompletion = completion
        topViewController?.present(controller, animated: true, completion: nil)
    }

    private var topViewController: UIViewController? {
        var candidate = presentingViewController
        while let controller = candidate?.presentedViewController {
            candidate = controller
        }
        return candidate
    }
}

extension PaymentSystemsRouter: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        topViewController?.dismiss(animated: true) {
            self.applePayCompletion?(nil)
            self.applePayCompletion = nil
        }
    }

   func paymentAuthorizationViewController(
        _ controller: PKPaymentAuthorizationViewController,
        didAuthorizePayment payment: PKPayment,
        handler completion: @escaping (PKPaymentAuthorizationResult) -> Void
   ) {
        topViewController?.dismiss(animated: true) {
            let cryptogram = PKPaymentConverter.convert(toString: payment)
            self.applePayCompletion?(cryptogram)
            self.applePayCompletion = nil
            completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
        }

    }
}
