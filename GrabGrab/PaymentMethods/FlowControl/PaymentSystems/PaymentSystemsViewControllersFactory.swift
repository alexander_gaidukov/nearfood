//
//  PaymentSystemsViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import PassKit

protocol PaymentSystemsViewControllersFactoryType {
    func applePayViewController(cart: Cart, delegate: PKPaymentAuthorizationViewControllerDelegate) -> UIViewController
    func applePayViewController(order: Order, delegate: PKPaymentAuthorizationViewControllerDelegate) -> UIViewController
}

struct PaymentSystemsViewControllersFactory: PaymentSystemsViewControllersFactoryType {
    // swiftlint:disable:next function_body_length
    func applePayViewController(cart: Cart, delegate: PKPaymentAuthorizationViewControllerDelegate) -> UIViewController {
        let request = PKPaymentRequest()
        request.merchantIdentifier = Constants.Identifiers.applePayMerchantId
        request.supportedNetworks = [.visa, .masterCard]
        request.merchantCapabilities = .capability3DS
        request.countryCode = "RU"
        request.currencyCode = "RUB"

        var surge: Double = 0
        var bonuses: Double = 0
        var delivery: Double = 0
        var discount: Double = 0

        for charge in cart.charges {
            switch charge.reason {
            case .dish, .combo:
                break
            case .surge:
                surge = charge.amount
            case .bonuses:
                bonuses = abs(charge.amount)
            case .coupon(let metadata):
                switch metadata.coupon.kind {
                case .discountFixed, .discountPercent:
                    discount += abs(charge.amount)
                case .cashbackFixed, .cashbackPercent:
                    break
                }
            case .area:
                delivery = charge.amount
            case .unknown:
                if charge.amount < 0 { discount += abs(charge.amount) }
            }
        }

        let subtotal = cart.totalPrice + bonuses + discount - surge - delivery

        let subtotalItem = PKPaymentSummaryItem(
            label: LocalizedStrings.PaymentMethod.ApplePay.subtotal,
            amount: NSDecimalNumber(value: subtotal)
        )

        var items = [subtotalItem]

        if surge > 0 {
            let item = PKPaymentSummaryItem(label: LocalizedStrings.PaymentMethod.ApplePay.surge, amount: NSDecimalNumber(value: surge))
            items.append(item)
        }

        if delivery > 0 {
            let item = PKPaymentSummaryItem(
                label: LocalizedStrings.PaymentMethod.ApplePay.delivery,
                amount: NSDecimalNumber(value: delivery)
            )
            items.append(item)
        }

        if bonuses > 0 {
            let item = PKPaymentSummaryItem(
                label: LocalizedStrings.PaymentMethod.ApplePay.bonuses,
                amount: NSDecimalNumber(value: -bonuses)
            )
            items.append(item)
        }

        if discount > 0 {
            let item = PKPaymentSummaryItem(
                label: LocalizedStrings.PaymentMethod.ApplePay.discount,
                amount: NSDecimalNumber(value: -discount)
            )
            items.append(item)
        }

        let totalItem = PKPaymentSummaryItem(
            label: LocalizedStrings.PaymentMethod.ApplePay.payee,
            amount: NSDecimalNumber(value: cart.totalPrice)
        )
        items.append(totalItem)

        request.paymentSummaryItems = items
        // swiftlint:disable:next force_unwrapping
        let controller = PKPaymentAuthorizationViewController(paymentRequest: request)!
        controller.delegate = delegate
        return controller
    }

    // swiftlint:disable:next function_body_length
    func applePayViewController(
        order: Order,
        delegate: PKPaymentAuthorizationViewControllerDelegate
    ) -> UIViewController {
        let request = PKPaymentRequest()
        request.merchantIdentifier = Constants.Identifiers.applePayMerchantId
        request.supportedNetworks = [.visa, .masterCard]
        request.merchantCapabilities = .capability3DS
        request.countryCode = "RU"
        request.currencyCode = "RUB"

        var bonuses = 0.0
        var discount = 0.0
        var delivery = 0.0
        var surge = 0.0

        order.estimation.calculation.charges?.elements.forEach {
            switch $0.reason {
            case .bonuses:
                bonuses += abs($0.amount)
            case .surge:
                surge += $0.amount
            case .coupon(let metadata):
                switch metadata.coupon.kind {
                case .discountFixed, .discountPercent:
                    discount += abs($0.amount)
                case .cashbackFixed, .cashbackPercent:
                    break
                }
            case .area:
                delivery = $0.amount
            case .unknown:
                if $0.amount < 0 {
                    discount += abs($0.amount)
                }
            case .dish, .combo:
                break
            }
        }

        let subtotal = order.estimation.calculation.total + bonuses + discount - surge - delivery

        let subtotalItem = PKPaymentSummaryItem(
            label: LocalizedStrings.PaymentMethod.ApplePay.subtotal,
            amount: NSDecimalNumber(value: subtotal)
        )

        var items = [subtotalItem]

        if surge > 0 {
            let item = PKPaymentSummaryItem(label: LocalizedStrings.PaymentMethod.ApplePay.surge, amount: NSDecimalNumber(value: surge))
            items.append(item)
        }

        if delivery > 0 {
            let item = PKPaymentSummaryItem(
                label: LocalizedStrings.PaymentMethod.ApplePay.delivery,
                amount: NSDecimalNumber(value: delivery)
            )
            items.append(item)
        }

        if bonuses > 0 {
            let item = PKPaymentSummaryItem(
                label: LocalizedStrings.PaymentMethod.ApplePay.bonuses,
                amount: NSDecimalNumber(value: -bonuses)
            )
            items.append(item)
        }

        if discount > 0 {
            let item = PKPaymentSummaryItem(
                label: LocalizedStrings.PaymentMethod.ApplePay.discount,
                amount: NSDecimalNumber(value: -discount)
            )
            items.append(item)
        }

        let totalItem = PKPaymentSummaryItem(
            label: LocalizedStrings.PaymentMethod.ApplePay.payee,
            amount: NSDecimalNumber(value: order.estimation.calculation.total)
        )
        items.append(totalItem)

        request.paymentSummaryItems = items
        // swiftlint:disable:next force_unwrapping
        let controller = PKPaymentAuthorizationViewController(paymentRequest: request)!
        controller.delegate = delegate
        return controller
    }
}
