//
//  PaymentSystemsCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum PaymentSystem {
    case applePay
}

final class PaymentSystemsCoordinator: Coordinator {

    private let completion: (String?) -> ()
    private let router: PaymentSystemsRouterType
    private let paymentSystem: PaymentSystem
    private let cart: Cart?
    private let order: Order?

    init(router: PaymentSystemsRouterType, paymentSystem: PaymentSystem, cart: Cart, completion: @escaping (String?) -> ()) {
        self.router = router
        self.completion = completion
        self.paymentSystem = paymentSystem
        self.cart = cart
        self.order = nil
    }

    init(router: PaymentSystemsRouterType, paymentSystem: PaymentSystem, order: Order, completion: @escaping (String?) -> ()) {
        self.router = router
        self.completion = completion
        self.paymentSystem = paymentSystem
        self.order = order
        self.cart = nil
    }

    func start() {
        switch paymentSystem {
        case .applePay:
            showApplePay()
        }
    }

    private func showApplePay() {

        if let cart = cart {
            router.showApplePayViewController(cart: cart) { token in
                self.completion(token)
            }
        } else if let order = order {
            router.showApplePayViewController(order: order) { token in
                self.completion(token)
            }
        }
    }
}
