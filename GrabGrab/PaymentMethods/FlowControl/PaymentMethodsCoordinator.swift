//
//  PaymentMethodsCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 14.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class PaymentMethodsCoordinator: Coordinator {

    private let completion: (PaymentMethod?, WebError?) -> ()
    private let router: PaymentMethodsRouterType
    private let webClient: WebClientType
    private let dataProvidersStorage: DataProvidersStorageType
    private let markCurrentPayment: Bool

    init(
        router: PaymentMethodsRouterType,
        dataProvidersStorage: DataProvidersStorageType,
        webClient: WebClientType,
        markCurrentPayment: Bool,
        completion: @escaping (PaymentMethod?, WebError?) -> ()
    ) {
        self.router = router
        self.completion = completion
        self.webClient = webClient
        self.dataProvidersStorage = dataProvidersStorage
        self.markCurrentPayment = markCurrentPayment
    }

    func start() {
        if dataProvidersStorage.customerDataProvider.paymentMethods.isEmpty {
            showAddPaymentMethod()
        } else {
            showPaymentMethods()
        }
    }

    private func showPaymentMethods() {
        router.showPaymentMethods(markCurrentPayment: markCurrentPayment) { action in
            switch action {
            case .select(let method):
                self.dataProvidersStorage.customerDataProvider.selectPaymentMethod(method)
                self.router.dismiss {
                    self.completion(method, nil)
                }
            case .addNew:
                self.router.dismiss {
                    self.showAddPaymentMethod()
                }
            case .close:
                self.router.dismiss {
                    self.completion(nil, nil)
                }
            }
        }
    }

    private func showCardScanner(completion: @escaping (String?) -> ()) {
        router.showCardScanner { result in
            self.router.dismiss {
                completion(result)
            }
        }
    }

    private func show3dSecure(_ secure3d: Secure3d, completion: @escaping (Result<(String, String), WebError>) -> ()) {
        router.start3DSecure(secure3d, completion: completion)
    }

    private func showAddPaymentMethod() {
        router.showAddNewPayment { action in
            switch action {
            case .close:
                self.router.dismiss {
                    self.completion(nil, nil)
                }
            case .added(let card):
                self.router.dismiss {
                    self.completion(.card(card), nil)
                }
            case .cardScanner(let completion):
                self.showCardScanner(completion: completion)
            case let .secure3d(secure3d, completion):
                self.show3dSecure(secure3d, completion: completion)
            }
        }
    }
}
