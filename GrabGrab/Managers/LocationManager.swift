//
//  LocationManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import CoreLocation

public func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
}

extension CLLocationCoordinate2D: Equatable {}

enum LocationManagerError: Error {
    case unauthorized
    case custom(Error)
}

protocol LocationManagerDelegate: AnyObject {
    func locationManager(_ locationManager: LocationManager, didFailWith error: LocationManagerError)
    func locationManager(_ locationManager: LocationManager, didUpdateLocation location: CLLocation)
}

protocol LocationManagerType {
    var delegate: LocationManagerDelegate? { get set }
    var currentLocation: CLLocation? { get }
    func start()
    func requestAuthorization(withCompletion completion: @escaping () -> ())
    func stop()
}

final class LocationManager: NSObject, LocationManagerType {

    weak var delegate: LocationManagerDelegate?

    private var isUpdating = false
    private var isUpdateRequested = false

    private var authorizationCompletion: (() -> ())?

    private(set) var currentLocation: CLLocation? {
        didSet {
            guard let location = currentLocation else { return }
            delegate?.locationManager(self, didUpdateLocation: location)
        }
    }

    private let locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()

    deinit {
        stop()
    }

    override init() {
        super.init()
        locationManager.delegate = self
    }

    static var isAuthorizationNeeded: Bool {
        CLLocationManager.authorizationStatus() == .notDetermined
    }

    func requestAuthorization(withCompletion completion: @escaping () -> () = {}) {
        guard LocationManager.isAuthorizationNeeded else {
            completion()
            return
        }
        authorizationCompletion = completion
        locationManager.requestWhenInUseAuthorization()
    }

    func start() {
        guard !isUpdating else {
            return
        }

        isUpdateRequested = true

        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
            || CLLocationManager.authorizationStatus() == .authorizedAlways {
            startLocationUpdates()
        } else if CLLocationManager.authorizationStatus() == .notDetermined {
            requestAuthorization()
        } else {
            delegate?.locationManager(self, didFailWith: .unauthorized)
        }
    }

    func stop() {
        guard isUpdating else { return }
        locationManager.stopUpdatingLocation()
        isUpdating = false
        isUpdateRequested = false
    }

    private func startLocationUpdates() {
        locationManager.startUpdatingLocation()
        isUpdating = true
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        let isAuthorized  = [.authorizedWhenInUse, .authorizedAlways].contains(status)
        if isAuthorized {
            if isUpdateRequested {
                startLocationUpdates()
            }
        } else {
            stop()
        }
        authorizationCompletion?()
        authorizationCompletion = nil
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.locationManager(self, didFailWith: .custom(error))
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        guard Date().timeIntervalSince(location.timestamp) < 60.0 else { return }
        currentLocation = location
    }
}
