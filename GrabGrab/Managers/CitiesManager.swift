//
//  CitiesManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 24.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CitiesManagerType {
    var cities: [City] { get }
    func refresh()
}

final class CitiesManager: CitiesManagerType {

    private let webClient: WebClientType

    private lazy var fileURL: URL = {
        // swiftlint:disable:next force_unwrapping
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsURL.appendingPathComponent("cities.json")
    }()

    private(set) var cities: [City] = [] {
        didSet {
            NotificationCenter.default.post(name: .citiesDidChangeNotification, object: nil)
        }
    }

    init(webClient: WebClientType) {
        self.webClient = webClient
        initiate()
    }

    private func initiate() {
        copyFromBundleIfNeeded()
        refresh()
    }

    private func copyFromBundleIfNeeded() {
        defer {
            configureCities()
        }
        guard !FileManager.default.fileExists(atPath: fileURL.path),
              let url = Bundle.main.url(forResource: "cities", withExtension: "json") else {
            return
        }
        try? FileManager.default.copyItem(at: url, to: fileURL)
    }

    private func save(response: CitiesResponse) {
        guard let data = try? Coders.encoder.encode(response) else { return }
        try? data.write(to: fileURL, options: .atomic)
        configureCities()
    }

    private func configureCities() {
        guard let data = try? Data(contentsOf: fileURL) else {
            cities = []
            return
        }
        let decoder = Coders.decoder
        cities = (try? decoder.decode(CitiesResponse.self, from: data).cities) ?? []
    }

    func refresh() {
        webClient.load(resource: ResourceBuilder.citiesResource()) {[weak self] result in
            guard let response = try? result.get() else { return }
            self?.save(response: response)
        }
    }
}
