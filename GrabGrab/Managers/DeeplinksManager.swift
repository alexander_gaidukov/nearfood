//
//  DeeplinksManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 23.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol DeeplinksManagerType {
    @discardableResult
    func handle(url: URL) -> Bool
}

final class DeeplinksManager: DeeplinksManagerType {

    private var invitationsDataProvider: InvitationsDataProviderType

    init(invitationsDataProvider: InvitationsDataProviderType) {
        self.invitationsDataProvider = invitationsDataProvider
    }

    enum Kind: String {
        case orders
        case invites
    }

    @discardableResult
    func handle(url: URL) -> Bool {
        let pathComponents = url
            .path
            .components(separatedBy: "/")
            .filter { !$0.trimmingCharacters(in: .whitespaces).isEmpty }

        guard let kind = pathComponents.first.flatMap(Kind.init) else { return false }

        let components = Array(pathComponents.dropFirst())

        switch kind {
        case .orders:
            return handleOrders(components)
        case .invites:
            return handleInvites(components)
        }
    }

    private func handleInvites(_ pathComponents: [String]) -> Bool {
        guard pathComponents.count == 1 else { return false }
        let invitation = pathComponents[0]
        invitationsDataProvider.receivedInvitation = invitation
        return true
    }

    private func handleOrders(_ pathComponents: [String]) -> Bool {
        guard pathComponents.count == 1 else { return false }
        let uuid = pathComponents[0]
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .showOrderDetailsNotification, object: nil, userInfo: ["uuid": uuid])
        }
        return true
    }
}
