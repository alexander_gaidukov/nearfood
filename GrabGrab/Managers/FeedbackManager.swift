//
//  FeedbackManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation
import StoreKit

protocol FeedbackManagerType {
    func orderCreated()
}

final class FeedbackManager: FeedbackManagerType {

    private let appVersionDataProvider: AppVersionDataProviderType

    init(appVersionDataProvider: AppVersionDataProviderType) {
        self.appVersionDataProvider = appVersionDataProvider
    }

    @UserDefaultsItem(key: StorageKey("com.grabgrab.feedback.orders"), defaultValue: 0)
    private var completedOrdersCount: Int

    @UserDefaultsItem(key: StorageKey("com.grabgrab.feedback.version"))
    private var requestedAppVersion: String?

    private var isFeedbackAvailable: Bool {
        completedOrdersCount > 1 && (completedOrdersCount - 2).isMultiple(of: 10)
    }

    private func requestUserFeedback() {
        if isFeedbackAvailable { SKStoreReviewController.requestReview() }
    }

    func orderCreated() {
        guard let currentAppVersion = appVersionDataProvider.currentVersion else {
            return
        }

        if requestedAppVersion != currentAppVersion {
            requestedAppVersion = currentAppVersion
            completedOrdersCount = 1
        } else {
            completedOrdersCount += 1
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.requestUserFeedback()
        }
    }
}
