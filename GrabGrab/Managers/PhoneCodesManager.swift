//
//  PhoneCodesManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol PhoneCodesManagerType {
    var selectedCountry: Country { get }
    var countries: [Country] { get }
    func refresh()
    func phoneNumber(from phone: String) -> PhoneNumber
    func select(country: Country)
}

final class PhoneCodesManager: PhoneCodesManagerType {

    private let webClient: WebClientType

    private lazy var fileURL: URL = {
        // swiftlint:disable:next force_unwrapping
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsURL.appendingPathComponent("countries.json")
    }()

    private(set) var countries: [Country] = [] {
        didSet {
            NotificationCenter.default.post(name: .phoneCodesDidChangeNotification, object: nil)
        }
    }

    @UserDefaultsItem(key: StorageKey("com.grabgrab.countries.selected"))
    private var selectedCountryUUID: String?

    var selectedCountry: Country {
        get {
            if let uuid = selectedCountryUUID,
               let country = countries.first(where: { $0.uuid == uuid }) {
                return country
            }

            let country = countries
                .first { $0.countryCode.lowercased() == Locale.current.regionCode?.lowercased() }
                ?? countries.first! // swiftlint:disable:this force_unwrapping
            selectedCountryUUID = country.uuid
            return country
        }

        set {
            selectedCountryUUID = newValue.uuid
        }
    }

    init(webClient: WebClientType) {
        self.webClient = webClient
        initiate()
    }

    func phoneNumber(from phone: String) -> PhoneNumber {
        let rawPhone = phone.hasPrefix("+") ? phone : "+" + phone
        // swiftlint:disable:next force_unwrapping
        let country = countries.first { $0.match(to: rawPhone) } ?? countries.first!
        let maskValue = rawPhone.suffix(from: rawPhone.index(rawPhone.startIndex, offsetBy: country.phoneCode.count))

        var number = PhoneNumber(country: country, maskValue: "")
        number.setNumber(number: String(maskValue))
        return number
    }

    func select(country: Country) {
        selectedCountry = country
    }

    private func initiate() {
        copyFromBundleIfNeeded()
        refresh()
    }

    private func configureCountries() {
        guard let data = try? Data(contentsOf: fileURL) else {
            countries = []
            return
        }
        let decoder = Coders.decoder
        countries = (try? decoder.decode(CountriesResponse.self, from: data).codes) ?? []
    }

    private func copyFromBundleIfNeeded() {
        defer {
            configureCountries()
        }
        guard !FileManager.default.fileExists(atPath: fileURL.path),
              let url = Bundle.main.url(forResource: "countries", withExtension: "json") else { return }
        try? FileManager.default.copyItem(at: url, to: fileURL)
    }

    private func save(response: CountriesResponse) {
        guard let data = try? Coders.encoder.encode(response) else { return }
        try? data.write(to: fileURL, options: .atomic)
        configureCountries()
    }

    func refresh() {
        webClient.load(resource: ResourceBuilder.phoneCountryCodesResource()) {[weak self] result in
            guard let response = try? result.get() else { return }
            self?.save(response: response)
        }
    }
}
