//
//  PushNotificationsManager.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UserNotifications
import UIKit

protocol PushNotificationsManagerType {
    func applicationDidRegisterForRemoteNotifcation(withDeviceToken deviceToken: Data)
    func registerForRemoteNotifications(completion: @escaping () -> ())
    func registerNotificationToken()
    func handle(_ userInfo: [AnyHashable: Any], completion: @escaping () -> ())
    func handleSilent(_ userInfo: [AnyHashable: Any], completion: @escaping () -> ())
}

enum SilentPushKind: String {
    case orders = "orders.updated"
    case messages = "messages.updated"
    case surges = "surges.updated"
    case feedbacks = "feedbacks.updated"
    case bonuses = "bonuses.updated"
}

enum CustomerPushKind: String {
    case newMessage = "messages.new"
    case newFeedback = "feedbacks.new"
    case newBonus = "bonuses.new"
}

final class PushNotificationsManager: PushNotificationsManagerType {

    let customerDataProvider: CustomerDataProviderType
    let webClient: WebClientType

    init(customerDataProvider: CustomerDataProviderType, webClient: WebClientType) {
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
    }

    static func isAuthorizationNeeded(completion: @escaping (Bool) -> ()) {
        UNUserNotificationCenter.current().getNotificationSettings {
            completion($0.authorizationStatus == .notDetermined)
        }
    }

    private(set) var notificationToken: String?

    func registerForRemoteNotifications(completion: @escaping () -> () = {}) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]) { _, _ in
            completion()
        }
    }

    func registerNotificationToken() {
        guard let customer = customerDataProvider.customer,
              customer.isActive,
              let token = notificationToken else { return }

        let resource = ResourceBuilder.registerDeviceForPNResource(
            customerUUID: customer.uuid,
            device: customerDataProvider.device,
            token: token,
            environment: Constants.Environments.pushNotification.rawValue
        )

        webClient.load(resource: resource) { _ in }
    }

    func applicationDidRegisterForRemoteNotifcation(withDeviceToken deviceToken: Data) {
        notificationToken = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        registerNotificationToken()
    }

    func handle(_ userInfo: [AnyHashable: Any], completion: @escaping () -> ()) {
        defer {
            completion()
        }
        guard let kind = (userInfo["kind"] as? String).map(CustomerPushKind.init) else { return }

        if kind == .newMessage {
            NotificationCenter.default.post(name: .newMessageNotification, object: nil)
        } else if kind == .newFeedback {
            let data = userInfo["data"] as? JSON
            var info = data?["order"] as? JSON
            if let feedbackUUID = data?["uuid"] as? String {
                info?["feedback_uuid"] = feedbackUUID
            }
            NotificationCenter.default.post(name: .newFeedbackNotification, object: nil, userInfo: info)
        } else if kind == .newBonus {
            NotificationCenter.default.post(name: .newMessageNotification, object: nil)
        }
    }

    func handleSilent(_ userInfo: [AnyHashable: Any], completion: @escaping () -> ()) {
        defer {
            completion()
        }
        guard let kind = (userInfo["kind"] as? String).map(SilentPushKind.init) else { return }
        if kind == .orders {
            let uuid = (userInfo["data"] as? JSON)?["uuid"]
            let info = uuid.map { ["uuid": $0] }
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .shouldUpdateOrdersNotification, object: nil, userInfo: info)
            }
        } else if kind == .messages {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .shouldUpdateMessagesNotification, object: nil)
            }
        } else if kind == .surges {
            let kitchenUUID = (userInfo["data"] as? JSON)?["kitchen_uuid"]
            let info = kitchenUUID.map { ["kitchen_uuid": $0] }
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .shouldUpdateSurgeNotification, object: nil, userInfo: info)
            }
        } else if kind == .feedbacks {
            let info = userInfo["data"] as? JSON
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .shouldUpdateFeedbackNotification, object: nil, userInfo: info)
            }
        } else if kind == .bonuses {
            guard let customer = customerDataProvider.customer else { return }
            webClient.load(resource: ResourceBuilder.bonusesResource(userUUID: customer.uuid)) {[weak self] result in
                guard let self = self, let bonuses = try? result.get() else { return }
                self.customerDataProvider.updateBonuses(bonuses: bonuses)
            }
        }
    }
}
