//
//  ManagersStorage.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 03.10.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol ManagersStorageType {
    var locationManager: LocationManagerType { get }
    var feedbackManager: FeedbackManagerType { get }
    var phoneCodesManager: PhoneCodesManagerType { get }
    var pushNotificationsManager: PushNotificationsManagerType { get }
    var citiesManager: CitiesManagerType { get }
    var deeplinksManager: DeeplinksManagerType { get }
}

final class ManagersStorage: ManagersStorageType {
    let locationManager: LocationManagerType

    let feedbackManager: FeedbackManagerType

    let phoneCodesManager: PhoneCodesManagerType

    let pushNotificationsManager: PushNotificationsManagerType

    let citiesManager: CitiesManagerType

    let deeplinksManager: DeeplinksManagerType

    init(webClient: WebClientType, dataProviders: DataProvidersStorageType) {
        locationManager = LocationManager()
        feedbackManager = FeedbackManager(appVersionDataProvider: dataProviders.appVersionDataProvider)
        phoneCodesManager = PhoneCodesManager(webClient: webClient)
        pushNotificationsManager = PushNotificationsManager(
            customerDataProvider: dataProviders.customerDataProvider,
            webClient: webClient
        )
        citiesManager = CitiesManager(webClient: webClient)
        deeplinksManager = DeeplinksManager(invitationsDataProvider: dataProviders.invitationsDataProvider)
    }
}
