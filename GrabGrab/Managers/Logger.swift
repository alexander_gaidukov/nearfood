//
//  Logger.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol LoggerType {
    var apiErrorsData: Data? { get }
    func logAPIError(url: String, params: JSON, response: Data?, code: Int) -> APIErrorItem
    func clearAPILogs()
}

struct APIErrorItem {
    let url: String
    let params: JSON
    let response: Data?
    let code: Int
}

final class Logger: LoggerType {

    private var apiErrors: [APIErrorItem] = []

    var apiErrorsData: Data? {
        let objects = apiErrors.map(\.json)
        return try? JSONSerialization.data(withJSONObject: objects, options: [])
    }

    func logAPIError(url: String, params: JSON, response: Data?, code: Int) -> APIErrorItem {
        let item = APIErrorItem(url: url, params: params, response: response, code: code)
        apiErrors.append(item)
        return item
    }

    func clearAPILogs() {
        apiErrors.removeAll()
    }
}

extension APIErrorItem {
    var json: JSON {
        let body = response.flatMap { String(data: $0, encoding: .utf8) } ?? ""
        let paramsData = try? JSONSerialization.data(withJSONObject: params, options: [])
        let paramsString = paramsData.flatMap { String(data: $0, encoding: .utf8) } ?? ""
        return ["url": url, "params": paramsString, "body": body, "status": code]
    }
}
