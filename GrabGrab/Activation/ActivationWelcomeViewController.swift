//
//  ActivationWelcomeViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class ActivationWelcomeViewController: UIViewController {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var invitationsButton: UIButton!
    @IBOutlet private weak var continueButton: UIButton!

    var presenter: ActivationWelcomePresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    private func configureViews() {
        titleLabel.text = LocalizedStrings.Activation.Welcome.title
        descriptionLabel.text = LocalizedStrings.Activation.Welcome.description

        continueButton.setTitle(LocalizedStrings.Activation.continueButton, for: .normal)
        invitationsButton.setTitle(LocalizedStrings.Activation.Welcome.invitationButton, for: .normal)

    }

    private func configureBindings() {
        presenter.state.$isLoading.observe {[weak self] isLoading in
            guard let self = self else { return }

            if isLoading {
                self.showLoading()
            } else {
                self.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] error in
            if let error = error {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    @IBAction private func invitationsButtonTapped() {
        presenter.goToInvition()
    }

    @IBAction private func continueButtonTapped() {
        presenter.goToMenu()
    }

}

extension ActivationWelcomeViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension ActivationWelcomeViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
