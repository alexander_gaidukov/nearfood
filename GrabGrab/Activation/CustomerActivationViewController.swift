//
//  CustomerActivationViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class CustomerActivationViewController: UIViewController {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var invitationLabel: UILabel!
    @IBOutlet private weak var invitationTextField: GGTextField!
    @IBOutlet private weak var activationButton: UIButton!
    @IBOutlet private weak var logoutButton: UIButton!
    @IBOutlet private weak var continueButton: UIButton!

    var presenter: CustomerActivationPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    private func configureViews() {
        titleLabel.text = presenter.state.title
        descriptionLabel.text = presenter.state.description
        invitationLabel.text = LocalizedStrings.Activation.codeTitle
        invitationTextField.placeholder = LocalizedStrings.Activation.codePlaceholder
        activationButton.setTitle(LocalizedStrings.Activation.activateButton, for: .normal)
        logoutButton.setTitle(LocalizedStrings.Activation.Beta.logoutButton, for: .normal)
        continueButton.setTitle(LocalizedStrings.Activation.continueButton, for: .normal)
        if presenter.state.isLogoutButtonHidden {
            logoutButton?.removeFromSuperview()
        }
        continueButton.isHidden = presenter.state.isContinueButtonHidden
    }

    private func configureBindings() {
        presenter.state.$invitation.bind(to: invitationTextField, keyPath: \.text).addToDisposableBag(disposableBag)
        presenter.state.invitationError.bind(to: invitationTextField, keyPath: \.errorMessage).addToDisposableBag(disposableBag)
        presenter.state.isActivateButtonEnabled.bind(to: activationButton, keyPath: \.isEnabled).addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe {[weak self] isLoading in
            guard let self = self else { return }
            if isLoading && self.invitationTextField.isFirstResponder {
                self.invitationTextField.resignFirstResponder()
            }
            if isLoading {
                self.showLoading()
            } else {
                self.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe {[weak self] error in
            if let error = error {
                self?.showBanner(withType: .error(error.localizedDescription))
            } else {
                self?.dismissBanner()
            }
        }.addToDisposableBag(disposableBag)
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        presenter.changeInvitation(text)
    }

    @IBAction private func logoutButtonTapped() {
        presenter.logout()
    }

    @IBAction private func activateTapped() {
        presenter.activate()
    }

    @IBAction private func continueButtonTapped() {
        presenter.goToMenu()
    }

}

extension CustomerActivationViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension CustomerActivationViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}

extension CustomerActivationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
