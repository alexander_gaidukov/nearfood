//
//  InviteInfoViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class InviteInfoViewController: UIViewController {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var discountLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var startButton: UIButton!

    var presenter: InviteInfoPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        titleLabel.text = LocalizedStrings.Activation.Invite.title
        discountLabel.text = presenter.state.discount
        descriptionLabel.text = presenter.state.description
        startButton.setTitle(LocalizedStrings.Activation.Invite.start, for: .normal)
    }

    @IBAction private func startButtonTapped() {
        presenter.start()
    }

}
