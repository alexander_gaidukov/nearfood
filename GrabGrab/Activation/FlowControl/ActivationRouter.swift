//
//  ActivationRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol ActivationRouterType {
    func showActivationWelcome(customer: Customer, completion: @escaping (ActivationWelcomeAction) -> ())
    func showCustomerActivation(customer: Customer, completion: @escaping (CustomerActivationAction) -> ())
    func showInviteInfo(invite: Invite, completion: @escaping (InviteInfoAction) -> ())
    func showLogoutConfirmation(completion: @escaping (Bool) -> ())
}

final class ActivationRouter: ActivationRouterType {
    private let window: UIWindow
    private let factory: ActivationViewControllersFactoryType

    private var navigationController: GGNavigationController? {
        window.rootViewController as? GGNavigationController
    }

    init(window: UIWindow, factory: ActivationViewControllersFactoryType) {
        self.window = window
        self.factory = factory
    }

    func showActivationWelcome(customer: Customer, completion: @escaping (ActivationWelcomeAction) -> ()) {
        let controller = factory.activationWelcomeViewController(customer: customer, completion: completion)
        pushOrShow(viewController: controller)
    }

    func showCustomerActivation(customer: Customer, completion: @escaping (CustomerActivationAction) -> ()) {
        let controller = factory.customerActivationViewController(customer: customer, completion: completion)
        pushOrShow(viewController: controller)
    }

    func showLogoutConfirmation(completion: @escaping (Bool) -> ()) {
        let controller = factory.logoutConfirmationViewController(completion: completion)
        navigationController?.present(controller, animated: true, completion: nil)
    }

    func showInviteInfo(invite: Invite, completion: @escaping (InviteInfoAction) -> ()) {
        let controller = factory.inviteInfoViweController(invite: invite, completion: completion)
        pushOrShow(viewController: controller)
    }

    private func pushOrShow(viewController: UIViewController) {
        if let navController = navigationController {
            navController.pushViewController(viewController, animated: true)
            return
        }

        let navController = GGNavigationController(rootViewController: viewController)
        window.rootViewController = navController
    }
}
