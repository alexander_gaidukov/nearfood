//
//  ActivationCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class ActivationCoordinator: Coordinator {
    var completion: () -> ()
    private let router: ActivationRouterType
    private let dataProvidersStorage: DataProvidersStorageType

    init(router: ActivationRouterType,
         dataProvidersStorage: DataProvidersStorageType,
         completion: @escaping () -> ()) {
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
        self.completion = completion
    }

    func start() {
        if dataProvidersStorage.invitationsDataProvider.receivedInvitation == nil {
            showWelcome()
        } else {
            showInvitations()
        }
    }

    private func showWelcome() {
        guard let customer = dataProvidersStorage.customerDataProvider.customer else { return }
        router.showActivationWelcome(customer: customer) { action in
            switch action {
            case .invition:
                self.showInvitations()
            case let .activate(date, invite):
                self.didActivate(date: date, invite: invite)
            }
        }
    }

    private func showInvitations() {
        guard let customer = dataProvidersStorage.customerDataProvider.customer else { return }
        router.showCustomerActivation(customer: customer) { action in
            switch action {
            case .logout:
                self.showLogoutConfirmation()
            case let .activate(date, invite):
                self.didActivate(date: date, invite: invite)
            }
        }
    }

    private func didActivate(date: Date, invite: Invite?) {
        self.dataProvidersStorage.customerDataProvider.activate(activationDate: date, invite: invite)
        if let invite = invite {
            self.showInviteInformation(invite)
        } else {
            self.completion()
        }
    }

    private func showInviteInformation(_ invite: Invite) {
        router.showInviteInfo(invite: invite) { action in
            switch action {
            case .complete:
                self.completion()
            }
        }
    }

    private func showLogoutConfirmation() {
        router.showLogoutConfirmation {
            if $0 { self.dataProvidersStorage.customerDataProvider.logout() }
        }
    }
}
