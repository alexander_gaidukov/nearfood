//
//  ActivationViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 10.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol ActivationViewControllersFactoryType {
    func activationWelcomeViewController(
        customer: Customer,
        completion: @escaping (ActivationWelcomeAction) -> ()
    ) -> ActivationWelcomeViewController
    func customerActivationViewController(
        customer: Customer,
        completion: @escaping (CustomerActivationAction) -> ()
    ) -> CustomerActivationViewController
    func inviteInfoViweController(invite: Invite, completion: @escaping (InviteInfoAction) -> ()) -> InviteInfoViewController
    func logoutConfirmationViewController(completion: @escaping (Bool) -> ()) -> UIAlertController
}

struct ActivationViewControllersFactory: ActivationViewControllersFactoryType {

    private let storyboard: Storyboard = .activation
    let webClient: WebClientType
    let dataProvidersStorage: DataProvidersStorageType

    func activationWelcomeViewController(
        customer: Customer,
        completion: @escaping (ActivationWelcomeAction) -> ()
    ) -> ActivationWelcomeViewController {
        let presenter = ActivationWelcomePresenter(customer: customer, webClient: webClient, completion: completion)
        let controller = ActivationWelcomeViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func customerActivationViewController(
        customer: Customer,
        completion: @escaping (CustomerActivationAction) -> ()
    ) -> CustomerActivationViewController {
        let presenter = CustomerActivationPresenter(
            customer: customer,
            webClient: webClient,
            invitationsDataProvider: dataProvidersStorage.invitationsDataProvider,
            completion: completion
        )
        let controller = CustomerActivationViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func inviteInfoViweController(invite: Invite, completion: @escaping (InviteInfoAction) -> ()) -> InviteInfoViewController {
        let presenter = InviteInfoPresenter(invite: invite, completion: completion)
        let controller = InviteInfoViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func logoutConfirmationViewController(completion: @escaping (Bool) -> ()) -> UIAlertController {
        let alert = UIAlertController(title: LocalizedStrings.Account.logoutConfirmationMessage, message: nil, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: LocalizedStrings.Common.cancel, style: .cancel) {_ in completion(false) }
        alert.addAction(cancelAction)

        let exitAction = UIAlertAction(title: LocalizedStrings.Account.logoutButtonTitle, style: .default) { _ in completion(true) }
        alert.addAction(exitAction)

        return alert
    }
}
