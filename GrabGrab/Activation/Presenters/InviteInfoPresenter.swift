//
//  InviteInfoPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 16.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol InviteInfoPresenterType {
    var state: InviteInfoViewState { get }
    func start()
}

enum InviteInfoAction {
    case complete
}

struct InviteInfoViewState {

    var invite: Invite

    var discount: String {
        "-\(Int(invite.multiplier * 100))%"
    }

    var description: String {
        invite.description
    }
}

final class InviteInfoPresenter: InviteInfoPresenterType {
    private let completion: (InviteInfoAction) -> ()

    private(set) var state: InviteInfoViewState

    init(invite: Invite, completion: @escaping (InviteInfoAction) -> ()) {
        self.completion = completion
        self.state = InviteInfoViewState(invite: invite)
    }

    func start() {
        completion(.complete)
    }
}
