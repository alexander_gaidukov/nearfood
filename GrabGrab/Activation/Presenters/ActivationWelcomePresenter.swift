//
//  ActivationWelcomePresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.02.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol ActivationWelcomePresenterType {
    var state: ActivationWelcomeViewState { get }
    func goToMenu()
    func goToInvition()
}

enum ActivationWelcomeAction {
    case invition
    case activate(Date, Invite?)
}

struct ActivationWelcomeViewState {
    @Observable
    var isLoading: Bool = false

    @Observable
    var error: WebError?
}

final class ActivationWelcomePresenter: ActivationWelcomePresenterType {
    private let completion: (ActivationWelcomeAction) -> ()
    private let customer: Customer
    private let webClient: WebClientType

    private(set) var state = ActivationWelcomeViewState()

    init(customer: Customer, webClient: WebClientType, completion: @escaping (ActivationWelcomeAction) -> ()) {
        self.completion = completion
        self.webClient = webClient
        self.customer = customer
    }

    func goToMenu() {
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.activateUserResource(uuid: customer.uuid, invite: nil)) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let customer):
                if let date = customer.activatedAt {
                    DispatchQueue.main.async {
                        self?.completion(.activate(date, customer.invite))
                    }
                } else {
                    self?.state.error = .wrongDataFormat
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }

    func goToInvition() {
        completion(.invition)
    }
}
