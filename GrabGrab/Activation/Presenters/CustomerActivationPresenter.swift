//
//  CustomerActivationPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 07.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol CustomerActivationPresenterType {
    var state: CustomerActivationViewState { get }
    func goToMenu()
    func activate()
    func logout()
    func changeInvitation(_ invitation: String)
}

enum CustomerActivationAction {
    case logout
    case activate(Date, Invite?)
}

struct CustomerActivationViewState {
    @Observable
    var invitation: String?

    @Observable
    var isLoading: Bool = false

    @Observable
    var error: WebError?

    var isInviteRequired: Bool

    var title: String {
        isInviteRequired ? LocalizedStrings.Activation.Beta.title : LocalizedStrings.Activation.title
    }

    var description: String? {
        isInviteRequired ? LocalizedStrings.Activation.Beta.description : nil
    }

    var isLogoutButtonHidden: Bool {
        !isInviteRequired
    }

    var isContinueButtonHidden: Bool {
        isInviteRequired
    }

    var invitationError: Observable<String?> {
        $error.map { $0?.apiError?.code?.localizedDescription }
    }

    var isActivateButtonEnabled: Observable<Bool> {
        $invitation.map { $0?.isEmpty == false }
    }
}

final class CustomerActivationPresenter: CustomerActivationPresenterType {
    private var invitationsDataProvider: InvitationsDataProviderType
    private let completion: (CustomerActivationAction) -> ()
    private let customer: Customer
    private let webClient: WebClientType

    private(set) var state: CustomerActivationViewState

    init(
        customer: Customer,
        webClient: WebClientType,
        invitationsDataProvider: InvitationsDataProviderType,
        completion: @escaping (CustomerActivationAction) -> ()
    ) {
        self.invitationsDataProvider = invitationsDataProvider
        self.completion = completion
        self.webClient = webClient
        self.customer = customer
        self.state = CustomerActivationViewState(
            invitation: invitationsDataProvider.receivedInvitation,
            isInviteRequired: customer.inviteRequired == true
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(receivedInvitationDidChange),
            name: .receivedInvitationDidChangeNotification,
            object: nil
        )
    }

    @objc private func receivedInvitationDidChange() {
        state.invitation = invitationsDataProvider.receivedInvitation
    }

    func changeInvitation(_ invitation: String) {
        state.invitation = invitation
    }

    func logout() {
        completion(.logout)
    }

    func goToMenu() {
        performActivation(nil)
    }

    func activate() {
        guard let invitation = state.invitation,
              let code = invitation.components(separatedBy: "/").last else {
            return
        }
        performActivation(code)
    }

    private func performActivation(_ invite: String?) {
        state.isLoading = true
        webClient.load(resource: ResourceBuilder.activateUserResource(uuid: customer.uuid, invite: invite)) {[weak self] result in
            self?.state.isLoading = false
            switch result {
            case .success(let customer):
                if let date = customer.activatedAt {
                    self?.invitationsDataProvider.receivedInvitation = nil
                    DispatchQueue.main.async {
                        self?.completion(.activate(date, customer.invite))
                    }
                } else {
                    self?.state.error = .wrongDataFormat
                }
            case .failure(let error):
                self?.state.error = error
            }
        }
    }
}
