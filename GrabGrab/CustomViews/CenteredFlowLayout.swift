//
//  CenteredFlowLayout.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class CenteredFlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributesObjects = super
            .layoutAttributesForElements(in: rect)?
            .map { $0.copy() }
            as? [UICollectionViewLayoutAttributes]

        layoutAttributesObjects?.forEach({ layoutAttributes in
            if layoutAttributes.representedElementCategory == .cell {
                if let newFrame = layoutAttributesForItem(at: layoutAttributes.indexPath)?.frame {
                    layoutAttributes.frame = newFrame
                }
            }
        })
        return layoutAttributesObjects
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let collectionView = collectionView else {
            return nil
        }

        guard let layoutAttributes = super
                .layoutAttributesForItem(at: indexPath)?
                .copy()
                as? UICollectionViewLayoutAttributes else {
            return nil
        }

        if scrollDirection == .horizontal {
            let containerHeight = collectionView.bounds.height - sectionInset.top - sectionInset.bottom
            layoutAttributes.frame.origin.y = (containerHeight - layoutAttributes.frame.height) / 2.0
        } else {
            let containerWidth = collectionView.bounds.width - sectionInset.left - sectionInset.right
            layoutAttributes.frame.origin.x = (containerWidth - layoutAttributes.frame.width) / 2.0
        }

        return layoutAttributes
    }
}
