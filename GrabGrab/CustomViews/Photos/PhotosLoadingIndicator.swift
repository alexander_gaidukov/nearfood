//
//  PhotosLoadingIndicator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class PhotosLoadingIndicator: UIView {
    private var photos: [UIImage] = []

    private var bgImageView: UIImageView!
    private var animationView: UIView!
    private var clockMaskView: ClockMaskView!
    private var foregroundImageView: UIImageView!

    private var currentBGIndex: Int = 0
    private var currentFGIndex: Int {
        if currentBGIndex < photos.count - 1 {
            return currentBGIndex + 1
        }
        return 0
    }

    private var isAnimating: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        configureViews()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        clockMaskView.frame = animationView.bounds
    }

    private func configureViews() {
        bgImageView = UIImageView(frame: .zero)
        bgImageView.contentMode = .scaleAspectFit
        fill(with: bgImageView)

        animationView = UIView(frame: .zero)
        animationView.backgroundColor = .background
        fill(with: animationView)

        foregroundImageView = UIImageView(frame: .zero)
        foregroundImageView.contentMode = .scaleAspectFit
        animationView.fill(with: foregroundImageView)

        clockMaskView = ClockMaskView(frame: bounds)
        clockMaskView.delegate = self
        animationView.mask = clockMaskView
    }

    private func nextStep() {
        if currentBGIndex < photos.count - 1 {
            currentBGIndex += 1
        } else {
            currentBGIndex = 0
        }
        bgImageView.image = photos[currentBGIndex]
        startAnimation()
    }

    private func startAnimation() {
        foregroundImageView.image = photos[currentFGIndex]
        clockMaskView.start()
    }

    func configure(with photos: [UIImage]) {
        self.photos = photos
        bgImageView.image = photos.first
        currentBGIndex = 0
    }

    func start() {
        guard !isAnimating && photos.count > 1 else { return }
        isAnimating = true
        startAnimation()
    }

    func stop() {
        guard isAnimating else { return }
        isAnimating = false
    }
}

extension PhotosLoadingIndicator: ClockMaskViewDelegate {
    func clockMaskViewDidFinish() {
        guard isAnimating else { return }
        nextStep()
    }
}
