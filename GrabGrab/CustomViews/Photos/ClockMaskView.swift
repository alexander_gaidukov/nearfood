//
//  ClockMaskView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 15.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol ClockMaskViewDelegate: AnyObject {
    func clockMaskViewDidFinish()
}

final class ClockMaskView: UIView {

    weak var delegate: ClockMaskViewDelegate?
    private var maskLayer: CAShapeLayer

    override init(frame: CGRect) {
        maskLayer = CAShapeLayer()
        super.init(frame: frame)
        backgroundColor = .clear
        layer.addSublayer(maskLayer)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        configureMaskLayer()
    }

    private func configureMaskLayer() {
        let radius = sqrt(pow(bounds.width, 2.0) + pow(bounds.height, 2.0)) / 2.0
        maskLayer.strokeColor = UIColor.white.cgColor
        maskLayer.fillColor = UIColor.clear.cgColor
        maskLayer.lineWidth = radius
        maskLayer.frame = layer.bounds

        let center = CGPoint(x: bounds.width/2, y: bounds.height/2)
        let startAngle: CGFloat = -0.25 * 2 * .pi
        let endAngle: CGFloat = startAngle + 2 * .pi
        maskLayer.path = UIBezierPath(
            arcCenter: center,
            radius: radius / 2,
            startAngle: startAngle,
            endAngle: endAngle,
            clockwise: true
        ).cgPath
    }

    func start() {
        CATransaction.begin()
        maskLayer.strokeEnd = 1.0
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = Constants.Intervals.photosLoadingIndicatorInterval
        CATransaction.setCompletionBlock {
            self.delegate?.clockMaskViewDidFinish()
        }
        maskLayer.add(animation, forKey: nil)
        CATransaction.commit()
    }
}
