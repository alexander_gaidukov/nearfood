//
//  SliderView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 25.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class SliderView: UIStackView {
    var itemsCount: Int = 0 {
        didSet {
            configureItems()
        }
    }

    var selectedIndex: Int = 0 {
        didSet {
            updateSelection()
        }
    }

    var didSelect: ((Int) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        distribution = .fillEqually
        alignment = .fill
    }

    private func configureItems() {
        arrangedSubviews.forEach { $0.removeFromSuperview() }

        guard itemsCount > 0 else { return }

        (0..<itemsCount).forEach {
            let view = sliderView(at: $0)
            view.subviews.first?.backgroundColor = $0 == selectedIndex ? .sliderSelected : .slider
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTap(_:))))
            addArrangedSubview(view)
        }
    }

    private func updateSelection() {
        arrangedSubviews.enumerated().forEach {
            $0.element.subviews.first?.backgroundColor = $0.offset == selectedIndex ? .sliderSelected : .slider
        }
    }

    @objc private func viewDidTap(_ recognizer: UITapGestureRecognizer) {
        // swiftlint:disable:next force_unwrapping
        didSelect?(recognizer.view!.tag)
    }

    private func sliderView(at index: Int) -> UIView {
        let innerView = UIView(frame: .zero)
        innerView.cornerRadius = 2
        innerView.translatesAutoresizingMaskIntoConstraints = false

        let view = UIView(frame: .zero)
        view.tag = index
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(innerView)

        NSLayoutConstraint.activate([
            innerView.heightAnchor.constraint(equalToConstant: 4),
            innerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            innerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            innerView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])

        return view
    }
}
