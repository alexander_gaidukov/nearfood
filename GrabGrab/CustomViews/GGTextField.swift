//
//  GGTextField.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class OffsetTextField: UITextField {

    @IBInspectable
    var offset: CGFloat = 12.0

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        CGRect(
            x: bounds.origin.x + offset,
            y: bounds.origin.y,
            width: bounds.width - offset,
            height: bounds.height
        )
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        CGRect(
            x: bounds.origin.x + offset,
            y: bounds.origin.y,
            width: bounds.width - offset,
            height: bounds.height
        )
    }
}

final class GGTextField: OffsetTextField {
    @IBOutlet private weak var errorLabel: UILabel?
    @IBOutlet private weak var inputContainerView: UIView?

    var errorMessage: String? = nil {
        didSet {
            errorLabel?.text = errorMessage
            configureView()
        }
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                configureView()
            }
        }
    }

    private func configureView() {
        let inputView = inputContainerView ?? self
        let hasError = errorMessage?.isEmpty == false
        inputView.backgroundColor = hasError ? .errorBackground : .inputBackground
        inputView.borderColor = hasError ? .errorBorder : .inputBorder
    }
}
