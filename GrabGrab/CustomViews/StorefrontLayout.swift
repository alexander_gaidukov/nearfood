//
//  ClippedCellLayout.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 11.05.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class StorefrontLayout: UICollectionViewFlowLayout {

    private var allAttributes: [[UICollectionViewLayoutAttributes]] = []
    private var sectionsOffsets: [CGFloat] = []
    private var contentSize: CGSize = .zero

    override func prepare() {
        setupAttributes()

        let lastItemFrame = allAttributes.filter { !$0.isEmpty }.last?.last?.frame ?? .zero
        contentSize = CGSize(width: lastItemFrame.maxX, height: lastItemFrame.maxY)
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()

        for sectionAttrs in allAttributes {
            for itemAttrs in sectionAttrs where rect.intersects(itemAttrs.frame) {
                layoutAttributes.append(itemAttrs)
            }
        }

        return layoutAttributes
    }

    override var collectionViewContentSize: CGSize {
        contentSize
    }

    private func setupAttributes() {
        guard let collectionView = collectionView else {
            return
        }

        allAttributes = []
        sectionsOffsets = []

        var yOffset: CGFloat = 0

        for section in 0..<collectionView.numberOfSections {
            var sectionAttrs: [UICollectionViewLayoutAttributes] = []
            sectionsOffsets.append(yOffset)
            for item in 0..<collectionView.numberOfItems(inSection: section) {
                let itemSize = size(forSection: section, item: item)
                let indexPath = IndexPath(item: item, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: 0, y: yOffset, width: itemSize.width, height: itemSize.height).integral
                attributes.zIndex = 1

                sectionAttrs.append(attributes)

                yOffset += itemSize.height
            }
            allAttributes.append(sectionAttrs)
        }
    }

    private func size(forSection section: Int, item: Int) -> CGSize {
        guard
            let collectionView = collectionView,
            let delegate = collectionView.delegate as? UICollectionViewDelegateFlowLayout,
            let size = delegate.collectionView?(
                collectionView,
                layout: self,
                sizeForItemAt: IndexPath(item: item, section: section)
            )
        else {
            return .zero
        }

        return size
    }

    func yOffset(for indexPath: IndexPath) -> CGFloat {
        allAttributes.get(at: indexPath.section)?.get(at: indexPath.item)?.frame.minY ?? 0
    }

    func section(for offset: CGFloat) -> Int {
        let index = sectionsOffsets.firstIndex { $0 > offset } ?? sectionsOffsets.count
        return max(index - 1, 0)
    }
}
