//
//  LoadingView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol LoadingViewPresenter {
    var loadingPresentingView: UIView { get }
    var isLoading: Bool { get }
}

extension LoadingViewPresenter {
    func dismissLoading(animated: Bool = true) {
        LoadingView.dismissLoadingView(from: loadingPresentingView, animated: animated)
    }

    func showLoading(animated: Bool = true, delayed: Bool = true) {
        if delayed {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                if self.isLoading {
                    LoadingView.showLoadingView(in: self.loadingPresentingView, animated: animated)
                }
            }
        } else {
            LoadingView.showLoadingView(in: self.loadingPresentingView, animated: animated)
        }
    }
}

final class LoadingView: UIView {

    private static let animationDuration: TimeInterval = 0.3

    @IBOutlet private weak var imageView: UIImageView!
    private let kRotationAnimationKey = "com.grabgrab.rotationanimationkey"

    private static func loadingView(in view: UIView) -> LoadingView? {
        view.subviews.first { $0 is LoadingView } as? LoadingView
    }

    static func showLoadingView(in view: UIView, animated: Bool = true) {
        guard loadingView(in: view) == nil else { return }
        let loadingView = instance()
        view.fill(with: loadingView)
        guard animated else { return }
        loadingView.alpha = 0.0
        UIView.animate(withDuration: animationDuration) { loadingView.alpha = 1.0 }
    }

    static func dismissLoadingView(from view: UIView, animated: Bool = true) {
        guard let loadingView = loadingView(in: view) else { return }

        guard animated else {
            loadingView.removeFromSuperview()
            return
        }

        UIView.animate(
            withDuration: animationDuration,
            animations: { loadingView.alpha = 0.0 },
            completion: { _ in loadingView.removeFromSuperview() }
        )
    }

    private static func instance() -> LoadingView {
        let loadingView = LoadingView.fromNib()
        loadingView.rotateImageView()
        return loadingView
    }

    private func rotateImageView(duration: Double = 0.8) {
        if imageView.layer.animation(forKey: kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")

            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float.pi * 2.0
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = Float.infinity

            imageView.layer.add(rotationAnimation, forKey: kRotationAnimationKey)
        }
    }
}
