//
//  ImagesStackView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 19.03.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class ImagesStackView: UIStackView {

    var overlapping: CGFloat = 11.0 {
        didSet {
            arrangedSubviews.forEach {
                ($0.subviews.first as? ComboPhotosView)?.overlapping = overlapping
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        arrangedSubviews.forEach {
            let photoView = ComboPhotosView.fromNib()
            photoView.overlapping = overlapping
            $0.fill(with: photoView)
        }
    }

    func prepareForReuse() {
        arrangedSubviews.forEach {
            ($0.subviews.first as? ComboPhotosView)?.prepareForReuse()
        }
    }

    func configure(with photos: [[Photo]]) {
        configure(with: photos.map { $0.map(\.url) })
    }

    func configure(with photoURLs: [[String]]) {
        for index in 0..<min(arrangedSubviews.count, photoURLs.count) {
            arrangedSubviews[index].isHidden = false
            (arrangedSubviews[index].subviews.first as? ComboPhotosView)?
                .configure(with: photoURLs[index])
        }

        if arrangedSubviews.count > photoURLs.count {
            for index in photoURLs.count..<arrangedSubviews.count {
                arrangedSubviews[index].isHidden = true
            }
        }
    }
}
