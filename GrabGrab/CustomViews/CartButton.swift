//
//  CartButton.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 28.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class CartButton: UIButton {
    convenience init(amount: Double) {
        self.init(type: .custom)
        setupView()
        configure(withAmount: amount)
    }

    private func setupView() {
        clipsToBounds = true
        layer.cornerRadius = 8
        titleLabel?.font = UIFont.boldSystemFont(ofSize: 18.0)
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.5
        contentEdgeInsets = UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10)
        setImage(#imageLiteral(resourceName: "bucket").withRenderingMode(.alwaysTemplate), for: .normal)
    }

    func configure(withAmount amount: Double) {
        isUserInteractionEnabled = amount > 0
        setTitleColor(amount > 0 ? .background : .tabbarText, for: .normal)
        tintColor = amount > 0 ? .background : .tabbarText
        backgroundColor = amount > 0 ? .mainText : .clear
        setTitle("  \(amount.string(with: Formatters.priceFormatter) ?? "")", for: .normal)
    }
}
