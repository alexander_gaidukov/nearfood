//
//  GGTextView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 04.09.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class GGTextView: UITextView {

    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!

    @IBInspectable
    var offset: CGFloat = 12.0

    @IBInspectable
    var minHeight: CGFloat = 37.0

    @IBInspectable
    var maxHeight: CGFloat = 100.0

    var placeholder: String = "" {
        didSet {
            configurePlaceholder()
        }
    }

    override var text: String! {
        get {
            super.text
        }

        set {
            super.text = newValue
            configurePlaceholder()
            let height = max(min(contentSize.height, maxHeight), minHeight)
            if height != heightConstraint.constant {
                heightConstraint.constant = height
            }
        }
    }

    var didChange: ((String) -> ())?
    var didBeginEditing: (() -> ())?
    var didEndEditing: (() -> ())?

    var isPlaceholder: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        var insets = textContainerInset
        insets.left = offset
        insets.right = offset

        insets.top = max(0, minHeight - (font?.lineHeight ?? 0)) / 2.0

        textContainerInset = insets
        delegate = self
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle {
                borderColor = .inputBorder
            }
        }
    }

    private func configurePlaceholder() {
        guard !isFirstResponder else { return }
        if text?.isEmpty != false {
            text = placeholder
            textColor = .labelText
            isPlaceholder = true
        } else {
            textColor = .mainText
            isPlaceholder = false
        }
    }
}

extension GGTextView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if isPlaceholder {
            text = ""
            textColor = .mainText
            isPlaceholder = false
        }
        didBeginEditing?()
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        configurePlaceholder()
        didEndEditing?()
    }

    func textViewDidChange(_ textView: UITextView) {
        didChange?(textView.text ?? "")
    }
}
