//
//  GGTableView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 12.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class GGTableView: TPKeyboardAvoidingTableView {

    private var registeredIdentifiers: Set<String> = []

    override func register(_ nib: UINib?, forCellReuseIdentifier identifier: String) {
        guard !registeredIdentifiers.contains(identifier) else { return }
        registeredIdentifiers.insert(identifier)
        super.register(nib, forCellReuseIdentifier: identifier)
    }

    override func register(_ cellClass: AnyClass?, forCellReuseIdentifier identifier: String) {
        guard !registeredIdentifiers.contains(identifier) else { return }
        registeredIdentifiers.insert(identifier)
        super.register(cellClass, forCellReuseIdentifier: identifier)
    }
}
