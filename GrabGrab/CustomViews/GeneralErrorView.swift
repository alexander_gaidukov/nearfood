//
//  GeneralErrorView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 20.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class GeneralErrorView: UIView {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var refreshButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        refreshButton.setTitle(LocalizedStrings.Common.refresh, for: .normal)
    }

    func configure(error: WebError?) {
        guard let error = error else {
            isHidden = true
            return
        }

        let is422Error = error.errorCode == 422
        isHidden = false
        titleLabel.isHidden = is422Error
        refreshButton.isHidden = is422Error
        imageView.image = is422Error ? #imageLiteral(resourceName: "disabled_zone") : #imageLiteral(resourceName: "exclamation")
        titleLabel.text = error.title
        descriptionLabel.text = error.localizedDescription
    }
}
