//
//  MainButton.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 30.07.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class MainButton: UIButton {
    override var isEnabled: Bool {
        get {
            super.isEnabled
        }

        set {
            super.isEnabled = newValue
            backgroundColor = newValue ? .primary : .backgroundDisabled
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleColor(.white, for: .normal)
        setTitleColor(.textDisabled, for: .disabled)
        clipsToBounds = true
        cornerRadius = 8
    }
}
