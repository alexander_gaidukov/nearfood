//
//  BannerView.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 06.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol BannerViewPresenter {
    var bannerPresentingView: UIView { get }
}

extension BannerViewPresenter {

    func showBanner(withType type: BannerView.BannerType) {
        BannerView.showBanner(withType: type, from: bannerPresentingView)
    }

    func dismissBanner() {
        BannerView.dismissBanner(from: bannerPresentingView)
    }
}

final class BannerView: UIView {
    enum BannerType {
        case error(String)
        case success(String)

        var backgroundColor: UIColor {
            switch self {
            case .error:
                return .errorBanner
            case .success:
                return .panelBackground
            }
        }

        var textColor: UIColor {
            switch self {
            case .error:
                return .white
            case .success:
                return .mainText
            }
        }

        var image: UIImage? {
            switch self {
            case .error:
                return #imageLiteral(resourceName: "error_icon")
            case .success:
                return #imageLiteral(resourceName: "success_icon")
            }
        }

        var message: String {
            switch self {
            case .error(let message), .success(let message):
                return message
            }
        }
    }

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var hideImageConstraint: NSLayoutConstraint!

    private var topLayoutConstraint: NSLayoutConstraint!
    private static let animationDuration: TimeInterval = 0.3
    private static var bannerUUID: String?

    static func bannerView(in view: UIView) -> BannerView? {
        view.subviews.first { $0 is BannerView } as? BannerView
    }

    static func showBanner(
        withType type: BannerType,
        from view: UIView,
        dismissAfter timeInterval: TimeInterval = 3.0
    ) {
        let bannerUUID = UUID().uuidString
        self.bannerUUID = bannerUUID

        if let bannerView = bannerView(in: view) {
            bannerView.configure(withType: type)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeInterval) {
                guard BannerView.bannerUUID == bannerUUID else { return }
                BannerView.dismissBanner(from: view)
            }
            return
        }

        guard !type.message.isEmpty else {
            return
        }

        let bannerView = instance(withType: type)
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        bannerView.topLayoutConstraint = bannerView.topAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.topAnchor,
            constant: -150
        )
        NSLayoutConstraint.activate([
            bannerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bannerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bannerView.topLayoutConstraint
        ])
        view.layoutIfNeeded()
        DispatchQueue.main.async {
            bannerView.topLayoutConstraint.constant = 0
            UIView.animate(
                withDuration: animationDuration,
                animations: {  bannerView.superview?.layoutIfNeeded() },
                completion: { _ in
                    DispatchQueue.main.asyncAfter(deadline: .now() + timeInterval) {
                        guard BannerView.bannerUUID == bannerUUID else { return }
                        BannerView.dismissBanner(from: view)
                    }

                }
            )
        }
    }

    static func dismissBanner(from view: UIView) {
        guard let bannerView = bannerView(in: view) else { return }
        bannerView.topLayoutConstraint.constant = -150
        UIView.animate(
            withDuration: animationDuration,
            animations: { bannerView.superview?.layoutIfNeeded() },
            completion: { _ in bannerView.removeFromSuperview()}
        )
    }

    static func instance(withType type: BannerType) -> BannerView {
        let bannerView = BannerView.fromNib()
        bannerView.configure(withType: type)
        bannerView.cornerRadius = 8.0
        return bannerView
    }

    func configure(withType type: BannerType) {
        backgroundColor = type.backgroundColor
        imageView.image = type.image
        messageLabel.text = type.message
        messageLabel.textColor = type.textColor
        hideImageConstraint.priority = type.image == nil ? .defaultHigh : .defaultLow
    }

    @IBAction private func buttonTapped() {
        if let superview = superview {
            BannerView.dismissBanner(from: superview)
        }
    }
}
