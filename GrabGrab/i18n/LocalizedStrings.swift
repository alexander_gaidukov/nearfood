//
//  LocalizedStrings.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 01.08.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

// swiftlint:disable type_body_length
// swiftlint:disable file_length

extension String {
    var localized: String {
        NSLocalizedString(self, comment: "")
    }
}

struct LocalizedStrings {

    struct Common {
        static let close = "common.close".localized
        static let minutes = "common.minutes".localized
        static let cancel = "common.cancel".localized
        static let remove = "common.remove".localized
        static let add = "common.add".localized
        static let save = "common.save".localized
        static let select = "common.select".localized
        static let refresh = "common.refresh".localized
        static let yes = "common.yes".localized
        // swiftlint:disable:next identifier_name
        static let no = "common.no".localized
        static let confirm = "common.confirm".localized
        static let today = "common.today".localized
        static let yesterday = "common.yesterday".localized
        static let done = "common.done".localized
        static let change = "common.change".localized
        static let free = "common.free".localized
        static let openSettings = "common.settings.open".localized
        static let apply = "common.apply".localized
        static let details = "common.details".localized
    }

    struct Error {
        static let noInternetTitle = "errors.noInternet.title".localized
        static let noInternetDescription = "errors.noInternet.description".localized
        static let commonTitle = "errors.common.title".localized
        static let commonDescription = "errors.common.description".localized
    }

    struct Login {
        static let phoneTitle = "login.phone.title".localized
        static let phonePlaceholder = "login.phone.placeholder".localized
        static let buttonTitle = "login.button.title".localized
    }

    struct PhoneConfirmation {
        static let navigationTitle = "phoneConfirmation.navigation.title".localized
        static let codeTitle = "phoneConfirmation.title".localized
        static let codePlaceholder = "phoneConfirmation.code.placeholder".localized
        static let sendCodeAgainLabel = "phoneConfirmation.code.sendAgain.label".localized
        static let sendCodeAgainButtonTitle = "phoneConfirmation.code.sendAgain.buttton".localized
    }

    struct Activation {
        struct Beta {
            static let title = "activation.beta.title".localized
            static let description = "activation.beta.description".localized
            static let logoutButton = "activation.beta.logout".localized
        }

        struct Invite {
            static let title = "activation.invite.title".localized
            static let start = "activation.invite.start".localized
        }

        struct Welcome {
            static let title = "activation.welcome.title".localized
            static let description = "activation.welcome.description".localized
            static let invitationButton = "activation.welcome.invitation.button".localized
        }

        static let title = "activation.title".localized
        static let continueButton = "activation.continue".localized
        static let activateButton = "activation.activate".localized
        static let codeTitle = "activation.code.title".localized
        static let codePlaceholder = "activation.code.placeholder".localized
    }

    struct Countries {
        static let title = "countries.title".localized
    }

    struct Cities {
        static let title = "cities.title".localized
    }

    struct Onboarding {
        struct Location {
            static let title = "onboarding.location.title".localized
            static let description = "onboarding.location.description".localized
            static let nextTitle = "onboarding.location.next".localized
        }

        struct Notifications {
            static let title = "onboarding.notifications.title".localized
            static let description = "onboarding.notifications.description".localized
            static let nextTitle = "onboarding.notifications.next".localized
        }

        struct Address {
            static let title = "onboarding.address.title".localized
            static let saveButtonTitle = "onboarding.address.button.title".localized
        }
    }

    struct Addresses {
        static let title = "address.title".localized
        static let saveButtonTitle = "address.saveButton.title".localized
        static let addressLabel = "address.address.label".localized
        static let addressPlaceholder = "address.address.placeholder".localized
        static let apartmentsLabel = "address.apartments.label".localized
        static let entranceLabel = "address.entrance.label".localized
        static let floorLabel = "address.floor.label".localized
        static let doorphoneLabel = "address.doorphone.label".localized
        static let noteLabel = "address.note.label".localized
        static let notePlaceholder = "address.note.placeholder".localized
        static let iamHereLabel = "address.iamhere".localized
        static let paidArea = "address.paidArea".localized

        static let screenshots = "address.screenshots".localized

        struct Suggestions {
            static let title = "address.suggestions.title".localized
            static let label = "address.suggestions.label".localized
            static let placeholder = "address.suggestions.placeholder".localized
        }
    }

    struct Menu {

        struct Closed {
            static let title = "menu.closed.title".localized
            static let description = "menu.closed.description".localized
        }

        struct Combos {
            static let title = "menu.combos.title".localized
        }
    }

    struct Combo {
        static let consistLabel = "menu.combo.consist.label".localized
    }

    struct Dish {

        struct Unit {
            static let gramsSuffix = "menu.dish.grams.suffix".localized
            static let litersSuffix = "menu.dish.liters.suffix".localized
            static let piecesSuffix = "menu.dish.pieces.suffix".localized
            static let millilitersSuffix = "menu.dish.milliliters.suffix".localized

            static let grams = "dish.details.grams".localized
            static let liters = "dish.details.liters".localized
            static let pieces = "dish.details.pieces".localized
            static let milliliters = "dish.details.milliliters".localized
        }

        struct Features {
            static let vegan = "dish.feature.vegan".localized
            static let vegetarian = "dish.feature.vegetarian".localized
            static let spicy = "dish.feature.spicy".localized
        }

        static let caloriesLabel = "dish.details.calories".localized
        static let proteinsLabel = "dish.details.proteins".localized
        static let fatsLabel = "dish.details.fats".localized
        static let carbsLabel = "dish.details.carbs".localized
    }

    struct Account {
        static let logoutButtonTitle = "account.logout.title".localized
        static let logoutConfirmationMessage = "account.logout.confirmation".localized
        static let customerNameLabel = "account.customerName.label".localized
        static let addressesLabel = "account.addresses.label".localized
        static let ordersLabel = "account.orders.label".localized
        static let bonusesLabel = "account.bonuses.label".localized
        static let paymentMethodsLabel = "account.paymentMethods.label".localized
        static let aboutUsLabel = "account.aboutUs.label".localized
        static let tipsLabel = "account.tips.label".localized

        struct Addreses {
            static let title = "account.addresses.title".localized
            struct New {
                static let title = "account.addresses.new.title".localized
                static let addressTitleLabel = "account.addresses.address.title".localized
                static let addressTitlePlaceholder = "account.addresses.address.title.placeholder".localized
            }
            struct Empty {
                static let title = "account.addresses.empty.title".localized
                static let description = "account.addresses.empty.description".localized
                static let button = "account.address.empty.button".localized
            }
        }

        struct Name {
            static let title = "account.name.title".localized
            static let description = "account.name.description".localized
            static let placeholder = "account.name.placeholder".localized
        }

        struct Orders {
            struct Empty {
                static let title = "account.orders.empty.title".localized
                static let description = "account.orders.empty.description".localized
            }
        }
    }

    struct Cart {
        static func title(for eta: Int) -> String {
            String(format: "cart.title".localized, eta)
        }
        static let cutleryTitle = "cart.cutlery".localized
        static let deliveryTitle = "cart.delivery".localized
        static let orderButtonTitle = "cart.orderButton.title".localized
        static let freeOrderButtonTitle = "cart.orderButton.free.title".localized

        struct Empty {
            static let title = "cart.dish.empty.title".localized
            static let buttonTitle = "cart.dish.empty.button".localized
        }

        struct Address {
            static let label = "cart.address.label".localized
        }

        struct Dishes {
            static let label = "cart.dishes.label".localized
            static let stopList = "cart.dishes.stoplist".localized
        }

        struct Surge {
            static let info = "cart.surge.info".localized
        }

        struct PaymentMethod {
            static let label = "cart.paymentMethod.label".localized
            static let addNew = "cart.paymentMethod.add".localized
        }

        struct Invalid {
            static let newPriceDescription = "cart.invalid.priceChanged.description".localized
            static let description = "cart.invalid.description".localized
            static let orderButtonTitle = "cart.invalid.orderButton".localized
            static let cancelButtonTitle = "cart.invalid.cancelButton".localized
            static let priceTitle = "cart.invalid.priceTitle".localized
        }

        struct AddressVerification {
            static let title = "cart.address.verify.title".localized
            static let confirm = "cart.address.verify.confirm".localized
            static let decline = "cart.address.verify.declline".localized

            static func description(for address: String) -> String {
                String(format: "cart.address.verify.description".localized, address)
            }
        }

        struct Contactless {
            static let title = "cart.contactless.info.title".localized
            static let description = "cart.contactless.info.description".localized
        }

        struct PaymentProcessing {
            static let loadingTitle = "payment.processing.loading.title".localized
            static let errorTitle = "payment.processing.error.title".localized
            static let errorDescription = "payment.processing.error.description".localized
            static let repeatButton = "payment.processing.repeat.button".localized
        }

        struct DeliveryTime {
            static let asap = "cart.deliveryTime.asap".localized
            static let title = "cart.deliveryTime.title".localized
            static func description(fontSize: Double) -> NSAttributedString {
                .instance(html: "cart.deliveryTime.description".localized, fontSize: fontSize)
            }
        }

        struct Coupons {
            static let placeholder = "cart.coupons.placeholder".localized
            static let cashbackInfo = "cart.coupon.cashback.info".localized

            static func maxDiscount(_ amount: String) -> String {
                String(format: "cart.coupon.maxDiscount".localized, amount)
            }

            static func maxCashback(_ amount: String) -> String {
                String(format: "cart.coupon.maxCashback".localized, amount)
            }

            static func cashback(_ amount: String) -> String {
                String(format: "cart.coupon.cashback".localized, amount)
            }

            static func applyConfirmation(_ code: String) -> String {
                String(format: "cart.coupon.addd.confirmation".localized, code)
            }

            static func removeConfirmation(_ code: String) -> String {
                String(format: "cart.coupon.removed.confirmation".localized, code)
            }
        }
    }

    struct Recipient {
        static let empty = "recipient.empty.title".localized
        struct AddOrUpdate {
            static let title = "recipient.add.title".localized
            static let description = "recipient.add.description".localized
            static let name = "recipient.add.name".localized
            static let phone = "recipient.add.phone".localized
            static let remove = "recipient.add.remove".localized
            static let add = "recipient.add.add".localized

            struct Phone {
                static let title = "recipient.phone.title".localized
                static let fill = "recipient.phone.new.fill".localized
                static let info = "recipient.phone.new.info".localized

                struct Empty {
                    static let title = "recipient.phone.empty.title".localized
                    static func description(for search: String) -> String {
                        String(format: "recipient.phone.empty.description".localized, search)
                    }
                }

                struct Access {
                    static let title =  "recipient.phone.access.title".localized
                    static let description = "recipient.phone.access.description".localized
                }
            }
        }

    }

    struct PaymentMethod {
        static let notConfirmed = "paymentMethods.notconfirmed".localized
        static let addNew = "paymentMethods.add".localized
        static let applePay = "paymentMethod.applePay".localized
        struct New {
            static let title = "paymentMethods.new.title".localized
            static let description = "paymentMethods.new.description".localized
            static let cardNumberPlaceholder = "paymentMethods.new.cardNumber.placeholder".localized
            static let cardNumberError = "paymentMethods.new.cardNumber.error".localized
            static let holderNamePlaceholder = "paymentMethods.new.holderName.placeholder".localized
            static let holderNameError = "paymentMethods.new.holderName.error".localized
            static let expirationDatePlaceholder = "paymentMethods.new.date.placeholder".localized
            static let expirationDateError = "paymentMethods.new.date.error".localized
            static let cvvPlaceholder = "paymentMethods.new.cvv.placeholder".localized
            static let cvvError = "paymentMethods.new.cvv.error".localized
            static let button = "paymentMethods.new.button".localized
        }

        struct ApplePay {
            static let payee = "paymentMethod.ApplePay.payee".localized
            static let subtotal = "paymentMethod.ApplePay.subtotal".localized
            static let delivery = "paymentMethod.ApplePay.delivery".localized
            static let surge = "paymentMethod.ApplePay.surge".localized
            static let bonuses = "paymentMethod.ApplePay.bonuses".localized
            static let discount = "paymentMethod.ApplePay.discount".localized
        }

        static func removalConfrimation(for card: String) -> String {
            String(format: "paymentMethods.remove.confirmation".localized, card)
        }
    }

    struct Order {
        static let total = "order.total.label".localized
        static let support = "order.support.label".localized
        static let receipt = "order.receipt.label".localized
        static let review = "order.review.label".localized
        static let cancel = "order.cancel.label".localized
        static let current = "order.current.title".localized
        static let cancelConfirmation = "order.cancel.confirmation".localized
        static let title = "order.title".localized
        struct Status {
            static let pending = "order.status.pending".localized
            static let preparing = "order.status.preparing".localized
            static let unpaid = "order.status.unpaid".localized
            static let ontheway = "order.status.ontheway".localized
            static let cancelled = "order.status.cancelled".localized
            static let arrived = "order.status.arrived".localized
            static let completedList = "order.status.completed.list".localized

            static func completed(in mins: Int) -> String {
                String(format: "order.status.completed".localized, mins)
            }
        }

        struct Address {
            static let savePrompt = "order.address.save.prompt".localized

            static func entrance(_ entrance: String) -> String {
                String(format: "order.address.entrance".localized, entrance)
            }
            static func apartments(_ apartments: String) -> String {
                String(format: "order.address.apartments".localized, apartments)
            }
            static func floor(_ floor: String) -> String {
                String(format: "order.address.floor".localized, floor)
            }
            static func doorphone(_ doorphone: String) -> String {
                String(format: "order.address.doorphone".localized, doorphone)
            }
        }

        struct Payments {
            static let problems = "order.payment.problem.label".localized
        }

        struct Bundle {
            static func message(for order: Int) -> String {
                String(format: "order.bundle.message".localized, order)
            }
        }

        struct Review {
            static let note = "order.review.note.placeholder".localized
            static let title = "order.review.title".localized
            static let description = "order.review.description".localized
            static let completeTitle = "order.review.complete.title".localized

            struct Rate {
                static let perfect = "order.review.rate.perfect".localized
                static let good = "order.review.rate.good".localized
                static let notReally = "order.review.rate.notReally".localized
                static let bad = "order.review.rate.bad".localized
            }
        }

        struct Cancellation {
            struct Fees {
                static let title = "order.cancellation.fees.title".localized
                static let description = "order.cancellation.fees.description".localized
                static let confirm = "order.cancellation.fees.confirm".localized
                static let cancel = "order.cancellation.fees.cancel".localized
            }
        }

        struct Comment {
            static let empty = "order.comment.empty".localized
            static let placeholder = "order.comment.placeholder".localized
            static let title = "order.comment.title".localized
            static let description = "order.comment.description".localized
        }

        struct Tips {
            static let receipt = "order.tips.receipt".localized
            static let rejected = "order.tips.rejected".localized
            static let noTips = "order.tips.notips".localized
            static let cancelled = "order.tips.cancelled".localized

            static func completed(amount: String) -> String {
                String(format: "order.tips.completed".localized, amount)
            }

            static func processing(amount: String) -> String {
                String(format: "order.tips.processing".localized, amount)
            }

            static func saveAsDefault(amount: String) -> String {
                String(format: "order.tips.save".localized, amount)
            }
        }
    }

    struct Chat {
        static let title = "chat.title".localized
        static let messagePlaceholder = "chat.message.placeholder".localized
        static let support = "chat.author.support".localized
        struct Media {
            static let camera = "chat.photo.source.camera".localized
            static let photoLibrary = "chat.photo.source.mediateque".localized
        }
        struct Actions {
            static let sendAgain = "chat.actions.sendAgain".localized
            static let remove = "chat.actions.remove".localized
        }
    }

    struct Surge {
        static let title = "surge.title".localized
        static let increasedTimeTitle = "surge.increasedTime.title".localized
        static let description = "surge.description".localized
        struct Confirnation {
            static let cancel = "surge.confirmation.cancel".localized
            static let confirm = "surge.confirmation.confirm".localized
        }
    }

    struct Widget {
        static let name = "widget.name".localized
        static let description = "widget.description".localized
        static let noOrders = "widget.noOrders".localized
    }

    struct AppVersion {
        struct Unsupported {
            static let title = "appVersion.unsupported.title".localized
            static let description = "appVersion.unsupported.description".localized
            static let updateButton = "appVersion.unsupported.update".localized
        }

        struct New {
            static let title = "appVersion.new.title".localized
            static let description = "appVersion.new.description".localized
        }
    }

    struct Info {
        static let title = "info.title".localized
        static let followUsLabel = "info.followUs.label".localized
    }

    struct Invitations {
        static let title = "invitations.title".localized
        struct Cell {
            static let share = "invitations.cell.share".localized
            static func availableUsages(_ usages: Int) -> String {
                String(format: "invitations.cell.usages".localized, usages)
            }
        }

        struct MenuCell {
            static let title = "invitations.menu.title".localized
            static let button = "invitations.menu.button".localized
            static func description(count: Int) -> String {
                String(format: "invitations.menu.description".localized, count)
            }
        }

        struct Empty {
            static let title = "invitations.empty.title".localized
            static let description = "invitations.empty.description".localized
        }
    }

    struct Intro {
        struct Taste {
            static let title = "intro.taste.title".localized
            static let description = "intro.taste.description".localized
        }

        struct Delivery {
            static let title = "intro.delivery.title".localized
            static let description = "intro.delivery.description".localized
        }

        struct Transparancy {
            static let title = "intro.transparancy.title".localized
            static let description = "intro.transparancy.description".localized
        }

        struct Quality {
            static let title = "intro.quality.title".localized
            static let description = "intro.quality.description".localized
        }

        static let nextButton = "intro.next.button".localized
        static let completeButton = "intro.complele.button".localized
    }

    struct Tips {
        struct Options {
            static let title = "tips.options.title".localized
            static func defaultOptionConfirmation(value: String) -> String {
                String(format: "tips.options.default.comfirmation".localized, value)
            }
            static func orderOptionConfirmation(value: String) -> String {
                String(format: "tips.options.order.comfirmation".localized, value)
            }

            struct Percent {
                static let title = "tips.options.percent.title".localized
                static let description = "tips.options.percent.description".localized
            }

            struct Fixed {
                static let title = "tips.options.fixed.title".localized
                static let description = "tips.options.fixed.description".localized
                static let otherAmount = "tips.options.fixed.other".localized
            }
        }
    }
}
