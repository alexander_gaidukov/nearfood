//
//  UnsupportedVersionViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class UnsupportedVersionViewController: UIViewController {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var updateButton: UIButton!

    var presenter: UnsupportedVersionPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }

    private func configureViews() {
        titleLabel.text = LocalizedStrings.AppVersion.Unsupported.title
        descriptionLabel.text = LocalizedStrings.AppVersion.Unsupported.description
        updateButton.setTitle(LocalizedStrings.AppVersion.Unsupported.updateButton, for: .normal)
    }

    @IBAction private func updateTapped() {
        presenter.updateApp()
    }
}
