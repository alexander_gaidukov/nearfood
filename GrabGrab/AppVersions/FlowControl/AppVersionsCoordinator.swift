//
//  AppVersionsCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

final class AppVersionsCoordinator: Coordinator {
    private let router: AppVersionsRouterType
    private let dataProvidersStorage: DataProvidersStorageType

    init(router: AppVersionsRouterType,
         dataProvidersStorage: DataProvidersStorageType
    ) {
        self.router = router
        self.dataProvidersStorage = dataProvidersStorage
    }

    func start() {
        showUnsupportedVersion()
    }

    private func showUnsupportedVersion() {
        router.showUnsupportedVersion { action in
            switch action {
            case .update:
                self.dataProvidersStorage.appVersionDataProvider.moveToAppStore()
            }
        }
    }
}
