//
//  AppVersionsRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol AppVersionsRouterType {
    func showUnsupportedVersion(completion: @escaping (UnsupportedVersionAction) -> ())
}

final class AppVersionsRouter: AppVersionsRouterType {
    let window: UIWindow
    private let factory: AppVersionsViewControllersFactoryType

    init(window: UIWindow, factory: AppVersionsViewControllersFactoryType) {
        self.window = window
        self.factory = factory
    }

    func showUnsupportedVersion(completion: @escaping (UnsupportedVersionAction) -> ()) {
        let controller = factory.unsupportedVersionViewController(completion: completion)
        window.rootViewController = controller
    }
}
