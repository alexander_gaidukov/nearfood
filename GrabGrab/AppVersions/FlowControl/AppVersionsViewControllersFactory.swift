//
//  AppVersionsViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol AppVersionsViewControllersFactoryType {
    func unsupportedVersionViewController(completion: @escaping (UnsupportedVersionAction) -> ()) -> UnsupportedVersionViewController
}

struct AppVersionsViewControllersFactory: AppVersionsViewControllersFactoryType {

    let storyboard: Storyboard = .appVersions

    func unsupportedVersionViewController(completion: @escaping (UnsupportedVersionAction) -> ()) -> UnsupportedVersionViewController {
        let presenter = UnsupportedVersionPresenter(completion: completion)
        let controller = UnsupportedVersionViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
