//
//  UnsupportedVersionPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 27.11.2020.
//  Copyright © 2020 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol UnsupportedVersionPresenterType {
    func updateApp()
}

enum UnsupportedVersionAction {
    case update
}

final class UnsupportedVersionPresenter: UnsupportedVersionPresenterType {
    private let completion: (UnsupportedVersionAction) -> ()

    init(completion: @escaping (UnsupportedVersionAction) -> ()) {
        self.completion = completion
    }

    func updateApp() {
        completion(.update)
    }
}
