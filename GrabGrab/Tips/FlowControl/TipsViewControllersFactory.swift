//
//  TipsViewControllersFactory.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol TipsViewControllersFactoryType {
    func tipsOptionsViewController(completion: @escaping (TipsOptionsActions) -> ()) -> TipsOptionsViewController

    func tipsOtherOptionViewController(
        context: TipsViewContext,
        completion: @escaping (TipsOtherOptionAction) -> ()
    ) -> TipsOtherOptionViewController

    func tipsInfoViewController(completion: @escaping (TipsInfoAction) -> ()) -> TipsInfoViewController
}

struct TipsViewControllersFactory: TipsViewControllersFactoryType {

    let dataProvidersStorage: DataProvidersStorageType
    let webClient: WebClientType

    private let storyboard: Storyboard = .tips

    func tipsOptionsViewController(completion: @escaping (TipsOptionsActions) -> ()) -> TipsOptionsViewController {
        let presenter = TipsOptionsPresenter(
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            webClient: webClient,
            completion: completion
        )
        let controller = TipsOptionsViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }

    func tipsOtherOptionViewController(
        context: TipsViewContext,
        completion: @escaping (TipsOtherOptionAction) -> ()
    ) -> TipsOtherOptionViewController {
        let presenter = TipsOtherOptionPresenter(
            context: context,
            customerDataProvider: dataProvidersStorage.customerDataProvider,
            webClient: webClient,
            completion: completion
        )

        let controller = TipsOtherOptionViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return  controller
    }

    func tipsInfoViewController(completion: @escaping (TipsInfoAction) -> ()) -> TipsInfoViewController {
        let presenter = TipsInfoPresenter(completion: completion)
        let controller = TipsInfoViewController.instance(storyboard: storyboard)
        controller.presenter = presenter
        return controller
    }
}
