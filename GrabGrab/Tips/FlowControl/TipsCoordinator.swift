//
//  TipsCoordinator.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

enum TipsViewContext {
    case account
    case order(String)
    case info
}

final class TipsCoordinator: Coordinator {
    private let completion: () -> ()
    private let orderCompletion: (OrderTips?) -> ()
    private let router: TipsRouterType
    private let context: TipsViewContext

    init(router: TipsRouterType, context: TipsViewContext, completion: @escaping () -> ()) {
        self.router = router
        self.context = context
        self.completion = completion
        self.orderCompletion = { _ in }
    }

    init(router: TipsRouterType, orderUUID: String, completion: @escaping (OrderTips?) -> ()) {
        self.router = router
        self.context = .order(orderUUID)
        self.orderCompletion = completion
        self.completion = {}
    }

    func start() {
        switch context {
        case .account:
            showTipsOptions()
        case .info:
            showInfo()
        case .order:
            showOtherTipsOption {
                self.orderCompletion($0)
            }
        }
    }

    private func showInfo() {
        router.showTipsInfo { action in
            switch action {
            case .complete:
                self.router.dismiss { self.completion() }
            }
        }
    }

    private func showTipsOptions() {
        router.showTipsOptions { action in
            switch action {
            case .otherAmount:
                self.showOtherTipsOption()
            case .complete:
                self.completion()
            }
        }
    }

    private func showOtherTipsOption(completion: @escaping (OrderTips?) -> () = { _ in }) {
        router.showTipsOtherOption(context: context) { action in
            switch action {
            case .complete(let orderTips):
                self.router.dismiss { completion(orderTips) }
            }
        }
    }
}
