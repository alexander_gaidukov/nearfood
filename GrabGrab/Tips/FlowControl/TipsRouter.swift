//
//  TipsRouter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

protocol TipsRouterType {
    func showTipsOptions(completion: @escaping (TipsOptionsActions) -> ())
    func showTipsOtherOption(context: TipsViewContext, completion: @escaping (TipsOtherOptionAction) -> ())
    func showTipsInfo(completion: @escaping (TipsInfoAction) -> ())
    func dismiss(completion: @escaping () -> ())
}

final class TipsRouter: NSObject, TipsRouterType {

    private let factory: TipsViewControllersFactoryType
    private weak var presentingViewController: UIViewController?

    init(factory: TipsViewControllersFactoryType, presentingViewController: UIViewController?) {
        self.factory = factory
        self.presentingViewController = presentingViewController
    }

    func showTipsOptions(completion: @escaping (TipsOptionsActions) -> ()) {
        let controller = factory.tipsOptionsViewController(completion: completion)
        if let navController = presentingViewController as? UINavigationController {
            navController.pushViewController(controller, animated: true)
        } else {
            let navController = GGNavigationController(rootViewController: controller)
            presentingViewController?.present(navController, animated: true, completion: nil)
        }
    }

    func showTipsOtherOption(context: TipsViewContext, completion: @escaping (TipsOtherOptionAction) -> ()) {
        let controller = factory.tipsOtherOptionViewController(context: context, completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func showTipsInfo(completion: @escaping (TipsInfoAction) -> ()) {
        let controller = factory.tipsInfoViewController(completion: completion)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = self
        topViewController?.present(controller, animated: true, completion: nil)
    }

    func dismiss(completion: @escaping () -> ()) {
        topViewController?.dismiss(animated: true, completion: completion)
    }

    private var topViewController: UIViewController? {
        var candidate = presentingViewController
        while let controller = candidate?.presentedViewController {
            candidate = controller
        }
        return candidate
    }
}

extension TipsRouter: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: false)
    }

    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        CustomTransitioningDelegate(isPresenting: true)
    }
}
