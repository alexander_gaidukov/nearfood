//
//  TipsOptionSectionModel.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

struct TipsOptionSectionModel: CollectionViewSectionModel {

    private let models: [TipsOptionCellModel]
    private let didSelect: (Int) -> ()

    private let skeletonItemsWidth: CGFloat = 54

    var selectedIndex: Int? {
        guard !isSkeleton else { return nil }
        return models.firstIndex(where: \.isSelected)
    }

    let isSkeleton: Bool

    init (options: [TipsOption], selectedOption: TipsOption?, additionalItem: String? = nil, didSelect: @escaping (Int) -> ()) {
        self.didSelect = didSelect
        self.isSkeleton = options.isEmpty
        if options.isEmpty {
            let count = Int((UIScreen.main.bounds.width / skeletonItemsWidth).rounded(.up))
            models = Array(repeating: TipsOptionCellModel(value: "", isSelected: false), count: count)
        } else {
            var models = options.map { TipsOptionCellModel(value: $0.formattedValue, isSelected: $0 == selectedOption) }
            if let additionalItem = additionalItem {
                models.append(TipsOptionCellModel(value: additionalItem, isSelected: false))
            }
            self.models = models
        }
    }

    var numberOfItems: Int {
        models.count
    }

    func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TipsOptionCell = collectionView.dequeue(forIndexPath: indexPath)
        if let model = models.get(at: indexPath.item) {
            cell.configure(with: model)
        }
        return cell
    }

    func size(for indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
        guard !isSkeleton else {
            return CGSize(width: skeletonItemsWidth, height: collectionView.bounds.height)
        }

        guard let model = models.get(at: indexPath.item) else { return .zero }

        return CGSize(width: TipsOptionCell.width(for: model), height: collectionView.bounds.height)
    }

    func didSelectItem(at indexPath: IndexPath) {
        guard !isSkeleton else { return }
        didSelect(indexPath.item)
    }
}
