//
//  TipsOptionCell.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit
import SkeletonView

struct TipsOptionCellModel {
    let value: String
    let isSelected: Bool

    var bgColor: UIColor {
        isSelected ? .primary : .backgroundDisabled
    }

    var textColor: UIColor {
        isSelected ? .white : .mainText
    }

    var isSkeleton: Bool {
        value.isEmpty
    }
}

final class TipsOptionCell: UICollectionViewCell {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var valueLabel: UILabel!

    static func width(for model: TipsOptionCellModel) -> CGFloat {
        (model.value as NSString).boundingRect(
            with: CGSize(width: .greatestFiniteMagnitude, height: 40.0),
            options: [.usesLineFragmentOrigin],
            attributes: [.font: UIFont.systemFont(ofSize: 16.0)],
            context: nil
        ).width + 32.0
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.isSkeletonable = true
        containerView.skeletonCornerRadius = 20.0
    }

    func configure(with model: TipsOptionCellModel) {
        containerView.backgroundColor = model.bgColor
        valueLabel.text = model.value
        valueLabel.textColor = model.textColor

        if model.isSkeleton && !containerView.sk.isSkeletonActive {
            containerView.showAnimatedGradientSkeleton()
        } else if !model.isSkeleton && containerView.sk.isSkeletonActive {
            containerView.hideSkeleton()
        }
    }
}
