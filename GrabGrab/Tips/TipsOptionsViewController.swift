//
//  TipsOptionsViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

class TipsOptionsViewController: UIViewController {

    @IBOutlet private weak var headerLabel: UILabel!
    @IBOutlet private weak var infoLabel: UILabel!

    @IBOutlet private weak var percentTipsTitleLabel: UILabel!
    @IBOutlet private weak var percentTipsDescriptionLabel: UILabel!
    @IBOutlet private weak var percentTipsCollectionView: UICollectionView!

    @IBOutlet private weak var fixedTipsTitleLabel: UILabel!
    @IBOutlet private weak var fixedTipsDescriptionLabel: UILabel!
    @IBOutlet private weak var fixedTipsCollectionView: UICollectionView!

    @IBOutlet private weak var scrollView: UIScrollView!

    var presenter: TipsOptionsPresenterType!

    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        if navigationController?.viewControllers.contains(self) != true {
            presenter.close()
        }
    }

    private func configureViews() {
        scrollView.contentInset = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: Constants.UI.tabBarHeight,
            right: 0
        )

        title = LocalizedStrings.Account.tipsLabel
        infoLabel.text = LocalizedStrings.Tips.Options.title
        percentTipsTitleLabel.text = LocalizedStrings.Tips.Options.Percent.title
        percentTipsDescriptionLabel.text = LocalizedStrings.Tips.Options.Percent.description
        fixedTipsTitleLabel.text = LocalizedStrings.Tips.Options.Fixed.title
        fixedTipsDescriptionLabel.text = LocalizedStrings.Tips.Options.Fixed.description

        fixedTipsCollectionView.register(cellType: TipsOptionCell.self)
        percentTipsCollectionView.register(cellType: TipsOptionCell.self)
    }

    private func configureBindings() {
        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$selectionMessage.observe { [weak self] in
            if let message = $0 {
                self?.showBanner(withType: .success(message))
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$percentTipsOptionModels.observe { [weak self] models in
            guard let self = self else { return }
            self.percentTipsCollectionView.reloadData()
            self.percentTipsCollectionView.isUserInteractionEnabled = models.first?.isSkeleton == false
            if let index = models.first?.selectedIndex {
                DispatchQueue.main.async {
                    self.percentTipsCollectionView.scrollToItem(
                        at: IndexPath(item: index, section: 0),
                        at: .centeredHorizontally,
                        animated: true
                    )
                }
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$fixedTipsOptionModels.observe { [weak self] models in
            guard let self = self else { return }
            self.fixedTipsCollectionView.reloadData()
            self.fixedTipsCollectionView.isUserInteractionEnabled = models.first?.isSkeleton == false
            if let index = models.first?.selectedIndex {
                DispatchQueue.main.async {
                    self.fixedTipsCollectionView.scrollToItem(
                        at: IndexPath(item: index, section: 0),
                        at: .centeredHorizontally,
                        animated: true
                    )
                }
            }
        }.addToDisposableBag(disposableBag)
    }

    @objc private func closeTapped() {
        self.presenter.close()
    }
}

extension TipsOptionsViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}

extension TipsOptionsViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        navigationController?.view ?? view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension TipsOptionsViewController: UICollectionViewDataSource {

    private func models(for collectionView: UICollectionView) -> [TipsOptionSectionModel] {
        collectionView == fixedTipsCollectionView
            ? presenter.state.fixedTipsOptionModels
            : presenter.state.percentTipsOptionModels
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        models(for: collectionView).count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        models(for: collectionView)[section].numberOfItems
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        models(for: collectionView)[indexPath.section].cell(collectionView: collectionView, indexPath: indexPath)
    }
}

extension TipsOptionsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        models(for: collectionView)[indexPath.section].size(for: indexPath, in: collectionView)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        models(for: collectionView)[indexPath.section].didSelectItem(at: indexPath)
    }
}
