//
//  TipsInfoPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 09.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol TipsInfoPresenterType {
    func close()
}

enum TipsInfoAction {
    case complete
}

final class TipsInfoPresenter: TipsInfoPresenterType {
    private let completion: (TipsInfoAction) -> ()

    init(completion: @escaping (TipsInfoAction) -> ()) {
        self.completion = completion
    }

    func close() {
        completion(.complete)
    }
}
