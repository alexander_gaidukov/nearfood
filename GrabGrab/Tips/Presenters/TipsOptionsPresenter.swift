//
//  TipsOptionsPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 05.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol TipsOptionsPresenterType {
    var state: TipsOptionsViewState { get }
    func close()
}

enum TipsOptionsActions {
    case otherAmount
    case complete
}

struct TipsOptionsViewState {
    @Observable
    var isLoading = false

    @Observable
    var error: WebError?

    @Observable
    var selectionMessage: String?

    @Observable
    fileprivate var percentTipsOptions: [TipsOption] = []

    @Observable
    fileprivate var fixedTipsOptions: [TipsOption] = []

    @Observable
    var selectedOption: TipsOption?

    @Observable
    var percentTipsOptionModels: [TipsOptionSectionModel] = []

    @Observable
    var fixedTipsOptionModels: [TipsOptionSectionModel] = []
}

final class TipsOptionsPresenter: TipsOptionsPresenterType {

    private(set) var state: TipsOptionsViewState
    private let completion: (TipsOptionsActions) -> ()
    private let customerDataProvider: CustomerDataProviderType
    private let webClient: WebClientType

    init(
        customerDataProvider: CustomerDataProviderType,
        webClient: WebClientType,
        completion: @escaping (TipsOptionsActions) -> ()
    ) {
        self.state = TipsOptionsViewState(selectedOption: customerDataProvider.customer?.tipsSettings)
        self.completion = completion
        self.customerDataProvider = customerDataProvider
        self.webClient = webClient
        configureModels()
        loadTipsOptions()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(customerDidChange),
            name: .tipsDidChangeNotification,
            object: nil
        )
    }

    func close() {
        completion(.complete)
    }

    private func options(of kind: TipsOption.Kind) -> [TipsOption] {
        kind == .percent ? state.percentTipsOptions : state.fixedTipsOptions
    }

    private func select(kind: TipsOption.Kind, at index: Int) {
        guard let tips = options(of: kind).get(at: index) else {
            completion(.otherAmount)
            return
        }
        updateCustomerTips(tips)
    }

    private func updateCustomerTips(_ tips: TipsOption, completion: @escaping (Bool) -> () = { _ in }) {
        guard let customer = customerDataProvider.customer, customer.tipsSettings != tips else { return }

        let resource = ResourceBuilder.updateTipsSettingsResource(tips, customerUUID: customer.uuid)
        state.isLoading = true
        webClient.load(resource: resource) { [weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success(let customer):
                self.customerDataProvider.updateTipsSettings(tips: customer.tipsSettings)
                completion(true)
            case .failure(let error):
                self.state.error = error
                completion(false)
            }
        }
    }

    private func loadTipsOptions() {
        guard let customer = customerDataProvider.customer else { return }
        let resource = ResourceBuilder.tipsOptionsResource(customerUUID: customer.uuid)
        webClient.load(resource: resource) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let tips):
                self.updateTipsOptions(with: tips)
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    private func updateTipsOptions(with options: [TipsOption]) {
        state.fixedTipsOptions = options.filter { $0.kind == .fixed }.sorted { $0.props.value < $1.props.value }
        state.percentTipsOptions = options.filter { $0.kind == .percent }.sorted { $0.props.value < $1.props.value }
        configureModels()
    }

    @objc
    private func customerDidChange() {
        guard state.selectedOption != customerDataProvider.customer?.tipsSettings else {
            return
        }

        state.selectedOption = customerDataProvider.customer?.tipsSettings

        if
            let option = state.selectedOption,
            option.kind == .fixed,
            !state.fixedTipsOptions.contains(option)
        {
            var options = state.fixedTipsOptions
            options.append(option)
            options.sort { $0.props.value < $1.props.value }
            state.fixedTipsOptions = options
            configureModels()
            loadTipsOptions()
        } else {
            configureModels()
        }

        if let option = state.selectedOption {
            state.selectionMessage = LocalizedStrings.Tips.Options.defaultOptionConfirmation(value: option.formattedValue)
        }
    }

    private func configureModels() {
        state.percentTipsOptionModels = [
            TipsOptionSectionModel(
                options: state.percentTipsOptions,
                selectedOption: state.selectedOption,
                didSelect: { [weak self] in self?.select(kind: .percent, at: $0) }
            )
        ]

        state.fixedTipsOptionModels = [
            TipsOptionSectionModel(
                options: state.fixedTipsOptions,
                selectedOption: state.selectedOption,
                additionalItem: LocalizedStrings.Tips.Options.Fixed.otherAmount,
                didSelect: { [weak self] in self?.select(kind: .fixed, at: $0) }
            )
        ]
    }
}
