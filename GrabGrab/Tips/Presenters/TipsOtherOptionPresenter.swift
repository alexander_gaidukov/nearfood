//
//  TipsOtherOptionPresenter.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 06.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import Foundation

protocol TipsOtherOptionPresenterType {
    var state: TipsOtherOptionViewState { get }
    func close()
    func save()
    func changeValue(_ value: String)
}

struct TipsOtherOptionViewState {
    @Observable
    var isLoading = false

    @Observable
    var error: WebError?

    var amountError: Observable<String?> {
        $error.map { $0?.apiError?.props?.value?.localizedDescription }
    }

    @Observable
    fileprivate var amount: Int?

    var stringAmount: Observable<String?> {
        $amount.map { $0.map { "\($0)" } }
    }

    var isSaveButtonEnabled: Observable<Bool> {
        $amount.map { ($0 ?? 0) > 0 }
    }
}

enum TipsOtherOptionAction {
    case complete(OrderTips?)
}

final class TipsOtherOptionPresenter: TipsOtherOptionPresenterType {
    private let completion: (TipsOtherOptionAction) -> ()
    private var customerDataProvider: CustomerDataProviderType
    private var webClient: WebClientType
    private let context: TipsViewContext

    private(set) var state  = TipsOtherOptionViewState()

    init(
        context: TipsViewContext,
        customerDataProvider: CustomerDataProviderType,
        webClient: WebClientType,
        completion: @escaping (TipsOtherOptionAction) -> ()
    ) {
        self.completion = completion
        self.webClient = webClient
        self.context = context
        self.customerDataProvider = customerDataProvider
    }

    func close() {
        completion(.complete(nil))
    }

    func save() {
        switch context {
        case .account:
            saveCustomerTipsSettings()
        case .order(let orderUUID):
            saveOrderTipsSettings(orderUUID: orderUUID)
        case .info:
            break
        }
    }

    func changeValue(_ value: String) {
        if value.isEmpty {
            state.amount = nil
        } else if let amount = Int(value) {
            state.amount = amount
        } else {
            state.amount = state.amount // Force text field to redraw
        }
    }

    private func saveOrderTipsSettings(orderUUID: String) {
        guard let amount = state.amount, amount > 0 else { return }
        let tips = TipsOption(kind: .fixed, value: Double(amount))
        let resource = ResourceBuilder.updateTipsSettingsResource(tips, orderUUID: orderUUID)

        state.isLoading = true
        webClient.load(resource: resource) { [weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success(let orderTips):
                DispatchQueue.main.async {
                    self.completion(.complete(orderTips))
                }
            case .failure(let error):
                self.state.error = error
            }
        }
    }

    private func saveCustomerTipsSettings() {
        guard
            let amount = state.amount, amount > 0,
            let customer = customerDataProvider.customer
        else { return }
        let tips = TipsOption(kind: .fixed, value: Double(amount))
        let resource = ResourceBuilder.updateTipsSettingsResource(tips, customerUUID: customer.uuid)

        state.isLoading = true
        webClient.load(resource: resource) { [weak self] result in
            guard let self = self else { return }
            self.state.isLoading = false
            switch result {
            case .success(let customer):
                self.customerDataProvider.updateTipsSettings(tips: customer.tipsSettings)
                DispatchQueue.main.async {
                    self.completion(.complete(nil))
                }
            case .failure(let error):
                self.state.error = error
            }
        }
    }
}
