//
//  TipsOtherOptionViewController.swift
//  GrabGrab
//
//  Created by Alexandr Gaidukov on 06.11.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import UIKit

final class TipsOtherOptionViewController: UIViewController, PopupViewController, KeyboardAvoiding {

    @IBOutlet weak var hideViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet private weak var textField: GGTextField!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!

    var presenter: TipsOtherOptionPresenterType!
    private let disposableBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureBindings()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureKeyboardAvoiding()
        textField.becomeFirstResponder()
    }

    private func configureView() {
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        titleLabel.text = LocalizedStrings.Tips.Options.Fixed.otherAmount
        saveButton.setTitle(LocalizedStrings.Common.save, for: .normal)
        cancelButton.setTitle(LocalizedStrings.Common.cancel, for: .normal)

        configureToolbar()

        textField.placeholder = 0.string(with: Formatters.priceFormatter)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
        bgView.addGestureRecognizer(tapGestureRecognizer)

        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        contentView.addGestureRecognizer(panGestureRecognizer)
    }

    private func configureBindings() {
        presenter.state.stringAmount.bind(
            to: textField,
            keyPath: \.text
        ).addToDisposableBag(disposableBag)

        presenter.state.amountError
            .bind(to: textField, keyPath: \.errorMessage)
            .addToDisposableBag(disposableBag)

        presenter.state.isSaveButtonEnabled.bind(
            to: saveButton,
            keyPath: \.isEnabled
        ).addToDisposableBag(disposableBag)

        presenter.state.$isLoading.observe { [weak self] in
            if $0 {
                self?.showLoading()
            } else {
                self?.dismissLoading()
            }
        }.addToDisposableBag(disposableBag)

        presenter.state.$error.observe { [weak self] in
            if let error = $0 {
                self?.showBanner(withType: .error(error.localizedDescription))
            }
        }.addToDisposableBag(disposableBag)
    }

    private func configureToolbar() {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50.0))
        toolbar.barStyle = .default
        toolbar.barTintColor = .tabbarBackground
        toolbar.tintColor = .presentedNavigationTint
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideKeyboard))
        ]
        toolbar.sizeToFit()
        textField.inputAccessoryView = toolbar
    }

    @IBAction private func saveTapped() {
        presenter.save()
    }

    @IBAction private func cancelTapped() {
        presenter.close()
    }

    @IBAction private func textFieldDidChange(_ textField: UITextField) {
        presenter.changeValue(textField.text ?? "")
    }

    @objc func close() {
        if textField.isFirstResponder {
            hideKeyboard()
        } else {
            presenter.close()
        }
    }

    @objc private func hideKeyboard() {
        textField.resignFirstResponder()
    }

    @objc private func didPan(_ recognizer: UIPanGestureRecognizer) {
        handlePanGesture(recognizer)
    }

}

extension TipsOtherOptionViewController: LoadingViewPresenter {
    var loadingPresentingView: UIView {
        view
    }

    var isLoading: Bool {
        presenter.state.isLoading
    }
}

extension TipsOtherOptionViewController: BannerViewPresenter {
    var bannerPresentingView: UIView {
        view
    }
}
