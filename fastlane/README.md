fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios store_release
```
fastlane ios store_release
```
Release a new build to App Store
### ios fb_release
```
fastlane ios fb_release
```
Release a new build to Firebase
### ios add_devices
```
fastlane ios add_devices
```
Add new devices to the developer program
### ios generate_screenshots
```
fastlane ios generate_screenshots
```
Capture screenshots
### ios post_release_actions
```
fastlane ios post_release_actions
```
Actions that need to be performed after application review
### ios recreate_profiles
```
fastlane ios recreate_profiles
```
Recreate provision profiles after adding new devices

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
