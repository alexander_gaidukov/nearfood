module Fastlane
  module Actions

    class RemoveOriginalScreenshotsAction < Action
      def self.run(params)
        params[:dir_paths].each do |path|
            UI.message "Remove orignal screenshots from: #{path}"
            Dir.foreach(path) do |f|
                fn = File.join(path, f)
                File.delete(fn) if f != '.' && f != '..' && !f.include?("framed") && File.extname(f) == '.png'
            end
        end
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Remove original screenshots after framing"
      end

      def self.details
        "You can use this action to clear the screenshots directory from the orifinal screenshots after you finish the framing"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :dir_paths,
                                       env_name: "FL_REMOVE_ORIGINAL_SCREENSHOTS_PATHS",
                                       description: "The screenshots folder paths",
                                       type: Array,
                                       verify_block: proc do |value|
                                          UI.user_error!("No dir paths for RemoveOriginalScreenshotsAction given, pass using `dir_paths: 'paths'`") unless (value and not value.empty?)
                                       end)
        ]
      end

      def self.output
      end

      def self.return_value
      end

      def self.authors
        ["alexander.gaidukov@gmail.com"]
      end

      def self.is_supported?(platform)
        platform == :ios
      end
    end
  end
end
