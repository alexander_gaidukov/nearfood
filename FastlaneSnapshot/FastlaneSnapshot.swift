//
//  FastlaneSnapshot.swift
//  FastlaneSnapshot
//
//  Created by Alexandr Gaidukov on 26.01.2021.
//  Copyright © 2021 Alexaner Gaidukov. All rights reserved.
//

import XCTest

class FastlaneSnapshot: XCTestCase {

    var app: XCUIApplication!

    override func setUp() {
        continueAfterFailure = false
        app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
        sleep(3)
    }

    func testExample() throws {
        snapshot("1.Menu")

        // Move to chat tab
        app.buttons[Identifiers.TabBar.chat].tap()
        sleep(3)
        snapshot("2.Chat")

        // Move to Order details
        app.buttons[Identifiers.TabBar.account].tap()

        let ordersCell = app.tables.element(boundBy: 0).cells[Identifiers.Account.orders]
        _ = ordersCell.waitForExistence(timeout: 10)
        ordersCell.tap()

        let orderCell = app
            .collectionViews
            .element(boundBy: 0)
            .cells
            .matching(identifier: Identifiers.Order.cell)
            .element(boundBy: 0)
        _ = orderCell.waitForExistence(timeout: 10)
        orderCell.tap()

        sleep(3)
        snapshot("3.Order")

        // Move to cart details
        app.buttons[Identifiers.Common.backButton].tap()
        app.buttons[Identifiers.TabBar.cart].tap()

        let tableView = app.tables.element(boundBy: 0)
        let paymentButton = app.buttons[Identifiers.Cart.PaymentMethod.changeButton]
        for _ in 0..<10 {
            if paymentButton.exists { break }
            tableView.swipeUp()
        }

        paymentButton.tap()

        snapshot("4.Cart")
    }
}

extension XCUIElement {
    func forceTap() {
        coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5)).tap()
    }
}
